﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CRM_BTM.Models;
using CustomAuth.Authorization;

namespace CRM_BTM.Areas.Dictionaries.Controllers
{   [Authorize]
    public class ProductStatusController : Controller
    {
        private CRMConnection db = new CRMConnection();

        // GET: Dictionaries/ProductStatus
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_index")]
        public ActionResult Index()
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            return View(db.PRODUCT_STATUS.ToList());
        }


        // GET: Dictionaries/ProductStatus/Create
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Dictionaries/ProductStatus/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_create")]
        public ActionResult Create([Bind(Include = "ID,NAME")] PRODUCT_STATUS pRODUCT_STATUS)
        {
            if (ModelState.IsValid)
            {
                var checkUnique = db.PRODUCT_STATUS.Where(x => x.NAME.Equals(pRODUCT_STATUS.NAME));
                if (checkUnique.Count() > 0)
                {
                    ViewMessage.addMessage(TempData, "Name must be unique.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
                db.PRODUCT_STATUS.Add(pRODUCT_STATUS);
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                return RedirectToAction("Index");
            }

            return View(pRODUCT_STATUS);
        }

        // GET: Dictionaries/ProductStatus/Edit/5
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_edit")]
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PRODUCT_STATUS pRODUCT_STATUS = db.PRODUCT_STATUS.Find(id);
            if (pRODUCT_STATUS == null)
            {
                return HttpNotFound();
            }
            ACTIVITY_LOG.PrepareForDisplay(ViewBag, pRODUCT_STATUS);
            return View(pRODUCT_STATUS);
        }

        // POST: Dictionaries/ProductStatus/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_edit")]
        public ActionResult Edit([Bind(Include = "ID,NAME")] PRODUCT_STATUS pRODUCT_STATUS)
        {
            if (ModelState.IsValid)
            {
                db.PRODUCT_STATUS.Attach(pRODUCT_STATUS);
                var checkUnique = db.PRODUCT_STATUS.Where(x => x.NAME.Equals(pRODUCT_STATUS.NAME)).Where(x => x.ID != pRODUCT_STATUS.ID).Any();
                if (checkUnique)
                {
                    ViewMessage.addMessage(TempData, "Name must be unique.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
                db.Entry(pRODUCT_STATUS).State = EntityState.Modified;
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                return RedirectToAction("Index");
            }
            return View(pRODUCT_STATUS);
        }

       

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
