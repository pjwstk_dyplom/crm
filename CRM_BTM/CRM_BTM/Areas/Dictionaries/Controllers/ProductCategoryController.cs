﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CRM_BTM.Models;
using CustomAuth.Authorization;

namespace CRM_BTM.Areas.Dictionaries.Controllers
{   [Authorize]
    public class ProductCategoryController : Controller
    {
        private CRMConnection db = new CRMConnection();

        // GET: Dictionaries/ProductCategory
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_index")]
        public ActionResult Index()
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            return View(db.PRODUCT_CATEGORY.ToList());
        }



        // GET: Dictionaries/ProductCategory/Create
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Dictionaries/ProductCategory/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_create")]
        public ActionResult Create([Bind(Include = "ID,DESCRIPTION")] PRODUCT_CATEGORY pRODUCT_CATEGORY)
        {
            if (ModelState.IsValid)
            {
                var checkUnique = db.PRODUCT_CATEGORY.Where(x => x.DESCRIPTION.Equals(pRODUCT_CATEGORY.DESCRIPTION));
                if (checkUnique.Count() > 0)
                {
                    ViewMessage.addMessage(TempData, "Description must be unique.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
                db.PRODUCT_CATEGORY.Add(pRODUCT_CATEGORY);
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                return RedirectToAction("Index");
            }

            return View(pRODUCT_CATEGORY);
        }

        // GET: Dictionaries/ProductCategory/Edit/5
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_edit")]
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PRODUCT_CATEGORY pRODUCT_CATEGORY = db.PRODUCT_CATEGORY.Find(id);
            if (pRODUCT_CATEGORY == null)
            {
                return HttpNotFound();
            }
            ACTIVITY_LOG.PrepareForDisplay(ViewBag, pRODUCT_CATEGORY);
            return View(pRODUCT_CATEGORY);
        }

        // POST: Dictionaries/ProductCategory/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_edit")]
        public ActionResult Edit([Bind(Include = "ID,DESCRIPTION")] PRODUCT_CATEGORY pRODUCT_CATEGORY)
        {
            if (ModelState.IsValid)
            {
                db.PRODUCT_CATEGORY.Attach(pRODUCT_CATEGORY);
                var checkUnique = db.PRODUCT_CATEGORY.Where(x => x.DESCRIPTION.Equals(pRODUCT_CATEGORY.DESCRIPTION)).Where(x => x.ID != pRODUCT_CATEGORY.ID).Any();
                if (checkUnique)
                {
                    ViewMessage.addMessage(TempData, "Description must be unique.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
                db.Entry(pRODUCT_CATEGORY).State = EntityState.Modified;
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                return RedirectToAction("Index");
            }
            return View(pRODUCT_CATEGORY);
        }

       
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
