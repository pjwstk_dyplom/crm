﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CRM_BTM.Models;
using CustomAuth.Authorization;

namespace CRM_BTM.Areas.Dictionaries.Controllers
{
    [Authorize]
    public class BuildingTypeController : Controller
    { 
        private CRMConnection db = new CRMConnection();

        // GET: Dictionaries/BuildingType
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_index")]
        public ActionResult Index()
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            return View(db.BUILDING_TYPE.ToList());
        }



        // GET: Dictionaries/BuildingType/Create
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Dictionaries/BuildingType/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_create")]
        public ActionResult Create([Bind(Include = "ID,DESCRIPTION")] BUILDING_TYPE bUILDING_TYPE)
        {
            if (ModelState.IsValid)
            {
                var checkUnique = db.BUILDING_TYPE.Where(x => x.DESCRIPTION.Equals(bUILDING_TYPE.DESCRIPTION));
                if (checkUnique.Count() > 0)
                {
                    ViewMessage.addMessage(TempData, "Description must be unique.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
                db.BUILDING_TYPE.Add(bUILDING_TYPE);
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                return RedirectToAction("Index");
            }

            return View(bUILDING_TYPE);
        }

        // GET: Dictionaries/BuildingType/Edit/5
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_edit")]
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BUILDING_TYPE bUILDING_TYPE = db.BUILDING_TYPE.Find(id);
            if (bUILDING_TYPE == null)
            {
                return HttpNotFound();
            }
            ACTIVITY_LOG.PrepareForDisplay(ViewBag, bUILDING_TYPE);
            return View(bUILDING_TYPE);
        }

        // POST: Dictionaries/BuildingType/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_edit")]
        public ActionResult Edit([Bind(Include = "ID,DESCRIPTION")] BUILDING_TYPE bUILDING_TYPE)
        {
            if (ModelState.IsValid)
            {
                db.BUILDING_TYPE.Attach(bUILDING_TYPE);
                var checkUnique = db.BUILDING_TYPE.Where(x => x.DESCRIPTION.Equals(bUILDING_TYPE.DESCRIPTION)).Where(x => x.ID != bUILDING_TYPE.ID).Any();
                if (checkUnique)
                {
                    ViewMessage.addMessage(TempData, "Description must be unique.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
                db.Entry(bUILDING_TYPE).State = EntityState.Modified;
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                return RedirectToAction("Index");
            }
            return View(bUILDING_TYPE);
        }

      
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
