﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CRM_BTM.Models;
using CustomAuth.Authorization;

namespace CRM_BTM.Areas.Dictionaries.Controllers
{   [Authorize]
    public class InteractDirectionController : Controller
    {
        private CRMConnection db = new CRMConnection();

        // GET: Dictionaries/InteractDirection
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_index")]
        public ActionResult Index()
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            return View(db.INTERACT_DIRECTION.ToList());
        }



        // GET: Dictionaries/InteractDirection/Create
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Dictionaries/InteractDirection/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_create")]
        public ActionResult Create([Bind(Include = "ID,NAME")] INTERACT_DIRECTION iNTERACT_DIRECTION)
        {
            if (ModelState.IsValid)
            {
                var checkUnique = db.INTERACT_DIRECTION.Where(x => x.NAME.Equals(iNTERACT_DIRECTION.NAME));
                if (checkUnique.Count() > 0)
                {
                    ViewMessage.addMessage(TempData, "Direction name must be unique.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }

                db.INTERACT_DIRECTION.Add(iNTERACT_DIRECTION);
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                return RedirectToAction("Index");
            }

            return View(iNTERACT_DIRECTION);
        }

        // GET: Dictionaries/InteractDirection/Edit/5
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_edit")]
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            INTERACT_DIRECTION iNTERACT_DIRECTION = db.INTERACT_DIRECTION.Find(id);
            if (iNTERACT_DIRECTION == null)
            {
                return HttpNotFound();
            }
            ACTIVITY_LOG.PrepareForDisplay(ViewBag, iNTERACT_DIRECTION);
            return View(iNTERACT_DIRECTION);
        }

        // POST: Dictionaries/InteractDirection/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_edit")]
        public ActionResult Edit([Bind(Include = "ID,NAME")] INTERACT_DIRECTION iNTERACT_DIRECTION)
        {
            if (ModelState.IsValid)
            {
                db.INTERACT_DIRECTION.Attach(iNTERACT_DIRECTION);
                var checkUnique = db.INTERACT_DIRECTION.Where(x => x.NAME.Equals(iNTERACT_DIRECTION.NAME)).Where(x => x.ID != iNTERACT_DIRECTION.ID).Any();
                if (checkUnique)
                {
                    ViewMessage.addMessage(TempData, "Direction name must be unique.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
                db.Entry(iNTERACT_DIRECTION).State = EntityState.Modified;
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                return RedirectToAction("Index");
            }
            return View(iNTERACT_DIRECTION);
        }

        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
