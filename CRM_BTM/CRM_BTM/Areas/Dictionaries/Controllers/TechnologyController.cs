﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CRM_BTM.Models;
using CustomAuth.Authorization;

namespace CRM_BTM.Areas.Dictionaries.Controllers
{   [Authorize]
    public class TechnologyController : Controller
    {
        private CRMConnection db = new CRMConnection();

        // GET: Dictionaries/Technology
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_index")]
        public ActionResult Index()
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            return View(db.TECHNOLOGY.ToList());
        }



        // GET: Dictionaries/Technology/Create
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Dictionaries/Technology/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_create")]
        public ActionResult Create([Bind(Include = "ID,DESCRIPTION")] TECHNOLOGY tECHNOLOGY)
        {
            if (ModelState.IsValid)
            {
                var checkUnique = db.TECHNOLOGY.Where(x => x.DESCRIPTION.Equals(tECHNOLOGY.DESCRIPTION));
                if (checkUnique.Count() > 0)
                {
                    ViewMessage.addMessage(TempData, "Description must be unique.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
                db.TECHNOLOGY.Add(tECHNOLOGY);
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                return RedirectToAction("Index");
            }

            return View(tECHNOLOGY);
        }

        // GET: Dictionaries/Technology/Edit/5
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_edit")]
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TECHNOLOGY tECHNOLOGY = db.TECHNOLOGY.Find(id);
            if (tECHNOLOGY == null)
            {
                return HttpNotFound();
            }
            ACTIVITY_LOG.PrepareForDisplay(ViewBag, tECHNOLOGY);
            return View(tECHNOLOGY);
        }

        // POST: Dictionaries/Technology/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_edit")]
        public ActionResult Edit([Bind(Include = "ID,DESCRIPTION")] TECHNOLOGY tECHNOLOGY)
        {
            if (ModelState.IsValid)
            {
                db.TECHNOLOGY.Attach(tECHNOLOGY);
                var checkUnique = db.TECHNOLOGY.Where(x => x.DESCRIPTION.Equals(tECHNOLOGY.DESCRIPTION)).Where(x => x.ID != tECHNOLOGY.ID).Any();
                if (checkUnique)
                {
                    ViewMessage.addMessage(TempData, "Description must be unique.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
                db.Entry(tECHNOLOGY).State = EntityState.Modified;
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                return RedirectToAction("Index");
            }
            return View(tECHNOLOGY);
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
