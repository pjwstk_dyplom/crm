﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CRM_BTM.Models;
using CustomAuth.Authorization;

namespace CRM_BTM.Areas.Dictionaries.Controllers
{   [Authorize]
    public class InteractMediumController : Controller
    {
        private CRMConnection db = new CRMConnection();

        // GET: Dictionaries/InteractMedium
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_index")]
        public ActionResult Index()
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            return View(db.INTERACT_MEDIUM.ToList());
        }



        // GET: Dictionaries/InteractMedium/Create
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Dictionaries/InteractMedium/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_create")]
        public ActionResult Create([Bind(Include = "ID,NAME")] INTERACT_MEDIUM iNTERACT_MEDIUM)
        {
            if (ModelState.IsValid)
            {
                var checkUnique = db.INTERACT_MEDIUM.Where(x => x.NAME.Equals(iNTERACT_MEDIUM.NAME));
                if (checkUnique.Count() > 0)
                {
                    ViewMessage.addMessage(TempData, "Medium name must be unique.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
                db.INTERACT_MEDIUM.Add(iNTERACT_MEDIUM);
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                return RedirectToAction("Index");
            }

            return View(iNTERACT_MEDIUM);
        }

        // GET: Dictionaries/InteractMedium/Edit/5
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_edit")]
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            INTERACT_MEDIUM iNTERACT_MEDIUM = db.INTERACT_MEDIUM.Find(id);
            if (iNTERACT_MEDIUM == null)
            {
                return HttpNotFound();
            }
            ACTIVITY_LOG.PrepareForDisplay(ViewBag, iNTERACT_MEDIUM);
            return View(iNTERACT_MEDIUM);
        }

        // POST: Dictionaries/InteractMedium/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_edit")]
        public ActionResult Edit([Bind(Include = "ID,NAME")] INTERACT_MEDIUM iNTERACT_MEDIUM)
        {
            if (ModelState.IsValid)
            {
                db.INTERACT_MEDIUM.Attach(iNTERACT_MEDIUM);
                var checkUnique = db.INTERACT_MEDIUM.Where(x => x.NAME.Equals(iNTERACT_MEDIUM.NAME)).Where(x => x.ID != iNTERACT_MEDIUM.ID).Any();
                if (checkUnique)
                {
                    ViewMessage.addMessage(TempData, "Medium name must be unique.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
                db.Entry(iNTERACT_MEDIUM).State = EntityState.Modified;
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                return RedirectToAction("Index");
            }
            return View(iNTERACT_MEDIUM);
        }

      
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
