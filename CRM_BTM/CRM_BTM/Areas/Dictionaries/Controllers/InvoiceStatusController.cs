﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CRM_BTM.Models;
using CustomAuth.Authorization;

namespace CRM_BTM.Areas.Dictionaries.Controllers
{   [Authorize]
    public class InvoiceStatusController : Controller
    {
        private CRMConnection db = new CRMConnection();

        // GET: Dictionaries/InvoiceStatus
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_index")]
        public ActionResult Index()
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            return View(db.INVOICE_STATUS.ToList());
        }

        // GET: Dictionaries/InvoiceStatus/Create
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Dictionaries/InvoiceStatus/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_create")]
        public ActionResult Create([Bind(Include = "ID,NAME")] INVOICE_STATUS iNVOICE_STATUS)
        {
            if (ModelState.IsValid)
            {
                var checkUnique = db.INVOICE_STATUS.Where(x => x.NAME.Equals(iNVOICE_STATUS.NAME));
                if (checkUnique.Count() > 0)
                {
                    ViewMessage.addMessage(TempData, "Name must be unique.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
                db.INVOICE_STATUS.Add(iNVOICE_STATUS);
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                return RedirectToAction("Index");
            }

            return View(iNVOICE_STATUS);
        }

        // GET: Dictionaries/InvoiceStatus/Edit/5
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_edit")]
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            INVOICE_STATUS iNVOICE_STATUS = db.INVOICE_STATUS.Find(id);
            if (iNVOICE_STATUS == null)
            {
                return HttpNotFound();
            }
            ACTIVITY_LOG.PrepareForDisplay(ViewBag, iNVOICE_STATUS);
            return View(iNVOICE_STATUS);
        }

        // POST: Dictionaries/InvoiceStatus/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_edit")]

        public ActionResult Edit([Bind(Include = "ID,NAME")] INVOICE_STATUS iNVOICE_STATUS)
        {
            if (ModelState.IsValid)
            {
                db.INVOICE_STATUS.Attach(iNVOICE_STATUS);
                var checkUnique = db.INVOICE_STATUS.Where(x => x.NAME.Equals(iNVOICE_STATUS.NAME)).Where(x => x.ID != iNVOICE_STATUS.ID).Any();
                if (checkUnique)
                {
                    ViewMessage.addMessage(TempData, "Name must be unique.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
                db.Entry(iNVOICE_STATUS).State = EntityState.Modified;
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                return RedirectToAction("Index");
            }
            return View(iNVOICE_STATUS);
        }

      
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
