﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CRM_BTM.Models;
using CustomAuth.Authorization;

namespace CRM_BTM.Areas.Dictionaries.Controllers
{   [Authorize]
    public class DeviceBrandModelController : Controller
    {
        private CRMConnection db = new CRMConnection();

        // GET: Dictionaries/DeviceBrandModel
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_index")]
        public ActionResult Index()
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            var dEVICE_BRAND_MODEL = db.DEVICE_BRAND_MODEL.Include(d => d.DEVICE_TYPE);
            return View(dEVICE_BRAND_MODEL.ToList());
        }


        // GET: Dictionaries/DeviceBrandModel/Create
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_create")]
        public ActionResult Create()
        {
            ViewBag.DEVICE_TYPE_ID = new SelectList(db.DEVICE_TYPE, "ID", "DESCRIPTION");
            return View();
        }

        // POST: Dictionaries/DeviceBrandModel/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_create")]
        public ActionResult Create([Bind(Include = "ID,BRAND,MODEL,DEVICE_TYPE_ID")] DEVICE_BRAND_MODEL dEVICE_BRAND_MODEL)
        {
            if (ModelState.IsValid)
            {
                dEVICE_BRAND_MODEL.BRAND = dEVICE_BRAND_MODEL.BRAND.ToLower();
                dEVICE_BRAND_MODEL.MODEL = dEVICE_BRAND_MODEL.MODEL.ToLower();

                var checkUnique = db.DEVICE_BRAND_MODEL
                    .Where(x => x.BRAND.Equals(dEVICE_BRAND_MODEL.BRAND))
                    .Where(x => x.MODEL.Equals(dEVICE_BRAND_MODEL.MODEL));
                if (checkUnique.Count() > 0)
                {
                    ViewMessage.addMessage(TempData, "Brand and Model pair like that already exists.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }

                db.DEVICE_BRAND_MODEL.Add(dEVICE_BRAND_MODEL);
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                return RedirectToAction("Index");
            }

            ViewBag.DEVICE_TYPE_ID = new SelectList(db.DEVICE_TYPE, "ID", "DESCRIPTION", dEVICE_BRAND_MODEL.DEVICE_TYPE_ID);
            return View(dEVICE_BRAND_MODEL);
        }

        // GET: Dictionaries/DeviceBrandModel/Edit/5
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_edit")]
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DEVICE_BRAND_MODEL dEVICE_BRAND_MODEL = db.DEVICE_BRAND_MODEL.Find(id);
            if (dEVICE_BRAND_MODEL == null)
            {
                return HttpNotFound();
            }
            ViewBag.DEVICE_TYPE_ID = new SelectList(db.DEVICE_TYPE, "ID", "DESCRIPTION", dEVICE_BRAND_MODEL.DEVICE_TYPE_ID);
            ACTIVITY_LOG.PrepareForDisplay(ViewBag, dEVICE_BRAND_MODEL);
            return View(dEVICE_BRAND_MODEL);
        }

        // POST: Dictionaries/DeviceBrandModel/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_edit")]
        public ActionResult Edit([Bind(Include = "ID,BRAND,MODEL,DEVICE_TYPE_ID")] DEVICE_BRAND_MODEL dEVICE_BRAND_MODEL)
        {
            if (ModelState.IsValid)
            {
                dEVICE_BRAND_MODEL.BRAND = dEVICE_BRAND_MODEL.BRAND.ToLower();
                dEVICE_BRAND_MODEL.MODEL = dEVICE_BRAND_MODEL.MODEL.ToLower();
                db.DEVICE_BRAND_MODEL.Attach(dEVICE_BRAND_MODEL);
                var checkUnique = db.DEVICE_BRAND_MODEL
                    .Where(x => x.BRAND.Equals(dEVICE_BRAND_MODEL.BRAND))
                    .Where(x => x.MODEL.Equals(dEVICE_BRAND_MODEL.MODEL))
                    .Where(x => x.ID != dEVICE_BRAND_MODEL.ID)
                    .Any();
                if (checkUnique)
                {
                    ViewMessage.addMessage(TempData, "Brand and Model pair like that already exists.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
                db.Entry(dEVICE_BRAND_MODEL).State = EntityState.Modified;
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                return RedirectToAction("Index");
            }
            ViewBag.DEVICE_TYPE_ID = new SelectList(db.DEVICE_TYPE, "ID", "DESCRIPTION", dEVICE_BRAND_MODEL.DEVICE_TYPE_ID);
            return View(dEVICE_BRAND_MODEL);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
