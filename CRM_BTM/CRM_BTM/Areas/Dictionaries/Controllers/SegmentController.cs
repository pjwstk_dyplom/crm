﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CRM_BTM.Models;
using CustomAuth.Authorization;

namespace CRM_BTM.Areas.Dictionaries.Controllers
{   [Authorize]
    public class SegmentController : Controller
    {   
        private CRMConnection db = new CRMConnection();

        // GET: Dictionaries/Segment
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_index")]
        public ActionResult Index()
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            return View(db.SEGMENT.ToList());
        }


        // GET: Dictionaries/Segment/Create
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Dictionaries/Segment/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_create")]
        public ActionResult Create([Bind(Include = "ID,NAME")] SEGMENT sEGMENT)
        {
            if (ModelState.IsValid)
            {
                var checkUnique = db.SEGMENT.Where(x => x.NAME.Equals(sEGMENT.NAME));
                if (checkUnique.Count() > 0)
                {
                    ViewMessage.addMessage(TempData, "Name must be unique.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
                db.SEGMENT.Add(sEGMENT);
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                return RedirectToAction("Index");
            }

            return View(sEGMENT);
        }

        // GET: Dictionaries/Segment/Edit/5
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_edit")]
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SEGMENT sEGMENT = db.SEGMENT.Find(id);
            if (sEGMENT == null)
            {
                return HttpNotFound();
            }
            ACTIVITY_LOG.PrepareForDisplay(ViewBag, sEGMENT);
            return View(sEGMENT);
        }

        // POST: Dictionaries/Segment/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "dict_edit")]
        public ActionResult Edit([Bind(Include = "ID,NAME")] SEGMENT sEGMENT)
        {
            if (ModelState.IsValid)
            {
                db.SEGMENT.Attach(sEGMENT);
                var checkUnique = db.SEGMENT.Where(x => x.NAME.Equals(sEGMENT.NAME)).Where(x => x.ID != sEGMENT.ID).Any();
                if (checkUnique)
                {
                    ViewMessage.addMessage(TempData, "Name must be unique.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
                db.Entry(sEGMENT).State = EntityState.Modified;
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                return RedirectToAction("Index");
            }
            return View(sEGMENT);
        }

   

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
