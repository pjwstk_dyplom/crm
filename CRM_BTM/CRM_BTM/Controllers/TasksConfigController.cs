﻿using CRM_BTM.Models;
using CRM_BTM.Models.ViewModels;
using CustomAuth.Authorization;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CRM_BTM.Controllers
{   [Authorize]
    public class TasksConfigController : Controller
    {
        private CRMConnection db = new CRMConnection();

        // GET: TaskConfig
        [AuthAuthorize(Roles = "super_admin", Claims = "tasksconfig_index")]
        public ActionResult Index()
        {
            ViewData["displayFull"] = false;
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            var model = new TasksConfigIndexViewModel() {
                Statuses = db.TASK_STATUS.ToList(),
                Types = db.TASK_TYPE.ToList(),
                Subtypes = db.TASK_SUBTYPE.ToList(),
                Parameters = db.TASK_PARAM_DEFINITION.ToList()
            };
            return View(model);
        }

        // GET: TaskConfig/StatusEdit/5
        [AuthAuthorize(Roles = "super_admin", Claims = "tasksconfig_edit")]
        public ActionResult StatusEdit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            TASK_STATUS sts = db.TASK_STATUS.Find(id);
            ACTIVITY_LOG.PrepareForDisplay(ViewBag, sts);
            if (sts == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            return View(sts);
        }

        // POST: TaskConfig/StatusEdit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "tasksconfig_edit")]
        public ActionResult StatusEdit(TASK_STATUS sts)
        {
            if (ModelState.IsValid)
            {
                db.TASK_STATUS.Attach(sts);
                var checkUnique = db.TASK_STATUS.Where(x => x.NAME.Equals(sts.NAME)).Where(x => x.ID != sts.ID).Any();
                if (checkUnique)
                {
                    ViewMessage.addMessage(TempData, "Name must be unique.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
                db.Entry(sts).State = EntityState.Modified;
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                ViewMessage.addMessage(TempData, "Status altered.", ViewMessage.Types.Success);
                return RedirectToAction("StatusList");
            }
            else
            {
                ViewMessage.addMessage(TempData, "Invalid data state.", ViewMessage.Types.Error);
                return RedirectToAction("StatusList");
            }
        }

        // GET: TaskConfig/StatusAdd
        [AuthAuthorize(Roles = "super_admin", Claims = "tasksconfig_add")]
        public ActionResult StatusAdd()
        {
            return View();
        }

        // POST: TaskConfig/StatusAdd
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "tasksconfig_add")]
        public ActionResult StatusAdd(TASK_STATUS newSts)
        {
            if (ModelState.IsValid)
            {
                var checkUnique = db.TASK_STATUS.Where(x => x.NAME.Equals(newSts.NAME)).Any();
                if (checkUnique)
                {
                    ViewMessage.addMessage(TempData, "Name must be unique.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
                db.TASK_STATUS.Add(newSts);
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                ViewMessage.addMessage(TempData, "Status created.", ViewMessage.Types.Success);
                return RedirectToAction("StatusList");
            }
            else
            {
                ViewMessage.addMessage(TempData, "Invalid data state.", ViewMessage.Types.Error);
                return RedirectToAction("StatusList");
            }
        }

        // GET: TaskConfig/TypeEdit/5
        [AuthAuthorize(Roles = "super_admin", Claims = "tasksconfig_edit")]
        public ActionResult TypeEdit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            TASK_TYPE taty = db.TASK_TYPE.Find(id);
            ACTIVITY_LOG.PrepareForDisplay(ViewBag, taty);
            if (taty == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            return View(taty);
        }

        // POST: TaskConfig/TypeEdit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "tasksconfig_edit")]
        public ActionResult TypeEdit(TASK_TYPE taty)
        {
            if (ModelState.IsValid)
            {
                db.TASK_TYPE.Attach(taty);
                var checkUnique = db.TASK_TYPE.Where(x => x.NAME.Equals(taty.NAME)).Where(x => x.ID != taty.ID).Any();
                if (checkUnique)
                {
                    ViewMessage.addMessage(TempData, "Name must be unique.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
                db.Entry(taty).State = EntityState.Modified;
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                ViewMessage.addMessage(TempData, "Type altered.", ViewMessage.Types.Success);
                return RedirectToAction("TypeList");
            }
            else
            {
                ViewMessage.addMessage(TempData, "Invalid data state.", ViewMessage.Types.Error);
                return RedirectToAction("TypeList");
            }
        }

        // GET: TaskConfig/TypeAdd
        [AuthAuthorize(Roles = "super_admin", Claims = "tasksconfig_add")]
        public ActionResult TypeAdd()
        {
            return View();
        }

        // POST: TaskConfig/TypeAdd
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "tasksconfig_add")]
        public ActionResult TypeAdd(TASK_TYPE newType)
        {
            if (ModelState.IsValid)
            {
                var checkUnique = db.TASK_TYPE.Where(x => x.NAME.Equals(newType.NAME)).Any();
                if (checkUnique)
                {
                    ViewMessage.addMessage(TempData, "Name must be unique.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
                db.TASK_TYPE.Add(newType);
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                ViewMessage.addMessage(TempData, "Type created.", ViewMessage.Types.Success);
                return RedirectToAction("TypeList");
            }
            else
            {
                ViewMessage.addMessage(TempData, "Invalid data state.", ViewMessage.Types.Error);
                return RedirectToAction("TypeList");
            }
        }

        // GET: TaskConfig/SubtypeEdit/5
        [AuthAuthorize(Roles = "super_admin", Claims = "tasksconfig_edit")]
        public ActionResult SubtypeEdit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            TASK_SUBTYPE subt = db.TASK_SUBTYPE.Find(id);
            ACTIVITY_LOG.PrepareForDisplay(ViewBag, subt);
            if (subt == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            return View(subt);
        }

        // POST: TaskConfig/SubtypeEdit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "tasksconfig_edit")]
        public ActionResult SubtypeEdit(TASK_SUBTYPE taty)
        {
            if (ModelState.IsValid)
            {
                db.TASK_SUBTYPE.Attach(taty);
                var checkUnique = db.TASK_SUBTYPE.Where(x => x.NAME.Equals(taty.NAME)).Where(x => x.ID != taty.ID).Any();
                if (checkUnique)
                {
                    ViewMessage.addMessage(TempData, "Name must be unique.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
                db.Entry(taty).State = EntityState.Modified;
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                ViewMessage.addMessage(TempData, "Subtype altered.", ViewMessage.Types.Success);
                return RedirectToAction("SubtypeList");
            }
            else
            {
                ViewMessage.addMessage(TempData, "Invalid data state.", ViewMessage.Types.Error);
                return RedirectToAction("SubtypeList");
            }
        }

        // GET: TaskConfig/SubtypeAdd
        [AuthAuthorize(Roles = "super_admin", Claims = "tasksconfig_add")]
        public ActionResult SubtypeAdd()
        {
            return View();
        }

        // POST: TaskConfig/SubtypeAdd
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "tasksconfig_add")]
        public ActionResult SubtypeAdd(TASK_SUBTYPE newSubtype)
        {
            if (ModelState.IsValid)
            {
                var checkUnique = db.TASK_SUBTYPE.Where(x => x.NAME.Equals(newSubtype.NAME)).Any();
                if (checkUnique)
                {
                    ViewMessage.addMessage(TempData, "Name must be unique.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
                db.TASK_SUBTYPE.Add(newSubtype);
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                ViewMessage.addMessage(TempData, "Subtype created.", ViewMessage.Types.Success);
                return RedirectToAction("SubtypeList");
            }
            else
            {
                ViewMessage.addMessage(TempData, "Invalid data state.", ViewMessage.Types.Error);
                return RedirectToAction("SubtypeList");
            }
        }

        // GET: TaskConfig/StatusList 
        [AuthAuthorize(Roles = "super_admin", Claims = "tasksconfig_list")]
        public ActionResult StatusList()
        {
            var model = db.TASK_STATUS.ToList();
            ViewData["displayFull"] = true;
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            return View("StatusList", model);
        }

        // GET: TaskConfig/TypeList 
        [AuthAuthorize(Roles = "super_admin", Claims = "tasksconfig_list")]
        public ActionResult TypeList()
        {
            var model = db.TASK_TYPE.ToList();
            ViewData["displayFull"] = true;
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            return View("TypeList", model);
        }

        // GET: TaskConfig/SubtypeList 
        [AuthAuthorize(Roles = "super_admin", Claims = "tasksconfig_list")]
        public ActionResult SubtypeList()
        {
            var model = db.TASK_SUBTYPE.ToList();
            ViewData["displayFull"] = true;
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            return View("SubtypeList", model);
        }

        // GET: TaskConfig/ParamList
        [AuthAuthorize(Roles = "super_admin", Claims = "tasksconfig_list")]
        public ActionResult ParamList()
        {
            var model = db.TASK_PARAM_DEFINITION.ToList();
            ViewData["displayFull"] = true;
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            return View("ParamList", model);
        }

        // GET: TasksConfig/ParamEdit/5
        [AuthAuthorize(Roles = "super_admin", Claims = "tasksconfig_edit")]
        public ActionResult ParamEdit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            TASK_PARAM_DEFINITION param = db.TASK_PARAM_DEFINITION.Find(id);
            if (param == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            ViewBag.Types = new SelectList( new List<SelectListItem>{
                    new SelectListItem { Selected = param.PARAM_TYPE.Equals("R"), Text = "RegExp", Value = "R"},
                    new SelectListItem { Selected = param.PARAM_TYPE.Equals("A"), Text = "Any", Value = "A"},
                    new SelectListItem { Selected = param.PARAM_TYPE.Equals("S"), Text = "Strict (List only)", Value = "S"}
                },
                "Value",
                "Text");
            ACTIVITY_LOG.PrepareForDisplay(ViewBag, param);
            return View(param);
        }

        // POST: TasksConfig/ParamEdit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "tasksconfig_edit")]
        public ActionResult ParamEdit(TASK_PARAM_DEFINITION tpd)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tpd).State = EntityState.Modified;
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                ViewMessage.addMessage(TempData, "Parameter altered.", ViewMessage.Types.Success);
            }
            else
            {
                ViewMessage.addMessage(TempData, "Invalid data state.", ViewMessage.Types.Error);
            }
            return RedirectToAction("ParamList");
        }

        // GET: TasksConfig/ParamAdd
        [AuthAuthorize(Roles = "super_admin", Claims = "tasksconfig_add")]
        public ActionResult ParamAdd()
        {
            ViewBag.Types = new SelectList(new List<SelectListItem>{
                    new SelectListItem { Selected = false, Text = "RegExp", Value = "R"},
                    new SelectListItem { Selected = true, Text = "Any", Value = "A"},
                    new SelectListItem { Selected = false, Text = "Strict (List only)", Value = "S"}
                },
                "Value",
                "Text");
            return View();
        }

        // POST: TasksConfig/ParamAdd
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "tasksconfig_add")]
        public ActionResult ParamAdd(TASK_PARAM_DEFINITION param)
        {
            Debug.WriteLine(param.ID);
            Debug.WriteLine(param.PARAM_TYPE);
            Debug.WriteLine(param.REGEXP);
            Debug.WriteLine(param.DESCRIPTION);
            Debug.WriteLine(param.FULL_DESCRIPTION);
            if (ModelState.IsValid)
            {
                db.TASK_PARAM_DEFINITION.Add(param);
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                ViewMessage.addMessage(TempData, "Parameter created.", ViewMessage.Types.Success);
                return RedirectToAction("ParamList");
            }
            else
            {
                ViewMessage.addMessage(TempData, "Invalid data state.", ViewMessage.Types.Error);
                return RedirectToAction("ParamList");
            }
        }

        // GET: TasksConfig/ParamEditValues/5
        [AuthAuthorize(Roles = "super_admin", Claims = "tasksconfig_edit")]
        public ActionResult ParamEditValues(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            TASK_PARAM_DEFINITION param = db.TASK_PARAM_DEFINITION.Find(id);
            if (param == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            if (!param.PARAM_TYPE.Equals("S"))
            {
                ViewMessage.addMessage(TempData, "Value lists allowed only for S type parameters.", ViewMessage.Types.Error);
                return RedirectToAction("ParamList");
            }
            ViewBag.Parameter = param;
            var model = param.TASK_PARAM_VALUE_LIST.ToList();
            return View(model);
        }

        // POST: TasksConfig/ParamEditValues/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "tasksconfig_edit")]
        public ActionResult ParamEditValues(long? paramId, List<TASK_PARAM_VALUE_LIST> model)
        {
            if (paramId == null)
            {
                ViewMessage.addMessage(TempData, "Invalid parameter ID", ViewMessage.Types.Error);
                return RedirectToAction("ParamList");
            }
            TASK_PARAM_DEFINITION param = db.TASK_PARAM_DEFINITION.Find(paramId);
            if (param == null)
            {
                ViewMessage.addMessage(TempData, "Invalid parameter ID", ViewMessage.Types.Error);
                return RedirectToAction("ParamList");
            }
            if (!param.PARAM_TYPE.Equals("S"))
            {
                ViewMessage.addMessage(TempData, "Value lists allowed only for S type parameters.", ViewMessage.Types.Error);
                return RedirectToAction("ParamList");
            }
            db.TASK_PARAM_VALUE_LIST.RemoveRange(param.TASK_PARAM_VALUE_LIST);
            var newValues = db.TASK_PARAM_VALUE_LIST.AddRange(model);
            param.TASK_PARAM_VALUE_LIST.Clear();
            param.TASK_PARAM_VALUE_LIST = newValues.ToList();
            db.SaveChanges();
            ViewMessage.addMessage(TempData, "Value list altered.", ViewMessage.Types.Success);
            return RedirectToAction("ParamList");
        }
        // GET: TasksConfig/ParamEditTypes/5
        [AuthAuthorize(Roles = "super_admin", Claims = "tasksconfig_edit")]
        public ActionResult ParamEditTypes(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            TASK_PARAM_DEFINITION param = db.TASK_PARAM_DEFINITION.Find(id);
            if (param == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            ViewBag.Parameter = param;
            var model = param.MTM_PARAMDEF_TASKTYPE.ToList();
            ViewBag.TypeList = db.TASK_TYPE.AsEnumerable();
            ViewBag.SubtypeList = db.TASK_SUBTYPE.AsEnumerable();
            return View(model);
        }

        // POST: TasksConfig/ParamEditTypes/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "tasksconfig_edit")]
        public ActionResult ParamEditTypes(long? paramId, List<MTM_PARAMDEF_TASKTYPE> model)
        {
            if (paramId == null)
            {
                ViewMessage.addMessage(TempData, "Invalid parameter ID", ViewMessage.Types.Error);
                return RedirectToAction("ParamList");
            }
            TASK_PARAM_DEFINITION param = db.TASK_PARAM_DEFINITION.Find(paramId);
            if (param == null)
            {
                ViewMessage.addMessage(TempData, "Invalid parameter ID", ViewMessage.Types.Error);
                return RedirectToAction("ParamList");
            }
            db.MTM_PARAMDEF_TASKTYPE.RemoveRange(param.MTM_PARAMDEF_TASKTYPE);
            var newValues = db.MTM_PARAMDEF_TASKTYPE.AddRange(model);
            param.MTM_PARAMDEF_TASKTYPE.Clear();
            param.MTM_PARAMDEF_TASKTYPE = newValues.ToList();
            db.SaveChanges();
            ViewMessage.addMessage(TempData, "List of allowed task types altered.", ViewMessage.Types.Success);
            return RedirectToAction("ParamList");
        }
        
    }
}