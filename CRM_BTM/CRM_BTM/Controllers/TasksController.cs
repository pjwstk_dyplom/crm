﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using CRM_BTM.Models;
using System.Web.Mvc;
using System.Net;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Mime;
using CustomAuth.Authorization;
using CRM_BTM.Models.ViewModels;

namespace CRM_BTM.Controllers
{
    [Authorize]
    public class TasksController : Controller
    {   
        private CRMConnection db = new CRMConnection();

        // GET: Tasks
        // your own tasks
        [AuthAuthorize(Roles = "super_admin", Claims = "tasks_index")]
        public ActionResult Index()
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            var identityName = User.Identity.Name;
            var taskQuery = db.TASKS.Where(tl => tl.BASKET.CRM_USER.IDENTITY_ID.Equals(identityName));

            ViewBag.queueList = new SelectList((from s in db.QUEUE.ToList() select new {ID = s.ID, name = s.NAME }), "ID", "name",null);
            ViewBag.taskTypes = new SelectList((from s in db.TASK_TYPE.ToList() select new {ID = s.ID, name = s.NAME }), "ID", "name", null);
            ViewBag.taskSubTypes = new SelectList((from s in db.TASK_SUBTYPE.ToList() select new {ID = s.ID, name = s.NAME}), "ID", "name", null);
            ViewBag.taskStatuses = new SelectList((from s in db.TASK_STATUS.ToList() select new {ID = s.ID, name = s.NAME}), "ID", "name", null);

            ViewBag.onlyMyTsk = "True";
            ViewBag.Messages = ViewMessage.handleTempData(TempData);

            var crmUserId = CRM_USER.getFromIdentity(db, User.Identity).ID;
            ViewBag.crmUserId = crmUserId;

            return View(taskQuery);
        }

        // POST: Tasks
        // your own tasks
        [HttpPost]
        [AuthAuthorize(Roles = "super_admin", Claims = "tasks_index")]
        public ActionResult Index(string sortOrder, long? s_id, SelectListItem s_type, SelectListItem s_stype, SelectListItem s_sts, long? s_accoid, long? s_clid, string s_clname, SelectListItem s_queue, string s_basket, string s_identity, string s_my_task)
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            Debug.WriteLine("s_queue: " + Request["s_queue"]+ " s_identity: " + s_identity + " s_my_task: " + s_my_task);
            
            sortOrder = String.IsNullOrEmpty(sortOrder) ? "id_desc" : sortOrder;
            var identityName = User.Identity.Name;
            var isQueue = Int64.TryParse(Request["s_queue"], out long queueId);
            var isType = Int64.TryParse(Request["s_type"], out long typeId);
            var isSubtype = Int64.TryParse(Request["s_stype"], out long subtypeId);
            var isStatus = Int64.TryParse(Request["s_sts"], out long statusId);
            ViewBag.onlyMyTsk = String.IsNullOrWhiteSpace(s_my_task) ? "False" : "True";

            var crmUserId = CRM_USER.getFromIdentity(db, User.Identity).ID;

            var taskQuery = 
                String.IsNullOrWhiteSpace(s_my_task) ? db.TASKS : db.TASKS.Where(tl => tl.BASKET.USER_ID == crmUserId);
            ViewBag.crmUserId = crmUserId;
            
            if (s_id != null) taskQuery = taskQuery.Where(tl => tl.ID == s_id);
            if (isQueue) taskQuery = taskQuery.Where(tl => tl.QUEUE_ID == queueId);
            if (isType) taskQuery = taskQuery.Where(tl => tl.TASK_TYPE_ID == typeId);
            if (isSubtype) taskQuery = taskQuery.Where(tl => tl.TASK_SUBTYPE_ID == subtypeId);
            if (isStatus) taskQuery = taskQuery.Where(tl => tl.TASK_STATUS_ID == statusId);
            if (s_accoid != null) taskQuery = taskQuery.Where(tl => tl.ACCOUNT_ID == s_accoid);
            if (s_clid != null) taskQuery = taskQuery.Where(tl => tl.ACCOUNT.CLIENT_ID == s_clid);
            if (!String.IsNullOrEmpty(s_clname)) taskQuery = taskQuery.Where(tl => s_clname.ToLower().Contains(tl.ACCOUNT.CLIENT.NAME.ToLower()) 
                                                                                || s_clname.ToLower().Contains(tl.ACCOUNT.CLIENT.LASTNAME.ToLower()));
            if (!String.IsNullOrEmpty(s_basket)) taskQuery = taskQuery.Where(tl => tl.BASKET.NAME.ToLower().Contains(s_basket.ToLower()));
            
            switch (sortOrder)
            {
                case "id_desc":
                    taskQuery = taskQuery.OrderByDescending(tl => tl.ID);
                    break;
                case "id_asc":
                    taskQuery = taskQuery.OrderBy(tl => tl.ID);
                    break;
                case "crea_desc":
                    taskQuery = taskQuery.OrderByDescending(tl => tl.CREATION_DATE);
                    break;
                case "crea_asc":
                    taskQuery = taskQuery.OrderBy(tl => tl.CREATION_DATE);
                    break;
                case "due_desc":
                    taskQuery = taskQuery.OrderByDescending(tl => tl.DUE_DATE);
                    break;
                case "due_asc":
                    taskQuery = taskQuery.OrderBy(tl => tl.DUE_DATE);
                    break;
                case "clos_desc":
                    taskQuery = taskQuery.OrderByDescending(tl => tl.CLOSE_DATE);
                    break;
                case "clos_asc":
                    taskQuery = taskQuery.OrderBy(tl => tl.CLOSE_DATE);
                    break;
                case "type_desc":
                    taskQuery = taskQuery.OrderByDescending(tl => tl.TASK_TYPE.NAME).ThenBy(tl => tl.CREATION_DATE);
                    break;
                case "type_asc":
                    taskQuery = taskQuery.OrderBy(tl => tl.TASK_TYPE.NAME).ThenBy(tl => tl.CREATION_DATE);
                    break;
                case "subtype_desc":
                    taskQuery = taskQuery.OrderByDescending(tl => tl.TASK_SUBTYPE.NAME).ThenBy(tl => tl.CREATION_DATE);
                    break;
                case "subtype_asc":
                    taskQuery = taskQuery.OrderBy(tl => tl.TASK_SUBTYPE.NAME).ThenBy(tl => tl.CREATION_DATE);
                    break;
                case "sts_desc":
                    taskQuery = taskQuery.OrderByDescending(tl => tl.TASK_STATUS.NAME).ThenBy(tl => tl.CREATION_DATE);
                    break;
                case "sts_asc":
                    taskQuery = taskQuery.OrderBy(tl => tl.TASK_STATUS.NAME).ThenBy(tl => tl.CREATION_DATE);
                    break;
                case "bask_desc":
                    taskQuery = taskQuery.OrderByDescending(tl => tl.BASKET.NAME).ThenBy(tl => tl.CREATION_DATE);
                    break;
                case "bask_asc":
                    taskQuery = taskQuery.OrderBy(tl => tl.BASKET.NAME).ThenBy(tl => tl.CREATION_DATE);
                    break;
                default:
                    taskQuery = taskQuery.OrderByDescending(tl => tl.ID);
                    break;
            }

            ViewBag.queueList = new SelectList((from s in db.QUEUE.ToList() select new { ID = s.ID, name = s.NAME }), "ID", "name", null);
            ViewBag.taskTypes = new SelectList((from s in db.TASK_TYPE.ToList() select new { ID = s.ID, name = s.NAME }), "ID", "name", null);
            ViewBag.taskSubTypes = new SelectList((from s in db.TASK_SUBTYPE.ToList() select new { ID = s.ID, name = s.NAME }), "ID", "name", null);
            ViewBag.taskStatuses = new SelectList((from s in db.TASK_STATUS.ToList() select new { ID = s.ID, name = s.NAME }), "ID", "name", null);

            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            return View(taskQuery);
        }

        [HttpPost]
        [AuthAuthorize(Roles = "super_admin", Claims = "tasks_picktask")]
        public ActionResult PickReleaseTask(long tskId, string a)
        {
            try
            {
                var identityName = User.Identity.Name;
                var task = db.TASKS.Find(tskId);
                if (a.Equals("Pick"))
                {
                    var basket = db.BASKET.Where(b => b.CRM_USER.IDENTITY_ID == identityName)
                                          .Where(b => b.IS_DEFAULT == true)
                                          .First();
                    task.BASKET_ID = basket.ID;
                }
                else if (a.Equals("Release"))
                {
                    task.BASKET_ID = null;
                    task.BASKET = null;
                }
                db.Entry(task).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                //Response.StatusCode = (int)HttpStatusCode.OK;
                //ViewMessage.addMessage(TempData, "Task "+ task.ID + " picked to " + task.BASKET.NAME + " basket" , ViewMessage.Types.Success);
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                //ViewMessage.addMessage(TempData, "Unresolved error - please contact application administrator", ViewMessage.Types.Error);
                //Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, (e.InnerException == null ? e.Message : e.InnerException.Message));
            }
        }

        // GET: Tasks/Details/5
        [AuthAuthorize(Roles = "super_admin", Claims = "tasks_details")]
        public ActionResult Details(long? id)
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TASKS tASKS = db.TASKS.Find(id);
            Debug.WriteLine("5 Product count from db in details " + tASKS.PRODUCT.Count());
            if (tASKS == null)
            {
                return HttpNotFound();
            }
            tASKS.ACCOUNT.ADDRESS.DbConnection = db;
            return View(tASKS);
        }

        // GET: Tasks/Create
        [AuthAuthorize(Roles = "super_admin", Claims = "tasks_create")]
        public ActionResult Create(long? id)
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);

            TASKS model = ModelFromTempData(TempData);
            ViewBag.DisableAccount = (id != null);       
            if (model != null)
            {
                ViewBag.QUEUE_ID = new SelectList(db.QUEUE, "ID", "NAME", model.QUEUE_ID);
                ViewBag.TASK_SUBTYPE_ID = new SelectList(db.TASK_SUBTYPE, "ID", "NAME", model.TASK_SUBTYPE_ID);
                ViewBag.TASK_TYPE_ID = new SelectList(db.TASK_TYPE, "ID", "NAME", model.TASK_TYPE_ID);
                ViewBag.ACCOUNT_ID = new SelectList(db.ACCOUNT.OrderBy(x => x.ID), "ID", "ID", model.ACCOUNT_ID);
                return View(model);
            }
            else
            {
                ViewBag.QUEUE_ID = new SelectList(db.QUEUE, "ID", "NAME");
                ViewBag.TASK_SUBTYPE_ID = new SelectList(db.TASK_SUBTYPE, "ID", "NAME");
                ViewBag.TASK_TYPE_ID = new SelectList(db.TASK_TYPE, "ID", "NAME");
                ViewBag.ACCOUNT_ID = new SelectList(db.ACCOUNT.OrderBy(x => x.ID), "ID", "ID", id);
                if (id != null)
                    ViewBag.SetAccountId = id;
                return View();
            }
        }

        // POST: Tasks/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "tasks_create")]
        public ActionResult Create(TASKS model, string submitBtn)
        {
            Debug.WriteLine("Create POST");
            Debug.WriteLine("Create QUEUE_ID: "+model.QUEUE_ID);
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            
            // ROUTING
            if (ModelState.IsValid)
            {
                if (submitBtn.Equals("Summary"))
                {
                    TempData["model"] = model;
                    return RedirectToAction("CreateSummary");
                }
                else if (submitBtn.Equals("Next"))
                {
                    TempData["model"] = model;
                    return RedirectToAction("CreateProducts");
                }
                else
                {
                    ViewMessage.addMessage(TempData, "Unhandled submit type: " + submitBtn, ViewMessage.Types.Error);
                    return RedirectToAction("Create");
                }
            }
            
            ViewMessage.addMessage(TempData, "Encountered an error: invalid model state", ViewMessage.Types.Error);
            foreach (ModelState modelState in ViewData.ModelState.Values)
                foreach (ModelError error in modelState.Errors)
                    ViewMessage.addMessage(TempData, error.Exception.Message, ViewMessage.Types.Warning);
            return RedirectToAction("Create");
        }

        // GET: Tasks/CreateProducts
        [AuthAuthorize(Roles = "super_admin", Claims = "tasks_create")]
        public ActionResult CreateProducts()
        {
            Debug.WriteLine("CreateProducts GET");
            ViewBag.Messages = ViewMessage.handleTempData(TempData);

            TASKS model = ModelFromTempData(TempData);
            if (model != null)
            {
                var alist = 
                db.PRODUCT
                    .Where(p => p.ACCOUNT_ID == model.ACCOUNT_ID)
                    .ToList();
                ViewBag.AccountProducts = alist;
                return View("CreateProducts", model);
            }
            else
            {
                ViewMessage.addMessage(TempData, "Encountered an error: correct model was not passed over.", ViewMessage.Types.Error);
                return RedirectToAction("Create");
            }
        }

        // POST: Tasks/CreateProducts
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "tasks_create")]
        public ActionResult CreateProducts(TASKS model, long[] LinkedProducts, string submitBtn)
        {
            Debug.WriteLine("CreateProducts POST");

            // MAINBODY
            model.PRODUCT.Clear();
            if (LinkedProducts != null && LinkedProducts.Length > 0)
            {
                var foundPr = db.PRODUCT.Where(p => LinkedProducts.ToList().Contains(p.ID)).ToList();
                foreach (var pr in foundPr)
                    model.PRODUCT.Add(pr);
            }
            
            // ROUTING
            if (ModelState.IsValid) { 
                if (submitBtn.Equals("Summary"))
                {
                    TempData["model"] = model;
                    return RedirectToAction("CreateSummary");
                }
                else if (submitBtn.Equals("Prev"))
                {
                    TempData["model"] = model;
                    return RedirectToAction("Create");
                }
                else if (submitBtn.Equals("Next"))
                {
                    TempData["model"] = model;
                    return RedirectToAction("CreateParameters");
                }
                else
                {
                    ViewMessage.addMessage(TempData, "Unhandled submit type: " + submitBtn, ViewMessage.Types.Error);
                    return RedirectToAction("Create");
                }
            }
            ViewMessage.addMessage(TempData, "Encountered an error: invalid model state", ViewMessage.Types.Error);
            foreach (ModelState modelState in ViewData.ModelState.Values)
                foreach (ModelError error in modelState.Errors)
                    ViewMessage.addMessage(TempData, error.Exception.Message, ViewMessage.Types.Warning);
            return RedirectToAction("Create");
        }

        // GET: Tasks/CreateParameters
        [AuthAuthorize(Roles = "super_admin", Claims = "tasks_create")]
        public ActionResult CreateParameters()
        {
            Debug.WriteLine("CreateParameters GET");
            ViewBag.Messages = ViewMessage.handleTempData(TempData);

            TASKS model = ModelFromTempData(TempData);
            if (model != null)
            {
                var plist = 
                    db.MTM_PARAMDEF_TASKTYPE
                        .Where(p => p.TASK_TYPE_ID == model.TASK_TYPE_ID)
                        .Where(p => p.TASK_SUBTYPE_ID == model.TASK_SUBTYPE_ID)
                        .Select(p => p.TASK_PARAM_DEFINITION)
                        .ToList();
                /*if (plist.Count() == 0)
                {
                    ViewMessage.addMessage(TempData, "No parameters available.", ViewMessage.Types.Info);
                    model.TASK_PARAM.Clear(); // clear parameters left over from previous forms
                    TempData["model"] = model;
                    return RedirectToAction("CreateSummary");
                }*/
                ViewBag.ParamList = plist;
                return View("CreateParameters", model);
            }
            else
            {
                ViewMessage.addMessage(TempData, "Encountered an error: correct model was not passed over.", ViewMessage.Types.Error);
                return RedirectToAction("Create");
            }
        }

        // POST: Tasks/CreateParameters
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "tasks_create")]
        public ActionResult CreateParameters(TASKS model, List<TaskParamSkeleton> Parameters, string submitBtn)
        {
            Debug.WriteLine("CreateParameters POST");

            // MAINBODY
            model.TASK_PARAM.Clear();
            if (Parameters != null)
                foreach(var ps in Parameters)
                {
                    if (ps.InUse)
                    {
                        var newParam = new TASK_PARAM();
                        newParam.TASK_PARAM_DEFINITION_ID = ps.ParamDefID;
                        newParam.VALUE = ps.ParamValue;
                        model.TASK_PARAM.Add(newParam);
                    }
                }

            // ROUTING
            if (ModelState.IsValid)
            {
                if (submitBtn.Equals("Summary"))
                {
                    TempData["model"] = model;
                    return RedirectToAction("CreateSummary");
                }
                else if (submitBtn.Equals("Prev"))
                {
                    TempData["model"] = model;
                    return RedirectToAction("CreateProducts");
                }
                else if (submitBtn.Equals("Next"))
                {
                    TempData["model"] = model;
                    return RedirectToAction("CreateSummary");
                }
                else
                {
                    ViewMessage.addMessage(TempData, "Unhandled submit type: " + submitBtn, ViewMessage.Types.Error);
                    return RedirectToAction("Create");
                }
            }
            ViewMessage.addMessage(TempData, "Encountered an error: invalid model state", ViewMessage.Types.Error);
            foreach (ModelState modelState in ViewData.ModelState.Values)
                foreach (ModelError error in modelState.Errors)
                    ViewMessage.addMessage(TempData, error.Exception.Message, ViewMessage.Types.Warning);
            return RedirectToAction("Create");
        }

        // GET: Tasks/CreateSummary
        [AuthAuthorize(Roles = "super_admin", Claims = "tasks_create")]
        public ActionResult CreateSummary()
        {
            Debug.WriteLine("CreateSummary GET");
            ViewBag.Messages = ViewMessage.handleTempData(TempData);

            TASKS model = ModelFromTempData(TempData);
            if (model != null)
            {
                var modelParamDefIDs = model.TASK_PARAM.Select(tp => tp.TASK_PARAM_DEFINITION_ID).Distinct();
                ViewBag.ParamDef =
                    db.TASK_PARAM_DEFINITION
                        .Where(d => modelParamDefIDs.Contains(d.ID))
                        .ToList();
                var modelProdDefIDs = model.PRODUCT.Select(p => p.ID).Distinct();
                ViewBag.LinkedProducts =
                    db.PRODUCT
                        .Where(p => modelProdDefIDs.Contains(p.ID))
                        .ToList();
                try
                {
                    ViewBag.QueueName = db.QUEUE.Find(model.QUEUE_ID).NAME;
                    ViewBag.TypeName = db.TASK_TYPE.Find(model.TASK_TYPE_ID).NAME;
                    ViewBag.SubtypeName = db.TASK_SUBTYPE.Find(model.TASK_SUBTYPE_ID).NAME;
                } catch (Exception e)
                {
                    ViewMessage.addMessage(TempData, "Encountered an error. Incorrect basic attributes: " + (e.InnerException == null ? e.Message : e.InnerException.Message), ViewMessage.Types.Error);
                    return RedirectToAction("Create");
                }
                return View("CreateSummary", model);
            }
            else
            {
                ViewMessage.addMessage(TempData, "Encountered an error: correct model was not passed over.", ViewMessage.Types.Error);
                return RedirectToAction("Create");
            }
        }

        // POST: Tasks/CreateSummary
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "tasks_create")]
        public ActionResult CreateSummary(TASKS model, string submitBtn)
        {
            Debug.WriteLine("CreateSummary POST");
            
            if (ModelState.IsValid)
            {
                if (submitBtn.Equals("Prev"))
                {
                    TempData["model"] = model;
                    return RedirectToAction("CreateParameters");
                }
                else if (submitBtn.Equals("Next"))
                {
                    // perform all the checks

                    CRM_USER user;
                    try {
                        user = CRM_USER.getFromIdentity(db, HttpContext.User.Identity);
                    } catch(Exception e) {
                        ViewMessage.addMessage(TempData, "Encountered an error when resolving your user: " + (e.InnerException == null ? e.Message : e.InnerException.Message), ViewMessage.Types.Error);
                        return RedirectToAction("Create");
                    }
                    if (user == null)
                    {
                        ViewMessage.addMessage(TempData, "Encountered an error when resolving your user: not found", ViewMessage.Types.Error);
                        return RedirectToAction("Create");
                    }

                    model.CRM_USER = user;
                    model.CREATION_DATE = DateTime.Now;
                    model.DUE_DATE = DateTime.Today.AddDays(8);
                    try { 
                        model.TASK_STATUS_ID = db.TASK_STATUS.Where(s => s.NAME.ToLower().Equals("new")).First().ID;
                    } catch (Exception e)
                    {
                        ViewMessage.addMessage(TempData, "Encountered an error: status 'new' not found: " + (e.InnerException == null ? e.Message : e.InnerException.Message), ViewMessage.Types.Error);
                        return RedirectToAction("Create");
                    }

                    var ProductIDs = model.PRODUCT.Select(p => p.ID).ToList();
                    model.PRODUCT.Clear();
                    foreach (var pr in ProductIDs)
                    {
                        model.PRODUCT.Add(db.PRODUCT.Find(pr));
                    }

                    db.TASKS.Add(model);
                    db.SaveChangesLog(HttpContext.User.Identity.Name);
                    ViewMessage.addMessage(TempData, "Task created", ViewMessage.Types.Success);
                    return RedirectToAction("Details", new { id = model.ID });
                }
                else
                {
                    ViewMessage.addMessage(TempData, "Unhandled submit type: " + submitBtn, ViewMessage.Types.Error);
                    return RedirectToAction("Create");
                }
            }
            ViewMessage.addMessage(TempData, "Encountered an error: invalid model state", ViewMessage.Types.Error);
            foreach (ModelState modelState in ViewData.ModelState.Values)
                foreach (ModelError error in modelState.Errors)
                    ViewMessage.addMessage(TempData, error.Exception.Message, ViewMessage.Types.Warning);
            return RedirectToAction("Create");
        }

        // GET: Tasks/DeactivateProducts
        [AuthAuthorize(Roles = "super_admin", Claims = "tasks_deactivate")]
        public ActionResult DeactivateProducts(long? id)
        {  
            try
            {
                Debug.WriteLine("CreateProducts GET");
                ViewBag.Messages = ViewMessage.handleTempData(TempData);

                TASKS model = ModelFromTempData(TempData);
                if (model == null)
                {
                    model = new TASKS()
                    {
                        ACCOUNT_ID = id == null ? -1 : (long)id,
                        TASK_TYPE_ID = db.TASK_TYPE.Where(d => d.NAME == "deactivation").Select(d => d.ID).First(),
                        TASK_SUBTYPE_ID = db.TASK_SUBTYPE.Where(d => d.NAME == "product deactivation").Select(d => d.ID).First(),
                        QUEUE_ID = db.QUEUE.Where(d => d.NAME == "default_").Select(d => d.ID).First()
                    };
                }
                if (model != null & db.ACCOUNT.Find(model.ACCOUNT_ID) != null)
                {
                    var alist =
                    db.PRODUCT
                        .Where(p => p.ACCOUNT_ID == model.ACCOUNT_ID)
                        .ToList();
                    ViewBag.AccountProducts = alist;
                    return View("DeactivateProducts", model);
                }
                else
                {
                    ViewMessage.addMessage(TempData, "Encountered an error: correct model was not passed over.", ViewMessage.Types.Error);
                    return RedirectToAction("Create");
                }
            }
            catch (Exception e)
            {
                ViewMessage.addMessage(TempData, "Unexpected error.", ViewMessage.Types.Error);
                return RedirectToAction("Create");
            }

        }

        // POST: Tasks/DeactivateProducts
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "tasks_deactivate")]
        public ActionResult DeactivateProducts(TASKS model, long[] LinkedProducts, string submitBtn)
        {
            try
            {
                Debug.WriteLine("CreateProducts POST");

                // MAINBODY
                model.PRODUCT.Clear();
                if (LinkedProducts != null && LinkedProducts.Length > 0)
                {
                    var foundPr = db.PRODUCT.Where(p => LinkedProducts.ToList().Contains(p.ID)).ToList();
                    foreach (var pr in foundPr)
                        model.PRODUCT.Add(pr);
                }

                // ROUTING
                if (ModelState.IsValid)
                {
                    if (submitBtn.Equals("Prev"))
                    {
                        TempData["model"] = model;
                        return RedirectToAction("Details", "Client", new { id = db.ACCOUNT.Find(model.ACCOUNT_ID).CLIENT_ID });
                    }
                    else if (submitBtn.Equals("Next"))
                    {
                        TempData["model"] = model;
                        return RedirectToAction("DeactivateSummary");
                    }
                    else
                    {
                        ViewMessage.addMessage(TempData, "Unhandled submit type: " + submitBtn, ViewMessage.Types.Error);
                        return RedirectToAction("Details", "Client", new { id = db.ACCOUNT.Find(model.ACCOUNT_ID).CLIENT_ID });
                    }
                }
                ViewMessage.addMessage(TempData, "Encountered an error: invalid model state", ViewMessage.Types.Error);
                foreach (ModelState modelState in ViewData.ModelState.Values)
                    foreach (ModelError error in modelState.Errors)
                        ViewMessage.addMessage(TempData, error.Exception.Message, ViewMessage.Types.Warning);
                return RedirectToAction("Details", "Client", new { id = db.ACCOUNT.Find(model.ACCOUNT_ID).CLIENT_ID });
            }
            catch (Exception e)
            {
                ViewMessage.addMessage(TempData, "Unexpected error.", ViewMessage.Types.Error);
                return RedirectToAction("Create");

            }
          
        }

        // GET: Tasks/DeactivateSummary
        [AuthAuthorize(Roles = "super_admin", Claims = "tasks_deactivate")]
        public ActionResult DeactivateSummary()
        {
            try
            {
                Debug.WriteLine("CreateSummary GET");
                ViewBag.Messages = ViewMessage.handleTempData(TempData);

                TASKS model = ModelFromTempData(TempData);
                if (model != null)
                {
                    var modelProdDefIDs = model.PRODUCT.Select(p => p.ID).Distinct();
                    ViewBag.LinkedProducts =
                        db.PRODUCT
                            .Where(p => modelProdDefIDs.Contains(p.ID))
                            .ToList();
                    try
                    {
                        ViewBag.QueueName = db.QUEUE.Find(model.QUEUE_ID).NAME;
                        ViewBag.TypeName = db.TASK_TYPE.Find(model.TASK_TYPE_ID).NAME;
                        ViewBag.SubtypeName = db.TASK_SUBTYPE.Find(model.TASK_SUBTYPE_ID).NAME;
                    }
                    catch (Exception e)
                    {
                        ViewMessage.addMessage(TempData, "Encountered an error. Incorrect basic attributes: " + (e.InnerException == null ? e.Message : e.InnerException.Message), ViewMessage.Types.Error);
                        return RedirectToAction("Details", "Client", new { id = db.ACCOUNT.Find(model.ACCOUNT_ID).CLIENT_ID });
                    }
                    return View("DeactivateSummary", model);
                }
                else
                {
                    ViewMessage.addMessage(TempData, "Encountered an error: correct model was not passed over.", ViewMessage.Types.Error);
                    return RedirectToAction("Details", "Client", new { id = db.ACCOUNT.Find(model.ACCOUNT_ID).CLIENT_ID });
                }
            }
            catch (Exception e)
            {
                ViewMessage.addMessage(TempData, "Unexpected error.", ViewMessage.Types.Error);
                return RedirectToAction("Create");
            }
        }

        // POST: Tasks/DeactivateSummary
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "tasks_deactivate")]
        public ActionResult DeactivateSummary(TASKS model, string submitBtn)
        {
            Debug.WriteLine("CreateSummary POST");

            if (ModelState.IsValid)
            {
                if (submitBtn.Equals("Prev"))
                {
                    TempData["model"] = model;
                    return RedirectToAction("DeactivateProducts");
                }
                else if (submitBtn.Equals("ToClient"))
                {
                    TempData["model"] = model;
                    return RedirectToAction("Details", "Client", new { id = db.ACCOUNT.Find(model.ACCOUNT_ID).CLIENT_ID });
                }
                else if (submitBtn.Equals("Next"))
                {
                    // perform all the checks

                    CRM_USER user;
                    try
                    {
                        user = CRM_USER.getFromIdentity(db, HttpContext.User.Identity);
                    }
                    catch (Exception e)
                    {
                        ViewMessage.addMessage(TempData, "Encountered an error when resolving your user: " + (e.InnerException == null ? e.Message : e.InnerException.Message), ViewMessage.Types.Error);
                        return RedirectToAction("Details", "Client", new { id = db.ACCOUNT.Find(model.ACCOUNT_ID).CLIENT_ID });
                    }
                    if (user == null)
                    {
                        ViewMessage.addMessage(TempData, "Encountered an error when resolving your user: not found", ViewMessage.Types.Error);
                        return RedirectToAction("Details", "Client", new { id = db.ACCOUNT.Find(model.ACCOUNT_ID).CLIENT_ID });
                    }

                    model.CRM_USER = user;
                    model.CREATION_DATE = DateTime.Now;
                    model.DUE_DATE = DateTime.Today.AddDays(8);
                    try
                    {
                        model.TASK_STATUS_ID = db.TASK_STATUS.Where(s => s.NAME.ToLower().Equals("in progress")).First().ID;
                    }
                    catch (Exception e)
                    {
                        ViewMessage.addMessage(TempData, "Encountered an error: status 'new' not found: " + (e.InnerException == null ? e.Message : e.InnerException.Message), ViewMessage.Types.Error);
                        return RedirectToAction("Details", "Client", new { id = db.ACCOUNT.Find(model.ACCOUNT_ID).CLIENT_ID });
                    }

                    var ProductIDs = model.PRODUCT.Select(p => p.ID).ToList();
                    model.PRODUCT.Clear();
                    foreach (var pr in ProductIDs)
                    {
                        model.PRODUCT.Add(db.PRODUCT.Find(pr));
                    }

                    db.TASKS.Add(model);
                    db.SaveChangesLog(HttpContext.User.Identity.Name);
                    ViewMessage.addMessage(TempData, "Deactivation task created", ViewMessage.Types.Success);
                    return RedirectToAction("Details", new { id = model.ID });
                }
                else
                {
                    ViewMessage.addMessage(TempData, "Unhandled submit type: " + submitBtn, ViewMessage.Types.Error);
                    return RedirectToAction("Details", "Client", new { id = db.ACCOUNT.Find(model.ACCOUNT_ID).CLIENT_ID });
                }
            }
            ViewMessage.addMessage(TempData, "Encountered an error: invalid model state", ViewMessage.Types.Error);
            foreach (ModelState modelState in ViewData.ModelState.Values)
                foreach (ModelError error in modelState.Errors)
                    ViewMessage.addMessage(TempData, error.Exception.Message, ViewMessage.Types.Warning);
            return RedirectToAction("Details", "Client", new { id = db.ACCOUNT.Find(model.ACCOUNT_ID).CLIENT_ID });
        }

        // GET: Tasks/Edit/5
        [AuthAuthorize(Roles = "super_admin", Claims = "tasks_edit")]
        public ActionResult Edit(long? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            TASKS t = db.TASKS.Find(id);
            if (t == null)
            {
                ViewMessage.addMessage(TempData, "Task not found", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }

            // get view data
            ViewBag.QUEUE_ID = new SelectList(db.QUEUE, "ID", "NAME", t.QUEUE_ID);
            ViewBag.TASK_STATUS_ID = new SelectList(db.TASK_STATUS, "ID", "NAME", t.TASK_STATUS_ID);
            
            ViewBag.ParamDef =
                db.MTM_PARAMDEF_TASKTYPE
                    .Where(p => p.TASK_TYPE_ID == t.TASK_TYPE_ID)
                    .Where(p => p.TASK_SUBTYPE_ID == t.TASK_SUBTYPE_ID)
                    .Select(p => p.TASK_PARAM_DEFINITION)
                    .ToList();
            
            ViewBag.LinkedProducts =
                db.PRODUCT
                    .Where(p => p.ACCOUNT_ID == t.ACCOUNT_ID)
                    .ToList();
            return View(t);
        }

        // POST: Tasks/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "tasks_edit")]
        public ActionResult Edit(TASKS model, long[] LinkedProducts, List<TaskParamSkeleton> Parameters)
        {
            if (ModelState.IsValid)
            {
                db.TASKS.Attach(model);

                // update parameters
                var OldParams = db.TASK_PARAM.Where(x => x.TASK_ID == model.ID).ToList();
                db.TASK_PARAM.RemoveRange(OldParams);
                if (Parameters != null)
                    foreach (var ps in Parameters)
                    {
                        if (ps.InUse)
                        {
                            var newParam = new TASK_PARAM();
                            newParam.TASK_PARAM_DEFINITION_ID = ps.ParamDefID;
                            newParam.VALUE = ps.ParamValue;
                            newParam.TASK_ID = model.ID;
                            db.TASK_PARAM.Add(newParam);
                        }
                    }

                var AccountProducts = db.PRODUCT.Where(p => p.ACCOUNT_ID == model.ACCOUNT_ID).Include(x => x.TASKS).ToList();
                var modelProductIDs = AccountProducts.Where(x => x.TASKS.Where(t => t.ID == model.ID).Any()).Select(x => x.ID).ToList();

                if (LinkedProducts != null && LinkedProducts.Length > 0)
                {
                    foreach (var pr in AccountProducts)
                    {
                        db.PRODUCT.Attach(pr);
                        if (LinkedProducts.ToList().Contains(pr.ID))
                        {
                            if (!modelProductIDs.Contains(pr.ID))
                                model.PRODUCT.Add(pr);
                        }
                        else
                        {
                            model.PRODUCT.Remove(pr);
                        }
                    }
                } 
                else
                {
                    model.PRODUCT.Clear();
                }

                db.Entry(model).State = System.Data.Entity.EntityState.Modified;

                db.SaveChangesLog(HttpContext.User.Identity.Name);
                
                ViewMessage.addMessage(TempData, "Changes saved", ViewMessage.Types.Success);
                return RedirectToAction("Details", new { id = model.ID });
            }
            ViewMessage.addMessage(TempData, "Encountered an error: invalid model state", ViewMessage.Types.Error);
            foreach (ModelState modelState in ViewData.ModelState.Values)
                foreach (ModelError error in modelState.Errors)
                    ViewMessage.addMessage(TempData, error.Exception.Message, ViewMessage.Types.Warning);
            return RedirectToAction("Create");
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [NonAction]
        private TASKS ModelFromTempData(TempDataDictionary td)
        {
            if (td["model"] == null)
            {
                //ViewMessage.addMessage(td, "Encountered an error: model was not passed over.", ViewMessage.Types.Error);
                return null;
            }
            else
            {
                TASKS result = null;
                try
                {
                    result = (TASKS)td["model"];
                }
                catch (Exception e)
                {
                    ViewMessage.addMessage(td, "Encountered an error: model could not be mapped correctly: " + (e.InnerException == null ? e.Message : e.InnerException.Message), ViewMessage.Types.Error);
                    return null;
                }
                return result;
            }
        }
    }
}
