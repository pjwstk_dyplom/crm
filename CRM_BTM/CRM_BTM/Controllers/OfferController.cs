﻿ using CRM_BTM.Models;
using CRM_BTM.Models.OfferModels;
using CRM_BTM.Models.ViewModels;
using CustomAuth.Authorization;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM_BTM.Controllers
{   [Authorize]
    public class OfferController : Controller
    {
        private CRMConnection db = new CRMConnection();

        // GET: Offer
        [AuthAuthorize(Roles = "super_admin", Claims = "offer_index")]
        public ActionResult Index()
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            var model = db.OFFER_DEFINITION.ToList();
            return View(model);
        }

        // GET Offer/Edit/5
        [AuthAuthorize(Roles = "super_admin", Claims = "offer_edit")]
        public ActionResult Edit(long? id)
        {
            if (id == null)
                return RedirectToAction("Index");

            var SelectedOffer = db.OFFER_DEFINITION.Find(id);

            if (SelectedOffer == null)
            {
                ViewMessage.addMessage(TempData, "Offer not found.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }

            var PricingPeriodsDB = db.OFFER_PRICING_PERIODS_V.Where(pp => pp.OFFER_DEFINITION_ID == id).ToList();

            // Offer_option_id => (offer_option_product_id => OfferOptionProductPricing)
            var PricingPeriods = new OfferPricingPeriods(PricingPeriodsDB);
            
            var model = new OfferEditViewModel()
            {
                Offer = SelectedOffer,
                PricingPeriods = PricingPeriods
            };

            return View(model);
        }


        // POST: Offer/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "offer_edit")]
        public ActionResult Edit(OfferEditViewModel model)
        {
            OFFER_DEFINITION Offer = model.Offer;
            if (ModelState.IsValid)
            {
                Debug.WriteLine("Model offer id: " + model.Offer.ID);
                db.Entry(Offer).State = System.Data.Entity.EntityState.Modified;
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                ViewMessage.addMessage(TempData, "Offer definition info updated", ViewMessage.Types.Success);
            }
            else
            {
                ViewMessage.addMessage(TempData, "Invalid data state.", ViewMessage.Types.Error);
            }
            return RedirectToAction("Index");
        }

        // GET: Offer/EditOptionBasic/4
        [AuthAuthorize(Roles = "super_admin", Claims = "offer_editoptionbasic")]
        public ActionResult EditOptionBasic(long? id)
        {
            if (id == null)
                return RedirectToAction("Index");

            var model = db.OFFER_OPTION.Find(id);

            if (model == null)
            {
                ViewMessage.addMessage(TempData, "Offer option not found.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            } 
            else
            {
                ViewBag.TECHNOLOGY_ID = new SelectList(db.TECHNOLOGY, "ID", "DESCRIPTION");
                return View(model);
            }
        }

        // POST: Offer/EditOptionBasic/4
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "offer_editoptionbasic")]
        public ActionResult EditOptionBasic(OFFER_OPTION model)
        {
            if (ModelState.IsValid)
            {
                db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                ViewMessage.addMessage(TempData, "Option altered successfully.", ViewMessage.Types.Success);
            }
            else
            {
                ViewMessage.addMessage(TempData, "Invalid data state.", ViewMessage.Types.Error);
            }
            return RedirectToAction("Index");
        }

        // GET: Offer/EditOptionRequirements/4
        [AuthAuthorize(Roles = "super_admin", Claims = "offer_editoptionrequirements")]
        public ActionResult EditOptionRequirements(long? id)
        {
            if (id == null)
                return RedirectToAction("Index");

            var opt = db.OFFER_OPTION.Find(id);
            var model = opt.MTM_OFFER_REQUIREMENT.ToList();
            
            if (model == null)
            {
                ViewMessage.addMessage(TempData, "Offer option not found.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.ReqDefinitions = new SelectList(db.OFFER_REQUIREMENT_DEFINITION, "ID", "DESCRIPTION");
                ViewBag.OfferOptionID = id;

                return View(model);
            }


            
        }

        // POST: Offer/EditOptionRequirements/4
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "offer_editoptionrequirements")]
        public ActionResult EditOptionRequirements(List<MTM_OFFER_REQUIREMENT> model)
        {
            if (ModelState.IsValid)
            {
                foreach (var item in model)
                {
                    db.Entry(item).State = System.Data.Entity.EntityState.Modified;
                }
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                ViewMessage.addMessage(TempData, "Requirements altered successfully.", ViewMessage.Types.Success);
            }
            else
            {
                ViewMessage.addMessage(TempData, "Invalid data state.", ViewMessage.Types.Error);
            }
            return RedirectToAction("Index");
        }

        // GET: Offer/DeleteOfferRequirement/31
        [AuthAuthorize(Roles = "super_admin", Claims = "offer_deleteofferrequirement")]    
        public ActionResult DeleteOfferRequirement(long? id)
        {
            if (id == null)
                return RedirectToAction("Index");

            var model = db.MTM_OFFER_REQUIREMENT.Find(id);

            if (model == null)
            {
                ViewMessage.addMessage(TempData, "Requirement not found.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            else
            {
                db.MTM_OFFER_REQUIREMENT.Remove(model);
                db.SaveChanges();
                ViewMessage.addMessage(TempData, "Requirement removed.", ViewMessage.Types.Success);
                return RedirectToAction("Index");
            }
        }

        // GET: Offer/AddOptionRequirement/31
        [AuthAuthorize(Roles = "super_admin", Claims = "offer_addofferrequirement")]
        public ActionResult AddOfferRequirement(long? id)
        {
            if (id == null)
                return RedirectToAction("Index");

            var model = db.OFFER_OPTION.Find(id);

            if (model == null)
            {
                ViewMessage.addMessage(TempData, "Offer option not found.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.ReqDefinitions = new SelectList(db.OFFER_REQUIREMENT_DEFINITION, "ID", "DESCRIPTION");
                ViewBag.OfferOptionID = id;
                var newReq = new MTM_OFFER_REQUIREMENT() { OFFER_OPTION_ID = id };
                return View(newReq);
            }
        }

        // POST: Offer/AddOptionRequirement/31
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "offer_addofferrequirement")]
        public ActionResult AddOfferRequirement(MTM_OFFER_REQUIREMENT req)
        {
            if (ModelState.IsValid)
            {
                db.MTM_OFFER_REQUIREMENT.Add(req);
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                ViewMessage.addMessage(TempData, "Option altered successfully.", ViewMessage.Types.Success);
            }
            else
            {
                ViewMessage.addMessage(TempData, "Invalid data state.", ViewMessage.Types.Error);
            }
            return RedirectToAction("Index");
        }

        // GET: Offer/EditOptionProducts/4
        [AuthAuthorize(Roles = "super_admin", Claims = "offer_editoptionproducts")]
        public ActionResult EditOptionProducts(long? id)
        {
            if (id == null)
                return RedirectToAction("Index");

            var opt = db.OFFER_OPTION.Find(id);

            if (opt == null)
            {
                ViewMessage.addMessage(TempData, "Offer option not found.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            if (!opt.canBeEdited())
            {
                ViewMessage.addMessage(TempData, "Option has already been sold and thus cannot be edited.", ViewMessage.Types.Warning);
                return RedirectToAction("Index");
            }
            else
            {
                var PricingPeriodsDB = db.OFFER_PRICING_PERIODS_V.Where(pp => pp.OFFER_OPTION_ID == id).ToList();
                var OptionProducts = db.OFFER_OPTION_PRODUCT.Where(oop => oop.OFFER_OPTION_ID == id).ToList();

                var PricingDict = new OfferOptionPricingDictionary();
                foreach (var ppdb in PricingPeriodsDB)
                {
                    if (!PricingDict.ContainsKey(ppdb.OFFER_OPTION_PRODUCT_ID))
                    {
                        PricingDict.Add(ppdb.OFFER_OPTION_PRODUCT_ID, new OfferOptionProductPricing(PricingPeriodsDB.Where(x => x.OFFER_OPTION_PRODUCT_ID.Equals(ppdb.OFFER_OPTION_PRODUCT_ID)).ToList()));
                    }
                }

                var model = new OfferEditOptionProductsViewModel() {
                    OptionID = opt.ID,
                    Pricing = PricingDict,
                    Products = OptionProducts
                };

                ViewBag.Definitions = db.PRODUCT_DEFINITION.ToList();
                return View(model);
            }
        }

        // POST: Offer/EditOptionProducts/4
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "offer_editoptionproducts")]
        public ActionResult EditOptionProducts(OfferEditOptionProductsViewModel model)
        {
            var option = db.OFFER_OPTION.Find(model.OptionID);
            if (option == null)
            {
                ViewMessage.addMessage(TempData, "Offer option not found.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            if (!option.canBeEdited())
            {
                ViewMessage.addMessage(TempData, "Option has already been sold and thus cannot be edited.", ViewMessage.Types.Warning);
                return RedirectToAction("Index");
            }

            var Data = model.RegenerateDbObjects(db);

            if (HttpContext.IsDebuggingEnabled) {
                Debug.WriteLine("======= model.RegenerateDbObjects CHECK 0");
                foreach (var prod in Data)
                {
                    Debug.WriteLine("prod.ID: " + prod.ID);
                    Debug.WriteLine("  prod.OFFER_OPTION_ID: " + prod.OFFER_OPTION_ID);
                    Debug.WriteLine("  prod.PRODUCT_DEFINITION_ID: " + prod.PRODUCT_DEFINITION_ID);
                    Debug.WriteLine("  prod.TYPE: " + prod.TYPE);
                    Debug.WriteLine("  prod.LENGTH: " + prod.LENGTH);
                    foreach (var sch in prod.OFFER_OPT_PROD_SCHEDULE.OrderBy(x => x.MTH_NUM))
                    {
                        Debug.WriteLine("    sch.ID: " + sch.ID);
                        Debug.WriteLine("      sch.OFFER_OPTION_PRODUCT_ID: " + sch.OFFER_OPTION_PRODUCT_ID);
                        Debug.WriteLine("      sch.MTH_NUM: " + sch.MTH_NUM);
                        Debug.WriteLine("      sch.PRICE: " + sch.PRICE);
                        Debug.WriteLine("      sch.OFFER_OPTION_PRODUCT: " + sch.OFFER_OPTION_PRODUCT);
                    }
                }
                Debug.WriteLine("======= model.RegenerateDbObjects CHECK 1");
            }
            
            try
            {
                option.ClearProducts(db);
                db.OFFER_OPTION_PRODUCT.AddRange(Data);
                db.SaveChangesLog(HttpContext.User.Identity.Name);
            } catch (Exception e)
            {
                ViewMessage.addMessage(TempData, (e.InnerException == null ? e.Message : e.InnerException.Message), ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            ViewMessage.addMessage(TempData, "Option successfully updated.", ViewMessage.Types.Success);
            return RedirectToAction("Index");
        }

        // GET Offer/Add
        [AuthAuthorize(Roles = "super_admin", Claims = "offer_add")]
        public ActionResult Add()
        {
            return View();
        }

        // POST: Offer/Add
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "offer_add")]
        public ActionResult Add(OFFER_DEFINITION od)
        {
            if (!ModelState.IsValid)
            {
                ViewMessage.addMessage(TempData, "Invalid data state.", ViewMessage.Types.Error);
            }
            else
            {
                db.OFFER_DEFINITION.Add(od);
                db.SaveChanges();
                ViewMessage.addMessage(TempData, "Offer added.", ViewMessage.Types.Success);
            }
            return RedirectToAction("Index");
        }

        // GET Offer/AddOption
        [AuthAuthorize(Roles = "super_admin", Claims = "offer_addoption")]
        public ActionResult AddOption(long? offer_id)
        {
            if (offer_id == null)
            {
                ViewMessage.addMessage(TempData, "Offer not specified", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            var offer = db.OFFER_DEFINITION.Find(offer_id);
            if (offer == null)
            {
                ViewMessage.addMessage(TempData, "Offer not found", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            ViewBag.TECHNOLOGY_ID = new SelectList(db.TECHNOLOGY, "ID", "DESCRIPTION");
            return View();
        }

        // POST Offer/AddOption
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "offer_addoption")]
        public ActionResult AddOption(long? offer_id, OFFER_OPTION option)
        {
            if (offer_id == null)
            {
                ViewMessage.addMessage(TempData, "Offer not specified - test", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            var offer = db.OFFER_DEFINITION.Find(offer_id);
            if (offer == null)
            {
                ViewMessage.addMessage(TempData, "Offer not found", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            option.OFFER_DEFINITION = offer;
            option.OFFER_DEFINITION_ID = Convert.ToInt64(offer_id);
            db.OFFER_OPTION.Add(option);
            db.SaveChanges();
            ViewMessage.addMessage(TempData, "Offer option added", ViewMessage.Types.Success);
            return RedirectToAction("Index");
        }

        // GET Offer/EditProductForcedAttr/5
        [AuthAuthorize(Roles = "super_admin", Claims = "offer_editproductforcedattr")]
        public ActionResult EditProductForcedAttr(long? productID)
        {
            if (productID == null)
            {
                ViewMessage.addMessage(TempData, "Offer option product not specified", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            var product = db.OFFER_OPTION_PRODUCT.Find(productID);
            if (product == null)
            {
                ViewMessage.addMessage(TempData, "Offer option product not found", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            var defs = db.MTM_ATTRDEF_PRODDEF
                .Where(mtm => mtm.PRODUCT_DEFINITION_ID == product.PRODUCT_DEFINITION_ID)
                .Select<MTM_ATTRDEF_PRODDEF, PRODUCT_ATTRIBUTE_DEFINITION>(m => m.PRODUCT_ATTRIBUTE_DEFINITION)
                .ToList()
                .Distinct();
            var model = new OfferOptionProductForcedAttrViewModel();
            model.ProductID = Convert.ToInt64(productID);
            model.Forced = product.OFFER_OPT_PROD_FORCED_ATTR;
            model.Definitions = defs;
            return View(model);
        }

        // POST Offer/EditProductForcedAttr/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "offer_editproductforcedattr")]
        public ActionResult EditProductForcedAttr(OfferOptionProductForcedAttrViewModel viewModel)
        {
            var offerProd = db.OFFER_OPTION_PRODUCT.Find(viewModel.ProductID);
            if (offerProd == null)
            {
                ViewMessage.addMessage(TempData, "Offer option product not found", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            if (viewModel.Forced != null)
            {
                // check whether values are allowed
                foreach (var attr in viewModel.Forced)
                {
                    var attrDef = db.PRODUCT_ATTRIBUTE_DEFINITION.Find(attr.PRODUCT_ATTRIBUTE_DEF_ID);
                    if (attrDef == null)
                    {
                        ViewMessage.addMessage(TempData, "Attribute definition not found: " + attr.PRODUCT_ATTRIBUTE_DEF_ID, ViewMessage.Types.Error);
                        return RedirectToAction("Index");
                    }
                    if (!attrDef.ValueIsValid(db, attr.VALUE))
                    {
                        ViewMessage.addMessage(TempData, "Value " + attr.VALUE + " is not valid for attribute " + attrDef.DESCRIPTION, ViewMessage.Types.Error);
                        return RedirectToAction("Index");
                    }
                }
                // check for duplicates
                if (viewModel.Forced.Select(x => x.PRODUCT_ATTRIBUTE_DEF_ID).Count() != viewModel.Forced.Select(x => x.PRODUCT_ATTRIBUTE_DEF_ID).Distinct().Count())
                {
                    ViewMessage.addMessage(TempData, "At least one attribute has been used twice.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
            }
            
            db.OFFER_OPT_PROD_FORCED_ATTR.RemoveRange(offerProd.OFFER_OPT_PROD_FORCED_ATTR);
            if (viewModel.Forced != null)
            {
                db.OFFER_OPT_PROD_FORCED_ATTR.AddRange(viewModel.Forced);
                offerProd.OFFER_OPT_PROD_FORCED_ATTR = viewModel.Forced.ToList();
            }
            db.SaveChanges();
            ViewMessage.addMessage(TempData, "Changes saved", ViewMessage.Types.Success);
            return RedirectToAction("Index");
        }

    }
}