﻿using System;
using System.Collections.Generic;
using System.Data;
//using System.Data.Entity;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CRM_BTM.Models;
using System.Diagnostics;
using CustomAuth.Authorization;

namespace CRM_BTM.Controllers
{
    [Authorize]
    public class InvoiceController : Controller
    {
        private CRMConnection db = new CRMConnection();

        [AuthAuthorize(Roles = "super_admin", Claims = "invoice_resendinvoice")]
        public ActionResult ResendInvoice(int? id)
        {
            var invoice = db.INVOICE.Find(id);
            try
            {
       
                TASKS rsTask = new TASKS()
                {
                    ACCOUNT_ID = invoice.ACCOUNT_ID,
                    CREATION_DATE = DateTime.Now,
                    DUE_DATE = DateTime.Now.AddDays(7),
                    QUEUE_ID = db.QUEUE.Where(d => d.NAME.Equals("default_")).Select(d => d.ID).First(),
                    TASK_STATUS_ID = db.TASK_STATUS.Where(d => d.NAME.Equals("new")).Select(d => d.ID).First(),
                    TASK_TYPE_ID = db.TASK_TYPE.Where(d => d.NAME.Equals("billing")).Select(d => d.ID).First(),
                    TASK_SUBTYPE_ID = db.TASK_SUBTYPE.Where(d => d.NAME.Equals("resend invoice")).Select(d => d.ID).First(),
                    CRM_USER = CRM_USER.getFromIdentity(db, HttpContext.User.Identity)
                };

                TASK_PARAM tsk_param = new TASK_PARAM()
                {
                    TASK_PARAM_DEFINITION_ID = db.TASK_PARAM_DEFINITION.Where(d => d.DESCRIPTION.Equals("resend invoice id")).Select(d => d.ID).First(),
                    TASKS = rsTask,
                    VALUE = invoice.ID.ToString()
                };

                db.TASKS.Add(rsTask);
                db.TASK_PARAM.Add(tsk_param);
                db.SaveChanges();
                ViewMessage.addMessage(TempData, "Invoice: " + invoice.INVOICE_NUMBER + " was scheduled to resend", ViewMessage.Types.Success);
                return RedirectToAction("Details", "Client", new { id = invoice.ACCOUNT.CLIENT_ID });
            }
            catch (Exception e)
            {
                ViewMessage.addMessage(TempData, "Unexpected error - please contact application support", ViewMessage.Types.Success);
            }

            return RedirectToAction("Details", "Client", new { id = invoice.ACCOUNT.CLIENT_ID });
        }


        //GET: CorrectiveInvoice
        [AuthAuthorize(Roles = "super_admin", Claims = "invoice_correctinvoice")]
        public ActionResult CorrectInvoice(int? id)
        {
            try { 
            InvoiceModel model = new InvoiceModel
            {
                CurrentInvoice = db.INVOICE.Where(a => a.ID == id)
                                           .Include(a => a.INVOICE_TYPE)
                                           .Include(a => a.INVOICE_STATUS)
                                           .Include(a => a.ACCOUNT)
                                                .ThenInclude(b => b.CLIENT)
                                                .Single(),

                CurrInvProds = db.MTM_INVOICE_PRODUCT.Where(a => a.INVOICE_ID == id)
                                                     .Include(a => a.PRODUCT)
                                                        .ThenInclude(b => b.PRODUCT_DEFINITION)
                                                            .ThenInclude(c => c.PRODUCT_CATEGORY)
                                                     .ToList(),

                CurrInvOths = db.INVOICE_OTHER_ITEMS.Where(a => a.INVOICE_ID == id)
                                                    .Include(a => a.INVOICE_OTHER_ITEMS_DESC)
                                                    .ToList()


            };

            if (model.CurrentInvoice == null) {
                ViewMessage.addMessage(TempData, "Invoice number: " + id + " dosen't exist", ViewMessage.Types.Error);
                return RedirectToAction("Index", "Home");
            }

            List<CorrectInvProds> tmpInvProds = new List<CorrectInvProds>();

            foreach (var n in model.CurrInvProds)
            {
                CorrectInvProds tmp = new CorrectInvProds
                {
                    InvProd = n
                };

                tmpInvProds.Add(tmp);
            }

            model.newInvProds = tmpInvProds;

            List<CorrectInvOths> tmpInvOths = new List<CorrectInvOths>();

            foreach (var n in model.CurrInvOths)
            {
                CorrectInvOths tmp = new CorrectInvOths
                {
                    InvItm = n
                };

                tmpInvOths.Add(tmp);
            }


            model.newInvOths = tmpInvOths;
                return View(model);
            }
            catch (Exception e )
            {
               ViewMessage.addMessage(TempData, "Invoice number: " + id + " dosen't exist", ViewMessage.Types.Error);
                return RedirectToAction("Index", "Home");
              }

        
        }

        //POST: Corrective Invoice
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "invoice_correctinvoice")]
        public ActionResult CorrectInvoice(InvoiceModel model, int id)
        {
            try
            {
                Debug.WriteLine("CurrentInvoice.ID " + model.CurrentInvoice.ID);
                Debug.WriteLine("CurrentInvoice.INVOICE_NUMBER " + model.CurrentInvoice.INVOICE_NUMBER);
                Debug.WriteLine("CurrentInvoice.ACCOUNT_ID " + model.CurrentInvoice.ACCOUNT_ID);
                var invoiceType = db.INVOICE_TYPE.Where(a => a.NAME == "re-invoice").First();
                var invoiceStatus = db.INVOICE_STATUS.Where(a => a.NAME == "new").First();
                var parentcnt = db.INVOICE.Where(a => a.PARENT_INVOICE == model.CurrentInvoice.ID).Count() + 1;
                List<MTM_INVOICE_PRODUCT> invProd = new List<MTM_INVOICE_PRODUCT>();
                List<INVOICE_OTHER_ITEMS> invOth = new List<INVOICE_OTHER_ITEMS>();
                decimal total = 0;



                INVOICE newInvoice = new INVOICE
                {
                    INVOICE_NUMBER = model.CurrentInvoice.INVOICE_NUMBER + "/R" + parentcnt,
                    TOTAL = total,
                    CREATION_DATE = DateTime.Today,
                    DUE_DATE = DateTime.Today.AddDays(12),
                    INVOICE_TYPE_ID = invoiceType.ID,
                    INVOICE_STATUS_ID = invoiceStatus.ID,
                    ACCOUNT_ID = model.CurrentInvoice.ACCOUNT_ID,
                    PARENT_INVOICE = model.CurrentInvoice.ID

                };


                Debug.WriteLine("newInvProds");
                foreach (var n in model.newInvProds)
                {

                    Debug.WriteLine("PRD ID: " + n.InvProd.PRODUCT_ID + " NEW VAL: " + n.newVal);
                    if (n.newVal != 0)
                    {
                        newInvoice.MTM_INVOICE_PRODUCT.Add(new MTM_INVOICE_PRODUCT { PRODUCT_ID = n.InvProd.PRODUCT_ID, AMOUNT = n.newVal });
                        newInvoice.TOTAL = newInvoice.TOTAL + n.newVal;

                    }
                }



                foreach (var n in model.newInvOths)
                {
                    if (n.newVal != 0)
                    {
                        Debug.WriteLine("OTH ITM " + n.InvItm.INVOICE_OTHER_ITEMS_DESC_ID + " " + n.InvItm.AMOUNT + " -> " + n.newVal);
                        newInvoice.INVOICE_OTHER_ITEMS.Add(new INVOICE_OTHER_ITEMS { INVOICE_OTHER_ITEMS_DESC_ID = n.InvItm.INVOICE_OTHER_ITEMS_DESC_ID, AMOUNT = n.newVal });
                        newInvoice.TOTAL = newInvoice.TOTAL + n.newVal;

                    }
                }



                Debug.WriteLine(invoiceStatus.NAME + " " + invoiceType.NAME + " " + parentcnt);


                if (newInvoice.MTM_INVOICE_PRODUCT.Count() != 0 || newInvoice.INVOICE_OTHER_ITEMS.Count() != 0)
                {
                    db.INVOICE.Add(newInvoice);
                    db.SaveChanges();
                    ViewMessage.addMessage(TempData, "Corrective invoice " + newInvoice.INVOICE_NUMBER +" created", ViewMessage.Types.Success);
                }
                else { ViewMessage.addMessage(TempData, "Corrective invoice for " + model.CurrentInvoice.INVOICE_NUMBER + " not created - no corrected items", ViewMessage.Types.Warning); }
                
                return RedirectToAction("Details", "Client", new { id = model.CurrentInvoice.ACCOUNT.CLIENT_ID });
            }
            catch (Exception e)
            {
                ViewMessage.addMessage(TempData, "Unresolved error - please contact aplication support.", ViewMessage.Types.Error);

            }


            return RedirectToAction("Index", "Home");
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
