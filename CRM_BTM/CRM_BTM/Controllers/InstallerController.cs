﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Diagnostics;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CRM_BTM.Models;
using CustomAuth.Authorization;

namespace CRM_BTM.Controllers
{
    [Authorize]
    public class InstallerController : Controller
    {
        private CRMConnection db = new CRMConnection();

        // GET: Installer
        [AuthAuthorize(Roles = "super_admin", Claims = "installer_index")]
        public ActionResult Index()
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            ViewBag.Title = "Index";
            InstallerModel model = new InstallerModel();
            model.tskOver = db.INSTALLER_TASKS_OVERVIEW_V.Where(d => d.IDENTITY_ID == User.Identity.Name).OrderBy(D => D.DUE_DATE_ORDER).ToList();
            model.devOver = db.INSTALLER_DEVICES_STATE_V.Where(d => d.IDENTITY_ID == User.Identity.Name).OrderBy(d =>d.DEVICE_TYPE).ToList();
            model.tskToday = db.INSTALLER_TODAY_TASKS.Where(d => d.IDENTITY_ID == User.Identity.Name).ToList();
            
            return View(model);
        }

        [AuthAuthorize(Roles = "super_admin", Claims = "installer_index")]
        public ActionResult DeviceOverview()
        {
                    
           var model = db.INSTALLER_DEVICES_STATE_V.Where(d => d.IDENTITY_ID == User.Identity.Name).OrderBy(d => d.DEVICE_TYPE).ToList();
            
            return View(model);
        }


        //GET: Installer/InstallTask
        [AuthAuthorize(Roles = "super_admin", Claims = "installer_installtask")]
        public ActionResult InstallTask(long id)
        {
            try
            {
                ViewBag.Title = "Device installation task";
                
                InstallerModel model = new InstallerModel
                {
                    installTask = db.INSTALLER_TODAY_TASKS.Where(d => d.OFFER_ID == id).First(),
                    prds = db.INSTALLER_PRODUCT_OFFER_V.Where(d => d.OFFER_ID == id).ToList(),
                };
                
                model.availDev = db.INSTALLER_AVAIL_DEVICES_V
                                   .Where(d => d.IDENTITY_ID == User.Identity.Name &&
                                                                    db.INSTALLER_PRODUCT_OFFER_V.Where(a => a.OFFER_ID == id).Select(a => a.MOD_BRAND_ID).Contains(d.DEV_MOD_ID));
                return View(model);
            }
            catch (Exception e)
            {

                ViewMessage.addMessage(TempData, "Offer id: " + id + " dosen't exist", ViewMessage.Types.Error);
            }

            return RedirectToAction("Index");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "installer_installtask")]
        public ActionResult InstallTask(InstallerModel model)
        {
            try
            {
                var task = db.TASKS.Find(model.installTask.TASK_ID);
                
                if (Request.Form["cl_ref"] != null)
                {
                    task.TASK_STATUS_ID = db.TASK_STATUS.Where(d => d.NAME == "client refused installation").Select(d => d.ID).First();
                    task.CLOSE_DATE = DateTime.Now;
                    db.Entry(task).State = EntityState.Modified;
                    ViewMessage.addMessage(TempData, "Task status set to: client refused installation", ViewMessage.Types.Warning);

                }
                if (Request.Form["cl_una"] != null)
                {
                    task.TASK_STATUS_ID = db.TASK_STATUS.Where(d => d.NAME == "client unavailable").Select(d => d.ID).First();
                    task.CLOSE_DATE = DateTime.Now;
                    db.Entry(task).State = EntityState.Modified;
                    ViewMessage.addMessage(TempData, "Task status set to: client unavailable", ViewMessage.Types.Warning);
                }
                if (Request.Form["ntp"] != null)
                {
                    task.TASK_STATUS_ID = db.TASK_STATUS.Where(d => d.NAME == "no technical possibilities").Select(d => d.ID).First();
                    task.CLOSE_DATE = DateTime.Now;
                    db.Entry(task).State = EntityState.Modified;
                    ViewMessage.addMessage(TempData, "Task status set to: no technical possibilities", ViewMessage.Types.Warning);
                }

                if (Request.Form["save"] != null)
                {

                    foreach (var item in model.prds)
                    {
                        var prod = db.PRODUCT.Find(item.PRODUCT_ID);
                        task.CLOSE_DATE = DateTime.Now;
                        prod.DEVICE_ID = item.DEVICE_ID;
                        db.Entry(prod).State = EntityState.Modified;
                    }

                    task.TASK_STATUS_ID = db.TASK_STATUS.Where(d => d.NAME == "in progress").Select(d => d.ID).First();
                    task.CLOSE_DATE = DateTime.Now;
                    db.Entry(task).State = EntityState.Modified;

                    ViewMessage.addMessage(TempData, "Task status set to: In progress", ViewMessage.Types.Success);

                }

                db.SaveChangesLog(HttpContext.User.Identity.Name);
            }
            catch (Exception e)
            {
                ViewMessage.addMessage(TempData, "Unresolved error - please contact applcation support", ViewMessage.Types.Error);
            }
            return RedirectToAction("Index");
        }


   

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
