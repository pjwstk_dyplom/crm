﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CRM_BTM.Models;
using CRM_BTM.Models.ViewModels;
using log4net;
using CustomAuth.Authorization;
using CRM_BTM.Models.OfferModels;

namespace CRM_BTM.Controllers
{
    [Authorize]
    public class ClientController : Controller
    {
        private static readonly ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private CRMConnection db = new CRMConnection();
        private readonly int DefaultRowLimit = 15;

        // GET: CLIENT
        [AuthAuthorize(Roles = "super_admin", Claims = "client_index")]
        public ActionResult Index()
        {
            return RedirectToAction("Index", "ClientSearch", new { area = "" });
        }
        
        // GET: CLIENT/Details/5
        [AuthAuthorize(Roles = "super_admin", Claims = "client_details")]
        public ActionResult Details(long? id, long? AccountID, int? rowsLimit)
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CLIENT Client = db.CLIENT.Find(id);
            if (Client == null)
            {
                return HttpNotFound();
            }
            if (rowsLimit == null)
                rowsLimit = DefaultRowLimit;
            var model = new ClientDetailsViewModel();
            model.Client = Client;

            var ClientAccounts = Client.ACCOUNT.ToList();
            if (ClientAccounts.Count == 1)
                AccountID = ClientAccounts.First().ID;
            var ClientAccountInstallAddressIDs = ClientAccounts.Select(x => x.INSTALL_ADDRESS_ID).ToList();
            var FullInstallAddr = db.ADDRESS_FULL_V
                .Where(x => ClientAccountInstallAddressIDs.Contains(x.ADDRESS_ID))
                .ToList()
                .ToDictionary(x => x.ADDRESS_ID, x => x.FullAddress);
            var ClientAccountSelectListItems = ClientAccounts.OrderBy(x => x.ID).Select(a => new SelectListItem()
            {
                Text = "(" + a.ID.ToString() + ") " + FullInstallAddr[a.INSTALL_ADDRESS_ID],
                Value = a.ID.ToString()
            });
            var ClientAccountSelectList = new SelectList(ClientAccountSelectListItems, "Value", "Text");
            ViewBag.ClientAccountSelectList = ClientAccountSelectList;
                
            ACTIVITY_LOG.PrepareForDisplay(ViewBag, Client);

            if (AccountID != null)
            {
                var Account = Client.ACCOUNT.Where(x => x.ID == AccountID).First();
                if (Account == null)
                {
                    ViewMessage.addMessage(TempData, "Account not found for current client.", ViewMessage.Types.Error);
                    return RedirectToAction("Details", new { id = id, rowsLimit = rowsLimit });
                }

                model.AccountViewModel = new AccountInformationViewModel();

                model.AccountViewModel.Account = Account;
                model.AccountViewModel.InvoiceSummary = db.ACCOUNT_INVOICE_SUMMARY_V.Where(x => x.ACCOUNT_ID == AccountID).First();

                model.AccountViewModel.Offers = db.OFFER.Where(x => x.ACCOUNT_ID == AccountID).Include(x => x.OFFER_OPTION).ToList();
                var OfferPricing = new OfferPricingPeriods(db, model.AccountViewModel.Offers.Select(x => x.OFFER_OPTION_ID).ToList());
                foreach (var off in model.AccountViewModel.Offers)
                {
                    off.OptionPricingPeriods = OfferPricing[off.OFFER_OPTION_ID];
                }
                
                model.AccountViewModel.Account.ADDRESS.DbConnection = db;
                ViewBag.InstallationAddress = FullInstallAddr[Account.INSTALL_ADDRESS_ID];
                ViewBag.BillingAddress = Account.ADDRESS.FullAddress;
            }
            
            return View(model);
        }

        [AuthAuthorize(Roles = "super_admin", Claims = "client_details")]
        public ActionResult AccountTasks(long? id)
        {
            if (!Request.IsAjaxRequest() | id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var model = db.TASKS.Where(x => x.ACCOUNT_ID == id).ToList();
            return PartialView("Modals/AccountTasksModal", model);
        }

        [AuthAuthorize(Roles = "super_admin", Claims = "client_details")]
        public ActionResult AccountInvoices(long? id)
        {
            if (!Request.IsAjaxRequest() | id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var model = db.ACCOUNT_INVOICES_V.Where(x => x.ACCOUNT_ID == id).ToList();
            return PartialView("Modals/AccountInvoicesModal", model);
        }

        [AuthAuthorize(Roles = "super_admin", Claims = "client_details")]
        public ActionResult AccountProducts(long? id)
        {
            if (!Request.IsAjaxRequest() | id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var model = db.PRODUCT.Where(x => x.ACCOUNT_ID == id).ToList();
            return PartialView("Modals/AccountProductsModal", model);
        }

        [AuthAuthorize(Roles = "super_admin", Claims = "client_details")]
        public ActionResult InvoiceDetails(long? id)
        {
            if (!Request.IsAjaxRequest() | id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var model = new InvoiceDetailsViewModel();
            model.InvoiceId = id;
            model.Products = db.MTM_INVOICE_PRODUCT.Where(x => x.INVOICE_ID == id);
            model.Others = db.INVOICE_OTHER_ITEMS.Where(x => x.INVOICE_ID == id);
            return PartialView("PartialViews/InvoiceDetails", model);
        }

        [AuthAuthorize(Roles = "super_admin", Claims = "client_details")]
        public ActionResult AccountInteractions(long? id)
        {
            if (!Request.IsAjaxRequest() | id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var model = db.INTERACT_INFO_V.Where(x => x.ACCOUNT_ID == id).ToList();
            return PartialView("PartialViews/AccountInteractions", model);
        }
        
        // GET: CLIENT/Edit/5
        [AuthAuthorize(Roles = "super_admin", Claims = "client_edit")]
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CLIENT cLIENT = db.CLIENT.Find(id);
            if (cLIENT == null)
            {
                return HttpNotFound();
            }
            ViewBag.SEGMENT_ID = new SelectList(db.SEGMENT, "ID", "NAME", cLIENT.SEGMENT_ID);

            return View(cLIENT);
        }

        // POST: CLIENT/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "client_edit")]
        public ActionResult Edit([Bind(Include = "ID,NAME,LASTNAME,PESEL,NIP,SEGMENT_ID,EMAIL,PHONE_PRIMARY,PHONE_SECONDARY")] CLIENT cLIENT)
        {
            if (cLIENT.SEGMENT_ID == 0)
            {
                cLIENT.SEGMENT_ID = 1;
            }

            if (ModelState.IsValid)
            {
                cLIENT.EMAIL = cLIENT.EMAIL.ToLower();
                db.Entry(cLIENT).State = EntityState.Modified;
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                return RedirectToAction("Details", "CLIENT", new { id = cLIENT.ID });
            }
            ViewBag.SEGMENT_ID = new SelectList(db.SEGMENT, "ID", "NAME", cLIENT.SEGMENT_ID);
            return View(cLIENT);
        }

        // AJAX: Province2County
        [AuthAuthorize(Roles = "super_admin", Claims = "client_edit")]
        public ActionResult Province2County(long? id)
        {
            if (!Request.IsAjaxRequest())
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var CountyList = db.ADDRESS_COUNTY.Where(c => c.ADDRESS_PROVINCE_ID == id).ToList();
            return PartialView("PartialViews/Province2County", CountyList);
        }

        // AJAX: County2City
        [AuthAuthorize(Roles = "super_admin", Claims = "client_edit")]
        public ActionResult County2City(long id)
        {
            if (!Request.IsAjaxRequest())
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var CityList = db.ADDRESS_CITY.Where(c => c.ADDRESS_COUNTY_ID == id).ToList();
            return PartialView("PartialViews/County2City", CityList);
        }

        // AJAX: City2Street
        [AuthAuthorize(Roles = "super_admin", Claims = "client_edit")]
        public ActionResult City2Street(long id)
        {
            if (!Request.IsAjaxRequest())
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var StreetList = db.ADDRESS_STREET.Where(c => c.ADDRESS_CITY_ID == id).ToList();
            return PartialView("PartialViews/City2Street", StreetList);
        }

        // AJAX: Street2Building
        [AuthAuthorize(Roles = "super_admin", Claims = "client_edit")]
        public ActionResult Street2Building(long id, bool OnlyWithTech = false, string FormName = "InstallationBuildingID")
        {
            if (!Request.IsAjaxRequest())
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var BuildingListQuery = db.ADDRESS_BUILDING.Where(c => c.ADDRESS_STREET_ID == id);
            if (OnlyWithTech)
                BuildingListQuery = BuildingListQuery.Where(x => x.MTM_AVAIL_TECH_ON_BUILDING.Count() > 0);
            var BuildingList = BuildingListQuery.ToList();
            if (BuildingList.Count() == 0)
                return new HttpStatusCodeResult(HttpStatusCode.NoContent);
            ViewBag.FormName = FormName;
            return PartialView("PartialViews/Street2Building", BuildingList);
        }

        [AuthAuthorize(Roles = "super_admin", Claims = "client_edit")]
        public ActionResult EditBillingAddress(long id)
        {
            EditBillingAddressModel model = new EditBillingAddressModel()
            {
                AccountID = id,
                ClientId = db.ACCOUNT.Find(id).CLIENT_ID,
                AddressProvince = new SelectList(db.ADDRESS_PROVINCE, "ID", "NAME")
            };
             
            return View(model);
        }

        [HttpPost]
        [AuthAuthorize(Roles = "super_admin", Claims = "client_edit")]
        public ActionResult EditBillingAddress(long id, EditBillingAddressModel model)
        {
            Debug.WriteLine("weszło");
            ACCOUNT Account = db.ACCOUNT.Find(model.AccountID);

            if (Account == null)
            {
                return RedirectToAction("Index");
            }
            else
            {
                // Verification
                if (model == null)
                {
                    ViewMessage.addMessage(TempData, "No model found.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
                if (model.BillingBuildingID == null)
                {
                    ViewMessage.addMessage(TempData, "Invalid building selected.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }

                var BillingBuilding = db.ADDRESS_BUILDING.Find(model.BillingBuildingID);
                if (BillingBuilding == null)
                {
                    ViewMessage.addMessage(TempData, "Invalid billing building.", ViewMessage.Types.Error);
                    return RedirectToAction("Details", "Client", new { id = model.ClientId });
                }

                ADDRESS BillingAddress;

                if (String.IsNullOrEmpty(model.BillingFlat))
                    BillingAddress = BillingBuilding.ADDRESS.Where(x => String.IsNullOrEmpty(x.FLAT)).FirstOrDefault();
                else
                    BillingAddress = BillingBuilding.ADDRESS.Where(x => !String.IsNullOrEmpty(x.FLAT) && x.FLAT.Equals(model.BillingFlat)).FirstOrDefault();
                if (BillingAddress == null)
                {
                    BillingAddress = db.ADDRESS.Add(new ADDRESS()
                    {
                        ADDRESS_BUILDING = BillingBuilding,
                        FLAT = model.BillingFlat,
                        INSERT_DATE = DateTime.Now
                    });
                    db.SaveChangesLog(HttpContext.User.Identity.Name);
                }

                Account.BILLING_ADDRESS_ID = BillingAddress.ID;
                db.Entry(Account).State = System.Data.Entity.EntityState.Modified;
                db.SaveChangesLog(HttpContext.User.Identity.Name);



            }

            return RedirectToAction("Details", "Client", new { id = model.ClientId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
