﻿using CRM_BTM.Models;
using CRM_BTM.Models.ActivityLogModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Mapping;
using System.Data.Entity.Core.Metadata.Edm;
using CustomAuth.Authorization;

namespace CRM_BTM.Controllers
{   [Authorize]
    public class ActivityLogController : Controller
    {
        private CRMConnection db = new CRMConnection();

        // GET: ActivityLog
        [AuthAuthorize(Roles = "super_admin", Claims = "activitylog_search")]
        public ActionResult Search(string table, long id)
        {
            var dbValues = db.ACTIVITY_LOG
                             .Where(a => a.REFERENCED_TABLE.Equals(table))
                             .Where(a => a.REFERENCED_ID == id)
                             .ToList();
            var actLogList = new ActLogEntryIDList(dbValues);
            
            return View(actLogList);
        }
    }
}