﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CRM_BTM.Models;
using CRM_BTM.Models.ViewModels;
using CustomAuth.Authorization;
using PagedList;


namespace CRM_BTM.Controllers
{   [Authorize]
    public class InteractController : Controller
    {
        private CRMConnection db = new CRMConnection();

        // GET: Interact
        [AuthAuthorize(Roles = "super_admin", Claims = "interact_index")]
        public ActionResult Index(string sortOrder, string currentFilter, int? page)
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            ViewBag.CurrentSort = sortOrder;

            var model = new InteractModel();

            if ((InteractModel)TempData["InteractSrh"] != null)
            {
                model = (InteractModel)TempData["InteractSrh"];
            }
            int pageSize = 50;
            int pageNumber = (page ?? 1);
            
            model.InteractInfos = db.INTERACT_INFO_V
                                 .Where( a=>
                                    !(model.PeselNip == null & model.ClientFName == null & model.ClientId == null & model.TaskId == null & model.dateFrom == null)
                                     & (a.INTERACT_DIRECTION_ID==model.InteractDirectionId | model.InteractDirectionId==0)
                                     & (a.INTERACT_MEDIUM_ID==model.InteractMediumId | model.InteractMediumId==0)
                                     & (a.INTERACT_TYPE_ID==model.InteractTypeId | model.InteractTypeId==0)
                                     & (a.INTERACT_DATE>=(model.dateFrom == null ? a.INTERACT_DATE : model.dateFrom) )
                                     & (a.INTERACT_DATE<= (model.dateTo == null ? a.INTERACT_DATE : model.dateTo) )
                                     & (a.CLIENT_ID== (model.ClientId == null ? 0 : model.ClientId) | model.ClientId==null)
                                     & (a.CUSTOMER_FULL_NAME.Contains(model.ClientFName) | model.ClientFName == null)
                                     & (a.CLIENT_PESEL==model.PeselNip | a.CLIENT_NIP==model.PeselNip | model.PeselNip == null)
                                     & (a.TASKS_ID==model.TaskId | model.TaskId==null))
                                 .ToList()
                                 .OrderBy(x => x.INTERACT_DATE)
                                 .ThenBy(x => x.INTERACT_ID)
                                 .ToPagedList(pageNumber, pageSize);
            
            model.InteractDirections = db.INTERACT_DIRECTION.ToList();
            model.InteractTypes = db.INTERACT_TYPE.ToList();
            model.InteractMediums = db.INTERACT_MEDIUM.ToList();

            TempData["InteractSrh"] = new InteractModel();
            TempData["InteractSrh"] = model;

            if (model.dateFrom == null) model.dateFrom = DateTime.Today.AddDays(-7);
            if (model.dateTo == null) model.dateTo = DateTime.Today.AddDays(1);

            return View(model);
        }


        [HttpPost]
        [AuthAuthorize(Roles = "super_admin", Claims = "interact_index")]
        public ActionResult Index(InteractModel model)
        {
            TempData["InteractSrh"] = new InteractModel();
            Debug.WriteLine("CLEAR_FORM " + model.clearForm);
            if (model.clearForm)
            {
                model = new InteractModel();
            }
            else
            {
                TimeSpan diff = (model.dateTo - model.dateFrom) == null ? new TimeSpan() : (TimeSpan)(model.dateTo - model.dateFrom);
                Debug.WriteLine("diff " + diff.Days);
                Debug.WriteLine("add " + (model.dateFrom == null ? new DateTime() : ((DateTime)model.dateFrom).AddDays(7)));

                if (model.PeselNip == null & model.ClientFName == null & model.ClientId == null & model.TaskId == null)
                {
                    if (model.dateFrom == null | model.dateTo == null)
                    {
                        ViewMessage.addMessage(TempData, "Can't query unlimited date range without specifying any of Clients attributes or Task ID. Date range set to last 7 days.", ViewMessage.Types.Warning);
                        model.dateTo = DateTime.Now;
                        model.dateFrom = DateTime.Today.AddDays(-7);
                    }
                    else
                    {
                        if (diff.Days > 14)
                        {
                            model.dateTo = ((DateTime)model.dateFrom).AddDays(14);

                            ViewMessage.addMessage(TempData, "Can't query more than 14 days long date range without specifying any of Clients attributes or Task ID. Date range set as " + model.dateFrom + " - " + model.dateTo, ViewMessage.Types.Warning);

                        }

                    }

                }

                TempData["InteractSrh"] = model;
            }
            
            return RedirectToAction("Index");
            
        }


        // GET: Interact/Details/5
        [AuthAuthorize(Roles = "super_admin", Claims = "interact_details")]
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            INTERACT iNTERACT = db.INTERACT.Find(id);
            if (iNTERACT == null)
            {
                return HttpNotFound();
            }
            return View(iNTERACT);
        }

        // GET: Create/5
        [AuthAuthorize(Roles = "super_admin", Claims = "interact_create")]
        public ActionResult Create(long? id)
        {
            if (id == null)
            {
                ViewMessage.addMessage(TempData, "No account provided", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            var Account = db.ACCOUNT.Find(id);
            if (Account == null)
            {
                ViewMessage.addMessage(TempData, "Account not found", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            var Interact = new INTERACT() { ACCOUNT_ID = (long)id };

            ViewBag.INTERACT_TYPE_ID = new SelectList(db.INTERACT_TYPE, "ID", "NAME");
            ViewBag.INTERACT_DIRECTION_ID = new SelectList(db.INTERACT_DIRECTION, "ID", "NAME");
            ViewBag.INTERACT_MEDIUM_ID = new SelectList(db.INTERACT_MEDIUM, "ID", "NAME");
            var SLItems = db.TASKS.Where(x => x.ACCOUNT_ID == id).ToList().Select(x => new SelectListItem() {
                Text = (new StringBuilder()).Append("(").Append(x.ID).Append(") ").Append(x.TASK_TYPE.NAME).Append(", ").Append(x.TASK_SUBTYPE.NAME).ToString(),
                Value = x.ID.ToString()
            });

            ViewBag.TASKS_ID = new SelectList(SLItems, "Value", "Text");

            return View(Interact);
        }


        // POST: Create/5
        [ValidateAntiForgeryToken]
        [HttpPost]
        [AuthAuthorize(Roles = "super_admin", Claims = "interact_create")]
        public ActionResult Create(INTERACT Inter)
        {
            if (ModelState.IsValid)
            {
                Inter.USER_ID = CRM_USER.getFromIdentity(db, User.Identity).ID;
                Inter.CREATION_DATE = DateTime.Now;
                db.INTERACT.Add(Inter);
                db.SaveChanges();
                ViewMessage.addMessage(TempData, "Interaction added", ViewMessage.Types.Success);
            } else
            {
                ViewMessage.addMessage(TempData, "Invalid data state", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            var ClientID = db.ACCOUNT.Find(Inter.ACCOUNT_ID).CLIENT_ID;
            return RedirectToAction("Details", "Client", new { id = ClientID });
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
