﻿using CRM_BTM.Models;
using CRM_BTM.Models.ViewModels;
using CustomAuth.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace CRM_BTM.Controllers
{
    [Authorize]
    public class JobMachineController : Controller
    {   
        private CRMConnection db = new CRMConnection();

        // GET: JobMachine
        [AuthAuthorize(Roles = "super_admin", Claims = "jobmachine_index")]
        public ActionResult Index()
        {
            var upvt = db.CRMAPP_JM_TRG_INFO_UPVT
                         //.Where(m => m.QUANTITY > 0)
                         .ToList();
            
            var statuses = upvt.Select(m => m.STATUS).Distinct().ToList();

            var chartData = new Dictionary<long, Dictionary<string, decimal?>>();
            foreach(var item in upvt)
            {
                if (!chartData.ContainsKey(item.TRIGGER_ID))
                {
                    chartData.Add(item.TRIGGER_ID, new Dictionary<string, decimal?>());
                    foreach(var status in statuses)
                        chartData[item.TRIGGER_ID].Add(status, 0);
                }
                chartData[item.TRIGGER_ID][item.STATUS] += item.QUANTITY;
            }

            var model = new JobMachineTriggerInfoViewModel();

            model.Triggers = db.CRMAPP_JM_TRG_INFO.ToList();
            model.TriggerCharts = new Dictionary<long, Chart>();

            foreach (var cd in chartData)
            {
                var newChart = new Chart(200, 100)
                               .AddSeries(
                                    name: "Default",
                                    chartType: "Pie",
                                    xValue: cd.Value, xField: "Key",
                                    yValues: cd.Value, yFields: "Value"
                                )
                                .AddLegend();
                model.TriggerCharts.Add(cd.Key, newChart);
            }
            
            model.Jobs = db.CRMAPP_JM_JOBS_INFO.ToList();
            var snapshots = db.CRMAPP_JM_JOBS_INFO_SNAP
                              .ToList()
                              .Where(s => s.SNAPSHOT_DATETIME.Date >= DateTime.Now.Date.AddDays(-2))
                              .OrderBy(s => s.SNAPSHOT_DATETIME);

            var jobChartsData = new Dictionary<string, JobmachineJobChartData>();

            foreach (var snap in snapshots)
            {
                if (!jobChartsData.ContainsKey(snap.JOB_NAME))
                {
                    jobChartsData.Add(snap.JOB_NAME, new JobmachineJobChartData());
                }
                jobChartsData[snap.JOB_NAME].Dates.Add(snap.SNAPSHOT_DATETIME);
                jobChartsData[snap.JOB_NAME].RunCount.Add(snap.RECENT_RUN_COUNT);
                jobChartsData[snap.JOB_NAME].RunTimeTotal.Add(snap.RECENT_DURATION_S);
                jobChartsData[snap.JOB_NAME].RunTimeAvg.Add(snap.RECENT_AVG_DURATION_S);
            }

            Int32 chartW = 1000;
            Int32 chartH = 300;

            var newChartJob = new Chart(chartW, chartH);
            newChartJob.AddTitle("Recent run count")
                    .AddLegend();
            foreach (var keyValue in jobChartsData)
            {
                newChartJob.AddSeries(
                        name: keyValue.Key,
                        chartType: "Line",
                        xValue: keyValue.Value.Dates,
                        yValues: keyValue.Value.RunCount
                    );
            }
            model.JobCharts.Add(newChartJob);

            newChartJob = new Chart(chartW, chartH);
            newChartJob.AddTitle("Total runtime")
                    .AddLegend();
            foreach (var keyValue in jobChartsData)
            {
                newChartJob.AddSeries(
                        name: keyValue.Key,
                        chartType: "Line",
                        xValue: keyValue.Value.Dates,
                        yValues: keyValue.Value.RunTimeTotal
                    );
            }
            model.JobCharts.Add(newChartJob);

            newChartJob = new Chart(chartW, chartH);
            newChartJob.AddTitle("Average runtime")
                    .AddLegend();
            foreach (var keyValue in jobChartsData)
            {
                newChartJob.AddSeries(
                        name: keyValue.Key,
                        chartType: "Line",
                        xValue: keyValue.Value.Dates,
                        yValues: keyValue.Value.RunTimeAvg
                    );
            }
            model.JobCharts.Add(newChartJob);

            return View(model);
        }
    }
}