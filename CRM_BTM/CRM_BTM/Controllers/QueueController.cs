﻿using CustomAuth.Authentication;
using CustomAuth.Authorization;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CRM_BTM.Models
{
    [Authorize]
    public class QueueController : Controller
    {
        
        private CRMConnection db = new CRMConnection();

        // GET: Queue
        [AuthAuthorize(Roles = "super_admin", Claims = "queue_index")]
        public ActionResult Index()
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            return View(db.QUEUE.ToList());
        }

        // GET: Queue/Create
        [AuthAuthorize(Roles = "super_admin", Claims = "queue_create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Queue/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "queue_create")]
        public ActionResult Create([Bind(Include = "ID,NAME")] QUEUE qUEUE)
        {
            if (ModelState.IsValid)
            {
                db.QUEUE.Attach(qUEUE);
                var checkUnique = db.QUEUE.Where(x => x.NAME.Equals(qUEUE.NAME)).Any();
                if (checkUnique)
                {
                    ViewMessage.addMessage(TempData, "Name must be unique.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
                db.QUEUE.Add(qUEUE);
                //db.SaveChanges();
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                ViewMessage.addMessage(TempData, "Queue " + qUEUE.NAME + " created.", ViewMessage.Types.Success);
                return RedirectToAction("Index");
            }
            else
            {
                ViewMessage.addMessage(TempData, "Invalid data state.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            
        }

        // GET: Queue/Edit/5
        [AuthAuthorize(Roles = "super_admin", Claims = "queue_edit")]
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                ViewMessage.addMessage(TempData, "Invalid queue.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            QUEUE qUEUE = db.QUEUE.Find(id);
            if (qUEUE == null)
            {
                ViewMessage.addMessage(TempData, "Queue not found.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            ACTIVITY_LOG.PrepareForDisplay(ViewBag, qUEUE);
            return View(qUEUE);
        }

        // POST: Queue/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "queue_edit")]
        public ActionResult Edit(QUEUE qUEUE)
        {
            if (ModelState.IsValid)
            {
                db.QUEUE.Attach(qUEUE);
                var checkUnique = db.QUEUE.Where(x => x.NAME.Equals(qUEUE.NAME)).Where(x => x.ID != qUEUE.ID).Any();
                if (checkUnique)
                {
                    ViewMessage.addMessage(TempData, "Name must be unique.", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
                db.Entry(qUEUE).State = EntityState.Modified;
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                ViewMessage.addMessage(TempData, "Queue ranamed to " + qUEUE.NAME + ".", ViewMessage.Types.Success);
                return RedirectToAction("Index");
            }
            ViewMessage.addMessage(TempData, "Invalid data state.", ViewMessage.Types.Error);
            return RedirectToAction("Index");
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
