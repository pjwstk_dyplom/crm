﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CustomAuth.DataAccess;
using CustomAuth.Models;
using CRM_BTM.Models;
using CustomAuth.Authorization;

namespace CRM_BTM.Controllers.Auth
{
    [Authorize]
    public class ManageUserController : Controller
    {
        private AuthDbContext db = new AuthDbContext();
        private CRMConnection crmDb = new CRMConnection();

        // GET: ManageUser
        [AuthAuthorize(Roles = "super_admin", Claims = "manageuser_index")]
        public ActionResult Index()
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            return View(db.Users.ToList());
        }

        // GET: ManageUser/Details/5
        [AuthAuthorize(Roles = "super_admin", Claims = "manageuser_details")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        [AuthAuthorize(Roles = "super_admin", Claims = "manageuser_manageuserroles")]
        public ActionResult ManageUserRoles(int? id)
        {
            var model = new ManageUserModel
            {   
                UserModel = db.Users.Find(id),
                RoleModel = db.Roles.ToList()
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "manageuser_manageuserroles")]
        public ActionResult ManageUserRoles(ManageUserModel model, int id)
        {
            var user = db.Users.Find(id);

            if (ModelState.IsValid)
            {
                var roleList = model.ModelOut.Select(int.Parse).ToList();
                if (model.ModelOut != null)
                {
                    if (user != null)
                    {
                        user.Roles.Clear();
                        var newRoles = db.Roles.Where(r => roleList.Contains(r.RoleId)).ToList();
                        user.Roles = newRoles;
                    }
                }
                else
                {
                    user.Roles.Clear();
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            
            return View(model);
        }



        //GET: ManageUser/ManageUserClaims
        [AuthAuthorize(Roles = "super_admin", Claims = "manageuser_manageuserclaims")]
        public ActionResult ManageUserClaims(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            var model = new ManageUserModel
            {
                UserModel = db.Users.Find(id),
                ClaimsModel = db.Claims.ToList(),
                RolesClaims= db.Users.Find(id)
                                     .Roles
                                     .ToList()
                                     .SelectMany(r => r.Claims.Select(c => c.ClaimId))
            };
            
            var roleClaims = model.UserModel.Roles
                    .ToList()
                    .SelectMany(r => r.Claims
                                    .Select(c=> c.ClaimId));

            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "manageuser_manageuserclaims")]
        public ActionResult ManageUserClaims(ManageUserModel model, int id)
        {
            var user = db.Users.Find(id);
            if (ModelState.IsValid)
            {
                var claimsList = model.ClaimsOut.Select(int.Parse).ToList();

                if (model.ClaimsOut != null)
                {
                    if (user != null)
                    {
                        user.Claims.Clear();
                        var newClaims = db.Claims.Where(r => claimsList.Contains(r.ClaimId)).ToList();
                        user.Claims = newClaims;
                    }
                }
                else
                {
                    user.Claims.Clear();
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }


        // GET: ManageUser/Create
        [AuthAuthorize(Roles = "super_admin", Claims = "manageuser_create")]
        public ActionResult Create()
        {
            var model = new ManageUserModel
            {
                RoleModel = db.Roles.ToList(),
                ClaimsModel = db.Claims.ToList(),
                UserModel = new User(),
                UserHrId = "",
                ClaimsOut = new List<String>(),
                ModelOut = new List<String>()
            };

            return View(model);
        }

        // POST: ManageUser/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "manageuser_create")]
        public ActionResult Create( ManageUserModel userOut)
        {
            if (String.IsNullOrEmpty(userOut.UserModel.Password) || userOut.UserModel.Password.Length < 8)
            {
                ViewMessage.addMessage(TempData, "Password too short, 8 characters minimum.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            userOut.UserModel.Salt = CustomAuth.Authentication.AuthMembershipProvider.Create_salt();
            userOut.UserModel.Password = CustomAuth.Authentication.AuthMembershipProvider.Hash_password(userOut.UserModel.Password, userOut.UserModel.Salt);

            if (ModelState.IsValid)
            {
                List<int> role_int_lst = userOut.ModelOut.Select(int.Parse).ToList();
                List<int> claims_int_lst= userOut.ClaimsOut.Select(int.Parse).ToList();
                
                var usr_roles = db.Roles.Where(r => role_int_lst.Contains(r.RoleId)).ToList();
                var usr_claims = db.Claims.Where(c => claims_int_lst.Contains(c.ClaimId)).ToList();

                userOut.UserModel.Roles = usr_roles;
                userOut.UserModel.Claims = usr_claims;
                db.Users.Add(userOut.UserModel);
                db.SaveChanges();

                var crmUser = new CRM_USER
                {
                    IDENTITY_ID = userOut.UserModel.Username,
                    HR_ID = userOut.UserHrId
                };
                crmDb.CRM_USER.Add(crmUser);

                var crmBasket = new BASKET
                {
                    NAME = "default",
                    USER_ID = crmDb.Entry<CRM_USER>(crmUser).Entity.ID,
                    IS_DEFAULT = true
                };
                crmDb.BASKET.Add(crmBasket);

                crmDb.SaveChanges();
                return RedirectToAction("Index");
            }

            var model = new ManageUserModel
            {
                RoleModel = db.Roles.ToList()
            };
            
            return View(model);
        }
        
        // GET: ManageUser/Edit/5
        [AuthAuthorize(Roles = "super_admin", Claims = "manageuser_edit")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var model = new ManageUserModel
            {
                RoleModel = db.Roles.ToList(),
                UserModel = db.Users.Find(id)
            };

            
            if (model.UserModel == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: ManageUser/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "manageuser_edit")]
        public ActionResult Edit(ManageUserModel model)
        {

            if (ModelState.IsValid)
            {
                var user = (from us in db.Users.AsNoTracking()
                            where model.UserModel.UserId == us.UserId
                            select us).FirstOrDefault();                

                if (model.UserPassword!=null)
                 {
                     model.UserModel.Salt = CustomAuth.Authentication.AuthMembershipProvider.Create_salt();
                     model.UserModel.Password = CustomAuth.Authentication.AuthMembershipProvider.Hash_password(model.UserPassword, model.UserModel.Salt);
                }
                 else { 
                    model.UserModel.Salt = user.Salt;
                    model.UserModel.Password = user.Password;
                }

                db.Entry(model.UserModel).State = EntityState.Modified;

                
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model.UserModel);
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
