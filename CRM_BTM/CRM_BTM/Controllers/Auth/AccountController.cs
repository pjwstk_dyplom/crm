﻿using CustomAuth.Authentication;
using CustomAuth.DataAccess;
using CustomAuth.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace CRM_BTM.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller
    {
        [Authorize]
        // GET: Account  
        public ActionResult Index()
        {
            //Debug.WriteLine("AccountIndex action");
            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Login(string ReturnUrl = "")
        {
            //Debug.WriteLine("AccountLogin GET action");
            if (User.Identity.IsAuthenticated)
            {
                //Debug.WriteLine("Already authenticated - redirecting");
                return RedirectToAction("Index", "Home");
            }
            ViewBag.ReturnUrl = ReturnUrl;
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(AuthLoginView loginView, string ReturnUrl = "")
        {
            //Debug.WriteLine("Login attempt");
            if (ModelState.IsValid)
            {
                //Debug.WriteLine("Model valid");
                if (Membership.ValidateUser(loginView.UserName, loginView.Password))
                {
                    //Debug.WriteLine("User validated");
                    var user = (AuthMembershipUser)Membership.GetUser(loginView.UserName, false);
                    
                    if (user != null)
                    {

                        AuthSerializeModel userModel = new AuthSerializeModel()
                        {
                            UserId = user.UserId,
                            FirstName = user.FirstName,
                            LastName = user.LastName,
                            Email = user.Email,
                            RoleName = user.Roles.Select(r => r.RoleId.ToString()).ToList(),
                            //ClaimNames = user.Roles.ToList().SelectMany(r => r.Claims.ToList().ConvertAll(c => c.ClaimName())).ToList()
                            ClaimNames = user.Claims
                                             .Select(r => r.ClaimId.ToString())
                                             .ToList()
                                             .Concat(user.Roles
                                                         .ToList()
                                                         .SelectMany(r => r.Claims
                                                                           .ToList()
                                                                           .ConvertAll(c => c.ClaimId.ToString()))
                                                         .ToList())
                                             .Distinct()
                                             .ToList()
                        };
                        //Debug.WriteLine("We have userModel");
                        string userData = JsonConvert.SerializeObject(userModel);
                        FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket
                            (
                            1, loginView.UserName, DateTime.Now, DateTime.Now.AddMinutes(15), false, userData
                            );

                        string enTicket = FormsAuthentication.Encrypt(authTicket);
                        HttpCookie faCookie = new HttpCookie("UserData", enTicket);
                        //Debug.WriteLine("Adding cookie for "+userModel.FirstName);
                        Debug.WriteLine("Claims: " + string.Join(",", userModel.ClaimNames));
                        Debug.WriteLine("Roles: " + string.Join(",", userModel.RoleName));
                        Debug.WriteLine("Email: " + userModel.Email);
                        Response.Cookies.Add(faCookie);
                    }

                    if (Url.IsLocalUrl(ReturnUrl))
                    {
                        return Redirect(ReturnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                } 
            } 
            ModelState.AddModelError("", "Invalid login request.");
            return View("Login", loginView);
        }

        [HttpGet]
        public ActionResult Registration()
        {
            return View("Registration");
        }

        [Authorize]
        [HttpPost]
        public ActionResult Registration(AuthRegistrationView registrationView)
        {
            bool statusRegistration = false;
            string messageRegistration = string.Empty;

            if (ModelState.IsValid)
            {
                // Email Verification  
                string userName = Membership.GetUserNameByEmail(registrationView.Email);
                if (!string.IsNullOrEmpty(userName))
                {
                    ModelState.AddModelError("Warning Email", "Sorry: Email already Exists");
                    return View(registrationView);
                }

                //Save User Data   
                using (AuthDbContext dbContext = new AuthDbContext())
                {
                    var new_salt = CustomAuth.Authentication.AuthMembershipProvider.Create_salt();
                    var encrypterdpass = CustomAuth.Authentication.AuthMembershipProvider.Hash_password(registrationView.Password, new_salt);

                    var user = new User()
                    {
                        Username = registrationView.Username,
                        FirstName = registrationView.FirstName,
                        LastName = registrationView.LastName,
                        Email = registrationView.Email,
                        Password = encrypterdpass,
                        Salt = new_salt,
                        ActivationCode = Guid.NewGuid(),
                    };

                    dbContext.Users.Add(user);
                    dbContext.SaveChanges();
                }

                //Verification Email  
                VerificationEmail(registrationView.Email, registrationView.ActivationCode.ToString());
                messageRegistration = "Your account has been created successfully.";
                statusRegistration = true;
            }
            else
            {
                messageRegistration = "Something went wrong.";
            }
            ViewBag.Message = messageRegistration;
            ViewBag.Status = statusRegistration;

            return View("Registration", registrationView);
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult ActivationAccount(string id)
        {
            bool statusAccount = false;
            using (AuthDbContext dbContext = new AuthDbContext())
            {
                var userAccount = dbContext.Users.Where(u => u.ActivationCode.ToString().Equals(id)).FirstOrDefault();

                if (userAccount != null)
                {
                    userAccount.IsActive = true;
                    dbContext.SaveChanges();
                    statusAccount = true;
                }
                else
                {
                    ViewBag.Message = "Something went wrong.";
                }

            }
            ViewBag.Status = statusAccount;
            return View("Login");
        }

        public ActionResult LogOut()
        {
            HttpCookie cookie = new HttpCookie("UserData", "");
            cookie.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie);

            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account", null);
        }

        [NonAction]
        public void VerificationEmail(string email, string activationCode)
        {
            var url = string.Format("/Account/ActivationAccount/{0}", activationCode);
            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, url);

            var fromEmail = new MailAddress("CRM_BTM@mail.com", "Account Activation");
            var toEmail = new MailAddress(email);

            var fromEmailPassword = "******************";
            string subject = "Activation Account !";

            string body = "<br/> Please click on the following link in order to activate your account" + "<br/><a href='" + link + "'> Activation Account ! </a>";

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };

            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true

            }) { };

            // smtp.Send(message);

        }
    }
}
