﻿using CustomAuth.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM_BTM.Controllers
{
    [Authorize]
    public class UserController : Controller
    {

        // GET: User  
        [AuthAuthorize(Roles = "super_admin", Claims = "user_index")]
        public ActionResult Index()
        {
            return View();
        }
    }
}