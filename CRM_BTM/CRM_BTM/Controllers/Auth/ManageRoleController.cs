﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CustomAuth.DataAccess;
using CustomAuth.Models;
using CustomAuth.Authorization;
using CRM_BTM.Models;

namespace CRM_BTM.Controllers.Auth
{   [Authorize]
    public class ManageRoleController : Controller
    {
        private AuthDbContext db = new AuthDbContext();


        // GET: ManageRoles
        [AuthAuthorize(Roles = "super_admin", Claims = "managerole_index")]
        public ActionResult Index()
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            return View(db.Roles.ToList());
        }


        // GET: ManageRoles/Create
        [AuthAuthorize(Roles = "super_admin", Claims = "managerole_create")]
        public ActionResult Create()
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            ManageRoleModel model = new ManageRoleModel()
            {
                ClaimModel = db.Claims.ToList()
            };
            return View(model);
        }

        
        // POST: ManageRoles/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "managerole_create")]
        public ActionResult Create(ManageRoleModel newRoleModel)
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            if (ModelState.IsValid)
            {
                var ClaimsIntList = newRoleModel.ClaimsList.Select(int.Parse).ToList();
                newRoleModel.ActRole.Claims = db.Claims.Where(c => ClaimsIntList.Contains(c.ClaimId)).ToList();

                db.Roles.Add(newRoleModel.ActRole);
                db.SaveChanges();
                ViewMessage.addMessage(TempData, "Role created", ViewMessage.Types.Success);
                return RedirectToAction("Index");
            }

            ViewMessage.addMessage(TempData, "Invalid data state.", ViewMessage.Types.Error);
            return View(newRoleModel);
        }


        // GET: ManageRoles/Edit/5
        [AuthAuthorize(Roles = "super_admin", Claims = "managerole_edit")]
        public ActionResult Edit(int? id)
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ManageRoleModel model = new ManageRoleModel()
            {
                ActRole = db.Roles.Find(id),
                ClaimModel = db.Claims.ToList()
            };

            if (model.ActRole == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: ManageRoles/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "managerole_edit")]
        public ActionResult Edit(ManageRoleModel roleModel)
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            if (ModelState.IsValid)
            {
                Role edtRole = db.Roles.Find(roleModel.ActRole.RoleId);
                
                if (roleModel.ClaimsList != null)
                {
                    var claimsIntList = roleModel.ClaimsList.Select(int.Parse).ToList();
         
                    edtRole.Claims.Clear();
                    edtRole.Claims = db.Claims.Where(c => claimsIntList.Contains(c.ClaimId)).ToList();
                }
                else
                {
                    edtRole.Claims.Clear();
                }
                edtRole.RoleName = roleModel.ActRole.RoleName;
                db.SaveChanges();
                ViewMessage.addMessage(TempData, "Role altered.", ViewMessage.Types.Success);
                return RedirectToAction("Index");
            }
            ViewMessage.addMessage(TempData, "Invalid data state.", ViewMessage.Types.Error);
            return View(roleModel);
        }

        // GET: ManageRoles/Delete/5
        [AuthAuthorize(Roles = "super_admin", Claims = "managerole_delete")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Role role = db.Roles.Find(id);
            if (role == null)
            {
                return HttpNotFound();
            }
            db.Roles.Remove(role);
            db.SaveChanges();
            ViewMessage.addMessage(TempData, "Role deleted", ViewMessage.Types.Success);
            return RedirectToAction("Index");
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [AuthAuthorize(Roles = "super_admin", Claims = "managerole_roleusers")]
        public ActionResult RoleUsers(int? id)
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            if (id == null)
            {
                ViewMessage.addMessage(TempData, "Role not provided.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            ManageRoleModel model = new ManageRoleModel
            {
                ActRole = db.Roles.Find(id),
                existingUserModel = null,
                UserModel = null,
            };
            return View(model);
        }

        // GET: ManageRoles/RoleUsersList
        [AuthAuthorize(Roles = "super_admin", Claims = "managerole_roleusers")]
        public ActionResult RoleUsersList(int? id, String UsernameString, String NameString, String LastNameString)
        {
            if (!Request.IsAjaxRequest()) {
                ViewMessage.addMessage(TempData, "Direct access not allowed.", ViewMessage.Types.Error);
                return RedirectToAction("RoleUsers", new { id = id });
            }
            if (id == null) { return HttpNotFound(); }

            List<int> roleUserId;
            ICollection<User> existingUserModellLst;
            ICollection<User> otherUserModelLst;

            var Role = db.Roles.Find(id);
            var UsersInRole = Role.Users.ToList();

            if (!String.IsNullOrEmpty(UsernameString))
                UsersInRole = UsersInRole.Where(u => u.Username.Contains(UsernameString)).ToList();
            if (!String.IsNullOrEmpty(NameString))
                UsersInRole = UsersInRole.Where(u => u.FirstName.Contains(NameString)).ToList();
            if (!String.IsNullOrEmpty(LastNameString))
                UsersInRole = UsersInRole.Where(u => u.LastName.Contains(LastNameString)).ToList();
            existingUserModellLst = UsersInRole.ToList();
            
            roleUserId = existingUserModellLst.Select(s => s.UserId).ToList();
            var AllUsersQ = db.Users.Where(u => !roleUserId.Contains(u.UserId)).ToList().AsQueryable();

            if (!String.IsNullOrEmpty(UsernameString))
                AllUsersQ = AllUsersQ.Where(u => u.Username.Contains(UsernameString));
            if (!String.IsNullOrEmpty(NameString))
                AllUsersQ = AllUsersQ.Where(u => u.FirstName.Contains(NameString));
            if (!String.IsNullOrEmpty(LastNameString))
                AllUsersQ = AllUsersQ.Where(u => u.LastName.Contains(LastNameString));
            otherUserModelLst = AllUsersQ.ToList();
            
            ManageRoleModel model = new ManageRoleModel
            {
                ActRole = db.Roles.Find(id),
                existingUserModel = existingUserModellLst,
                UserModel = otherUserModelLst,
            };
            return PartialView(model);
        }

        [AuthAuthorize(Roles = "super_admin", Claims = "managerole_roleusers")]
        public ActionResult RemoveUser(int? u_id, int? r_id)
        {
            User user = db.Users.Find(u_id);
            Role role = db.Roles.Find(r_id);

            if (user == null | role == null)
            {
                ViewMessage.addMessage(TempData, "User or role not found", ViewMessage.Types.Error);
                return RedirectToAction("RoleUsers", new { id = r_id });
            }

            role.Users.Remove(user);
            db.SaveChanges();

            ViewMessage.addMessage(TempData, "User removed from role", ViewMessage.Types.Success);
            return RedirectToAction("RoleUsers", new { id = r_id });
        }

        [AuthAuthorize(Roles = "super_admin", Claims = "managerole_roleusers")]
        public ActionResult AddUser(int? u_id, int? r_id)
        {
            User user = db.Users.Find(u_id);
            Role role = db.Roles.Find(r_id);
            if (user == null | role == null)
            {
                ViewMessage.addMessage(TempData, "User or role not found", ViewMessage.Types.Error);
                return RedirectToAction("RoleUsers", new { id = r_id });
            }
            role.Users.Add(user);
            db.SaveChanges();

            ViewMessage.addMessage(TempData, "User added to role", ViewMessage.Types.Success);
            return RedirectToAction("RoleUsers", new { id = r_id });
        }


    }
}
