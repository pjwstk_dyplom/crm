﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM_BTM.Controllers
{   [AllowAnonymous]
    public class ErrorController : Controller
    {
        // GET: Error  
        public ActionResult AccessDenied()
        {
            return View();
        }

        public ActionResult Error()
        {
            return View();
        }
    }
}
