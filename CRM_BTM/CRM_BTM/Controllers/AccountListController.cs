﻿using CRM_BTM.Email;
using CRM_BTM.Models;
using CRM_BTM.Models.ViewModels;
using CustomAuth.Authentication;
using CustomAuth.Authorization;
using CustomAuth.DataAccess;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.EntityFrameworkCore;
using System.Data.Entity.Core.EntityClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Security;

namespace CRM_BTM.Controllers
{
    [Authorize]
    public class AccountListController : Controller
    {
        private CRMConnection db = new CRMConnection();

        // GET: AccountList
        [AuthAuthorize(Roles = "super_admin", Claims = "accountlist_index")]
        public ActionResult Index()
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            var model = db.CRMAPP_ACCLIST_BASE.Include(m => m.CRMAPP_ACCLIST_FILES).ToList();
            return View(model);
        }

        [NonAction]
        private void QueryToCsv(OracleDataReader reader, string filepath)
        {
            StringBuilder sb = new StringBuilder();
            // Get All column names for csv
            var columnNames = Enumerable.Range(0, reader.FieldCount)
                                    .Select(reader.GetName)
                                    .ToList();
            sb.Append(string.Join(",", columnNames));
            sb.AppendLine();

            StreamWriter sw = new StreamWriter(filepath, false, Encoding.UTF8);
            sw.Write(sb.ToString());

            // Get the data
            while (reader.Read())
            {
                sb = new StringBuilder();
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    if (i > 0)
                        sb.Append(',');
                    var x = reader[i];
                    int num;
                    if (int.TryParse(reader[i].ToString(), out num))
                    {
                        sb.Append(num);
                    }
                    else
                    {
                        sb.Append('"');
                        sb.Append(reader[i]);
                        sb.Append('"');
                    }
                }
                sb.AppendLine();
                sw.Write(sb.ToString());
            }
            sw.Close();
        }

        // GET: AccountList/LaunchReport/1
        [AuthAuthorize(Roles = "super_admin", Claims = "accountlist_launchreport")]
        public ActionResult LaunchReport(int id)
        {
            var AccListBase = db.CRMAPP_ACCLIST_BASE.Find(id);
            if (AccListBase == null)
            {
                ViewMessage.addMessage(TempData, "Report not found", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            var AccListSQL = db.CRMAPP_ACCLIST_SQL.Find(id);
            if (AccListSQL == null)
            {
                ViewMessage.addMessage(TempData, "Report has no SQL generated", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            var CurrentUser = User;

            // get fresh account list
            OracleConnection DBConnection = new OracleConnection(db.Database.Connection.ConnectionString);
            if (DBConnection.State != ConnectionState.Open)
                DBConnection.Open();
            OracleCommand GetAccountsCommand = new OracleCommand("CRMAPP_ACCLIST_P.GET_ACCOUNTS", DBConnection);
            GetAccountsCommand.CommandType = System.Data.CommandType.StoredProcedure;
            GetAccountsCommand.Parameters.Add(new OracleParameter(parameterName: "p_base_id", type: OracleDbType.Int64, direction: ParameterDirection.Input));
            GetAccountsCommand.Parameters["p_base_id"].Value = 1;
            GetAccountsCommand.Parameters.Add(new OracleParameter(parameterName: "p_request_id", type: OracleDbType.Int64, direction: ParameterDirection.Output));
            GetAccountsCommand.ExecuteNonQuery();
            var RequestID = Int64.Parse(GetAccountsCommand.Parameters["p_request_id"].Value.ToString());

            // prepare SQL
            string SqlToRun = AccListSQL.SQL_CODE.Replace(":1", RequestID.ToString());
            var downloadUrl = Url.Action("Download");

            // prepare paths
            string SettingsPath = System.Configuration.ConfigurationManager.AppSettings["AccountListFileRepository"].ToString();
            string CsvDestinationPath = HostingEnvironment.ApplicationPhysicalPath.Replace("\\", "/") + SettingsPath;
            CsvDestinationPath = CsvDestinationPath.Replace("//", "/");
            var invalids = System.IO.Path.GetInvalidFileNameChars();
            string NormalizedTitle = String.Join("", AccListBase.TITLE.Split(invalids, StringSplitOptions.RemoveEmptyEntries)).TrimEnd('.');
            string CsvFileName = String.Join("_", new string[] {
                        AccListBase.ID.ToString("D4"),
                        NormalizedTitle.Substring(0, Math.Min(NormalizedTitle.Length, 20)),
                        RequestID.ToString("D5")
                    }) + ".csv";

            StringBuilder PathForUser = new StringBuilder();

            PathForUser.Append(Request.Url.Authority);
            PathForUser.Append(Request.ApplicationPath);
            PathForUser.Append(downloadUrl);
            PathForUser.Append("?filename=");
            PathForUser.Append(CsvFileName);
            PathForUser.Replace("//", "/");

            // get further lgoic variables
            var UserMail = ((AuthPrincipal)CurrentUser).Email;

            // run the results in a separate thread
            new Thread(() =>
            {
                if (DBConnection.State != ConnectionState.Open)
                    DBConnection.Open();
                OracleCommand GetResultCommand = new OracleCommand(SqlToRun, DBConnection);
                GetResultCommand.CommandType = CommandType.Text;
                string CsvPath = CsvDestinationPath + CsvFileName;
                Debug.WriteLine("CsvPath " + CsvPath);
                Debug.WriteLine("CsvDestinationPath " + CsvDestinationPath);
                Debug.WriteLine("CsvFileName " + CsvFileName);
                using (var reader = GetResultCommand.ExecuteReader())
                {
                    QueryToCsv(reader, CsvPath);
                }
                DBConnection.Close();
                DBConnection.Dispose();

                var AuthUser = (AuthPrincipal)CurrentUser;
                var newFile = new CRMAPP_ACCLIST_FILES()
                {
                    BASE_ID = id,
                    DIRPATH = SettingsPath,
                    FILENAME = CsvFileName,
                    REQUEST_ID = RequestID,
                    REQUESTOR = CurrentUser.Identity.Name,
                    CREATE_DATE = DateTime.Now
                };

                db.CRMAPP_ACCLIST_FILES.Add(newFile);
                db.SaveChanges();

                MailControl.SendMail(
                    UserMail,
                    "Report ready: " + AccListBase.TITLE,
                    "Report can be found here:<br/>" + PathForUser.ToString()
                );

            }).Start();

            ViewMessage.addMessage(TempData, "Report request has been issued. You will recieve an email when it is ready to be downloaded.", ViewMessage.Types.Success);
            return RedirectToAction("Index");
        }

        // GET: AccountList/Edit/1
        [AuthAuthorize(Roles = "super_admin", Claims = "accountlist_edit")]
        public ActionResult Edit(int id)
        {
            var AccListBase = db.CRMAPP_ACCLIST_BASE.Find(id);
            if (AccListBase == null)
            {
                ViewMessage.addMessage(TempData, "Report not found", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            AccountListEditViewModel model = new AccountListEditViewModel()
            {
                Base = AccListBase,
                Columns = AccListBase.CRMAPP_ACCLIST_COLUMN_DEF.ToList(),
                Conditions = AccListBase.CRMAPP_ACCLIST_CONDITIONS.ToList()
            };
            ViewBag.ColumnSelectList = new SelectList(db.CRMAPP_ACCLIST_COLUMN_DEF, "ID", "TABLE_FIELD");
            ViewBag.ConditionSelectList = new SelectList(db.OFFER_REQUIREMENT_DEFINITION, "ID", "DESCRIPTION");
            return View(model);
        }

        // POST: AccountList/Edit/1
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "accountlist_edit")]
        public ActionResult Edit(AccountListEditViewModel model)
        {
            var AccListBase = db.CRMAPP_ACCLIST_BASE.Find(model.Base.ID);
            if (AccListBase == null)
            {
                ViewMessage.addMessage(TempData, "Report not found", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }

            if (!ModelState.IsValid)
            {
                ViewMessage.addMessage(TempData, "Invalid data state", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }

            AccListBase.TITLE = model.Base.TITLE;
            AccListBase.DESCRIPTION = model.Base.DESCRIPTION;

            AccListBase.CRMAPP_ACCLIST_COLUMN_DEF.Clear();
            if (model.Columns != null)
                foreach (var col in model.Columns.Distinct())
                    AccListBase.CRMAPP_ACCLIST_COLUMN_DEF.Add(db.CRMAPP_ACCLIST_COLUMN_DEF.Find(col.ID));

            AccListBase.CRMAPP_ACCLIST_CONDITIONS.Clear();
            if (model.Conditions != null)
                foreach (var cond in model.Conditions.Distinct())
                    AccListBase.CRMAPP_ACCLIST_CONDITIONS.Add(cond);

            db.SaveChanges();
            db.CRMAPP_ACCLIST_P_SAVE_SQL_FOR_BASE(model.Base.ID);

            ViewMessage.addMessage(TempData, "Report modified", ViewMessage.Types.Success);
            return RedirectToAction("Index");
        }

        // GET: AccountList/Add
        [AuthAuthorize(Roles = "super_admin", Claims = "accountlist_add")]
        public ActionResult Add()
        {
            AccountListEditViewModel model = new AccountListEditViewModel();
            ViewBag.ColumnSelectList = new SelectList(db.CRMAPP_ACCLIST_COLUMN_DEF, "ID", "TABLE_FIELD");
            ViewBag.ConditionSelectList = new SelectList(db.OFFER_REQUIREMENT_DEFINITION, "ID", "DESCRIPTION");
            return View(model);
        }

        // POST: AccountList/Add
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "accountlist_add")]
        public ActionResult Add(AccountListEditViewModel model)
        {
            if (!ModelState.IsValid)
            {
                ViewMessage.addMessage(TempData, "Invalid data state", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            if (String.IsNullOrEmpty(model.Base.DESCRIPTION))
            {
                ViewMessage.addMessage(TempData, "Description cannot be empty", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }

            var AccListBase = model.Base;
            db.CRMAPP_ACCLIST_BASE.Add(AccListBase);

            AccListBase.CRMAPP_ACCLIST_COLUMN_DEF.Clear();
            if (model.Columns != null)
                foreach (var col in model.Columns.Distinct())
                    AccListBase.CRMAPP_ACCLIST_COLUMN_DEF.Add(db.CRMAPP_ACCLIST_COLUMN_DEF.Find(col.ID));

            AccListBase.CRMAPP_ACCLIST_CONDITIONS.Clear();
            if (model.Conditions != null)
                foreach (var cond in model.Conditions.Distinct())
                    AccListBase.CRMAPP_ACCLIST_CONDITIONS.Add(cond);

            db.SaveChanges();
            db.CRMAPP_ACCLIST_P_SAVE_SQL_FOR_BASE(model.Base.ID);

            ViewMessage.addMessage(TempData, "Report added", ViewMessage.Types.Success);
            return RedirectToAction("Index");
        }

        // GET: AccountList/Delete
        [AuthAuthorize(Roles = "super_admin", Claims = "accountlist_delete")]
        public ActionResult Delete(int id)
        {
            var AccListBase = db.CRMAPP_ACCLIST_BASE.Find(id);
            if (AccListBase == null)
            {
                ViewMessage.addMessage(TempData, "Report not found", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }

            db.CRMAPP_ACCLIST_BASE.Remove(AccListBase);
            db.SaveChanges();

            ViewMessage.addMessage(TempData, "Report deleted", ViewMessage.Types.Success);
            return RedirectToAction("Index");
        }

        // GET: AccountList/Download/
        [AuthAuthorize(Roles = "super_admin", Claims = "accountlist_download")]
        public ActionResult Download(string filename)
        {
            try
            {
                var dbRecord = db.CRMAPP_ACCLIST_FILES.Where(x => x.FILENAME.Equals(filename)).FirstOrDefault();
                if (dbRecord == null)
                {
                    ViewMessage.addMessage(TempData, "File not found", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }

                string fullName = Server.MapPath("~/" + dbRecord.DIRPATH.ToString() + filename);
                byte[] fileBytes = GetFile(fullName);
                return File(
                    fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, filename);
            }
            catch (Exception e)
            {
                ViewMessage.addMessage(TempData, "Report doesn't exist", ViewMessage.Types.Error);

            }

            return RedirectToAction("Index");
        }

        private byte[] GetFile(string s)
        {
            System.IO.FileStream fs = System.IO.File.OpenRead(s);
            byte[] data = new byte[fs.Length];
            int br = fs.Read(data, 0, data.Length);
            if (br != fs.Length)
                throw new System.IO.IOException(s);

            return data;

        }

    }
}