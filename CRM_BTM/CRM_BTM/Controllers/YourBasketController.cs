﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CRM_BTM.Models;
using CustomAuth.Authorization;

namespace CRM_BTM.Controllers
{   [Authorize]
    public class YourBasketController : Controller
    {
        private CRMConnection db = new CRMConnection();

        // GET: YourBasket
        [AuthAuthorize(Roles = "super_admin", Claims = "yourbasket_index")]
        public ActionResult Index()
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);

            var q = db.CRM_USER.Where(u => u.IDENTITY_ID.Equals(User.Identity.Name));
            if (q.Any())
            {
                List<BASKET> userBaskets = db.BASKET.Where(b => b.CRM_USER.Equals(q.FirstOrDefault())).ToList();
                return View(userBaskets);
            }
            return View(new List<BASKET>());
        }

        // GET: YourBasket/Create
        [AuthAuthorize(Roles = "super_admin", Claims = "yourbasket_create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: YourBasket/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "yourbasket_create")]
        public ActionResult Create([Bind(Include = "NAME")] BASKET newBasket)
        {
            newBasket.NAME = newBasket.NAME.ToLower();
            if (ModelState.IsValidField("Name")) { 
                var q = db.CRM_USER.Where(u => u.IDENTITY_ID.Equals(User.Identity.Name));
                if (q.Any())
                {
                    var user = q.FirstOrDefault();
                    newBasket.CRM_USER = user;
                    newBasket.IS_DEFAULT = false;
                    db.BASKET.Add(newBasket);
                    db.SaveChangesLog(HttpContext.User.Identity.Name);

                    ViewMessage.addMessage(TempData, "Basket " + newBasket.NAME + " created.", ViewMessage.Types.Success);
                    return RedirectToAction("Index");
                }
                ViewMessage.addMessage(TempData, "User not identified.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            ViewMessage.addMessage(TempData, "Invalid data state.", ViewMessage.Types.Error);
            return RedirectToAction("Index");
        }

        // GET: YourBasket/Edit/5
        [AuthAuthorize(Roles = "super_admin", Claims = "yourbasket_edit")]
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BASKET b = db.BASKET.Find(id);
            ACTIVITY_LOG.PrepareForDisplay(ViewBag, b);
            if (b == null)
            {
                return HttpNotFound();
            }
            
            if (!b.CRM_USER.IDENTITY_ID.Equals(User.Identity.Name))
            {
                ViewMessage.addMessage(TempData, "Unauthorized user.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            return View(b);
        }

        // POST: YourBasket/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "yourbasket_edit")]
        public ActionResult Edit(BASKET b)
        {
            if (ModelState.IsValid)
            {
                b.NAME = b.NAME.ToLower();
                db.Entry(b).State = EntityState.Modified;
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                db.SaveChanges();
                ViewMessage.addMessage(TempData, "Basket " + b.NAME + " changed.", ViewMessage.Types.Success);
                return RedirectToAction("Index");
            }
            ViewMessage.addMessage(TempData, "Invalid data state.", ViewMessage.Types.Error);
            return RedirectToAction("Index");
        }

        // GET: YourBasket/MakeDefault/5
        [AuthAuthorize(Roles = "super_admin", Claims = "yourbasket_makedefault")]
        public ActionResult MakeDefault(long? id)
        {

            BASKET b = db.BASKET.Find(id);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var query = db.BASKET.Where(x => x.USER_ID == b.USER_ID).ToList();
            if (query.Any())
            {
                foreach (var defBasket in query.ToList())
                { 
                    if (ModelState.IsValid) {

                        defBasket.IS_DEFAULT = defBasket.ID==b.ID ? true : false;
                       db.Entry(defBasket).State = EntityState.Modified;

                    }
                    
                }
            }

            

            if (!b.CRM_USER.IDENTITY_ID.Equals(User.Identity.Name))
            {
                ViewMessage.addMessage(TempData, "Unauthorized user.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }


            db.SaveChangesLog(HttpContext.User.Identity.Name);
            
            ViewMessage.addMessage(TempData, "Basket " + b.NAME + " is now your default.", ViewMessage.Types.Success);
            return RedirectToAction("Index");
        }
                
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
