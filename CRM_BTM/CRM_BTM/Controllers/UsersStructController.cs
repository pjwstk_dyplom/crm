﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CRM_BTM.Models;
using CustomAuth.Authorization;

namespace CRM_BTM.Controllers
{
    public class UsersStructController : Controller
    {
        private CRMConnection db = new CRMConnection();

        // GET: UsersStruct
        [AuthAuthorize(Roles = "super_admin", Claims = "usersstruct_index")]
        public ActionResult Index()
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            
            return View(new UserStructModel());
        }

        // Post: UsersStruct/UsersStructList
        [HttpPost]
        [AuthAuthorize(Roles = "super_admin", Claims = "usersstruct_index")]
        public ActionResult UsersStructList(String AgentString, String UsernameString, String ManagerString, String CodeString, String TeamString)
        {
            if (!Request.IsAjaxRequest()) return RedirectToAction("Index");

            var salesStruct_srch = db.SALES_STRUCTURE_V
                                            .ToList()
                                            .Where(u =>
                                             (u.FIRSTNAME.ToUpper().Contains(AgentString == null ? "" : AgentString.ToUpper()) | u.LASTNAME.ToUpper().Contains(AgentString == null ? "" : AgentString.ToUpper()) | AgentString == null)
                                            & (u.USERNAME.ToUpper().Contains(UsernameString == null ? "" : UsernameString.ToUpper()) | UsernameString == null)
                                            & (u.FIRSTNAME_MANAGER == null ? "".Contains(ManagerString == null ? "" : ManagerString.ToUpper()) : (u.FIRSTNAME_MANAGER.ToUpper().Contains(ManagerString == null ? "" : ManagerString.ToUpper()) | u.LASTNAME_MANAGER.ToUpper().Contains(ManagerString == null ? "" : ManagerString.ToUpper())) | ManagerString == null)
                                            & (u.CODE == null ? "".Contains(CodeString == null ? "" : CodeString.ToUpper()) : (u.CODE.ToUpper().Contains(CodeString == null ? "" : CodeString.ToUpper())) | CodeString == null)
                                            & (u.TEAM == null ? "".Contains(TeamString == null ? "" : TeamString.ToUpper()) : (u.TEAM.ToUpper().Contains(TeamString == null ? "" : TeamString.ToUpper())) | TeamString == null)
                                            );

            return PartialView(salesStruct_srch);
        }


        [AuthAuthorize(Roles = "super_admin", Claims = "usersstruct_usermanager")]
        public ActionResult UserManager(int? id)
        {

            try
            { 
            UserStructModel model = new UserStructModel
            {
                allUsers = db.IDENTITY2USER_STRUCT_V.Where(d => d.STRUCT_ID == null & d.STRUCT_ID != id).ToList(),
                salesStructure = db.SALES_STRUCTURE_V.Where(d => d.USR_STC_ID != id).ToList(),
                userIdent = db.IDENTITY2USER_STRUCT_V.Where(d => d.STRUCT_ID == id).First(),
                userStcEdt = db.USERS_STRUCT.Find(id)



            };

                if (id == null | id == 0 | model.userIdent == null | model.userStcEdt == null)
                {
                    ViewMessage.addMessage(TempData, "Unresolved Edit User Manager error - please contact aplication support.", ViewMessage.Types.Error);

                    return RedirectToAction("Index");

                }

            return View(model);
            }
            catch (Exception e)
            {
                ViewMessage.addMessage(TempData, "Unresolved Edit User Manager error - please contact aplication support (" + (e.InnerException == null ? e.Message : e.InnerException.Message) + ").", ViewMessage.Types.Error);

                return RedirectToAction("Index");

            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "usersstruct_usermanager")]
        public ActionResult UserManager( UserStructModel model, int id)
        {
            Debug.WriteLine("id " + id);
            Debug.WriteLine("managerStructid " + model.managerStructid);

            try
            {
                var userStcEdt = db.USERS_STRUCT.Find(id);
                var usr = db.IDENTITY2USER_STRUCT_V.Where(d => d.STRUCT_ID == id).First();

                if (model.managerStructid == userStcEdt.MANAGER_ID)
                {
                    var mng = db.IDENTITY2USER_STRUCT_V.Where(d => d.STRUCT_ID == model.managerStructid).First();

                    ViewMessage.addMessage(TempData, mng.FIRST_NAME + " " + mng.LAST_NAME + " is already manager of " + usr.FIRST_NAME + " " + usr.LAST_NAME, ViewMessage.Types.Warning);
                    return RedirectToAction("Index");

                }
                if (model.managerStructid == 0 | model.managerStructid is null)
                {
                    ViewMessage.addMessage(TempData, "Invalid Manager ID", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }


                if (userStcEdt.CRM_USER.ID != 0)
                {
                    db.SALES_STRUCTURE_PCK_ADD_USER_TO_STRUCT(
                            p_USER_ID: userStcEdt.CRM_USER.ID,
                            p_MANAGER_ID: model.managerStructid,
                            p_TEAM_ID: null
                            );

                    ViewMessage.addMessage(TempData, "Manager of " + usr.FIRST_NAME + " " + usr.LAST_NAME + " changed!", ViewMessage.Types.Success);
                }
                else
                {
                    ViewMessage.addMessage(TempData, "Invalid user.", ViewMessage.Types.Error);
                }
            }
            catch (Exception e)
            {
                ViewMessage.addMessage(TempData, "Unresolved Edit User Manager error - please contact aplication support (" + (e.InnerException == null ? e.Message : e.InnerException.Message) + ").", ViewMessage.Types.Error);
            }




            return RedirectToAction("Index");
        }

        [AuthAuthorize(Roles = "super_admin", Claims = "usersstruct_adduser")]
        public ActionResult AddUser()
        {
            ViewBag.Title = "Add user to Sales Structure";
            UserStructModel model = new UserStructModel
            {
                allUsers = db.IDENTITY2USER_STRUCT_V.Where(d => d.STRUCT_ID==null).ToList(),
                salesStructure = db.SALES_STRUCTURE_V.ToList(),
                teamsModel = db.TEAMS.ToList()
                

            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "usersstruct_adduser")]
        public ActionResult AddUser(UserStructModel model)
        {
            try
            {
                if (model.crmUserId != 0)
                {
                    db.SALES_STRUCTURE_PCK_ADD_USER_TO_STRUCT(
                          p_USER_ID: model.crmUserId,
                          p_MANAGER_ID: model.managerStructid,
                          p_TEAM_ID: model.teamId
                          );
                    ViewMessage.addMessage(TempData, "User added to Sale Structure!", ViewMessage.Types.Success);
                }
                else
                {
                    ViewMessage.addMessage(TempData, "Invalid user.", ViewMessage.Types.Error);
                }
            }
            catch (Exception e)
            {
                ViewMessage.addMessage(TempData, "Unresolved add user to sale strucutre error - please contact aplication support (" + (e.InnerException == null ? e.Message : e.InnerException.Message) + ").", ViewMessage.Types.Error);
            }
            
            return RedirectToAction("Index");
            
        }

        [AuthAuthorize(Roles = "super_admin", Claims = "usersstruct_teamslist")]
        public ActionResult TeamsList(string TeamNameString, string CodeString, string LeaderString)
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            
            return View();
        }

        // Post: UsersStruct/TeamsListSearch
        [HttpPost]
        [AuthAuthorize(Roles = "super_admin", Claims = "usersstruct_teamslist")]
        public ActionResult TeamsListSearch(string TeamNameString, string CodeString, string LeaderString)
        {
            if (!Request.IsAjaxRequest()) return RedirectToAction("TeamsList");

            var team2leader = db.TEAM_LEADER2IDENTITY_V.ToList().Where(d =>
                        (d.DESCRIPTION.ToUpper().Contains(TeamNameString == null ? "" : TeamNameString.ToUpper()) | TeamNameString == null)
                      & (d.TEAMCODE.ToUpper().Contains(CodeString == null ? "" : CodeString.ToUpper()) | CodeString == null)
                      & ((d.LEADERFIRSTNAME + " " + d.LEADERLASTNAME).ToUpper().Contains(LeaderString == null ? " " : LeaderString.ToUpper())));
                      
            return PartialView(team2leader);
        }

        [AuthAuthorize(Roles = "super_admin", Claims = "usersstruct_teamusers")]
        public ActionResult TeamUsers(int? id, string NameString, string LastnameString, string TeamString, string CodeString)
        {
            try
            {
                UserStructModel model = new UserStructModel
                {
                    teamEdit = db.TEAMS.Find(id),
                    salesStructure = db.SALES_STRUCTURE_V.ToList()
                };
                return View(model);
            }
            catch (Exception e)
            {
                ViewMessage.addMessage(TempData, "Unresolved Team Users error - please contact aplication support (" + (e.InnerException == null ? e.Message : e.InnerException.Message) + ").", ViewMessage.Types.Error);
                return RedirectToAction("TeamsList");
            }
        }

        [AuthAuthorize(Roles = "super_admin", Claims = "usersstruct_teamedit")]
        public ActionResult TeamUsersSearch(int? id, string NameString, string LastnameString, string TeamString, string CodeString)
        {
            if (!Request.IsAjaxRequest()) return RedirectToAction("TeamsList");

            var salesStructureDB = db.SALES_STRUCTURE_V.ToList().Where(d =>
                                (d.FIRSTNAME.ToUpper().Contains(NameString == null ? "" : NameString.ToUpper()) | NameString == null)
                                & (d.LASTNAME.ToUpper().Contains(LastnameString == null ? "" : LastnameString.ToUpper()) | LastnameString == null)
                                & ((d.TEAM == null ? "" : d.TEAM).ToUpper().Contains(TeamString == null ? "" : TeamString.ToUpper()) | TeamString == null)
                                & ((d.CODE == null ? "" : d.CODE).ToUpper().Contains(CodeString == null ? "" : CodeString.ToUpper()) | CodeString == null));

            UserStructModel model = new UserStructModel
            {
                teamEdit = db.TEAMS.Find(id),
                salesStructure = salesStructureDB
            };

            return PartialView(model);
        }
        [AuthAuthorize(Roles = "super_admin", Claims = "usersstruct_teamedit")]
        public ActionResult TeamEdit(int? id, string NameString, string LastnameString, string TeamString, string CodeString)
        {
            try
            {
                UserStructModel model = new UserStructModel
                {

                    teamEdit = db.TEAMS.Find(id),
                    salesStructure = db.SALES_STRUCTURE_V.ToList().Where(d =>
                            (d.FIRSTNAME.ToUpper().Contains(NameString == null ? "" : NameString.ToUpper()) | NameString == null)
                            & (d.LASTNAME.ToUpper().Contains(LastnameString == null ? "" : LastnameString.ToUpper()) | LastnameString == null)
                            & ((d.TEAM == null ? "" : d.TEAM).ToUpper().Contains(TeamString == null ? "" : TeamString.ToUpper()) | TeamString == null)
                            & ((d.CODE == null ? "" : d.CODE).ToUpper().Contains(CodeString == null ? "" : CodeString.ToUpper()) | CodeString == null)).ToList()

                    

            };
                
                if (model.teamEdit == null)
                {
                    ViewMessage.addMessage(TempData, "Invalid team id.", ViewMessage.Types.Error);

                    return RedirectToAction("TeamsList");
                }

                ViewBag.Title = "Edit Team: " + model.teamEdit.DESCRIPTION;

                if (Request.IsAjaxRequest()) return PartialView(model);
                return View(model);
            }
            catch (Exception e)
            {
                ViewMessage.addMessage(TempData, "Unresolved Team edit error - please contact aplication support (" + (e.InnerException == null ? e.Message : e.InnerException.Message) + ").", ViewMessage.Types.Error);

                return RedirectToAction("TeamsList");
            }


        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "usersstruct_teamedit")]
        public ActionResult TeamEdit(UserStructModel model, int id)
        {
            Debug.WriteLine("t_id" + id);
            Debug.WriteLine("model.teamEdit.DESCRIPTION " + model.teamEdit.DESCRIPTION);
            Debug.WriteLine("model.teamLeaderId " + model.teamLeaderId);

            var team = db.TEAMS.Where(d => d.ID == id).First();

            if (team == null)
            {
                ViewMessage.addMessage(TempData, "Invalid team id.", ViewMessage.Types.Error);

                return RedirectToAction("TeamsList");
            }

            if (model.teamLeaderId != 0 & model.teamLeaderId != null & model.teamEdit.DESCRIPTION != "" & model.teamEdit.DESCRIPTION != null)
            {
                team.TEAM_LEADER_ID = (long)model.teamLeaderId;
                team.DESCRIPTION = model.teamEdit.DESCRIPTION;
                db.SaveChanges();
                ViewMessage.addMessage(TempData, "Team edited!", ViewMessage.Types.Success);
            }
            else
            {
                ViewMessage.addMessage(TempData, "Invalid team leader or team name.", ViewMessage.Types.Error);
            }
            
            return RedirectToAction("TeamsList");
        }

        [AuthAuthorize(Roles = "super_admin", Claims = "usersstruct_teamremoveuser")]
        public ActionResult TeamRemoveUser(int? u_id, int? t_id, UserStructModel model)
        {
            try
            {
                var act_struct = db.SALES_STRUCTURE_V.Where(d => d.USR_STC_ID == u_id).First();

                var sl_code = db.SALE_CODE.Where(d => d.ID == act_struct.SALE_CODE_ID).First();

                sl_code.VALID_TO = DateTime.Today.AddDays(-1);

                ViewMessage.addMessage(TempData, "User " + act_struct.FIRSTNAME + " " + act_struct.LASTNAME + "removed!", ViewMessage.Types.Success);
                
                db.SaveChanges();
            }
            catch (Exception e)
            {
                ViewMessage.addMessage(TempData, "Unresolved Remove User From Team error - please contact aplication support (" + (e.InnerException == null ? e.Message : e.InnerException.Message) + ").", ViewMessage.Types.Error);

            }
            return RedirectToAction("TeamEdit", new { id = t_id });
        }

        [AuthAuthorize(Roles = "super_admin", Claims = "usersstruct_teamadduser")]
        public ActionResult TeamAddUser(int? u_id, int? t_id)
        {
            try
            {
                db.SALES_STRUCTURE_PCK_ADD_SALE_CODE(
                    p_SALES_STRUCT_ID: u_id,
                    p_TEAMS_ID: t_id,
                    p_VALID_FROM: DateTime.Today,
                    p_VALID_TO: new DateTime(2200, 1, 1, 0, 0, 0)
                    );

                var act_struct = db.SALES_STRUCTURE_V.Where(d => d.USR_STC_ID == u_id).First();

                ViewMessage.addMessage(TempData, "User " + act_struct.FIRSTNAME + " " + act_struct.LASTNAME + "removed!", ViewMessage.Types.Success);
            }
            catch (Exception e)
            {
                ViewMessage.addMessage(TempData, "Unresolved Add User To Team error - please contact aplication support (" + (e.InnerException == null ? e.Message : e.InnerException.Message) + ").", ViewMessage.Types.Error);
            }

            return RedirectToAction("TeamEdit", new { id = t_id });
        }

        [AuthAuthorize(Roles = "super_admin", Claims = "usersstruct_teamadd")]
        public ActionResult TeamAdd()
        {
            ViewBag.Title = "Add team";
            try
            {
                UserStructModel model = new UserStructModel
                {

                    salesStructure = db.SALES_STRUCTURE_V.ToList()

                };

                return View(model);
            }
            catch (Exception e)
            {
                ViewMessage.addMessage(TempData, "Unresolved Add User To Team error - please contact aplication support (" + (e.InnerException == null ? e.Message : e.InnerException.Message) + ").", ViewMessage.Types.Error);
                return RedirectToAction("TeamsList");
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "usersstruct_teamadd")]
        public ActionResult TeamAdd(UserStructModel model)
        {
            Debug.WriteLine("teamDesc " + model.teamEdit.DESCRIPTION + " teamLeaderId " + model.teamLeaderId);



            if (model.teamEdit.DESCRIPTION == null | model.teamEdit.DESCRIPTION == "" |
                model.teamLeaderId == null |
                model.teamEdit.TEAM_CODE == null | model.teamEdit.TEAM_CODE == "")
            {
                string message = "";

                if (model.teamEdit.DESCRIPTION == null | model.teamEdit.DESCRIPTION == "")
                {
                    message = message + "team name ";
                }

                if (model.teamLeaderId == null)
                {
                    message = (message != "" ? (message + ", ") : "") + "team leader";
                }

                if (model.teamEdit.TEAM_CODE == null | model.teamEdit.TEAM_CODE == "")
                {
                    message = (message != "" ? (message + ", ") : "") + "team code";
                }

                ViewMessage.addMessage(TempData, "Invalid data (" + message + ")", ViewMessage.Types.Error);

            }
            else
            {
                try
                {
                    db.SALES_STRUCTURE_PCK_ADD_TEAM(
                    p_DESCRIPTION: model.teamEdit.DESCRIPTION,
                    p_TEAM_LEADER_ID: model.teamLeaderId,
                    p_TEAM_CODE: model.teamEdit.TEAM_CODE);

                    ViewMessage.addMessage(TempData, "Team " + model.teamEdit.DESCRIPTION + " created.", ViewMessage.Types.Success);
                }
                catch (Exception e)
                {
                    ViewMessage.addMessage(TempData, "ERROR: team name or team code already used (" + (e.InnerException == null ? e.Message : e.InnerException.Message) + ").", ViewMessage.Types.Error);
                }
 

            }


            return RedirectToAction("TeamsList");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
