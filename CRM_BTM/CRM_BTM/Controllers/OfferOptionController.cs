﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CRM_BTM.Models;

namespace CRM_BTM.Controllers
{
    public class OfferOptionController : Controller
    {
        private CRMConnection db = new CRMConnection();

        // GET: OfferOption
        public async Task<ActionResult> Index()
        {
            var oFFER_OPTION = db.OFFER_OPTION.Include(o => o.OFFER_DEFINITION);
            return View(await oFFER_OPTION.ToListAsync());
        }

        // GET: OfferOption/Details/5
        public async Task<ActionResult> Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OFFER_OPTION oFFER_OPTION = await db.OFFER_OPTION.FindAsync(id);
            if (oFFER_OPTION == null)
            {
                return HttpNotFound();
            }
            return View(oFFER_OPTION);
        }

        // GET: OfferOption/Create
        public ActionResult Create()
        {
            ViewBag.OFFER_DEFINITION_ID = new SelectList(db.OFFER_DEFINITION, "ID", "INVOICE_NAME");
            return View();
        }

        // POST: OfferOption/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,OFFER_DEFINITION_ID,INVOICE_NAME,INTERNAL_NAME,DESCRIPTION,VALID_FROM,VALID_TO")] OFFER_OPTION oFFER_OPTION)
        {
            if (ModelState.IsValid)
            {
                db.OFFER_OPTION.Add(oFFER_OPTION);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.OFFER_DEFINITION_ID = new SelectList(db.OFFER_DEFINITION, "ID", "INVOICE_NAME", oFFER_OPTION.OFFER_DEFINITION_ID);
            return View(oFFER_OPTION);
        }

        // GET: OfferOption/Edit/5
        public async Task<ActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OFFER_OPTION oFFER_OPTION = await db.OFFER_OPTION.FindAsync(id);
            if (oFFER_OPTION == null)
            {
                return HttpNotFound();
            }
            ViewBag.OFFER_DEFINITION_ID = new SelectList(db.OFFER_DEFINITION, "ID", "INVOICE_NAME", oFFER_OPTION.OFFER_DEFINITION_ID);
            return View(oFFER_OPTION);
        }

        // POST: OfferOption/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,OFFER_DEFINITION_ID,INVOICE_NAME,INTERNAL_NAME,DESCRIPTION,VALID_FROM,VALID_TO")] OFFER_OPTION oFFER_OPTION)
        {
            if (ModelState.IsValid)
            {
                db.Entry(oFFER_OPTION).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.OFFER_DEFINITION_ID = new SelectList(db.OFFER_DEFINITION, "ID", "INVOICE_NAME", oFFER_OPTION.OFFER_DEFINITION_ID);
            return View(oFFER_OPTION);
        }

        // GET: OfferOption/Delete/5
        public async Task<ActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OFFER_OPTION oFFER_OPTION = await db.OFFER_OPTION.FindAsync(id);
            if (oFFER_OPTION == null)
            {
                return HttpNotFound();
            }
            return View(oFFER_OPTION);
        }

        // POST: OfferOption/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(long id)
        {
            OFFER_OPTION oFFER_OPTION = await db.OFFER_OPTION.FindAsync(id);
            db.OFFER_OPTION.Remove(oFFER_OPTION);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
