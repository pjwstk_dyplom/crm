﻿using System;
using System.Collections.Generic;
using System.Data;
//using System.Data.Entity;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CRM_BTM.Models;
using System.Diagnostics;
using System.IO;
using Microsoft.VisualBasic.FileIO;
using Oracle.ManagedDataAccess.Client;
using System.Web.Hosting;
using System.Text.RegularExpressions;
using CustomAuth.Authorization;

namespace CRM_BTM.Controllers
{
    [Authorize]
    public class AddressBaseController : Controller
    {
        private CRMConnection db = new CRMConnection();

        // GET: AddressBuilding
        [AuthAuthorize(Roles = "super_admin", Claims = "addressbase_index")]
        public ActionResult Index( )
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);

            var model = new AddressBaseModel();

            if ((AddressBaseModel)TempData["AddressSrh"] != null)
            {
                model = (AddressBaseModel)TempData["AddressSrh"];


                
            }

            model.province = db.ADDRESS_PROVINCE.ToList();

            model.county = db.ADDRESS_COUNTY.Where(a => a.ADDRESS_PROVINCE_ID.Equals(model.provinceId)).ToList();

            model.city = db.ADDRESS_CITY.Where(a => a.ADDRESS_COUNTY_ID.Equals(model.countyId)).ToList();

            model.postCodeList = db.ADDRESS_BUILDING_INFO_V
                                  .Where(a => a.CITY_ID.Equals(model.cityId))
                                  .Select(s => s.POST_CODE).Distinct().ToList();

            model.street = db.ADDRESS_STREET.Where(a => a.ADDRESS_CITY_ID.Equals(model.cityId)).ToList();


            model.building_info = db.ADDRESS_BUILDING_INFO_V
                .Where(a => a.STREET_ID.Equals(model.streetID) |a.POST_CODE.Equals(model.postCodeIn) | a.BUILDING_ID.Equals(model.buildingId)).ToList().OrderBy(a => a.STREET ).ThenBy(b => b.BUILDING_NUMBER);

           return View(model);
        }

        [HttpPost]
        [AuthAuthorize(Roles = "super_admin", Claims = "addressbase_index")]
        public ActionResult Index(AddressBaseModel model)
        {
            TempData["AddressSrh"] = new AddressBaseModel();
            TempData["AddressSrh"] = model;

            return RedirectToAction("Index");
        }



        // GET: AddressBuilding/Details/5
        [AuthAuthorize(Roles = "super_admin", Claims = "addressbase_details")]
        public ActionResult Details(decimal id)
        {
            try { 
            AddressBaseModel model = new AddressBaseModel
            {

                buildingInfoDetail = db.ADDRESS_BUILDING_INFO_V.Where(a => a.BUILDING_ID.Equals(id)).First(),
                availTechnologies = db.MTM_AVAIL_TECH_ON_BUILDING.Where(a => a.ADDRESS_BUILDING_ID.Equals(id)).Include(a => a.TECHNOLOGY).ToList(),


                account = db.ACCOUNT
                            .Include(a => a.ADDRESS1)
                            .Include(a => a.CLIENT)
                                .ThenInclude(b => b.SEGMENT)
                            .Include(a => a.PRODUCT)
                                .ThenInclude(b => b.PRODUCT_DEFINITION)
                            .Include(a => a.PRODUCT)
                                .ThenInclude(b => b.PRODUCT_STATUS)



                            .Include(a => a.PRODUCT)
                                .ThenInclude(b => b.TECHNOLOGY)
                            .Where(a => a.ADDRESS1.ADDRESS_BUILDING_ID.Equals(id))

            };

                if (model.buildingInfoDetail == null)
                {
                    ViewMessage.addMessage(TempData, "Invalid building id", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }

            return View(model);
            }
            catch (Exception e)
            {
                ViewMessage.addMessage(TempData, "Unresolved  Building details error - please contact aplication support (" + (e.InnerException == null ? e.Message : e.InnerException.Message) + ").", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }

        }


        [AuthAuthorize(Roles = "super_admin", Claims = "addressbase_importaddressbase")]
        public ActionResult ImportAddressBase()
        {
        

            return View();
        }


        [HttpPost]
        [AuthAuthorize(Roles = "super_admin", Claims = "addressbase_importaddressbase")]
        public ActionResult ImportAddressBase(AddressBaseModel model)
        {
            string FileName = Path.GetFileNameWithoutExtension(model.FileUpload.FileName);

            string FileExtension = Path.GetExtension(model.FileUpload.FileName);

            string UploadPath = HostingEnvironment.ApplicationPhysicalPath+System.Configuration.ConfigurationManager.AppSettings["UserFilePath"].ToString();

            model.FilePath = UploadPath + FileName+ FileExtension;

            Debug.WriteLine(UploadPath);

            model.FileUpload.SaveAs(model.FilePath);

            DataTable csvData = new DataTable();
            try
            {
                using (TextFieldParser csvReader = new TextFieldParser(model.FilePath))
                {
                    csvReader.SetDelimiters(new string[] { "," });
                    csvReader.HasFieldsEnclosedInQuotes = true;
                    

                    csvData.Columns.Add("LON");
                    csvData.Columns.Add("LAT");
                    csvData.Columns.Add("NUMBER");
                    csvData.Columns.Add("STREET");
                    csvData.Columns.Add("UNIT");
                    csvData.Columns.Add("CITY");
                    csvData.Columns.Add("DISTRICT");
                    csvData.Columns.Add("REGION");
                    csvData.Columns.Add("POSTCODE");
                    csvData.Columns.Add("ID");
                    csvData.Columns.Add("HASH");

                csvReader.ReadLine();
                int rcount = 0;
                var connection = new OracleConnection(db.Database.Connection.ConnectionString);
                int maxpck = 100000;

                connection.Open();
                OracleCommand truncT = connection.CreateCommand();
                truncT.CommandText = "TRUNCATE TABLE ADDRESS_IMPORT_SOURCE";
                truncT.ExecuteNonQuery();
                connection.Close();

                while (!csvReader.EndOfData)
                    {
                    
                    for (int p=0; p< maxpck; p++)
                    {
                        
                        string[] fieldData = csvReader.ReadFields();
      
                        //Making empty value as null
                        for (int i = 0; i < fieldData.Length; i++)
                        {
                            if (fieldData[i] == "")
                            {
                                fieldData[i] = null;
                            }

                            if (i == 9) 
                            {
                                fieldData[i] = rcount.ToString();
                            }
                        }
                        csvData.Rows.Add(fieldData);
                        rcount++;
                        if (csvReader.EndOfData) { p = maxpck+1; }
                    }
                    bulkImport(csvData);
                    csvData.Rows.Clear();
                    Debug.WriteLine("ROWS INSERTED: " + rcount);
                    }
                    

    
                }

                 var connProc = new OracleConnection(db.Database.Connection.ConnectionString);

                String info;

                connProc.Open();
                OracleCommand run_proc = new OracleCommand("address_base_import_pck.run_all", connProc);
                run_proc.CommandType = CommandType.StoredProcedure;
                run_proc.Parameters.Add("@info", OracleDbType.Varchar2, 200);
                run_proc.Parameters["@info"].Direction = ParameterDirection.Output;
                run_proc.ExecuteNonQuery();
                info = run_proc.Parameters["@info"].Value.ToString();
                connProc.Close();


               info = info.Replace("|", "<br>");
                
                

                ViewMessage.addMessage(TempData, "Import complete! <br> <br> <b><u>IMPORTED:</u></b> <br>"+ info, ViewMessage.Types.Success);

                System.IO.File.Delete(model.FilePath);

                return RedirectToAction("Index");

            }
            catch (Exception ex)
            {
                ViewMessage.addMessage(TempData, "Unresolved import error - contact application support! <br>"+ ex.Message, ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            




            
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        [NonAction]
        protected void bulkImport(DataTable csvData)
        {

            using (var connection = new OracleConnection(db.Database.Connection.ConnectionString))
            {
                connection.Open();
                
                string[] NUMBERs = new string[csvData.Rows.Count];
                string[] STREETs = new string[csvData.Rows.Count];
                string[] CITYs = new string[csvData.Rows.Count];
                string[] DISTRICTs = new string[csvData.Rows.Count];
                string[] REGIONs = new string[csvData.Rows.Count];
                string[] POSTCODEs = new string[csvData.Rows.Count];
                string[] IDs = new string[csvData.Rows.Count];
                

                

                for (int j = 0; j < csvData.Rows.Count; j++)
                {
                    
                    NUMBERs[j] = Convert.ToString(csvData.Rows[j]["NUMBER"]);
                    STREETs[j] = Convert.ToString(csvData.Rows[j]["STREET"]);
                    CITYs[j] = Convert.ToString(csvData.Rows[j]["CITY"]);
                    DISTRICTs[j] = Convert.ToString(csvData.Rows[j]["DISTRICT"]);
                    REGIONs[j] = Convert.ToString(csvData.Rows[j]["REGION"]);
                    POSTCODEs[j] = Convert.ToString(csvData.Rows[j]["POSTCODE"]);
                    IDs[j] = Convert.ToString(csvData.Rows[j]["ID"]);
                    
                }


                OracleParameter NUMBER_ = new OracleParameter();
                NUMBER_.OracleDbType = OracleDbType.Varchar2;
                NUMBER_.Value = NUMBERs;

                OracleParameter STREET = new OracleParameter();
                STREET.OracleDbType = OracleDbType.Varchar2;
                STREET.Value = STREETs;


                OracleParameter CITY = new OracleParameter();
                CITY.OracleDbType = OracleDbType.Varchar2;
                CITY.Value = CITYs;

                OracleParameter DISTRICT = new OracleParameter();
                DISTRICT.OracleDbType = OracleDbType.Varchar2;
                DISTRICT.Value = DISTRICTs;

                OracleParameter REGION = new OracleParameter();
                REGION.OracleDbType = OracleDbType.Varchar2;
                REGION.Value = REGIONs;

                OracleParameter POSTCODE = new OracleParameter();
                POSTCODE.OracleDbType = OracleDbType.Varchar2;
                POSTCODE.Value = POSTCODEs;

                OracleParameter ID = new OracleParameter();
                ID.OracleDbType = OracleDbType.Varchar2;
                ID.Value = IDs;

                OracleCommand cmd = connection.CreateCommand();
                cmd.CommandText = "INSERT INTO ADDRESS_IMPORT_SOURCE ( NUMBER_, STREET,  CITY, DISTRICT, REGION, POSTCODE, ID) VALUES (:1, :2, :3, :4 , :5, :6, :7)";
                cmd.ArrayBindCount = IDs.Length;
                
                cmd.Parameters.Add(NUMBER_);
                cmd.Parameters.Add(STREET);
                cmd.Parameters.Add(CITY);
                cmd.Parameters.Add(DISTRICT);
                cmd.Parameters.Add(REGION);
                cmd.Parameters.Add(POSTCODE);
                cmd.Parameters.Add(ID);
                cmd.ExecuteNonQuery();
                connection.Close();

            }



        }
    }


   

}
