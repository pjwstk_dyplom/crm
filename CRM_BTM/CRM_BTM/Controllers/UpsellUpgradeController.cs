﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CRM_BTM.Models;
using CRM_BTM.Models.OfferModels;
using CRM_BTM.Models.ViewModels;
using CustomAuth.Authentication;
using CustomAuth.Authorization;
using log4net;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;

namespace CRM_BTM.Controllers
{
    [Authorize]
    public class UpsellUpgradeController : Controller
    {
        private CRMConnection db = new CRMConnection();

        // GET: Index/5
        // Starting screen for upsell/upgrade, informative
        [AuthAuthorize(Roles = "super_admin", Claims = "upsupgr_upsell,upsupgr_upgrade")]
        public ActionResult Index(long? id)
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            if (id == null)
            {
                ViewMessage.addMessage(TempData, "Account not provided.", ViewMessage.Types.Error);
                return RedirectToAction("Index", controllerName: "Home");
            }
            var Account = db.ACCOUNT.Find(id);

            ViewBag.AccountID = Account.ID;
            ViewBag.ClientID = Account.CLIENT_ID;

            return View();
        }

        // GET: OfferSelection/5
        // Gets available offers for this building and picks one
        [AuthAuthorize(Roles = "super_admin", Claims = "upsupgr_upsell,upsupgr_upgrade")]
        public ActionResult OfferSelection(long? id)
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            var model = new UpsellUpgradeProcessModel();
            if (id == null)
            {
                ViewMessage.addMessage(TempData, "Account not provided.", ViewMessage.Types.Error);
                return RedirectToAction("Index", controllerName: "Home");
            }
            model.AccountID = id;
            var Account = db.ACCOUNT.Find(model.AccountID);
            if (Account == null)
            {
                ViewMessage.addMessage(TempData, "Invalid account.", ViewMessage.Types.Error);
                return RedirectToAction("Index", controllerName: "Home");
            }
            var CurrentOffersValueDB = db.ACCOUNT_PROMO_VALUE_V.Where(x => x.ACCOUNT_ID == Account.ID).ToList();
            var CurrentOffersValue = Account.RemainingPromoValue(db);

            var OfferOptAvailability = new OfferOptionAvailability(db, accoID: (int)Account.ID, buildingId: (long)Account.INSTALL_ADDRESS_ID, onlyAvailable: true);
            List<OFFER_OPTION> OfferOptions = OfferOptAvailability.OfferEvaluation.Select(x => x.Key).Where(x => x.OFFER_OPTION_PRODUCT.Count > 0).ToList();
            ViewBag.OfferList = OfferOptions;
            
            var OfferOptionIDs = OfferOptions.Select(x => x.ID).ToList();
            var PricingPeriodsDB = db.OFFER_PRICING_PERIODS_V.Where(x => OfferOptionIDs.Contains(x.OFFER_OPTION_ID)).ToList();
            OfferPricingPeriods PricingPeriods = new OfferPricingPeriods(PricingPeriodsDB);
            ViewBag.PricingPeriods = PricingPeriods;

            var OfferOptionsAboveCurrentValue = new List<long>();
            foreach (long i in PricingPeriods.Keys)
                if (PricingPeriods[i].TotalPriceInPromo() > CurrentOffersValue)
                    OfferOptionsAboveCurrentValue.Add(i);
            ViewBag.OfferOptionsAboveCurrentValue = OfferOptionsAboveCurrentValue;

            return View(model);
        }

        // POST: OfferSelection
        [HttpPost]
        [AuthAuthorize(Roles = "super_admin", Claims = "upsupgr_upsell,upsupgr_upgrade")]
        public ActionResult OfferSelection(UpsellUpgradeProcessModel model, string ProcessWithOffer)
        {
            // Verification
            if (model == null)
            {
                ViewMessage.addMessage(TempData, "No model found.", ViewMessage.Types.Error);
                return RedirectToAction("Index", controllerName: "Home");
            }
            if (String.IsNullOrEmpty(ProcessWithOffer))
            {
                ViewMessage.addMessage(TempData, "No offer option selected.", ViewMessage.Types.Error);
                return RedirectToAction("OfferSelection", new { id = model.AccountID });
            }
            try
            {
                var StringSplit = ProcessWithOffer.Split('_');
                model.ProcessType = (UpsellUpgradeProcessModel.ProcessTypes)Enum.Parse(typeof(UpsellUpgradeProcessModel.ProcessTypes), StringSplit[0]);
                model.OfferOptionID = Int64.Parse(StringSplit[1]);
            } catch (Exception e)
            {
                ViewMessage.addMessage(TempData, "Invalid offer option selected:" + (e.InnerException == null ? e.Message : e.InnerException.Message), ViewMessage.Types.Error);
                return RedirectToAction("OfferSelection", new { id = model.AccountID });
            }
            
            // Routing
            TempData["UpsellUpgradeProcessModel"] = model;
            return RedirectToAction("Summary");
        }

        // GET: Summary
        [AuthAuthorize(Roles = "super_admin", Claims = "upsupgr_upsell,upsupgr_upgrade")]
        public ActionResult Summary()
        {
            UpsellUpgradeProcessModel model = (UpsellUpgradeProcessModel)TempData["UpsellUpgradeProcessModel"];
            if (model == null)
            {
                ViewMessage.addMessage(TempData, "No model found.", ViewMessage.Types.Error);
                return RedirectToAction("Index", controllerName: "Home");
            }
            var Offer = db.OFFER_OPTION.Find(model.OfferOptionID);
            if (Offer == null)
            {
                ViewMessage.addMessage(TempData, "Invalid offer.", ViewMessage.Types.Error);
                return RedirectToAction("OfferSelection", new { id = model.AccountID });
            }
            Offer.SetSingleOptionPricingPeriods(db);
            ViewBag.OfferOption = Offer;

            return View(model);
        }

        // POST: Summary
        [HttpPost]
        [AuthAuthorize(Roles = "super_admin", Claims = "upsupgr_upsell,upsupgr_upgrade")]
        public ActionResult Summary(UpsellUpgradeProcessModel model)
        {
            // Verification
            if (model == null)
            {
                ViewMessage.addMessage(TempData, "No model found.", ViewMessage.Types.Error);
                return RedirectToAction("Index", controllerName: "Home");
            }
            long SaleCodeID = 0;
            try
            {
                var CurrentUser = CRM_USER.getFromIdentity(db, HttpContext.User.Identity);
                var UserStruct = db.USERS_STRUCT.Where(x => x.USER_ID == CurrentUser.ID).Where(x => x.VALID_FROM <= DateTime.Today && DateTime.Today <= x.VALID_TO).First();
                var SaleCode = UserStruct.SALE_CODE.Where(x => x.VALID_FROM <= DateTime.Today && DateTime.Today <= x.VALID_TO).First();
                SaleCodeID = SaleCode.ID;
            } catch (Exception e)
            {
                ViewMessage.addMessage(TempData, "Error resolving your sale code: " + (e.InnerException == null ? e.Message : e.InnerException.Message), ViewMessage.Types.Error);
                return RedirectToAction("OfferSelection", new { id = model.AccountID });
            }
            // Body
            var Account = db.ACCOUNT.Find(model.AccountID);
            var OfferOption = db.OFFER_OPTION.Find(model.OfferOptionID);
            if (OfferOption == null)
            {
                ViewMessage.addMessage(TempData, "Invalid offer, not found: " + model.OfferOptionID, ViewMessage.Types.Error);
                return RedirectToAction("OfferSelection", new { id = model.AccountID });
            }

            var AuthPrincipalUser = (AuthPrincipal)User;
            string RequiredClaim = "";
            string OraCommandName = "";
            switch (model.ProcessType)
            {
                case UpsellUpgradeProcessModel.ProcessTypes.Upsell:
                    RequiredClaim = "upsupgr_upsell";
                    OraCommandName = "CLIENT_OFFER_PCK.UPSELL_OFFER";
                    break;
                case UpsellUpgradeProcessModel.ProcessTypes.Upgrade:
                    RequiredClaim = "upsupgr_upgrade";
                    OraCommandName = "CLIENT_OFFER_PCK.UPGRADE_OFFER";
                    break;
                default:
                    ViewMessage.addMessage(TempData, "Invalid upsell process.", ViewMessage.Types.Error);
                    return RedirectToAction("OfferSelection", new { id = model.AccountID });
            }
            if (!AuthPrincipalUser.HasClaim(RequiredClaim))
            {
                ViewMessage.addMessage(TempData, "Permission denied.", ViewMessage.Types.Error);
                return RedirectToAction("OfferSelection", new { id = model.AccountID });
            }
            
            // execute ora command to create new client
            OracleConnection DBConnection = new OracleConnection(db.Database.Connection.ConnectionString);
            if (DBConnection.State != ConnectionState.Open)
                DBConnection.Open();
            OracleCommand OraCommand = new OracleCommand(OraCommandName, DBConnection);
            OraCommand.CommandType = System.Data.CommandType.StoredProcedure;
            
            OraCommand.Parameters.Add("IN_ACCOUNT_ID", OracleDbType.Long).Value = model.AccountID;
            OraCommand.Parameters.Add("IN_OFFER_OPTION_ID", OracleDbType.Long).Value = model.OfferOptionID;
            OraCommand.Parameters.Add("IN_SALE_CODE_ID", OracleDbType.Long).Value = SaleCodeID;
            OraCommand.Parameters.Add("IN_SALE_DATE", OracleDbType.Date).Value = DateTime.Now;
            
            OraCommand.Prepare();

            try { 
                OraCommand.ExecuteNonQuery();
            } catch (Exception e)
            {
                ViewMessage.addMessage(TempData, "Error encountered: " + (e.InnerException == null ? e.Message : e.InnerException.Message), ViewMessage.Types.Error);
                return RedirectToAction("OfferSelection", new { id = model.AccountID });
            }

            // Routing
            ViewMessage.addMessage(TempData, "Offer sold!", ViewMessage.Types.Success);
            return RedirectToAction("Details", "Client", new { id = Account.CLIENT_ID, AccountID = model.AccountID });
        }


    }
}

 