﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CRM_BTM.Models;
using CRM_BTM.Models.ViewModels;
using System.Diagnostics;
using CRM_BTM.Models.Helpers;
using CustomAuth.Authorization;

namespace CRM_BTM.Controllers
{
    [Authorize]
    public class ClientSearchController : Controller
    {
        private CRMConnection db = new CRMConnection();
        private long SearchLimit = 200;

        // GET: CLIENT_SEARCH_V
        [AuthAuthorize(Roles = "super_admin", Claims = "clientsearch_index")]
        public ActionResult Index()
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);

            var addrCities = db.ADDRESS_CITY
                .Include(ac => ac.ADDRESS_COUNTY)
                .ToList()
                .Select(ac => new SelectListItem()
                {
                    Text = ac.NAME + " (" + ac.ADDRESS_COUNTY.NAME + ")",
                    Value = ac.ID.ToString()
                });

            ViewBag.Cities = new SelectList(addrCities, "Value", "Text");
            var identityName = User.Identity.Name;
            List<string> yourTasksListStatus = db.YOUR_TASKS_LIST_V.Where(s => s.IDENTITY_ID == identityName).Select(s => s.TASK_STATUS).Distinct().ToList();
            ViewBag.TasksStatus = new SelectList(yourTasksListStatus);
            ViewBag.VisibleTable = false;

            var userTasksTable = db.USER_TASKS_TABLE_V.Where(s => s.IDENTITY_ID == identityName);
            var yourTasksList = db.YOUR_TASKS_LIST_V.Where(s => s.IDENTITY_ID == identityName);

            return View();
        }

        // POST: CLIENT_SEARCH_V
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "clientsearch_index")]
        public ActionResult Index(int? clientId, string clientSurname, long? productId, string peselNumber, string nipNumber, string contractNumber, string invoiceNumber, long? Cities, string Street, decimal? building, decimal? flat, int? taskId, string TasksStatus)
        {
            ViewBag.Title = "Client search";

            var addrCities = db.ADDRESS_CITY
                .Include(ac => ac.ADDRESS_COUNTY)
                .ToList()
                .Select(ac => new SelectListItem()
                {
                    Text = ac.NAME + " (pow. " + ac.ADDRESS_COUNTY.NAME + ")",
                    Value = ac.ID.ToString()
                });

            ViewBag.Cities = new SelectList(addrCities, "Value", "Text");
            var identityName = User.Identity.Name;
            List<string> yourTasksListStatus = db.YOUR_TASKS_LIST_V.Where(s => s.IDENTITY_ID == identityName).Select(s => s.TASK_STATUS).Distinct().ToList();
            ViewBag.TasksStatus = new SelectList(yourTasksListStatus);
            ViewBag.VisibleTable = false;

            //Debug.WriteLine("Identity name" + identityName);
            IQueryable<CLIENT_SEARCH_V> search = db.CLIENT_SEARCH_V.Where(s => s.ID != -1);

            if (!String.IsNullOrEmpty(invoiceNumber))
            {
                //Debug.WriteLine("INVOICE NUMBER" + invoiceNumber);
                long invoiceAccountId = 0;
                invoiceAccountId = db.INVOICE.Where(s => s.INVOICE_NUMBER.ToLower() == invoiceNumber.ToLower()).Select(s => s.ACCOUNT_ID).FirstOrDefault();
                //Debug.WriteLine("INVOICE accounts" + invoiceAccountId);
                if (invoiceAccountId > 0)
                {
                    //Debug.WriteLine("INVOICE accounts search" + search.ToString());
                    search = search.Where(s => s.ID == invoiceAccountId);
                }

            }
            if (productId != null)
            {
                //Debug.WriteLine("productId" + productId);
                var productAccountId = db.PRODUCT.Where(s => s.ID == productId).Select(s => s.ACCOUNT_ID).FirstOrDefault();
                search = search.Where(s => s.ID == productAccountId);
            }

            if (clientId != null)
            {
                //Debug.WriteLine("clients: " + clientId);
                search = search.Where(s => s.ID == clientId);
            }
            if (!String.IsNullOrEmpty(clientSurname))
            {
                //Debug.WriteLine("clientSurname:" + clientSurname);
                search = search.Where(s => s.LASTNAME.ToLower().Contains(clientSurname.ToLower()));
            }
            if (!String.IsNullOrEmpty(peselNumber))
            {
                //Debug.WriteLine("peselNumber:" + peselNumber);
                search = search.Where(s => s.PESEL.Equals(peselNumber));
            }
            if (!String.IsNullOrEmpty(nipNumber))
            {
                //Debug.WriteLine("nipNumber:" + nipNumber);
                search = search.Where(s => s.NIP.Equals(nipNumber));
            }
            if (!String.IsNullOrEmpty(contractNumber))
            {
                //Debug.WriteLine("contractNumber:" + contractNumber);
                search = search.Where(s => s.CONTRACT_NUMBER.ToLower().Contains(contractNumber.ToLower()));
            }
            if (Cities != null)
            {
                //Debug.WriteLine("city:" + Cities);
                search = search.Where(s => s.CITYID == Cities);
            }
            if (!String.IsNullOrEmpty(Street))
            {
                //Debug.WriteLine("Streets:" + Street);
                search = search.Where(s => s.STREET.ToLower().Contains(Street.ToLower()));
            }
            if (building != null)
            {
                //Debug.WriteLine("building" + building);
                search = search.Where(s => s.BUILDINGID == building);
            }
            if (flat != null)
            {
                //Debug.WriteLine("flat" + flat);
                search = search.Where(s => s.FLATID == flat);
            }

            if (search.Count() > this.SearchLimit)
            {
                ViewMessage.addMessage(TempData, "Too many clients match your search criteria. Please be more specific.", ViewMessage.Types.Warning);
                return RedirectToAction("Index");
            }

            var Comparer = new ClientSearchVEqualityComparer();
            ViewBag.ClientSearchVEqualityComparer = Comparer;

            return View(search.ToList());
        }
    }
}
