﻿using CRM_BTM.Models;
using CRM_BTM.Models.ViewModels;
using CustomAuth.DataAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace CRM_BTM.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private CRMConnection db = new CRMConnection();
        private AuthDbContext IdentityDB = new AuthDbContext();

        public ActionResult Index()
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);

            var CurrentUser = CRM_USER.getFromIdentity(db, User.Identity);
            var YourTasks = db.YOUR_TASKS_LIST_V.Where(x => x.USER_ID == CurrentUser.ID).ToList();
            var TaskMovements = db.YOUR_TASKS_MOVEMENTS_V.Where(x => x.USER_ID == CurrentUser.ID).ToList();
            var IdentityUser = IdentityDB.Users.Where(x => x.Username.Equals(User.Identity.Name)).First();

            var Model = new HomeIndexViewModel() {
                CrmUser = CurrentUser,
                IdentityUser = IdentityUser,
                CurrentTasks = YourTasks,
                RecentTasks = TaskMovements
            };

            var CurrentTasksChartData = new Dictionary<string, long>();
            foreach (var item in Model.CurrentTasks)
            {
                if (!CurrentTasksChartData.ContainsKey(item.TASK_TYPE))
                    CurrentTasksChartData.Add(item.TASK_TYPE, 0);
                CurrentTasksChartData[item.TASK_TYPE]++;
            }
            var CurrentTaskChart = new Chart(500, 500)
                .AddSeries(
                    name: "Default",
                    chartType: "Pie",
                    xValue: CurrentTasksChartData, xField: "Key",
                    yValues: CurrentTasksChartData, yFields: "Value"
                )
                .AddLegend();
            Model.CurrentTasksChart = CurrentTaskChart;

            var RecentTasksChartData = new Dictionary<string, long>();
            foreach (var item in Model.CurrentTasks)
            {
                if (!RecentTasksChartData.ContainsKey(item.TASK_STATUS))
                    RecentTasksChartData.Add(item.TASK_STATUS, 0);
                RecentTasksChartData[item.TASK_STATUS]++;
            }
            var RecentTasksChart = new Chart(500, 500)
                .AddSeries(
                    name: "Default",
                    chartType: "Pie",
                    xValue: RecentTasksChartData, xField: "Key",
                    yValues: RecentTasksChartData, yFields: "Value"
                )
                .AddLegend();
            Model.RecentTasksChart = RecentTasksChart;

            return View(Model);
        }
        
    }
}