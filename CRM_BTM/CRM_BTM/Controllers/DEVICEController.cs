﻿using System;
using System.Collections.Generic;
using System.Data;
/*using Microsoft.EntityFrameworkCore;*/
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CRM_BTM.Models;
using CRM_BTM.Models.ViewModels;
using System.Diagnostics;
using CustomAuth.Authorization;

namespace CRM_BTM.Controllers
{
    [Authorize]
    public class DeviceController : Controller
    {
        private CRMConnection db = new CRMConnection();
        
        // AJAX: Device/Type2BrandModel
        [AuthAuthorize(Roles = "super_admin", Claims = "device_index")]
        public ActionResult Type2BrandModel(long id)
        {
            if (!Request.IsAjaxRequest())
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var DevType = db.DEVICE_TYPE.Find(id);
            if (DevType == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            var BrandModelList = db.DEVICE_BRAND_MODEL.Where(x => x.DEVICE_TYPE_ID == id).ToList();
            return PartialView("PartialViews/Type2BrandModel", BrandModelList);
        }

        // AJAX: Device/BrandModel2Device
        [AuthAuthorize(Roles = "super_admin", Claims = "device_index")]
        public ActionResult BrandModel2Device(long id, string NotPicked, string NotInstalled)
        {
            if (!Request.IsAjaxRequest())
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var BrandModel = db.DEVICE_BRAND_MODEL.Find(id);
            if (BrandModel == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            
            var Devices = db.DEVICE.Where(x => x.DEVICE_BRAND_MODEL_ID == id);
            if (NotPicked.Equals("true"))
                Devices = Devices.Where(x => x.CRM_USER_ID == null);
            if (NotInstalled.Equals("true"))
                Devices = Devices.Where(x => x.PRODUCT.Count() == 0);

            var DeviceList = Devices.ToList();
            return PartialView("PartialViews/BrandModel2Device", DeviceList);
        }
        
        // GET: DEVICE
        [AuthAuthorize(Roles = "super_admin", Claims = "device_index")]
        public ActionResult Index()
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            var OverviewData = db.INSTALLER_DEVICES_STATE_V.Where(d => d.IDENTITY_ID == User.Identity.Name).OrderBy(d => d.DEVICE_TYPE).ToList();

            ViewBag.DEVICE_TYPE_ID = new SelectList(db.DEVICE_TYPE, "ID", "DESCRIPTION", null);

            return View(OverviewData);
        }

        [AuthAuthorize(Roles = "super_admin", Claims = "device_pickdevice")]
        public ActionResult PickDevice(int id)
        {
            DEVICE dev = db.DEVICE.Find(id);

            if (dev == null)
            {
                ViewMessage.addMessage(TempData, "Device not found", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }

            var usr = db.IDENTITY2USER_STRUCT_V.Where(d => d.USERNAME.Equals(User.Identity.Name)).Select(s => s.USER_ID).First();

            dev.CRM_USER_ID = usr;
            db.SaveChangesLog(HttpContext.User.Identity.Name);

            ViewMessage.addMessage(TempData, "Device has been assigned to you.", ViewMessage.Types.Success);

            return RedirectToAction("Index");
        }

        // GET: DEVICE/Details/5
        [AuthAuthorize(Roles = "super_admin", Claims = "device_details")]
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DEVICE dEVICE = db.DEVICE.Include(d => d.DEVICE_BRAND_MODEL)/*.ThenInclude(d => d.DEVICE_TYPE)*/.ToList().Where(t => t.ID == id).First();
            if (dEVICE == null)
            {
                return HttpNotFound();
            }
            return View(dEVICE);
        }

        // GET: DEVICE/Create
        [AuthAuthorize(Roles = "super_admin", Claims = "device_create")]
        public ActionResult Create()
        { 
            
            ViewBag.DEVICE_BRAND_MODEL_ID = new SelectList((from s in db.DEVICE_BRAND_MODEL.Include(d => d.DEVICE_TYPE).ToList()
                                                         select new
                                                         {
                                                             ID = s.ID,
                                                             BRAND_MODEL ="("+s.DEVICE_TYPE.DESCRIPTION+") "+ s.BRAND+ " - "+s.MODEL
                                                         }),
                                                            "ID",
                                                            "BRAND_MODEL",
                                                            null);
            return View();
        }

        // POST: DEVICE/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "device_create")]
        public ActionResult Create([Bind(Include = "ID,SERIAL,MAC,DEVICE_BRAND_MODEL_ID")] DEVICE dEVICE)
        {
            if (ModelState.IsValid)
            {
                db.DEVICE.Add(dEVICE);
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                ViewMessage.addMessage(TempData, "Device created", ViewMessage.Types.Success);
                return RedirectToAction("Index");
            }
            else
            {
                ViewMessage.addMessage(TempData, "Invalid data state", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
        }

        // GET: DEVICE/Edit/5
        [AuthAuthorize(Roles = "super_admin", Claims = "device_edit")]
        public ActionResult Edit(long? id)
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DEVICE dEVICE = db.DEVICE.Find(id);
            if (dEVICE == null)
            {
                return HttpNotFound();
            }

            ViewBag.DEVICE_BRAND_MODEL_ID = new SelectList((from s in db.DEVICE_BRAND_MODEL.Include(d => d.DEVICE_TYPE).ToList()
                                                            select new
                                                            {
                                                                ID = s.ID,
                                                                BRAND_MODEL = "(" + s.DEVICE_TYPE.DESCRIPTION + ") " + s.BRAND + " - " + s.MODEL
                                                            }),
                                                            "ID",
                                                            "BRAND_MODEL",
                                                            dEVICE.DEVICE_BRAND_MODEL_ID);
            ACTIVITY_LOG.PrepareForDisplay(ViewBag, dEVICE);
            return View(dEVICE);
        }

        // POST: DEVICE/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "device_edit")]
        public ActionResult Edit([Bind(Include = "ID,SERIAL,MAC,DEVICE_BRAND_MODEL_ID,CRM_USER_ID")] DEVICE dEVICE)
        {
            if (ModelState.IsValid)
            {  
                db.Entry(dEVICE).State = System.Data.Entity.EntityState.Modified;
                db.SaveChangesLog(HttpContext.User.Identity.Name);
                ViewMessage.addMessage(TempData, "Device modified", ViewMessage.Types.Success);
                return RedirectToAction("Index");
            }
            else
            {
                ViewMessage.addMessage(TempData, "Invalid data state", ViewMessage.Types.Error);
                return RedirectToAction("Edit", new { id = dEVICE.ID });
            }

        }

        // GET: DEVICE/Delete/5
        [AuthAuthorize(Roles = "super_admin", Claims = "device_delete")]
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DEVICE dEVICE = db.DEVICE.Find(id);
            if (dEVICE == null)
            {
                return HttpNotFound();
            }
            db.DEVICE.Remove(dEVICE);
            db.SaveChangesLog(User.Identity.Name);
            //db.SaveChanges();
            ViewMessage.addMessage(TempData, "Device removed", ViewMessage.Types.Success);

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
