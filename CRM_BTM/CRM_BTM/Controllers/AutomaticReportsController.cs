﻿using CustomAuth.DataAccess;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using CRM_BTM.Models;
using CRM_BTM.ViewModels;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using CRM_BTM.Views.Shared;
using System.Diagnostics;
using CustomAuth.Authorization;

namespace CRM_BTM.Controllers
{
    [Authorize]
    public class AutomaticReportsController : Controller
    {
        private CRMConnection db = new CRMConnection();

        // GET: AutomaticReports Index
        [AuthAuthorize(Roles = "super_admin", Claims = "automaticreports_index")]
        public ActionResult Index()
        {
            ViewBag.Title = "Automatic reports - List";
            List<REPORT> reports = db.REPORT.Include(r => r.ALLOWED_USERS).ToList();
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            
            return View(reports);
        }

        // GET: AutomaticReports Run
        [AuthAuthorize(Roles = "super_admin", Claims = "automaticreports_run")]
        public ActionResult Run(int? id)
        {
            if (id != null)
            {
                try
                {
                    REPORT rep = db.REPORT.Find(id);
                    if (rep != null)
                    {
                        db.AUTOMATIC_REPORTS_P_RUN_REPORT(rep.ID);
                        ViewMessage.addMessage(TempData, "Report has been ordered to run.", ViewMessage.Types.Success);
                        return RedirectToAction("Index");
                    }
                } catch (Exception e)
                {
                    ViewMessage.addMessage(TempData, "Unexpected error (" + (e.InnerException == null ? e.Message : e.InnerException.Message) + ").", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
            }
            ViewMessage.addMessage(TempData, "Invalid report ID.", ViewMessage.Types.Error);
            return RedirectToAction("Index");
        }

        // GET: AutomaticReports Enable
        [AuthAuthorize(Roles = "super_admin", Claims = "automaticreports_enable")]
        public ActionResult Enable(int? id)
        {
            if (id != null)
            {
                try
                {
                    REPORT rep = db.REPORT.Find(id);
                    if (rep != null)
                    {
                        db.AUTOMATIC_REPORTS_P_ENABLE_REPORT(rep.ID);
                        ViewMessage.addMessage(TempData, "Report has been enabled.", ViewMessage.Types.Success);
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception e)
                {
                    ViewMessage.addMessage(TempData, "Unexpected error (" + (e.InnerException == null ? e.Message : e.InnerException.Message) + ").", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
            }
            ViewMessage.addMessage(TempData, "Invalid report ID.", ViewMessage.Types.Error);
            return RedirectToAction("Index");
        }

        // GET: AutomaticReports Disable
        [AuthAuthorize(Roles = "super_admin", Claims = "automaticreports_disable")]
        public ActionResult Disable(int? id)
        {
            if (id != null)
            {
                try
                {
                    REPORT rep = db.REPORT.Find(id);
                    if (rep != null)
                    {
                        db.AUTOMATIC_REPORTS_P_DISABLE_REPORT(rep.ID);
                        ViewMessage.addMessage(TempData, "Report has been disabled.", ViewMessage.Types.Success);
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception e)
                {
                    ViewMessage.addMessage(TempData, "Unexpected error (" + (e.InnerException == null ? e.Message : e.InnerException.Message) + ").", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
            }
            ViewMessage.addMessage(TempData, "Invalid report ID.", ViewMessage.Types.Error);
            return RedirectToAction("Index");
        }

        // GET: AutomaticReports Remove
        [AuthAuthorize(Roles = "super_admin", Claims = "automaticreports_remove")]
        public ActionResult Remove(int? id)
        {
            if (id != null)
            {
                try
                {
                    REPORT rep = db.REPORT.Find(id);
                    if (rep != null)
                    {
                        db.AUTOMATIC_REPORTS_P_REMOVE_REPORT(rep.ID);
                        ViewMessage.addMessage(TempData, "Report has been removed.", ViewMessage.Types.Success);
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception e)
                {
                    ViewMessage.addMessage(TempData, "Unexpected error (" + (e.InnerException == null ? e.Message : e.InnerException.Message) + ").", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
            }
            ViewMessage.addMessage(TempData, "Invalid report ID.", ViewMessage.Types.Error);
            return RedirectToAction("Index");
        }

        // GET: AutomaticReports Edit report
        [AuthAuthorize(Roles = "super_admin", Claims = "automaticreports_add")]
        public ActionResult Add()
        {
            ViewBag.Title = "Automatic reports - New";
            
            List<CRM_USER> AllUsers = db.CRM_USER.ToList();
            return View(new AutomaticReportsAddViewModel { 
                Report = new REPORT(),
                Users = AllUsers
            });
        }

        // POST: AutomaticReports Add report
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "automaticreports_add")]
        public ActionResult Add(AutomaticReportsAddViewModel m, int owner)
        {
            ViewBag.Title = "Automatic reports - Add";
            CRM_USER u = db.CRM_USER.Find(owner);
            m.Report.OWNER = u;
            if (ModelState.IsValid && u != null)
            {
                m.Report.CREATE_DATE = DateTime.Now;
                try
                {
                    db.AUTOMATIC_REPORTS_P_ADD_REPORT(
                    m.Report.TITLE.ToLower(),
                    m.Report.DESCRIPTION,
                    (m.Report.ENABLED ? 1 : 0),
                    m.Report.INTERVAL,
                    DateTime.Now,
                    m.Report.TYPE,
                    m.Report.ACTION,
                    owner,
                    m.Report.EXPORT_PATH);
                    ViewMessage.addMessage(TempData, "Report created.", ViewMessage.Types.Success);
                    return RedirectToAction("Index");
                } catch (Exception e)
                {
                    ViewMessage.addMessage(TempData, "Invalid report ID (" + (e.InnerException == null ? e.Message : e.InnerException.Message) + ").", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
                
            }
            else if (u == null)
            {
                ViewMessage.addMessage(TempData, "Invalid owner ID.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            else
            {
                ViewMessage.addMessage(TempData, "Invalid data state.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
        }

        // GET: AutomaticReports Edit report
        [AuthAuthorize(Roles = "super_admin", Claims = "automaticreports_edit")]
        public ActionResult Edit(int? id)
        {
            ViewBag.Title = "Automatic reports - Edit";
            if (id != null)
            {
                try
                {
                    REPORT rep = db.REPORT.Find(id);
                    if (rep != null)
                    {
                        List<CRM_USER> AllUsers = db.CRM_USER.ToList();
                        return View(new AutomaticReportsEditViewModel {
                            Report = rep,
                            Users = AllUsers
                        });
                    }
                }
                catch (Exception e)
                {
                    ViewMessage.addMessage(TempData, "Unexpected error (" + (e.InnerException == null ? e.Message : e.InnerException.Message) + ").", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
            }
            ViewMessage.addMessage(TempData, "Invalid report ID.", ViewMessage.Types.Error);
            return RedirectToAction("Index");
        }
        
        // POST: AutomaticReports Edit report
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "automaticreports_edit")]
        public ActionResult Edit(AutomaticReportsEditViewModel m, int owner)
        {
            ViewBag.Title = "Automatic reports - Edit";
            CRM_USER u = db.CRM_USER.Find(owner);
            m.Report.OWNER = u;
            if (ModelState.IsValid && u != null)
            {
                try
                {
                    db.AUTOMATIC_REPORTS_P_UPDATE_REPORT(
                    m.Report.ID,
                    m.Report.TITLE,
                    m.Report.DESCRIPTION,
                    (m.Report.ENABLED ? 1 : 0),
                    m.Report.INTERVAL,
                    DateTime.Now,
                    m.Report.TYPE,
                    m.Report.ACTION,
                    owner,
                    m.Report.EXPORT_PATH);
                    ViewMessage.addMessage(TempData, "Report changed.", ViewMessage.Types.Success);
                    return RedirectToAction("Index");
                } catch (Exception e)
                {
                    ViewMessage.addMessage(TempData, "Unexpected error (" + (e.InnerException == null ? e.Message : e.InnerException.Message) + ").", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
            }
            else if (u == null)
            {
                ViewMessage.addMessage(TempData, "Invalid owner ID.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            else
            {
                ViewMessage.addMessage(TempData, "Invalid data state.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
        }

        // GET: AutomaticReports User access
        [AuthAuthorize(Roles = "super_admin", Claims = "automaticreports_useraccess")]
        public ActionResult UserAccess(int? id)
        {
            ViewBag.Title = "Automatic reports - User Access";
            if (id != null)
            {
                try
                {
                    REPORT rep = db.REPORT.Where(r => r.ID == id).First();
                    List<CRM_USER> allUsers = db.CRM_USER.ToList();
                    if (rep != null)
                    {
                        AutomaticReportsUserAccessViewModel model = new AutomaticReportsUserAccessViewModel
                        {
                            Report = rep,
                            AllUsers = allUsers
                        };
                        return View(model);
                    }
                }
                catch (InvalidOperationException e)
                {
                    ViewMessage.addMessage(TempData, "Invalid report ID (" + (e.InnerException == null ? e.Message : e.InnerException.Message) + ").", ViewMessage.Types.Error);
                    return RedirectToAction("Index");
                }
            }
            ViewMessage.addMessage(TempData, "Invalid report ID.", ViewMessage.Types.Warning);
            return RedirectToAction("Index");
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthAuthorize(Roles = "super_admin", Claims = "automaticreports_useraccess")]
        public ActionResult UserAccess(int reportId, long[] accessGranted)
        {
            try
            {
                List<long> grantedTo = (accessGranted == null ? new List<long>() : accessGranted.ToList());
                REPORT rep = db.REPORT.Find(reportId);
                List<CRM_USER> selectedUsers = db.CRM_USER.Where(u => grantedTo.Contains(u.ID)).ToList();
                Debug.WriteLine("selectedUsers: " + selectedUsers.Count);
                rep.ALLOWED_USERS.Clear();
                rep.ALLOWED_USERS = selectedUsers;
                Debug.WriteLine("ALLOWED_USERS: " + rep.ALLOWED_USERS.Count);
                db.SaveChanges();

                ViewMessage.addMessage(TempData, "Access changed for report " + rep.TITLE + ".", ViewMessage.Types.Success);

                return RedirectToAction("Index");
            }
            catch (InvalidOperationException e)
            {
                ViewMessage.addMessage(TempData, "Invalid report ID (" + (e.InnerException == null ? e.Message : e.InnerException.Message) + ").", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
        }

    }
}