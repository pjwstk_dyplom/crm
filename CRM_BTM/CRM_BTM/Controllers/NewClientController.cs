﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CRM_BTM.Models;
using CRM_BTM.Models.OfferModels;
using CRM_BTM.Models.ViewModels;
using CustomAuth.Authorization;
using log4net;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;

namespace CRM_BTM.Controllers
{
    [Authorize]
    public class NewClientController : Controller
    {
        private CRMConnection db = new CRMConnection();

        // GET: Index
        [AuthAuthorize(Roles = "super_admin", Claims = "client_create")]
        public ActionResult Index()
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            return View();
        }

        // AJAX: Province2County
        [AuthAuthorize(Roles = "super_admin", Claims = "client_create")]
        public ActionResult Province2County(long? id)
        {
            if (!Request.IsAjaxRequest())
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var CountyList = db.ADDRESS_COUNTY.Where(c => c.ADDRESS_PROVINCE_ID == id).ToList();
            return PartialView("PartialViews/Province2County", CountyList);
        }

        // AJAX: County2City
        [AuthAuthorize(Roles = "super_admin", Claims = "client_create")]
        public ActionResult County2City(long id)
        {
            if (!Request.IsAjaxRequest())
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var CityList = db.ADDRESS_CITY.Where(c => c.ADDRESS_COUNTY_ID == id).ToList();
            return PartialView("PartialViews/County2City", CityList);
        }

        // AJAX: City2Street
        [AuthAuthorize(Roles = "super_admin", Claims = "client_create")]
        public ActionResult City2Street(long id)
        {
            if (!Request.IsAjaxRequest())
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var StreetList = db.ADDRESS_STREET.Where(c => c.ADDRESS_CITY_ID == id).ToList();
            return PartialView("PartialViews/City2Street", StreetList);
        }

        // AJAX: Street2Building
        [AuthAuthorize(Roles = "super_admin", Claims = "client_create")]
        public ActionResult Street2Building(long id, bool OnlyWithTech = false, string FormName = "BuildingID")
        {
            if (!Request.IsAjaxRequest())
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var BuildingListQuery = db.ADDRESS_BUILDING.Where(c => c.ADDRESS_STREET_ID == id);
            if (OnlyWithTech)
                BuildingListQuery = BuildingListQuery.Where(x => x.MTM_AVAIL_TECH_ON_BUILDING.Count() > 0);
            var BuildingList = BuildingListQuery.ToList();
            if (BuildingList.Count() == 0)
                return new HttpStatusCodeResult(HttpStatusCode.NoContent);
            ViewBag.FormName = FormName;
            return PartialView("PartialViews/Street2Building", BuildingList);
        }

        // GET: PreOffer
        // Gets installation building
        [AuthAuthorize(Roles = "super_admin", Claims = "client_create")]
        public ActionResult PreOffer()
        {
            NewClientProcessModel model = (NewClientProcessModel)TempData["NewClientProcessModel"];
            if (model == null)
                model = new NewClientProcessModel();

            ViewBag.AddressProvince = new SelectList(db.ADDRESS_PROVINCE, "ID", "NAME");

            return View(model);
        }

        // POST: PreOffer
        [HttpPost]
        [AuthAuthorize(Roles = "super_admin", Claims = "client_create")]
        public ActionResult PreOffer(NewClientProcessModel model)
        {
            // Verification
            if (model == null)
            {
                ViewMessage.addMessage(TempData, "No model found.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            if (model.BuildingID == null)
            {
                ViewMessage.addMessage(TempData, "Invalid building selected.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            
            // Routing
            TempData["NewClientProcessModel"] = model;
            return RedirectToAction("OfferSelection");
        }

        // GET: OfferSelection
        // Gets available offers for this building and picks one
        [AuthAuthorize(Roles = "super_admin", Claims = "client_create")]
        public ActionResult OfferSelection()
        {
            NewClientProcessModel model = (NewClientProcessModel)TempData["NewClientProcessModel"];
            if (model == null)
            {
                ViewMessage.addMessage(TempData, "No model found.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            
            var OfferOptAvailability = new OfferOptionAvailability(db, accoID: -1 /* new client */, buildingId: (long)model.BuildingID, onlyAvailable: true);
            List<OFFER_OPTION> OfferOptions = OfferOptAvailability.OfferEvaluation.Select(x => x.Key).Where(x => x.OFFER_OPTION_PRODUCT.Count > 0).ToList();
            ViewBag.OfferList = OfferOptions;
            
            var OfferOptionIDs = OfferOptions.Select(x => x.ID).ToList();
            var PricingPeriodsDB = db.OFFER_PRICING_PERIODS_V.Where(x => OfferOptionIDs.Contains(x.OFFER_OPTION_ID)).ToList();
            OfferPricingPeriods PricingPeriods = new OfferPricingPeriods(PricingPeriodsDB);
            ViewBag.PricingPeriods = PricingPeriods;

            return View(model);
        }

        // POST: OfferSelection
        [HttpPost]
        [AuthAuthorize(Roles = "super_admin", Claims = "client_create")]
        public ActionResult OfferSelection(NewClientProcessModel model)
        {
            // Verification
            if (model == null)
            {
                ViewMessage.addMessage(TempData, "No model found.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            if (model.OfferOptionID == null)
            {
                ViewMessage.addMessage(TempData, "Invalid offer option selected.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }

            // Routing
            TempData["NewClientProcessModel"] = model;
            return RedirectToAction("AdditionalInfo");
        }

        // GET: AdditionalInfo
        // Get the rest of the client information required
        [AuthAuthorize(Roles = "super_admin", Claims = "client_create")]
        public ActionResult AdditionalInfo()
        {
            ViewBag.Messages = ViewMessage.handleTempData(TempData);
            NewClientProcessModel model = (NewClientProcessModel)TempData["NewClientProcessModel"];
            if (model == null)
            {
                ViewMessage.addMessage(TempData, "No model found.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }

            ViewBag.SegmentID = new SelectList(db.SEGMENT, "ID", "NAME", model.SegmentID);
            ViewBag.DivisionID = new SelectList(db.DIVISION, "ID", "NAME", model.DivisionID);
            
            return View(model);
        }

        // POST: AdditionalInfo
        [HttpPost]
        [AuthAuthorize(Roles = "super_admin", Claims = "client_create")]
        public ActionResult AdditionalInfo(NewClientProcessModel model)
        {
            // Verification
            if (model == null)
            {
                ViewMessage.addMessage(TempData, "No model found.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            if (String.IsNullOrEmpty(model.ClientName))
            {
                ViewMessage.addMessage(TempData, "Client name cannot be empty.", ViewMessage.Types.Error);
                TempData["NewClientProcessModel"] = model;
                return RedirectToAction("AdditionalInfo");
            }
            if (String.IsNullOrEmpty(model.ClientLastname))
            {
                ViewMessage.addMessage(TempData, "Client last name cannot be empty.", ViewMessage.Types.Error);
                TempData["NewClientProcessModel"] = model;
                return RedirectToAction("AdditionalInfo");
            }
            if (String.IsNullOrEmpty(model.ClientNIP) && String.IsNullOrEmpty(model.ClientPESEL))
            {
                ViewMessage.addMessage(TempData, "Please fill PESEL or NIP.", ViewMessage.Types.Error);
                TempData["NewClientProcessModel"] = model;
                return RedirectToAction("AdditionalInfo");
            }
            else {
                if (!String.IsNullOrEmpty(model.ClientNIP))
                {
                    var NIPCount = db.CLIENT.Where(x => x.NIP.Equals(model.ClientNIP)).Count();
                    if (NIPCount > 0)
                    {
                        ViewMessage.addMessage(TempData, "NIP already used.", ViewMessage.Types.Error);
                        TempData["NewClientProcessModel"] = model;
                        return RedirectToAction("AdditionalInfo");
                    }
                    if (model.ClientNIP.Length != 10)
                    {
                        ViewMessage.addMessage(TempData, "Invalid NIP length.", ViewMessage.Types.Error);
                        TempData["NewClientProcessModel"] = model;
                        return RedirectToAction("AdditionalInfo");
                    }
                }
                if (!String.IsNullOrEmpty(model.ClientPESEL))
                {
                    var NIPCount = db.CLIENT.Where(x => x.PESEL.Equals(model.ClientPESEL)).Count();
                    if (NIPCount > 0)
                    {
                        ViewMessage.addMessage(TempData, "PESEL already used.", ViewMessage.Types.Error);
                        TempData["NewClientProcessModel"] = model;
                        return RedirectToAction("AdditionalInfo");
                    }
                    if (model.ClientPESEL.Length != 11)
                    {
                        ViewMessage.addMessage(TempData, "Invalid PESEL length.", ViewMessage.Types.Error);
                        TempData["NewClientProcessModel"] = model;
                        return RedirectToAction("AdditionalInfo");
                    }
                }
                var Segment = db.SEGMENT.Find(model.SegmentID);
                if (Segment == null)
                {
                    ViewMessage.addMessage(TempData, "Invalid segment.", ViewMessage.Types.Error);
                    TempData["NewClientProcessModel"] = model;
                    return RedirectToAction("AdditionalInfo");
                }
                if (Segment.NAME.Equals("home") && String.IsNullOrEmpty(model.ClientPESEL))
                {
                    ViewMessage.addMessage(TempData, "HOME segment requires PESEL.", ViewMessage.Types.Error);
                    TempData["NewClientProcessModel"] = model;
                    return RedirectToAction("AdditionalInfo");
                }
                if (!Segment.NAME.Equals("home") && String.IsNullOrEmpty(model.ClientNIP))
                {
                    ViewMessage.addMessage(TempData, "Segments other than HOME require NIP.", ViewMessage.Types.Error);
                    TempData["NewClientProcessModel"] = model;
                    return RedirectToAction("AdditionalInfo");
                }
            }
            if (String.IsNullOrEmpty(model.Email))
            {
                ViewMessage.addMessage(TempData, "Client email cannot be empty.", ViewMessage.Types.Error);
                TempData["NewClientProcessModel"] = model;
                return RedirectToAction("AdditionalInfo");
            }
            if (String.IsNullOrEmpty(model.PhonePrimary))
            {
                ViewMessage.addMessage(TempData, "Client primary phone number cannot be empty.", ViewMessage.Types.Error);
                TempData["NewClientProcessModel"] = model;
                return RedirectToAction("AdditionalInfo");
            }

            // Body
            if (model.BillingAddressFromInstallation)
            {
                model.BillingBuildingID = model.BuildingID;
                model.BillingFlat = model.InstallationFlat;
            }

            // Routing
            TempData["NewClientProcessModel"] = model;
            if (model.BillingAddressFromInstallation)
                return RedirectToAction("Summary");
            else
                return RedirectToAction("BillingAddress");
        }

        // GET: BillingAddress
        // Get the billing address building if different than installation
        [AuthAuthorize(Roles = "super_admin", Claims = "client_create")]
        public ActionResult BillingAddress()
        {
            NewClientProcessModel model = (NewClientProcessModel)TempData["NewClientProcessModel"];
            if (model == null)
            {
                ViewMessage.addMessage(TempData, "No model found.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }

            ViewBag.AddressProvince = new SelectList(db.ADDRESS_PROVINCE, "ID", "NAME", model.DivisionID);

            return View(model);
        }

        // POST: BillingAddress
        [HttpPost]
        [AuthAuthorize(Roles = "super_admin", Claims = "client_create")]
        public ActionResult BillingAddress(NewClientProcessModel model)
        {
            // Verification
            if (model == null)
            {
                ViewMessage.addMessage(TempData, "No model found.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            if (model.BillingBuildingID == null)
            {
                ViewMessage.addMessage(TempData, "Invalid building selected.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }

            // Routing
            TempData["NewClientProcessModel"] = model;
            return RedirectToAction("Summary");
        }

        // GET: Summary
        [AuthAuthorize(Roles = "super_admin", Claims = "client_create")]
        public ActionResult Summary()
        {
            NewClientProcessModel model = (NewClientProcessModel)TempData["NewClientProcessModel"];
            if (model == null)
            {
                ViewMessage.addMessage(TempData, "No model found.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            var Division = db.DIVISION.Find(model.DivisionID);
            if (Division == null)
            {
                ViewMessage.addMessage(TempData, "Invalid division.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            ViewBag.DivisionName = Division.NAME;
            var Segment = db.SEGMENT.Find(model.SegmentID);
            if (Segment == null)
            {
                ViewMessage.addMessage(TempData, "Invalid segment.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            ViewBag.SegmentName = Segment.NAME;

            var OfferOption = db.OFFER_OPTION.Find(model.OfferOptionID);
            OfferOption.SetSingleOptionPricingPeriods(db);
            ViewBag.OfferOption = OfferOption;

            return View(model);
        }

        // POST: Summary
        [HttpPost]
        [AuthAuthorize(Roles = "super_admin", Claims = "client_create")]
        public ActionResult Summary(NewClientProcessModel model)
        {
            // Verification
            if (model == null)
            {
                ViewMessage.addMessage(TempData, "No model found.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            var InstallationBuilding = db.ADDRESS_BUILDING.Find(model.BuildingID);
            if (InstallationBuilding == null)
            {
                ViewMessage.addMessage(TempData, "Invalid installation building.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            var BillingBuilding = db.ADDRESS_BUILDING.Find(model.BillingBuildingID);
            if (BillingBuilding == null)
            {
                ViewMessage.addMessage(TempData, "Invalid billing building.", ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            long SaleCodeID = 0;
            try
            {
                var CurrentUser = CRM_USER.getFromIdentity(db, HttpContext.User.Identity);
                var UserStruct = db.USERS_STRUCT.Where(x => x.USER_ID == CurrentUser.ID).Where(x => x.VALID_FROM <= DateTime.Today && DateTime.Today <= x.VALID_TO).First();
                var SaleCode = UserStruct.SALE_CODE.Where(x => x.VALID_FROM <= DateTime.Today && DateTime.Today <= x.VALID_TO).First();
                SaleCodeID = SaleCode.ID;
            } catch (Exception e)
            {
                ViewMessage.addMessage(TempData, "Error resolving your sale code: " + (e.InnerException == null ? e.Message : e.InnerException.Message), ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }
            // Body
            // get installation and billing addresses
            ADDRESS InstallationAddress;
            ADDRESS BillingAddress;
            if (String.IsNullOrEmpty(model.InstallationFlat))
                InstallationAddress = InstallationBuilding.ADDRESS.Where(x => String.IsNullOrEmpty(x.FLAT)).FirstOrDefault();
            else
                InstallationAddress = InstallationBuilding.ADDRESS.Where(x => !String.IsNullOrEmpty(x.FLAT) && x.FLAT.Equals(model.InstallationFlat)).FirstOrDefault();
            if (InstallationAddress == null)
            {
                InstallationAddress = db.ADDRESS.Add(new ADDRESS() {
                    ADDRESS_BUILDING = InstallationBuilding,
                    FLAT = model.InstallationFlat,
                    INSERT_DATE = DateTime.Now
                });
                db.SaveChangesLog(HttpContext.User.Identity.Name);
            }
            if (model.BillingAddressFromInstallation)
                BillingAddress = InstallationAddress;
            else
            {
                if (String.IsNullOrEmpty(model.BillingFlat))
                    BillingAddress = BillingBuilding.ADDRESS.Where(x => String.IsNullOrEmpty(x.FLAT)).FirstOrDefault();
                else
                    BillingAddress = BillingBuilding.ADDRESS.Where(x => !String.IsNullOrEmpty(x.FLAT) && x.FLAT.Equals(model.BillingFlat)).FirstOrDefault();
                if (BillingAddress == null)
                {
                    BillingAddress = db.ADDRESS.Add(new ADDRESS()
                    {
                        ADDRESS_BUILDING = BillingBuilding,
                        FLAT = model.BillingFlat,
                        INSERT_DATE = DateTime.Now
                    });
                    db.SaveChangesLog(HttpContext.User.Identity.Name);
                }
            }
            var OfferOption = db.OFFER_OPTION.Find(model.OfferOptionID);
            if (OfferOption == null)
            {
                ViewMessage.addMessage(TempData, "Invalid offer, not found: " + model.OfferOptionID, ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }

            // execute ora command to create new client
            
            OracleConnection DBConnection = new OracleConnection(db.Database.Connection.ConnectionString);
            if (DBConnection.State != ConnectionState.Open)
                DBConnection.Open();
            OracleCommand OraCommand = new OracleCommand("CLIENT_OFFER_PCK.NEW_CLIENT_OFFER", DBConnection);
            OraCommand.CommandType = System.Data.CommandType.StoredProcedure;

            OraCommand.Parameters.Add("C_NAME", OracleDbType.Varchar2).Value = model.ClientName;
            OraCommand.Parameters.Add("C_LASTNAME", OracleDbType.Varchar2).Value = model.ClientLastname;
            OraCommand.Parameters.Add("C_PESEL", OracleDbType.Varchar2).Value = model.ClientPESEL;
            OraCommand.Parameters.Add("C_NIP", OracleDbType.Varchar2).Value = model.ClientNIP;
            OraCommand.Parameters.Add("C_SEGMENT_ID", OracleDbType.Long).Value = model.SegmentID;
            OraCommand.Parameters.Add("C_EMAIL", OracleDbType.Varchar2).Value = model.Email;
            OraCommand.Parameters.Add("C_PHONE_PRIMARY", OracleDbType.Varchar2).Value = model.PhonePrimary;
            OraCommand.Parameters.Add("C_PHONE_SECONDARY", OracleDbType.Varchar2).Value = model.PhoneSecondary;

            OraCommand.Parameters.Add("A_BILLING_DAY", OracleDbType.Long).Value = model.BillingDay;
            OraCommand.Parameters.Add("A_DIVISION_ID", OracleDbType.Long).Value = model.DivisionID;
            OraCommand.Parameters.Add("A_INSTALL_ADDRESS_ID", OracleDbType.Long).Value = InstallationAddress.ID;
            OraCommand.Parameters.Add("A_BILLING_ADRESS_ID", OracleDbType.Long).Value = BillingAddress.ID;

            OraCommand.Parameters.Add("CN_SIGN_DATE", OracleDbType.Date).Value = DateTime.Now;
            OraCommand.Parameters.Add("CN_START_DATE", OracleDbType.Date).Value = DateTime.Now;
            OraCommand.Parameters.Add("CN_END_DATE", OracleDbType.Date).Value = DateTime.Now.AddYears(100);
            OraCommand.Parameters.Add("CN_SALE_CODE_ID", OracleDbType.Long).Value = SaleCodeID;

            OraCommand.Parameters.Add("O_OFFER_OPTION_ID", OracleDbType.Long).Value = model.OfferOptionID;
            OraCommand.Parameters.Add("O_SALE_DATE", OracleDbType.Date).Value = DateTime.Now;

            OracleParameter OUT_CLIENT_ID = new OracleParameter();
            OUT_CLIENT_ID.ParameterName = "OUT_CLIENT_ID";
            OUT_CLIENT_ID.OracleDbType = OracleDbType.Int64;
            OUT_CLIENT_ID.Direction = System.Data.ParameterDirection.Output;
            OraCommand.Parameters.Add(OUT_CLIENT_ID);

            OraCommand.Prepare();

            try { 
                OraCommand.ExecuteNonQuery();
            } catch (Exception e)
            {
                ViewMessage.addMessage(TempData, "Error encountered: " + (e.InnerException == null ? e.Message : e.InnerException.Message), ViewMessage.Types.Error);
                return RedirectToAction("Index");
            }

            // Routing
            ViewMessage.addMessage(TempData, "New client added", ViewMessage.Types.Success);
            return RedirectToAction("Details", "Client", new { id = OUT_CLIENT_ID.Value });
        }


    }
}

 