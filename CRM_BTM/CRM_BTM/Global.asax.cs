﻿using CRM_BTM.Models;
using CRM_BTM.Scheduler;
using CustomAuth.Authentication;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace CRM_BTM
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ScheduledTasks.Start();
            log4net.Config.XmlConfigurator.Configure();
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            //Debug.WriteLine("Application_PostAuthenticateRequest trying to get a cookie");
            HttpCookie authCookie = Request.Cookies["UserData"];
            if (authCookie != null)
            {
                //Debug.WriteLine("Cookie is not null!");
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);

                var serializeModel = JsonConvert.DeserializeObject<AuthSerializeModel>(authTicket.UserData);

                AuthPrincipal principal = new AuthPrincipal(authTicket.Name);

                principal.UserId = serializeModel.UserId;
                principal.FirstName = serializeModel.FirstName;
                principal.LastName = serializeModel.LastName;
                principal.Email = serializeModel.Email;
                principal.Roles = serializeModel.RoleName.ToArray<string>();
                principal.Claims = serializeModel.ClaimNames.ToArray<string>();

                HttpContext.Current.User = principal;
                //Debug.WriteLine("Current users name = " + principal.FirstName);
               // Debug.WriteLine("Current users role = " + principal.Roles.First<string>());
            }
           //else Debug.WriteLine("Cookie is empty");

        }
    }

    

}
