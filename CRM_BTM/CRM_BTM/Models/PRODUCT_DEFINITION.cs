//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CRM_BTM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PRODUCT_DEFINITION
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PRODUCT_DEFINITION()
        {
            this.MTM_ATTRDEF_PRODDEF = new HashSet<MTM_ATTRDEF_PRODDEF>();
            this.PRODUCT = new HashSet<PRODUCT>();
            this.OFFER_OPTION_PRODUCT = new HashSet<OFFER_OPTION_PRODUCT>();
        }
    
        public long ID { get; set; }
        public string DESCRIPTION { get; set; }
        public long PRODUCT_CATEGORY_ID { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MTM_ATTRDEF_PRODDEF> MTM_ATTRDEF_PRODDEF { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRODUCT> PRODUCT { get; set; }
        public virtual PRODUCT_CATEGORY PRODUCT_CATEGORY { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OFFER_OPTION_PRODUCT> OFFER_OPTION_PRODUCT { get; set; }
    }
}
