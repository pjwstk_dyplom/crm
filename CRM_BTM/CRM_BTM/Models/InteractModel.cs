﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using PagedList;

namespace CRM_BTM.Models
{
    public class InteractModel
    {
        public IPagedList<INTERACT_INFO_V> InteractInfos { get; set; }
        public IEnumerable<INTERACT_TYPE> InteractTypes { get; set; }
        public IEnumerable<INTERACT_DIRECTION> InteractDirections { get; set; } 
        public IEnumerable<INTERACT_MEDIUM> InteractMediums { get; set; }
        
        public long InteractTypeId { get; set; }
        public long InteractDirectionId { get; set; }
        public long InteractMediumId { get; set; }

        public Nullable<long> ClientId { get; set; }
        public Nullable<long> TaskId { get; set; }

        public string ClientFName { get; set; }
        public string PeselNip { get; set; }

        public int PageCount { get; set; }
        public int PageNumber { get; set; }

        public  bool clearForm { get; set; }

        [Display(Name = "Attribute valid from")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<DateTime> dateFrom { get; set; }

        [Display(Name = "Attribute valid from")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<DateTime> dateTo { get; set; }

        public InteractModel()
        {
            InteractInfos = new List<INTERACT_INFO_V>().ToPagedList(1,1);
            InteractTypes = new List<INTERACT_TYPE>();
            InteractDirections = new List<INTERACT_DIRECTION>();
            InteractMediums = new List<INTERACT_MEDIUM>();

            InteractTypeId = 0;
            InteractDirectionId = 0;
            InteractMediumId = 0;

            dateFrom = null;
            dateTo = null;

            clearForm = false;

            PageCount = 1;
            PageNumber = 1;

            
        }
    }
}