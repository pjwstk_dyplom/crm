﻿using System.Collections.Generic;

namespace CustomAuth.Authentication
{
    public class AuthSerializeModel
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<string> RoleName { get; set; }
        public List<string> ClaimNames { get; set; }
    }
}

