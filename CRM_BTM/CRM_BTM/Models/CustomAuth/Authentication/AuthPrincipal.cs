﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Principal;
using System.Web;
using CustomAuth.DataAccess;


namespace CustomAuth.Authentication
{
    public class AuthPrincipal : IPrincipal
    {
        #region Identity Properties  

        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string[] Roles { get; set; }
        public string[] Claims { get; set; }
        private static List<Role> RolesDict = new List<Role>();
        private static List<CRMClaim> ClaimsDict = new List<CRMClaim>();
        #endregion
        private static AuthDbContext db = new AuthDbContext();

        public static void RefreshAuthDict()
        {
            RolesDict = db.Roles.ToList();
            ClaimsDict = db.Claims.ToList();
        }

        public IIdentity Identity
        {
            get; private set;
        }

        public bool IsInRole(string role)
        {
            var roleID = RolesDict.Where(r => role.Contains(r.RoleName)).Select(r => r.RoleId.ToString()).ToList();
           
            if (Roles.Any(r => roleID.Contains(r)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool HasClaim(string claimsList) {

            var claimsId = ClaimsDict.Where(c => claimsList.Contains(c.ClaimName())).Select(c => c.ClaimId.ToString()).ToList();

            if (claimsId != null && Claims != null)
            {
                foreach (string claim in claimsId)
                {
                    if (Claims.Any(c => c.ToString().Equals(claim)))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public AuthPrincipal(string username)
        {
            if (RolesDict == null || ClaimsDict == null)
            {
                RefreshAuthDict();
            }
            Identity = new GenericIdentity(username);
        }
    }
}
