﻿using System;
using CustomAuth.DataAccess;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Diagnostics;
using Microsoft.EntityFrameworkCore;

namespace CustomAuth.Authentication
{
    public class AuthMembershipUser : MembershipUser
    {
        #region User Properties  

        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public ICollection<Role> Roles { get; set; }
        public ICollection<CRMClaim> Claims { get; set; }

        #endregion

        public AuthMembershipUser(User user) : base("AuthMembershipProvider", user.Username, user.UserId, user.Email, string.Empty, string.Empty, true, false, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now)
        {
            UserId = user.UserId;
            FirstName = user.FirstName;
            LastName = user.LastName;
            //Email = user.Email;
            Roles = user.Roles;
            Claims = user.Claims
                         .Concat(Roles.SelectMany(r => r.Claims.ToList()))
                         .Distinct()
                         .ToList();

            //Debug.WriteLine("(AuthMembershipUser) RoleClaims:  " + String.Join(",", Roles.Select(r => r.Claims.SelectMany(c => c.ClaimName())).ToList()));
            //Debug.WriteLine("(AuthMembershipUser) Claims:      " + String.Join(",", Claims.Select(c => c.ClaimName()).ToList()));
            Debug.WriteLine("(AuthMembershipUser) Email:      " + this.Email);
        }

    }
}
