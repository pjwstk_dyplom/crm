﻿using CustomAuth.Authentication;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CustomAuth.Authorization
{
    public class AuthAuthorizeAttribute : AuthorizeAttribute
    {
        public string Claims { get; set; }
        

        protected virtual AuthPrincipal CurrentUser
        {
            get { return HttpContext.Current.User as AuthPrincipal; }
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            Debug.WriteLine("AuthorizeCore:" + CurrentUser);
            if (CurrentUser != null)
            {
                if (CurrentUser.IsInRole(Roles) || CurrentUser.HasClaim(Claims))
                {
                    return true;
                }
            }
            return false;

            //return ((CurrentUser != null && !CurrentUser.IsInRole(Roles)) || CurrentUser == null) ? false : true;
        }
        
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            RedirectToRouteResult routeData = null;
            Debug.WriteLine("HandleUnauthorizedRequest");

            if (CurrentUser == null)
            {
                routeData = new RedirectToRouteResult
                    (new System.Web.Routing.RouteValueDictionary
                    (new
                    {
                        controller = "Account",
                        area = "",
                        action = "Login",
                    }
                    ));
            }
            else
            {
                routeData = new RedirectToRouteResult
                (new System.Web.Routing.RouteValueDictionary
                 (new
                 {
                     controller = "Error",
                     area = "",
                     action = "AccessDenied"
                 }
                 ));
            }

            filterContext.Result = routeData;
        }

    }
}
