﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using CustomAuth.DataAccess;

namespace CustomAuth.Models
{
    public class ManageUserModel
    {
        public IEnumerable<Role> RoleModel { get; set; }
        public IEnumerable<CRMClaim> ClaimsModel { get; set; }
        public IList<string> ClaimsOut { get; set; }
        public IList<string> ModelOut { get; set; }
        public  User UserModel { get; set; }

        [Required]
        [MinLength(8)]
        public string UserPassword { get; set; }
        public string UserHrId { get; set; }
        public IEnumerable<int> RolesClaims { get; set; }



        public ManageUserModel()
        {

            ModelOut = new List<string>();
            RoleModel = new List<Role>();
            ClaimsOut = new List<string>();
            ClaimsModel = new List<CRMClaim>();
            UserModel = new User();
            UserPassword = "";
            UserHrId = "";
            RolesClaims = new List<int>();
        }

    }


  
}