﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CustomAuth.DataAccess;

namespace CustomAuth.Models
{


    public class ManageRoleModel
    {
        public IEnumerable<Role> RoleModel { get; set; }
        public IEnumerable<CRMClaim> ClaimModel { get; set; }
        public List<String> ClaimsList { get; set; }
        public IEnumerable<User> UserModel { get; set; }
        public Role ActRole { get; set; }
        public IEnumerable<User> existingUserModel { get; set; }
        public ManageRoleModel()
        {
            RoleModel = new List<Role>();
            ClaimModel = new List<CRMClaim>();
            ClaimsList = new List<String>();
            UserModel = new List<User>();
            existingUserModel = new List<User>();
            ActRole = new Role();

        }

    }
}