﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CustomAuth.DataAccess
{
    public class CRMClaim
    {
        private const char SEPARATOR = '_';

        [Key]
        public int ClaimId { get; set; }

        [Required]
        public string Category { get; set; }

        [Required]
        public string Operation { get; set; }

        [Required]
        public string Description { get; set; }

        public string ClaimName()
        {
            return String.Concat(Category, SEPARATOR, Operation);
        } 

        public override string ToString() {
            return this.ClaimName();
        }

        public virtual ICollection<Role> Roles { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}