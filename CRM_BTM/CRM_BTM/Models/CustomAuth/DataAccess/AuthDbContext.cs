﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace CustomAuth.DataAccess
{
    public class AuthDbContext : DbContext
    {
        public AuthDbContext()
            : base("IdentityConnection")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasDefaultSchema("IDENTITY");

            modelBuilder.Entity<User>().ToTable("USERS");
            modelBuilder.Entity<Role>().ToTable("ROLES");
            modelBuilder.Entity<CRMClaim>().ToTable("CLAIMS");
            modelBuilder.Entity<CRMClaim>().HasKey<int>(c => c.ClaimId);

            modelBuilder.Entity<User>().Property(u => u.Username).IsUnicode(false).HasMaxLength(100);
            modelBuilder.Entity<User>().Property(u => u.FirstName).IsUnicode(false).HasMaxLength(100);
            modelBuilder.Entity<User>().Property(u => u.LastName).IsUnicode(false).HasMaxLength(100);
            modelBuilder.Entity<User>().Property(u => u.Email).IsUnicode(false).HasMaxLength(100);
            modelBuilder.Entity<User>().Property(u => u.Password).IsUnicode(false).HasMaxLength(4000);
            modelBuilder.Entity<Role>().Property(r => r.RoleName).IsUnicode(false).HasMaxLength(100);
            modelBuilder.Entity<CRMClaim>().Property(c => c.Category).IsUnicode(false).HasMaxLength(100);
            modelBuilder.Entity<CRMClaim>().Property(c => c.Operation).IsUnicode(false).HasMaxLength(100);
            modelBuilder.Entity<CRMClaim>().Property(c => c.Description).IsUnicode(false).HasMaxLength(4000);

            modelBuilder.Entity<User>()
                .HasMany(u => u.Roles)
                .WithMany(r => r.Users)
                .Map(m =>
                {
                    m.ToTable("USERROLES");
                    m.MapLeftKey("UserId");
                    m.MapRightKey("RoleId");
                });


            modelBuilder.Entity<CRMClaim>()
                .HasMany(c => c.Roles)
                .WithMany(r => r.Claims)
                .Map(m =>
                {
                    m.ToTable("ROLECLAIMS");
                    m.MapLeftKey("ClaimId");
                    m.MapRightKey("RoleId");
                });

            modelBuilder.Entity<CRMClaim>()
                .HasMany(c => c.Users)
                .WithMany(u => u.Claims)
                .Map(m =>
                {
                    m.ToTable("USERCLAIMS");
                    m.MapLeftKey("ClaimId");
                    m.MapRightKey("UserId");
                });

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<CRMClaim> Claims { get; set; }
    }
}
