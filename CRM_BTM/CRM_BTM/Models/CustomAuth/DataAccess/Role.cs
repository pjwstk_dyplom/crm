﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CustomAuth.DataAccess
{
    public class Role
    {
        public int RoleId { get; set; }

        [Required]
        [MinLength(5)]
        public string RoleName { get; set; }

        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<CRMClaim> Claims { get; set; }
    }
}
