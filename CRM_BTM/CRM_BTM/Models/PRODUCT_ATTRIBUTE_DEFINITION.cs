//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CRM_BTM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PRODUCT_ATTRIBUTE_DEFINITION
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PRODUCT_ATTRIBUTE_DEFINITION()
        {
            this.MTM_ATTRDEF_PRODDEF = new HashSet<MTM_ATTRDEF_PRODDEF>();
            this.OFFER_OPT_PROD_FORCED_ATTR = new HashSet<OFFER_OPT_PROD_FORCED_ATTR>();
            this.PRODUCT_ATTRIBUTE = new HashSet<PRODUCT_ATTRIBUTE>();
            this.PRODUCT_ATTRIBUTE_VALUE_LIST = new HashSet<PRODUCT_ATTRIBUTE_VALUE_LIST>();
        }
    
        public long ID { get; set; }
        public string PARAM_TYPE { get; set; }
        public string DESCRIPTION { get; set; }
        public string FULL_DESCRIPTION { get; set; }
        public string REGEXP { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MTM_ATTRDEF_PRODDEF> MTM_ATTRDEF_PRODDEF { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OFFER_OPT_PROD_FORCED_ATTR> OFFER_OPT_PROD_FORCED_ATTR { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRODUCT_ATTRIBUTE> PRODUCT_ATTRIBUTE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRODUCT_ATTRIBUTE_VALUE_LIST> PRODUCT_ATTRIBUTE_VALUE_LIST { get; set; }
    }
}
