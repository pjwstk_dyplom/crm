//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CRM_BTM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TECHNOLOGY
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TECHNOLOGY()
        {
            this.MTM_AVAIL_TECH_ON_BUILDING = new HashSet<MTM_AVAIL_TECH_ON_BUILDING>();
            this.PRODUCT = new HashSet<PRODUCT>();
            this.OFFER_OPTION = new HashSet<OFFER_OPTION>();
        }
    
        public long ID { get; set; }
        public string DESCRIPTION { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MTM_AVAIL_TECH_ON_BUILDING> MTM_AVAIL_TECH_ON_BUILDING { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRODUCT> PRODUCT { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OFFER_OPTION> OFFER_OPTION { get; set; }
    }
}
