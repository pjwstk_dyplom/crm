﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CRM_BTM.Models
{
    public class ClientsMetadata
    {
        [MaxLength(40)]
        [MinLength(2)]
        [Required]
        [Display(Name = "First name")]
        public string NAME;

        [MaxLength(60)]
        [MinLength(2)]
        [Required]
        [Display(Name ="Last name")]
        public string LASTNAME;

        
        [RegularExpression(@"^\d{11}$",
                ErrorMessage = "PESEL should be length of 11 digits")]
        [Display(Name = "Pesel number")]
        public string PESEL;

        [RegularExpression(@"^\d{10}$",
                ErrorMessage = "NIP should be length of 10 digits")]
        [Display(Name = "NIP number")]
        public string NIP;

        [EmailAddress]
        [Display(Name ="E-mail address")]
        [MaxLength(50)]
        [Required]
        public string EMAIL;

        [Display(Name = "Client ID")]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage ="Client ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name = "Segment ID")]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Segment ID should be length between 1-12 digits")]
        public string SEGMENT_ID;

        [Display(Name = "Phone number")]
        [MaxLength(20)]
        [Required]
        public string PHONE_PRIMARY;

        [Display(Name = "2nd Phone number")]
        [MaxLength(20)]
        public string PHONE_SECONDARY;

        [Display(Name = "Client name")]
        public string fullName;
    }

    public class AccountsMetadata
    {
        [Display(Name = "Account ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage ="Account ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name = "Client ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Client ID should be length between 1-12 digits")]
        public string CLIENT_ID;

        [Display(Name = "Account creation date")]
        [Timestamp]
        [Required]
        public string CREATION_DATE;

        [Display(Name = "Billing day")]
        [Required]
        [RegularExpression(@"^((1?\d)|(2(0|1|2|3|4|5|6|7|8)))$",
                ErrorMessage ="Billing day should be smaller than 29")]      
        public string BILLING_DAY;

        [Display(Name = "Division ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Division ID should be length between 1-12 digits")]
        public string DIVISION_ID;

        [Display(Name = "Install address ID")]
        [Required]
        [RegularExpression(@"^\d{1,20}$",
                ErrorMessage = "Install address ID should be length between 1-12 digits")]
        public string INSTALL_ADDRESS_ID;

        [Display(Name = "Billing address ID")]
        [Required]
        [RegularExpression(@"^\d{1,20}$",
                ErrorMessage = "Billing address ID should be length between 1-12 digits")]
        public string BILLING_ADDRESS_ID;

    }


    public class SegmentMetadata
    {
        [Display(Name = "Sgement ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Sgement ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name ="Segment")]
        [Required]
        [RegularExpression(@"^([a-z]|\d){1,40}$",
                ErrorMessage = "Segment name should be length between 1-40  only lowercase characters")]
        public string NAME;
    }

    public class DivisionMetadata
    {
        [Display(Name = "Division ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Division ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name = "Division")]
        [Required]
        [MaxLength(40)]
        [RegularExpression(@"^([a-z 0-9\\._\\-])+$",
                ErrorMessage = "Division name should be only lowercase characters")]
        public string NAME;
    }

    public class ContractMetadata
    {
        [Display(Name ="Contract ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Contract ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name ="Contract sign date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required]
        [DataType(DataType.Date)]
        public DateTime SIGN_DATE;

        [Display(Name ="Contract start date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required]
        [DataType(DataType.Date)]
        public DateTime START_DATE;

        [Display(Name ="Contract end date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required]
        [DataType(DataType.Date)]
        public DateTime END_DATE;

        [Display(Name ="Account ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Account ID should be length between 1-12 digits")]
        public string ACCOUNT_ID;

        [Display(Name = "Contract number")]
        public string CONTRACT_NUMBER;


    }

    public class InteractMetadata
    {
        [Display(Name = "Interaction ID")]
        public string ID;

        [Display(Name = "Interaction create date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required]
        [DataType(DataType.Date)]
        public DateTime CREATION_DATE;

        [Display(Name = "Account ID")]
        [Required]
        public string ACCOUNT_ID;

        [Display(Name = "Type ID")]
        [Required]
        public string INTERACT_TYPE_ID;

        [Display(Name = "Direction ID")]
        [Required]
        public string INTERACT_DIRECTION_ID;

        [Display(Name = "Medium ID")]
        [Required]
        public string INTERACT_MEDIUM_ID;

        [Display(Name = "User ID")]
        [Required]
        public string USER_ID;

        [Display(Name = "Interaction description")]
        [MaxLength(4000)]
        public string DESCRIPTION;

        [Display(Name = "Task ID")]
        public string TASKS_ID;
    }

    public class Interact_typeMetadata
    {
        [Display(Name = "Interaction type ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Interaction type ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name = "Interaction type")]
        [Required]
        [RegularExpression(@"^([a-z]|\d){1,50}$",
                ErrorMessage = "Interaction type name should be length between 1-40 only lowercase characters")]
        public string NAME;
    }

    public class Interact_directionMetadata
    {
        [Display(Name = "Direction type ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Direction type ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name = "Direction type")]
        [Required]
        [RegularExpression(@"^([a-z]|\d){1,40}$",
                ErrorMessage = "Direction type name should be length between 1-40 only lowercase characters")]
        public string NAME;
    }

    public class Interact_mediumMetadata
    {
        [Display(Name = "Medium type ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Medium type ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name = "Medium type")]
        [Required]
        [RegularExpression(@"^([a-z]|\d){1,50}$",
                ErrorMessage = "Medium type name should be length between 1-40 only lowercase characters")]
        public string NAME;
    }


    public class InvoiceMetadata   {

        [Display(Name = "Invoice ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Invoice ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name = "Invoice number")]
        [Required]
        [MaxLength(50)]
        public string INVOICE_NUMBER;

        [Display(Name = "Total amount")]
        [Required]
        public string TOTAL;

        [Display(Name = "Creation date")]
        [Required]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public string CREATION_DATE;

        [Display(Name = "Send date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public string SEND_DATE;

        [Display(Name ="Due date")]
        [Required]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public string DUE_DATE;

        [Display(Name ="Payment date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public string PAYMENT_DATE;

        [Display(Name ="Paid amount")]
        [RegularExpression(@"^\d+(,\d\d)?$",
                ErrorMessage ="Paid amount should be in DD,DD format")]
        public string PAID_AMOUNT;

        [Display(Name = "Invoice type ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Invoice type ID should be length between 1-12 digits")]
        public string INVOICE_TYPE_ID;

        [Display(Name = "Invoice status ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Invoice status ID should be length between 1-12 digits")]
        public string INVOICE_STATUS_ID;

        [Display(Name = "Account ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Account ID should be length between 1-12 digits")]
        public string ACCOUNT_ID;

        [Display(Name = "Parent invoice ID")]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Parent invoice ID should be length between 1-12 digits")]
        public string PARENT_INVOICE;


    }

    public class Invoice_other_itemsMetadata
    {
        [Display(Name = "Invoice other item ID")]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Invoice other item ID should be length between 1-12 digits")]
        [Required]
        public string ID;

        [Display(Name = "Invoice ID")]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Invoice ID should be length between 1-12 digits")]
        [Required]
        public string INVOICE_ID;

        [Display(Name = "Invoice other item amount")]
        [RegularExpression(@"^-?\d+(,\d\d)?$",
             ErrorMessage = "Invoice other item amount should be in DD,DD format")]
        [Required]
        public string AMOUNT;

        [Display(Name = "Invoice other item description ID")]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Invoice other item description ID should be length between 1-12 digits")]
        [Required]
        public string INVOICE_OTHER_ITEMS_DESC_ID;
    }


    public class Invoice_other_items_descMetadata
    {
        [Display(Name = "Other item description ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Other item description ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name = "Other item description")]
        [Required]
        [MaxLength(150)]
        public string DESCRIPTION;
    }

    public class Invoice_typeMetadata
    {
        [Display(Name = "Invoice type ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Invoice type ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name = "Invoice type")]
        [Required]
        [MaxLength(20)]
        [RegularExpression(@"^([a-z0-9 _\\-])+$",
        ErrorMessage = "Invoice type name should be only lowercase characters")]
        public string NAME;
    }

    public class Invoice_statusMetadata
    {
        [Display(Name = "Invoice status ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Invoice status ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name = "Invoice status")]
        [Required]
        [RegularExpression(@"^([a-z ]|\d){1,20}$",
        ErrorMessage = "Invoice status name should be length between 1-20 only lowercase characters")]
        public string NAME;
    }

    public class mtm_invoice_productMetadata
    {
        [Display(Name = "Invoice ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Invoice ID should be length between 1-12 digits")]
        public string INVOICE_ID;

        [Display(Name = "Product ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Product ID should be length between 1-12 digits")]
        public string PRODUCT_ID;

        [Display(Name = "Amount")]
        [RegularExpression(@"^-?\d+(,\d\d)?$",
                ErrorMessage = "Amount should be in DD,DD format")]
        [Required]
        public string AMOUNT;
    }

    public class productMetadata
    {

        [Display(Name = "Product ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Product ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name = "Product definition ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Product definition ID should be length between 1-12 digits")]
        public string PRODUCT_DEFINITION_ID;

        [Display(Name = "Product activation date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime ACTIVATION_DATE;

        [Display(Name = "Product deactivation date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime DEACTIVATION_DATE;

        [Display(Name = "Product status ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Product status ID should be length between 1-12 digits")]
        public string PRODUCT_STATUS_ID;

        [Display(Name = "Parent product ID")]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Parent product ID should be length between 1-12 digits")]
        public string PARENT_PRODUCT_ID;

        [Display(Name = "Account ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Account ID should be length between 1-12 digits")]
        public string ACCOUNT_ID;

        [Display(Name = "Device ID")]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Device ID should be length between 1-12 digits")]
        public string DEVICE_ID;

        [Display(Name = "Technology ID")]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Technology ID should be length between 1-12 digits")]
        public string TECHNOLOGY_ID;
    }


    public class Product_statusMetadata
    {
        [Display(Name = "Product status ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Invoice type ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name = "Product status")]
        [Required]
        [RegularExpression(@"^([a-z]|\d){1,20}$",
        ErrorMessage = "Product status name should be length between 1-20 only lowercase characters")]
        public string NAME;
    }

    public class product_attributeMetadata
    {
        [Display(Name = "Product attribute ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Product attribute ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name = "Product status ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Invoice type ID should be length between 1-12 digits")]
        public string PRODUCT_ID;

        [Display(Name = "Attribute definition ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Attribute definition ID should be length between 1-12 digits")]
        public string PRODUCT_ATTR_DEF_ID;

        [Display(Name ="Product attribute value")]
        [MaxLength(255)]
        public string VALUE;
    }

    public class product_attribute_definition
    {
        [Display(Name = "Attribute definition ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Attribute definition ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name ="Attribute type")]
        [RegularExpression(@"^R|A|S$",
                ErrorMessage ="Attriubute type should be \"R\" for regular expression), \"S\" for strict- dropdown list or \"A\" for accepting any value")]
        public string PARAM_TYPE;

        [Display(Name = "Attribute description")]
        [MaxLength(255)]
        public string DESCRIPTION;

        [Display(Name ="Full attribute description")]
        [MaxLength(255)]
        public string FULL_DESCRIPTION;

        [Display(Name ="Attribute definition regex")]
        [MaxLength(255)]
        public string REGEXP;
        
    }

    public class product_attribute_value_list
    {

        [Display(Name = "Value list item ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "List item ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name = "Attribute definition ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Attribute definition ID should be length between 1-12 digits")]
        public string PRODUCT_ATTR_DEF_ID;

        [Display(Name ="Value list item")]
        [Required]
        [MaxLength(50)]
       public string VALUE;
    }

    public class mtm_attrdef_proddefMetadata
    {
        [Display(Name ="MTM attrdef to prod")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "MTM attribure definition to product ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name = "Product definition ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Product definition ID should be length between 1-12 digits")]
        public string PRODUCT_DEFINITION_ID;

        [Display(Name = "Attribute definiton ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Attribute definiton ID should be length between 1-12 digits")]
        public string PROD_ATTR_DEF_ID;

        [Display(Name = "Attribute valid from")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required]
        [Timestamp]
        public string VALID_FROM;

        [Display(Name ="Attribute valid to")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required]
        [Timestamp]
        public string VALID_TO;

        [Display(Name ="Is required")]
        [Required]
        [RegularExpression(@"^Y|N$", 
                ErrorMessage ="Is required should be \"Y\" (YES) or \"N\" (NO)")]
        public string IS_REQUIRED;

    }

    public class product_definitionMetadata 
    {
        [Display(Name = "Product definition ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Product definition ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name = "Product definition")]
        [Required]
        [MaxLength(150)]
        public string DESCRIPTION;

        [Display(Name = "Product category ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Product category ID should be length between 1-12 digits")]
        public string PRODUCT_CATEGORY_ID;
    }

    public  class product_categoryMetadata  
    {
        [Display(Name = "Product category ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Product category ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name ="Product category")]
        [MaxLength(50)]
        [Required]
        public string DESCRIPTION;
    }

    public class mtm_offer_productMetadata
    {
        [Display(Name = "MTM offer to product ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
        ErrorMessage = "MTM offer to product ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name = "Product ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
        ErrorMessage = "Product ID should be length between 1-12 digits")]
        public string PRODUCT_ID;

        [Display(Name = "Offer ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
        ErrorMessage = "Offer ID should be length between 1-12 digits")]
        public string OFFER_ID;

        [Display(Name ="Valid from date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required]
        [DataType(DataType.Date)]
        public string VALID_FROM;

        [Display(Name = "Valid to date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required]
        [DataType(DataType.Date)]
        public string VALID_TO;

    }

    public class offerMetadata
    {
        [Display(Name = "Offer ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Offer ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name = "Account ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Account ID should be length between 1-12 digits")]
        public string ACCOUNT_ID;

        [Display(Name = "Sale code ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Sale code ID should be length between 1-12 digits")]
        public string SALE_CODE_ID;

        [Display(Name = "Sale date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required]
        [DataType(DataType.Date)]
        public DateTime SALE_DATE;

        [Display(Name = "Offer option ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Offer option ID should be length between 1-12 digits")]
        public string OFFER_OPTION_ID;

    }

    public class offer_optionMetada
    {
        [Display(Name = "Offer option ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
        ErrorMessage = "Offer option ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name = "Offer definition ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
        ErrorMessage = "Offer definition ID should be length between 1-12 digits")]
        public string OFFER_DEFINITION_ID;

        [Display(Name = "Offer option invoice name")]
        [Required]
        [MaxLength(50)]
        public string INVOICE_NAME;

        [Display(Name = "Offer option internal name")]
        [Required]
        [MaxLength(150)]
        public string INTERNAL_NAME;

        [Display(Name = "Offer option description")]
        [Required]
        [MaxLength(400)]
        public string DESCRIPTION;

        [Display(Name = "Valid from date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required]
        [DataType(DataType.Date)]
        public DateTime VALID_FROM;

        [Display(Name = "Valid to date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required]
        [DataType(DataType.Date)]
        public DateTime VALID_TO;
    }

    public class offer_definitionMetada
    {
        [Display(Name = "Offer definition ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
        ErrorMessage = "Offer definition ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name = "Offer definition invoice name")]
        [Required]
        [MaxLength(50)]
        public string INVOICE_NAME;

        [Display(Name = "Offer definition internal name")]
        [Required]
        [MaxLength(100)]
        public string INTERNAL_NAME;

        [Display(Name = "Valid from date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required]
        [DataType(DataType.Date)]
        public DateTime VALID_FROM;

        [Display(Name = "Valid to date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required]
        [DataType(DataType.Date)]
        public DateTime VALID_TO;
    }

    public class mtm_offer_opt2categoryMetadata
    {
        [Display(Name = "Offer category ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
        ErrorMessage = "Offer category ID should be length between 1-12 digits")]
        public string OFFER_CATEGORY_ID;

        [Display(Name = "Offer option ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
        ErrorMessage = "Offer option ID should be length between 1-12 digits")]
        public string OFFER_OPTION_ID;
    }

    public class offer_categoryMetadata
    {
        [Display(Name = "Offer category ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
        ErrorMessage = "Offer category ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name ="Offer category description ")]
        [Required]
        [MaxLength(150)]
        public string DESCRIPTION;

        [Display(Name = "Offer category full description ")]
        [Required]
        [MaxLength(4000)]
        public string FULL_DESCRIPTION;
    }

    public class mtm_offer_requirementMetadata
    {
        [Display(Name = "MTM offer requiremnet ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
ErrorMessage = "MTM offer requiremnet ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name = "Offer definition ID")]
        [RegularExpression(@"^\d{1,12}$",
ErrorMessage = "Offer definition ID should be length between 1-12 digits")]
        public string OFFER_DEFINITION_ID;

        [Display(Name = "Offer option ID")]
        [RegularExpression(@"^\d{1,12}$",
ErrorMessage = "Offer option ID should be length between 1-12 digits")]
        public string OFFER_OPTION_ID;

        [Display(Name = "Offer requirement definition ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
ErrorMessage = "Offer requirement definition ID should be length between 1-12 digits")]
        public string OFFER_REQUIREMENT_DEF_ID;

        [Display(Name ="Requirement Value")]
        [Required]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public string VALUE;

        [Display(Name ="Relation")]
        [RegularExpression(@"^=|<|>|!$",
            ErrorMessage = "Offer requirement relation should be \"=\" (equal) ,\"<\" (more),\">\" (less) or \"!\" (not equal) ")]
        public string RELATION;

    }

    public class offer_opt_prod_scheduleMetadata
    {

        [Display(Name = "Offer option shedule ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Offer option shedule ID should be length between 1-12 digits")]
        public string ID;


        [Display(Name = "Offer option product ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Offer option product ID should be length between 1-12 digits")]
        public string OFFER_OPTION_PRODUCT_ID;

        [Display(Name ="Month of loyality period")]
        [RegularExpression(@"^\d{1,2}$",
            ErrorMessage ="Month of loyality period should be value between 1 and 99")]
        public string MTH_NUM;

        [Display(Name ="Product price")]
        [Required]
        [RegularExpression(@"^\d+(,\d\d)?$",
                ErrorMessage = "Product price should be in DD,DD format")]
        public string PRICE;
    }

    public class offer_option_productMetadata
    {
        [Display(Name = "Offer option product ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Offer option product ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name = "Offer option ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Offer option ID should be length between 1-12 digits")]
        public string OFFER_OPTION_ID;

        [Display(Name = "Product definition ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
                ErrorMessage = "Product definition ID should be length between 1-12 digits")]
        public string PRODUCT_DEFINITION_ID;

        [Display(Name ="Offer option type")]
        [Required]
        [RegularExpression(@"^L|U$",
            ErrorMessage ="Offer option type shuld be \"L\" (LOCKED) or \"U\" (UNLOCKED)")]
        public string TYPE;

        [Display(Name ="Loyality period length")]
        [RegularExpression(@"^(((1|2)?\d)|(3(0|1|2|3|4|5|6)))$",
                ErrorMessage = "Loyality period length should be value between 1 and 36 ")]
        public string LENGTH;

    }


    public class offer_requirement_definitionMetadata
    {
        [Display(Name = "Offer requirement definition ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
        ErrorMessage = "Offer requirement definition ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name ="Offer requirement name")]
        [Required]
        [MaxLength(150)]
        public string DESCRIPTION;

        [Display(Name = "Offer requirement description")]
        [Required]
        [MaxLength(4000)]
        public string FULL_DESCRIPTION;

        [Display(Name = "Offer function ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
        ErrorMessage = "Offer function ID should be length between 1-12 digits")]
        public string OFFER_FUNCTION_ID;
    }

    public class offer_req_functionMetadata 
    {

        [Display(Name = "Offer requirement function ID")]
        [Required]
        [RegularExpression(@"^\d{1,12}$",
ErrorMessage = "Offer requirement function ID should be length between 1-12 digits")]
        public string ID;

        [Display(Name ="Requirement function name")]
        [Required]
        [MaxLength(50)]
        public string FUNCTION_NAME;
    }

    public class crm_userMetadata
    {

    }

    public class YourTasksListVMetadata
    {
        [Display(Name = "User ID")]
        public long USER_ID { get; set; }

        [Display(Name = "User login")]
        public string IDENTITY_ID { get; set; }

        [Display(Name = "Basket")]
        public string BASKET_NAME { get; set; }

        [Display(Name = "ID")]
        public long TASK_ID { get; set; }

        [Display(Name = "Created")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required]
        [DataType(DataType.Date)]
        public DateTime CREATION_DATE { get; set; }

        [Display(Name = "Due")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> DUE_DATE { get; set; }

        [Display(Name = "Closed")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> CLOSE_DATE { get; set; }

        [Display(Name = "Type")]
        public string TASK_TYPE { get; set; }
    
        [Display(Name = "Subtype")]
        public string TASK_SUBTYPE { get; set; }

        [Display(Name = "Status")]
        public string TASK_STATUS { get; set; }

        [Display(Name = "Account")]
        public long ACCOUNT_ID { get; set; }

        [Display(Name = "Client")]
        public long CLIENT_ID { get; set; }

        [Display(Name = "Client name")]
        public string CLIENT_NAME { get; set; }



    }
    
    public class addressMetadata
    {
        [Display(Name = "Flat ID")]
        public decimal ID { get; set; }

        [Display(Name = "Flat")]
        public string FLAT { get; set; }

        [Display(Name = "Building ID")]
        public decimal ADDRESS_BUILDING_ID { get; set; }

        [Display(Name = "Inserted")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public System.DateTime INSERT_DATE { get; set; }

    }

    public class address_buildingMetadata
    {
        [Display(Name = "Building ID")]
        [DataType(DataType.Text)]
        public int ID { get; set; }

        [Display(Name = "Building Number")]
        public string BUILDING_NUMBER { get; set; }

        [Display(Name = "Postal Code")]
        public string POST_CODE { get; set; }

    }

    public class address_building_infoMetadata
    {
        [Display(Name = "Building number")]
        [DataType(DataType.Text)]
        public int BUILDING_NUMBER { get; set; }

        [Display(Name = "Post code")]
        public string POST_CODE { get; set; }

        [Display(Name = "Street")]
        public string STREET { get; set; }

        [Display(Name = "City")]
        public string CITY { get; set; }

        [Display(Name = "County")]
        public string COUNTY { get; set; }

        [Display(Name = "Province")]
        public string PROVINCE { get; set; }

        [Display(Name = "Building type")]
        public string BUILDING_TYPE { get; set; }

        [Display(Name = "Building ID")]
        [DataType(DataType.Text)]
        public int BUILDING_ID { get; set; }

        [Display(Name = "Street ID")]
        [DataType(DataType.Text)]
        public int STREET_ID { get; set; }

        [Display(Name = "City ID")]
        [DataType(DataType.Text)]
        public int CITY_ID { get; set; }

        [Display(Name = "County ID")]
        [DataType(DataType.Text)]
        public int COUNTY_ID { get; set; }

        [Display(Name = "Province ID")]
        [DataType(DataType.Text)]
        public int PROVINCE_ID { get; set; }

        [Display(Name = "Building type ID")]
        [DataType(DataType.Text)]
        public int BUILDING_TYPE_ID { get; set; }

        [Display(Name = "Technologies")]
        public string TECHNOLOGIES { get; set; }

        [Display(Name = "Technologies Ids")]
        public string TECHNOLOGIE_IDS { get; set; }

        [Display(Name = "Bandwidths")]
        public string BANDWIDTHS { get; set; }


    }
    public class address_streetMetadata
    {
        [Display(Name = "Street")]
        public string NAME { get; set; }

    }

    public class address_cityMetadata
    {
        [Display(Name = "City")]
        public string NAME { get; set; }

    }

    public class address_countyMetadata
    {
        [Display(Name = "County")]
        public string NAME { get; set; }

    }

    public class address_provinceMetadata
    {
        [Display(Name = "Province")]
        public string NAME { get; set; }

    }

    public class building_typeMetadata
    {
        [Display(Name = "Building type")]
        [RegularExpression(@"^([a-z]|\d){1,40}$",
                ErrorMessage = "Building type should be length between 1-40 only lowercase characters")]
        [Required]
        [MaxLength(150)]
        public string DESCRIPTION { get; set; }


    }

    public class mtm_avalil_techMetadata
    {
        [Display(Name = "HP")]
        public int HP { get; set; }

        [Display(Name = "Bandwidth (mb)")]
        public int BANDWIDTH_MB { get; set; }


}

    public class task_typeMetadata
    {
        [Display(Name = "Type ID")]
        public long ID { get; set; }

        [Display(Name = "Type")]
        [Required]
        [RegularExpression(@"^([a-z]|\d){1,30}$",
        ErrorMessage = "Task type name should be length between 1-30 only lowercase characters")]
        public string NAME { get; set; }

    }

    public class task_subtypeMetadata
    {
        [Display(Name = "Subtype ID")]
        public long ID { get; set; }

        [Display(Name = "Subtype")]
        [Required]
        [RegularExpression(@"^([a-z]|\d){1,50}$",
        ErrorMessage = "Task subtype name should be length between 1-50 only lowercase characters")]
        public string NAME { get; set; }

    }

    public class task_statusMetadata
    {
        [Display(Name = "Status ID")]
        public long ID { get; set; }

        [Display(Name = "Status")]
        [MaxLength(30)]
        [Required]
        [RegularExpression(@"^([a-z0-9 _])+$",
        ErrorMessage = "Task status name should be only lowercase characters")]
        public string NAME { get; set; }

    }

    public class basketMetadata
    {
        [Display(Name = "Basket ID")]
        public long ID { get; set; }

        [Display(Name = "Owner ID")]
        public long USER_ID { get; set; }

        [Display(Name = "Basket")]
        [Required]
        [RegularExpression(@"^([a-z 0-9_\\d]){1,20}$",
                ErrorMessage = "Name should be only lowercase characters")]
        [MaxLength(20)]
        public string NAME { get; set; }

        [Display(Name = "Is default")]
        public bool IS_DEFAULT { get; set; }

    }

    public class queueMetadata
    {
        [Display(Name = "Queue ID")]
        public long ID { get; set; }

        [Display(Name = "Queue")]
        [Required]
        [MaxLength(30)]
        [RegularExpression(@"^([a-z 0-9_\\-])+$",
                ErrorMessage = "Queue name should be only lowercase characters")]
        public string NAME { get; set; }

    }

    public class tasksMetadata
    {
        [Display(Name = "Task ID")]
        public long ID { get; set; }

        [Display(Name = "Status ID")]
        public long TASK_STATUS_ID { get; set; }

        [Display(Name = "Type ID")]
        public long TASK_TYPE_ID { get; set; }

        [Display(Name = "Subtype ID")]
        public long TASK_SUBTYPE_ID { get; set; }

        [Display(Name = "Creation date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public System.DateTime CREATION_DATE { get; set; }

        [Display(Name = "Due date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> DUE_DATE { get; set; }

        [Display(Name = "Close date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> CLOSE_DATE { get; set; }

        [Display(Name = "Closed by ID")]
        public Nullable<long> CLOSER_ID { get; set; }

        [Display(Name = "Queue ID")]
        public long QUEUE_ID { get; set; }

        [Display(Name = "Basket ID")]
        public Nullable<long> BASKET_ID { get; set; }

        [Display(Name = "Created by ID")]
        public long USER_ID_AUTHOR { get; set; }

        [Display(Name = "Parent task")]
        public Nullable<long> PARENT_TASK_ID { get; set; }

        [Display(Name = "Account ID")]
        public long ACCOUNT_ID { get; set; }

    }

    public class task_paramMetadata
    {

        [Display(Name = "Task parameter ID")]
        public long ID { get; set; }

        [Display(Name = "Definition ID")]
        public long TASK_PARAM_DEFINITION_ID { get; set; }

        [Display(Name = "Task ID")]
        public long TASK_ID { get; set; }

        [Display(Name = "Value")]
        [Required]
        [MaxLength(50)]
        public string VALUE { get; set; }
    }

    public class task_movement_historyMetadata
    {

        [Display(Name = "Movement ID")]
        public long ID { get; set; }

        [Display(Name = "Task ID")]
        public long TASK_ID { get; set; }

        [Display(Name = "Queue ID")]
        public long QUEUE_ID { get; set; }

        [Display(Name = "Basket ID")]
        public Nullable<long> BASKET_ID { get; set; }

        [Display(Name = "Move date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public System.DateTime MOVE_DATE { get; set; }
    }

    public class task_param_definitionMetadata
    {

        [Display(Name = "Definition ID")]
        public long ID { get; set; }

        [Display(Name = "Parameter type")]
        public string PARAM_TYPE { get; set; }

        [Display(Name = "Description")]
        [Required]
        [MaxLength(150)]
        public string DESCRIPTION { get; set; }

        [Display(Name = "Full description")]
        [Required]
        [MaxLength(255)]
        public string FULL_DESCRIPTION { get; set; }

        [MaxLength(255)]
        [Display(Name = "RegExp pattern")]
        public string REGEXP { get; set; }
    }

    public class technologyMetadata
    {

        [Display(Name = "Technology ID")]
        public long ID { get; set; }
        [MaxLength(150)]
        [Display(Name = "Technology")]
        [RegularExpression(@"^([a-z0-9 \\-_])+$",
        ErrorMessage = "Technology name should be only lowercase characters")]
        [Required]
        public string DESCRIPTION { get; set; }
    }

    public class mtm_paramdef_tasktypeMetadata
    {
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Start date")]
        public DateTime VALID_FROM { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "End date")]
        public DateTime VALID_TO { get; set; }
    }


    public class interactInfoMetadata
    {
        [Display(Name = "Interact ID")]
        public long INTERACT_ID { get; set; }

        [Display(Name = "Create date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime INTERACT_DATE { get; set; }

        [Display(Name = "Client ID")]
        public long CLIENT_ID { get; set; }

        [Display(Name = "Client name")]
        public string CUSTOMER_FULL_NAME { get; set; }

        [Display(Name = "Direction")]
        public string INTERACT_DIRECTION { get; set; }

        [Display(Name = "Medium")]
        public string INTERACT_MEDIUM { get; set; }

        [Display(Name = "Type")]
        public string INTERACT_TYPE { get; set; }

        [Display(Name = "Description")]
        public string DESCRIPTION { get; set; }

        [Display(Name = "Task ID")]
        public long TASKS_ID { get; set; }

        [Display(Name = "PESEL")]
        public string CLIENT_PESEL { get; set; }

        [Display(Name = "NIP")]
        public string CLIENT_NIP { get; set; }
    }

    public class device_typeMetadata
    {
        [Display(Name = "Device type")]
        [Required]
        [RegularExpression(@"^([a-z]|\d){1,150}$",
        ErrorMessage = "Device type name should be length between 1-150 only lowercase characters")]
        public string DESCRIPTION { get; set; }

    }
    
    public  class CLIENT_OFFER_OPTION_DETAILES_VMetada
    {

        public Nullable<decimal> LENGTH { get; set; }
        public Nullable<decimal> MTH_NUM { get; set; }
        public decimal PRICE { get; set; }

        [Display(Name = "Definition")]
        public string PROD_DEF_DESCR { get; set; }
        [Display(Name = "Category")]
        public string PROD_CAT_DESCR { get; set; }
        public string BRAND { get; set; }
        public string MODEL { get; set; }
        public string MODEL_DESCR { get; set; }
    }
    public class CLIENT_SEARCH_VMetada
    {
        [Display(Name = "CONTRACT")]
        public string CONTRACT_NUMBER { get; set; }
    }
    
    public class InvoiceDetailsViewModelMetadata
    {

    }

    public class ACCOUNT_INVOICES_VMetadata
    {
        [Display(Name = "Created")]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime CREATION_DATE { get; set; }

        [Display(Name = "ID")]
        public long ID { get; set; }

        [Display(Name = "Invoice number")]
        public string INVOICE_NUMBER { get; set; }

        [Display(Name = "Total")]
        public decimal TOTAL { get; set; }

        [Display(Name = "Paid")]
        public decimal PAID_AMOUNT { get; set; }

        [Display(Name = "Account ID")]
        public long ACCOUNT_ID { get; set; }

        [Display(Name = "Parent invoice")]
        public string PARENT_INVOICE_NUMBER { get; set; }

        [Display(Name = "Type")]
        public string INVOICE_TYPE { get; set; }

        [Display(Name = "Status")]
        public string INVOICE_STATUS { get; set; }

        [Display(Name = "Due date")]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime DUE_DATE { get; set; }

        [Display(Name = "Payment date")]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime PAYMENT_DATE { get; set; }


    }

    public class ACCOUNT_INVOICE_SUMMARY_VMetadata
    {
        [Display(Name = "Balance")]
        public decimal Balance;

        [Display(Name = "Overdue")]
        public bool IsOverdue;

        [Display(Name = "# Invoices")]
        public Nullable<decimal> INVOICE_COUNT { get; set; }

        [Display(Name = "Total value")]
        public Nullable<decimal> TOTAL { get; set; }

        [Display(Name = "Paid amount")]
        public Nullable<decimal> PAID_AMOUNT { get; set; }

        [Display(Name = "Earliest overdue")]
        public Nullable<System.DateTime> MIN_DUE_UNPAID { get; set; }
    }

    public class REPORTMetadata
    {
        [Display(Name = "Title")]
        [Required]
        [MaxLength(100)]
        public string TITLE;

        [Display(Name = "Description")]
        [MaxLength(4000)]
        [Required]
        public string DESCRIPTION;

        [Display(Name = "Create date")]
        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime CREATE_DATE;

        [Required]
        [Display(Name = "Enabled")]
        public string ENABLED;

        [Required]
        [Display(Name = "Interval")]
        public string INTERVAL;

        [Required]
        [MaxLength(200)]
        [Display(Name = "Action")]
        public string ACTION;

        [Required]
        [MaxLength(20)]
        [Display(Name = "Type")]
        public string TYPE;

        [Display(Name = "User ID")]
        public long USER_ID;

        [Display(Name = "Export path")]
        [MaxLength(255)]
        public string EXPORT_PATH;


    }

    public class CRMAPP_JM_JOBS_INFOMetadata
    {
        [Display(Name = "Job name")]
        public string JOB_NAME;

        [Display(Name = "Job action")]
        public string JOB_ACTION;

        [Display(Name = "Repeat interval")]
        public string REPEAT_INTERVAL;

        [Display(Name = "Enabled")]
        public string ENABLED;

        [Display(Name = "Run count")]
        public string RUN_COUNT;

        [Display(Name = "Failure count")]
        public string FAILURE_COUNT;

        [Display(Name = "Last start date")]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy HH:mm:ss}")]
        public DateTime LAST_START_DATE;

        [Display(Name = "Next run date")]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy HH:mm:ss}")]
        public DateTime NEXT_RUN_DATE;

        [Display(Name = "Last error date")]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy HH:mm:ss}")]
        public DateTime LAST_ERROR_DATE;

        [Display(Name = "Last error code")]
        public string LAST_ERROR_CODE;

        [Display(Name = "Last error message")]
        public string LAST_ERROR_MESSAGE;

        [Display(Name = "Recent run count")]
        public string RECENT_RUN_COUNT;

        [Display(Name = "Recent CPU time")]
        public string RECENT_CPU_S;

        [Display(Name = "Recent duration time")]
        public string RECENT_DURATION_S;

        [Display(Name = "Recent failures")]
        public string RECENT_FAILURES;

        [Display(Name = "Recent AVG CPU time")]
        public string RECENT_AVG_CPU_S;

        [Display(Name = "Recent AVG duration time")]
        public string RECENT_AVG_DURATION_S;

    }

    public class CRMAPP_JM_TRG_INFOMetadata
    {

        [Display(Name = "Valid from")]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime VALID_FROM;

        [Display(Name = "Valid to")]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime VALID_TO;

        [Display(Name = "Priority")]
        public string PRIORITY;

        [Display(Name = "Task status")]
        public string TASK_STATUS;

        [Display(Name = "Task type")]
        public string TASK_TYPE;

        [Display(Name = "Task subtype")]
        public string TASK_SUBTYPE;

        [Display(Name = "Generated")]
        public string GENERATED;

        [Display(Name = "Is new")]
        public string IS_NEW;

        [Display(Name = "Cancelled")]
        public string CANCELLED;

        [Display(Name = "Running")]
        public string RUNNING;

        [Display(Name = "Had error")]
        public string HAD_ERROR;

        [Display(Name = "Finished")]
        public string FINISHED;
    }

    public class DEVICE_BRAND_MODELMetadata
    {
        [Display(Name = "Brand")]
        [Required]
        [MaxLength(255)]
        /*[RegularExpression("[a-z0-9_\\- ]", ErrorMessage = "Brand needs to be lowercase.")]*/
        [RegularExpression(@"^([a-z]|\d){1,255}$",
        ErrorMessage = "Device brand should be length between 1-255 only lowercase characters")]
        public string BRAND;

        [Display(Name = "Model")]
        [Required]
        [MaxLength(255)]

        public string MODEL;

        [Display(Name = "Device type ID")]
        public string DEVICE_TYPE_ID;
    }

    public class CRMAPP_ACCLIST_BASEMetadata
    {
        [Required]
        [Display(Name = "Title")]
        public string TITLE;

        [Required]
        [Display(Name = "Description")]
        public string DESCRIPTION;

        [Display(Name = "Author ID")]
        public string AUTHOR_ID;
    }

    public class INSTALLER_DEVICES_STATE_VMetadata
    {

        [Display(Name = "User ID")]
        public string USER_ID;

        [Display(Name = "Identity ID")]
        public string IDENTITY_ID;

        [Display(Name = "Device type")]
        public string DEVICE_TYPE;

        [Display(Name = "Brand")]
        public string BRAND;

        [Display(Name = "Model")]
        public string MODEL;

        [Display(Name = "Required today")]
        public string REQUIRED_TODAY;

        [Display(Name = "Picked")]
        public string PICKED;

        [Display(Name = "Assigned")]
        public string ASSIGNED;
    }

    public class INSTALLER_TASKS_OVERVIEW_VMetadata
    {
        [Display(Name = "User ID")]
        public string USER_ID;

        [Display(Name = "Identity ID")]
        public string IDENTITY_ID;

        [Display(Name = "Due time")]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime DUE_TIME;

        [Display(Name = "Due time order")]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime DUE_DATE_ORDER;

        [Display(Name = "New")]
        public string NEW;

        [Display(Name = "Confirmed")]
        public string CONFIRMED;

        [Display(Name = "Deferred")]
        public string DEFERRED;

        [Display(Name = "Completed")]
        public string COMPLETED;

        [Display(Name = "Cancelled")]
        public string CANCELLED;
        

    }

    public class INSTALLER_TODAY_TASKSMetadata
    {

        [Display(Name = "Identity ID")]
        public string IDENTITY_ID;

        [Display(Name = "Due date")]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime DUE_DATE;

        [Display(Name = "Status")]
        public string STATUS;

        [Display(Name = "Client name")]
        public string CLIENT_NAME;

        [Display(Name = "City")]
        public string CITY;

        [Display(Name = "Street")]
        public string STREET;

        [Display(Name = "Building number")]
        public string BUILDING_NUM;

        [Display(Name = "Flat number")]
        public string FLAT_NUM;

        [Display(Name = "Products")]
        public string PRODUCTS;

        [Display(Name = "Devices")]
        public string DEVICES;

        [Display(Name = "Task ID")]
        public string TASK_ID;

        [Display(Name = "Client ID")]
        public string CLIENT_ID;

        [Display(Name = "Account ID")]
        public string ACCOUNT_ID;

        [Display(Name = "Offer ID")]
        public string OFFER_ID;

    }

    public class INSTALLER_PRODUCT_OFFER_VMetadata
    {

        [Display(Name = "Offer ID")]
        public string OFFER_ID;

        [Display(Name = "Product definition")]
        public string PROD_DEF;

        [Display(Name = "Product category")]
        public string PROD_CAT;

        [Display(Name = "Product ID")]
        public string PRODUCT_ID;

        [Display(Name = "Model brand ID")]
        public string MOD_BRAND_ID;

        [Display(Name = "Device type")]
        public string DEV_TYPE;

        [Display(Name = "Brand")]
        public string BRAND;

        [Display(Name = "Model")]
        public string MODEL;

        [Display(Name = "Device ID")]
        public string DEVICE_ID;

        [Display(Name = "Serial number")]
        public string SERIAL;

        [Display(Name = "MAC address")]
        public string MAC;
    }

    public class INSTALLER_AVAIL_DEVICES_VMetadata
    {
        [Display(Name = "User ID")]
        public string USER_ID;

        [Display(Name = "Identity ID")]
        public string IDENTITY_ID;

        [Display(Name = "Device type")]
        public string DEVICE_TYPE;

        [Display(Name = "Device model ID")]
        public string DEV_MOD_ID;

        [Display(Name = "Brand")]
        public string BRAND;

        [Display(Name = "Model")]
        public string MODEL;

        [Display(Name = "Device ID")]
        public string DEVICE_ID;

        [Display(Name = "Serial number")]
        public string SERIAL;

        [Display(Name = "MAC address")]
        public string MAC;

    }

    public class DEVICEMetadata
    {
        [Required]
        [MaxLength(100)]
        [Display(Name = "Serial number")]
        public string SERIAL;

        [Required]
        [MaxLength(50)]
        [Display(Name = "MAC address")]
        public string MAC;

        [Display(Name = "Device brand model ID")]
        public string DEVICE_BRAND_MODEL_ID;

        [Display(Name = "User ID")]
        public string CRM_USER_ID;
    }

}