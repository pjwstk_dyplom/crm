﻿using System.Collections.Generic;
using System.Linq;

namespace CRM_BTM.Models.OfferModels
{
    // Dictionary of <OFFER_OPTION_PRODUCT_ID, OfferOptionProductPricing>
    public class OfferOptionPricingDictionary : Dictionary<long, OfferOptionProductPricing>
    {
        public int TotalLength()
        {
            return this.Values.Max(x => x.TotalLength);
        }

        public decimal TotalPriceInPromo()
        {
            return this.Values.SelectMany(x => x.Periods).Sum(x => x.PeriodLength * x.Price);
        }

        public decimal AveragePriceInPromo()
        {
            if (TotalLength() == 0)
                return 0;
            var AllPeriods = this.Values.SelectMany(x => x.Periods);
            var SumPricePeriods = TotalPriceInPromo();
            var ProductsEndingEarly = this.Values.Where(x => x.TotalLength < this.TotalLength());
            var SumPriceEndingEarly = ProductsEndingEarly.Sum(x => (this.TotalLength() - x.TotalLength) * x.OutOfPromoPrice);
            var SumPriceUnlimited = this.Values.Where(x => x.Type == 'U').Sum(x => x.OutOfPromoPrice);
            var TotalPrice = SumPricePeriods + SumPriceEndingEarly;
            return decimal.Round(TotalPrice / TotalLength(), 2) + SumPriceUnlimited;
        }

        public decimal AveragePriceUnlimited()
        {
            var SumPriceUnlimited = this.Values.Where(x => x.Type == 'U').Sum(x => x.OutOfPromoPrice);
            return SumPriceUnlimited;
        }
    }
}