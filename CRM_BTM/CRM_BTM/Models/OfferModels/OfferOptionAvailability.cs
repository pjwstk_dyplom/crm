﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace CRM_BTM.Models.OfferModels
{
    public class OfferOptionAvailability
    {
        // option_id, evaluation (0/1)
        private Dictionary<long, int> dbResults = new Dictionary<long, int>();

        public int AccountID { get; set; }
        public Dictionary<OFFER_OPTION, bool> OfferEvaluation = new Dictionary<OFFER_OPTION, bool>();

        public OfferOptionAvailability(CRMConnection db, int accoID, bool onlyAvailable = false)
        {
            this.AccountID = accoID;

            using (var reader = getEvaluationCursor(db, accoID))
            {
                while (reader.Read())
                    if (!onlyAvailable || reader.GetInt32(1) == 1)
                        dbResults.Add(reader.GetInt32(0), reader.GetInt32(1));
            }

            loadOptions(db);
        }

        public OfferOptionAvailability(CRMConnection db, int accoID, long buildingId, bool onlyAvailable = false)
            : this(db, accoID, onlyAvailable)
        {
            var BuildingObj = db.ADDRESS_BUILDING.Find(buildingId);
            if (BuildingObj != null)
            {
                var BuildingTechIDs = BuildingObj.MTM_AVAIL_TECH_ON_BUILDING.Select(x => x.TECHNOLOGY_ID).ToList();
                OfferEvaluation = OfferEvaluation
                    .Where(x => BuildingTechIDs.Contains(x.Key.TECHNOLOGY_ID))
                    .ToDictionary(k => k.Key, v => v.Value);
            }

        }

        private OracleDataReader getEvaluationCursor(CRMConnection db, int accoID)
        {
            var cmd = db.Database.Connection.CreateCommand() as OracleCommand;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "crmapp_get_offer_options";
            var p_account_id = new OracleParameter("p_accound_id", OracleDbType.Int32, ParameterDirection.Input);
            var o_cursor = new OracleParameter("o_refCursor", OracleDbType.RefCursor, ParameterDirection.Output);
            p_account_id.Value = accoID;
            cmd.Parameters.Add(p_account_id);
            cmd.Parameters.Add(o_cursor);

            if (db.Database.Connection.State != ConnectionState.Open)
                db.Database.Connection.Open();

            return cmd.ExecuteReader();
        }

        private void loadOptions(CRMConnection db)
        {
            List<OFFER_OPTION> options = db.OFFER_OPTION.ToList().Where(x => dbResults.ContainsKey(x.ID)).ToList();
            foreach (var opt in options)
            {
                OfferEvaluation.Add(opt, dbResults[opt.ID] == 1);
            }
        }

    }
}