﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace CRM_BTM.Models.OfferModels
{
    public class OfferOptionProductPricing
    {
        public List<OfferOptionProductPricingPeriod> Periods { get; set; }

        public int TotalLength { get; set; }
        public char Type { get; set; }
        public decimal OutOfPromoPrice { get; set; }
        
        public decimal AveragePriceInPromo()
        {
            return Periods.Sum(p => p.Price) / TotalLength;
        }

        public string guessType()
        {
            return (Periods == null || Periods.Count == 0 ? "U" : "L");
        }

        public OfferOptionProductPricing()
        {
            Periods = new List<OfferOptionProductPricingPeriod>();
            TotalLength = 0;
            Type = ' ';
        }

        public OfferOptionProductPricing(IEnumerable<OFFER_PRICING_PERIODS_V> pp) : this()
        {
            foreach (var p in pp)
            {
                if (p.OUT_OF_PROMO.Equals("Y"))
                {
                    OutOfPromoPrice = p.PRICE;
                }
                else
                {
                    Periods.Add(new OfferOptionProductPricingPeriod(p));
                }
            }
            TotalLength = Convert.ToInt16(pp.Max(p => p.TOTAL_LENGTH));
            Type = pp == null ? ' ' : pp.FirstOrDefault().TYPE[0];
        }
    }
}