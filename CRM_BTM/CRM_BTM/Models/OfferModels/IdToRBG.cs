﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_BTM.Views.OfferModels
{
    public class IdToRBG
    {
        private static byte upperByteValue = 150;

        public static byte[] getRGB(int n)
        {
            Random r = new Random(n);
            var RGBBytes = new byte[3] { 0, 0, 0 }; //RGB values

            /*int squared = 255 * 255;
            decimal quotient = n / squared;
            var mod = n % squared;
            byte theByte = (byte)Math.Floor(quotient);
            RGBBytes[0] = theByte;

            quotient = mod / 255;
            mod = mod % 255;
            theByte = (byte)Math.Floor(quotient);
            RGBBytes[1] = theByte;

            theByte = (byte)Math.Floor((decimal)mod);
            RGBBytes[2] = theByte;*/

            r.NextBytes(RGBBytes);
            if (RGBBytes[0] > upperByteValue) RGBBytes[0] = Convert.ToByte((RGBBytes[0] + 1) / 2);
            if (RGBBytes[1] > upperByteValue) RGBBytes[1] = Convert.ToByte((RGBBytes[1] + 1) / 2);
            if (RGBBytes[2] > upperByteValue) RGBBytes[2] = Convert.ToByte((RGBBytes[2] + 1) / 2);

            return RGBBytes;
        }
    }
}