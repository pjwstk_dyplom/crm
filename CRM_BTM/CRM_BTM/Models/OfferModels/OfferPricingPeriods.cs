﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_BTM.Models.OfferModels
{
    public class OfferPricingPeriods : Dictionary<long, OfferOptionPricingDictionary> {

        public OfferPricingPeriods(IEnumerable<OFFER_PRICING_PERIODS_V> PricingPeriodsDB) : base()
        {
            Initialize(PricingPeriodsDB);
        }

        public OfferPricingPeriods(CRMConnection db, IEnumerable<long> OptionIDs) : base()
        {
            var PricingPeriodsDB = db.OFFER_PRICING_PERIODS_V.Where(x => OptionIDs.Contains(x.OFFER_OPTION_ID)).ToList();
            Initialize(PricingPeriodsDB);
        }

        public OfferPricingPeriods(CRMConnection db, long OptionID) : base()
        {
            var PricingPeriodsDB = db.OFFER_PRICING_PERIODS_V.Where(x => x.OFFER_OPTION_ID == OptionID).ToList();
            Initialize(PricingPeriodsDB);
        }

        private void Initialize(IEnumerable<OFFER_PRICING_PERIODS_V> PricingPeriodsDB)
        {
            foreach (var ppdb in PricingPeriodsDB)
            {
                long option_id = Convert.ToInt32(ppdb.OFFER_OPTION_ID);
                long option_prod_id = Convert.ToInt32(ppdb.OFFER_OPTION_PRODUCT_ID);

                if (!this.ContainsKey(option_id))
                    this.Add(option_id, new OfferOptionPricingDictionary());

                if (!this[option_id].ContainsKey(option_prod_id))
                    this[option_id].Add(option_prod_id, new OfferOptionProductPricing(PricingPeriodsDB.Where(pp => pp.OFFER_OPTION_PRODUCT_ID.Equals(option_prod_id))));
            }
        }
    }
}