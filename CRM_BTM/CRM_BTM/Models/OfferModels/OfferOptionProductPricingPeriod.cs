﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_BTM.Models.OfferModels
{
    public class OfferOptionProductPricingPeriod
    {
        public int PeriodStart { get; set; }
        public int PeriodEnd { get; set; }
        public int PeriodLength { get { return PeriodEnd - PeriodStart + 1; } }
        public decimal Price { get; set; }

        public OfferOptionProductPricingPeriod() { }

        public OfferOptionProductPricingPeriod(OFFER_PRICING_PERIODS_V period) {
            PeriodStart = Convert.ToInt16(period.PERIOD_START);
            PeriodEnd = Convert.ToInt16(period.PERIOD_END);
            Price = period.PRICE;
        }
    }



}