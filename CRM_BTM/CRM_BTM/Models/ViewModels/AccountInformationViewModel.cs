﻿using CRM_BTM.Models.OfferModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_BTM.Models.ViewModels
{
    public class AccountInformationViewModel
    {
        public ACCOUNT Account { get; set; }
        public ACCOUNT_INVOICE_SUMMARY_V InvoiceSummary { get; set; }
        public IEnumerable<OFFER> Offers { get; set; }
    }
}