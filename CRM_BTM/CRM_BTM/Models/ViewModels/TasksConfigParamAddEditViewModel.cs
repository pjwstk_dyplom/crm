﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_BTM.Models.ViewModels
{
    public class TasksConfigParamAddEditViewModel
    {
        public IEnumerable<MTM_PARAMDEF_TASKTYPE> TaskTypes { get; set; }
        public IEnumerable<TASK_PARAM_VALUE_LIST> ValueList { get; set; }
        public TASK_PARAM_DEFINITION Parameter { get; set; }
    }
}