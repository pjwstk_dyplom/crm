﻿using CRM_BTM.Models.OfferModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_BTM.Models.ViewModels
{
    public class OfferEditViewModel
    {
        public OFFER_DEFINITION Offer { get; set; }
        public OfferPricingPeriods PricingPeriods { get; set; }
    }

    
}