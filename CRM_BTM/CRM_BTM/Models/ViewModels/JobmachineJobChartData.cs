﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_BTM.Models.ViewModels
{
    public class JobmachineJobChartData
    {

        public JobmachineJobChartData()
        {
            Dates = new List<DateTime>();
            RunCount = new List<decimal?>();
            RunTimeTotal = new List<decimal?>();
            RunTimeAvg = new List<decimal?>();
        }

        public List<DateTime> Dates { get; set; }
        public List<decimal?> RunCount { get; set; }
        public List<decimal?> RunTimeTotal { get; set; }
        public List<decimal?> RunTimeAvg { get; set; }
    }
}