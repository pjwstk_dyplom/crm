﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM_BTM.Models.ViewModels
{
    public class UpsellUpgradeProcessModel
    {
        public enum ProcessTypes {
            Upsell,
            Upgrade
        }

        [Display(Name = "Account")]
        public long? AccountID { get; set; }

        [Display(Name = "Option ID")]
        public long? OfferOptionID { get; set; }
        public ProcessTypes ProcessType { get; set; }
    }
}