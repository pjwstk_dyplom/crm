﻿namespace CRM_BTM.Models.ViewModels
{
    public class NewClientOfferOption
    {
        public long Offer_option_id { get; set; }
        public long Option_evaluation { get; set; }


        public NewClientOfferOption(long v1, long v2)
        {
            this.Offer_option_id = v1;
            this.Option_evaluation = v2;
        }
    }
}