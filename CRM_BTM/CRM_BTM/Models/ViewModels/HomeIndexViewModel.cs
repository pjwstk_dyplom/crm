﻿using CustomAuth.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;

namespace CRM_BTM.Models.ViewModels
{
    public class HomeIndexViewModel
    {
        public CRM_USER CrmUser { get; set; }
        public User IdentityUser { get; set; }
        public IEnumerable<YOUR_TASKS_LIST_V> CurrentTasks { get; set; }
        public IEnumerable<YOUR_TASKS_MOVEMENTS_V> RecentTasks { get; set; }
        public Chart CurrentTasksChart { get; set; }
        public Chart RecentTasksChart { get; set; }

    }
}