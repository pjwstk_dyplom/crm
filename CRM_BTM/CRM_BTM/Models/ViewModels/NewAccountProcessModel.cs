﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM_BTM.Models.ViewModels
{
    public class NewAccountProcessModel
    {
        [Display(Name = "Installation building")]
        public long? InstallationBuildingID { get; set; }

        [Display(Name = "Option ID")]
        public long? OfferOptionID { get; set; }

        [Required]
        public long? ClientID { get; set; }
        
        [Required]
        [Range(1, 27)]
        [Display(Name = "Billing day")]
        public int? BillingDay { get; set; }

        [Required]
        [Display(Name = "Division")]
        public long? DivisionID { get; set; }

        [Display(Name = "Installation flat")]
        public string InstallationFlat { get; set; }

        [Display(Name = "Billing same as installation")]
        public bool BillingAddressFromInstallation { get; set; }

        [Display(Name = "Billing flat")]
        public string BillingFlat { get; set; }

        [Display(Name = "Billing building")]
        public long? BillingBuildingID { get; set; }
    }
}