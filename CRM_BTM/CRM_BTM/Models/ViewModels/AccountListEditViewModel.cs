﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_BTM.Models.ViewModels
{
    public class AccountListEditViewModel
    {
        public CRMAPP_ACCLIST_BASE Base { get; set; }
        public List<CRMAPP_ACCLIST_COLUMN_DEF> Columns { get; set; }
        public List<CRMAPP_ACCLIST_CONDITIONS> Conditions { get; set; }
    }
}