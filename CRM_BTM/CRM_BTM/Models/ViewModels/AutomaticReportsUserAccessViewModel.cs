﻿using System;
using System.Collections.Generic;
using CRM_BTM.Models;
using System.Linq;
using System.Web;

namespace CRM_BTM.ViewModels
{
    public class AutomaticReportsUserAccessViewModel
    {
        public REPORT Report { get; set; }
        public List<CRM_USER> AllUsers { get; set; }
    }
}