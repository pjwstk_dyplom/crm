﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;

namespace CRM_BTM.Models.ViewModels
{
    public class JobMachineTriggerInfoViewModel
    {
        public JobMachineTriggerInfoViewModel()
        {
            Triggers = new List<CRMAPP_JM_TRG_INFO>();
            JobCharts = new List<Chart>();
            Jobs = new List<CRMAPP_JM_JOBS_INFO>();
        }

        public Dictionary<long, Chart> TriggerCharts { get; set; }
        public List<CRMAPP_JM_TRG_INFO> Triggers { get; set; }

        public List<Chart> JobCharts { get; set; }
        public List<CRMAPP_JM_JOBS_INFO> Jobs { get; set; }
    }
}