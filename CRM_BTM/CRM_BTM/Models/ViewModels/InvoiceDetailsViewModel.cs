﻿using System.Collections.Generic;

namespace CRM_BTM.Models.ViewModels
{
    public class InvoiceDetailsViewModel
    {

        public InvoiceDetailsViewModel()
        {
            Products = new List<MTM_INVOICE_PRODUCT>();
            Others = new List<INVOICE_OTHER_ITEMS>();
        }

        public IEnumerable<MTM_INVOICE_PRODUCT> Products { get; set; }
        public IEnumerable<INVOICE_OTHER_ITEMS> Others { get; set; }
        public long? InvoiceId { get; set; }
    }
}