﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM_BTM.Models.ViewModels
{
    public class EditBillingAddressModel
    {

        public long AccountID { get; set; }
        public long ClientId  { get; set; }
        public string BillingFlat { get; set; }
        public long BillingBuildingID { get; set; }
        public SelectList AddressProvince { get; set; }

        public EditBillingAddressModel()
        {

        }
    }
}