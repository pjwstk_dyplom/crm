﻿using CRM_BTM.Models.OfferModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace CRM_BTM.Models.ViewModels
{
    public class OfferEditOptionProductsViewModel
    {
        public OfferOptionPricingDictionary Pricing { get; set; }
        public List<OFFER_OPTION_PRODUCT> Products { get; set; }
        public long OptionID { get; set; }

        public List<OFFER_OPTION_PRODUCT> RegenerateDbObjects(CRMConnection db)
        {
            var option = db.OFFER_OPTION.Find(OptionID);
            var result = new List<OFFER_OPTION_PRODUCT>();
            for (int i = 0; i < Products.Count; i++)
            {
                if (!Pricing.ContainsKey(i))
                    throw new InvalidOperationException("Invalid data state: Pricing key not found");
                var prodDef = db.PRODUCT_DEFINITION.Find(Products[i].PRODUCT_DEFINITION_ID);
                var newProduct = new OFFER_OPTION_PRODUCT()
                {
                    OFFER_OPTION_ID = OptionID,
                    OFFER_OPTION = option,
                    PRODUCT_DEFINITION_ID = Products[i].PRODUCT_DEFINITION_ID,
                    PRODUCT_DEFINITION = prodDef,
                    TYPE = Pricing[i].guessType()
                };
                newProduct.OFFER_OPT_PROD_SCHEDULE = new List<OFFER_OPT_PROD_SCHEDULE>();
                if (Pricing[i].Periods != null)
                { 
                    foreach(var period in Pricing[i].Periods.OrderBy(x => x.PeriodStart))
                    {
                        for (int mthNum = period.PeriodStart; mthNum <= period.PeriodEnd; mthNum++)
                        {
                            if (mthNum > 0)
                                if (!newProduct.OFFER_OPT_PROD_SCHEDULE.Select(x => x.MTH_NUM).ToList().Contains(Convert.ToByte(mthNum)))
                                    newProduct.OFFER_OPT_PROD_SCHEDULE.Add(new OFFER_OPT_PROD_SCHEDULE() {
                                        MTH_NUM = Convert.ToByte(mthNum),
                                        PRICE = period.Price,
                                        OFFER_OPTION_PRODUCT = newProduct
                                    });
                        }
                    }
                    newProduct.LENGTH = Convert.ToByte(Pricing[i].Periods.Sum(p => p.PeriodLength));
                }
                if (Pricing[i].OutOfPromoPrice > 0)
                {
                    if (!newProduct.OFFER_OPT_PROD_SCHEDULE.Any(x => x.MTH_NUM == null))
                        newProduct.OFFER_OPT_PROD_SCHEDULE.Add(new OFFER_OPT_PROD_SCHEDULE()
                        {
                            MTH_NUM = null,
                            PRICE = Pricing[i].OutOfPromoPrice,
                            OFFER_OPTION_PRODUCT = newProduct
                        });
                }
                result.Add(newProduct);
            }
            
            return result;
        }
    }    
}