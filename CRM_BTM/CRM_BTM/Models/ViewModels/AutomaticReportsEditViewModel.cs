﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CRM_BTM.Models;

namespace CRM_BTM.ViewModels
{
    public class AutomaticReportsEditViewModel
    {
        public REPORT Report { get; set; }
        public List<CRM_USER> Users { get; set; }
    }
}