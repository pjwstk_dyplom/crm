﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_BTM.Models.ViewModels
{
    public class TaskParamSkeleton
    {
        public bool InUse { get; set; }
        public long ParamDefID { get; set; }
        public string ParamValue { get; set; }
    }
}