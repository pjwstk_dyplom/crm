﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_BTM.Models.ViewModels
{
    public class ClientDetailsViewModel
    {
        public CLIENT Client { get; set; }
        public AccountInformationViewModel AccountViewModel { get; set; }
    }
}