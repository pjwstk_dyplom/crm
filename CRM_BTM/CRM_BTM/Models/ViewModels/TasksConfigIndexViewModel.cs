﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_BTM.Models.ViewModels
{
    public class TasksConfigIndexViewModel
    {
        public List<TASK_STATUS> Statuses { get; set; }
        public List<TASK_TYPE> Types { get; set; }
        public List<TASK_SUBTYPE> Subtypes { get; set; }
        public List<TASK_PARAM_DEFINITION> Parameters { get; set; }
    }
}