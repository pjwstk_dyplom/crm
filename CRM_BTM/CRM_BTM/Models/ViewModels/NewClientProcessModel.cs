﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM_BTM.Models.ViewModels
{
    public class NewClientProcessModel
    {
        [Display(Name = "Installation building")]
        public long? BuildingID { get; set; }

        [Display(Name = "Option ID")]
        public long? OfferOptionID { get; set; }

        [Required]
        [Display(Name="Name")]
        [MaxLength(50)]
        public string ClientName { get; set; }

        [Required]
        [Display(Name = "Lastname")]
        [MaxLength(50)]
        public string ClientLastname { get; set; }

        [Display(Name = "PESEL")]
        [MaxLength(11)]
        [RegularExpression(@"^\d*$",
                ErrorMessage = "Digits only")]
        public string ClientPESEL { get; set; }

        [Display(Name = "NIP")]
        [MaxLength(50)]
        [RegularExpression(@"^\d*$",
                ErrorMessage = "Digits only")]
        public string ClientNIP { get; set; }

        [Required]
        [Display(Name = "Segment")]
        public long? SegmentID { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "1st Phone")]
        [MinLength(9)]
        [MaxLength(20)]
        [RegularExpression(@"^\d+$",
                ErrorMessage = "Telephone can contain only digits")]
        public string PhonePrimary { get; set; }

        [Display(Name = "2nd Phone")]
        [RegularExpression(@"^\d*$",
                ErrorMessage = "Telephone can contain only digits")]
        [MaxLength(20)]
        public string PhoneSecondary { get; set; }

        [Required]
        [Range(1, 27)]
        [Display(Name = "Billing day")]
        public int? BillingDay { get; set; }

        [Required]
        [Display(Name = "Division")]
        public long? DivisionID { get; set; }

        [Display(Name = "Installation flat")]
        public string InstallationFlat { get; set; }

        [Display(Name = "Billing same as installation")]
        public bool BillingAddressFromInstallation { get; set; }

        [Display(Name = "Billing flat")]
        public string BillingFlat { get; set; }

        [Display(Name = "Billing building")]
        public long? BillingBuildingID { get; set; }
    }
}