﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_BTM.Models.ViewModels
{
    public class OfferOptionProductForcedAttrViewModel
    {
        public long ProductID { get; set; }
        public IEnumerable<PRODUCT_ATTRIBUTE_DEFINITION> Definitions { get; set; }
        public IEnumerable<OFFER_OPT_PROD_FORCED_ATTR> Forced { get; set; }
    }
}