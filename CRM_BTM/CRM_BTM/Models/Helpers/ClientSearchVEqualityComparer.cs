﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_BTM.Models.Helpers
{
    public class ClientSearchVEqualityComparer : IEqualityComparer<CLIENT_SEARCH_V>
    {
        public bool Equals(CLIENT_SEARCH_V a, CLIENT_SEARCH_V b)
        {
            if (a == null && b == null)
                return true;
            else if (a == null || b == null)
                return false;
            else if (a.ID == b.ID)
                return true;
            else
                return false;
        }

        public int GetHashCode(CLIENT_SEARCH_V cs)
        {
            long hCode = (long)cs.ID;
            return hCode.GetHashCode();
        }
    }
}
