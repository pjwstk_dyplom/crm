//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CRM_BTM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class INSTALLER_DEVICES_STATE_V
    {
        public Nullable<long> USER_ID { get; set; }
        public string IDENTITY_ID { get; set; }
        public string DEVICE_TYPE { get; set; }
        public long DBM_ID { get; set; }
        public string BRAND { get; set; }
        public string MODEL { get; set; }
        public Nullable<decimal> REQUIRED_TODAY { get; set; }
        public Nullable<decimal> PICKED { get; set; }
        public Nullable<decimal> ASSIGNED { get; set; }
    }
}
