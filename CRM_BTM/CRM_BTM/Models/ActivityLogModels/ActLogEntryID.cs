﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_BTM.Models.ActivityLogModels
{
    public class ActLogEntryID : List<ActLogEntry>
        // a full entryID, list of entries changed
    {
        public long EntryID { get { return this.FirstOrDefault().EntryID; } }
        public DateTime EntryTime { get { return this.FirstOrDefault().EntryTime; } }
        public string UserLogin { get { return this.FirstOrDefault().UserLogin; } }

        public void Add(ACTIVITY_LOG log)
        {
            var logEntry = this.Find(le => le.ReferencedTable.Equals(log.REFERENCED_TABLE) && le.ReferencedID == log.REFERENCED_ID);
            if (logEntry == null)
            {
                var le = new ActLogEntry();
                le.Add(log);
                this.Add(le);
            } 
            else
            {
                logEntry.Add(log);
            }
        }

        public void AddRange(IEnumerable<ACTIVITY_LOG> logs)
        {
            foreach(var log in logs)
            {
                this.Add(log);
            }
        }
    }
}