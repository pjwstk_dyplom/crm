﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_BTM.Models.ActivityLogModels
{
    public interface IActivityLoggable
    {
        Type ActivityLogType { get; }
        long ActivityLogID { get; }
        string StorageTable { get; }
    }
}
