﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_BTM.Models.ActivityLogModels
{
    public class ActLogEntryIDList : List<ActLogEntryID>
    {
        public ActLogEntryIDList(List<ACTIVITY_LOG> dbValues)
        {
            this.AddRange(dbValues);
        }
        
        public void Add(ACTIVITY_LOG log)
        {
            var logEntryID = this.Find(le => le.EntryID == log.ENTRY_ID);
            if (logEntryID == null)
            {
                var leid = new ActLogEntryID();
                leid.Add(log);
                this.Add(leid);
            }
            else
            {
                logEntryID.Add(log);
            }
        }

        public void AddRange(IEnumerable<ACTIVITY_LOG> logs)
        {
            foreach (var log in logs)
            {
                this.Add(log);
            }
        }
    }
}