﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_BTM.Models.ActivityLogModels
{
    public class ActLogEntry : List<ACTIVITY_LOG>
        // a set of changes to single ID
    {
        public DateTime EntryTime { get { return this.Max(a => a.ENTRY_TIME); } }
        public long EntryID { get { return this.FirstOrDefault().ENTRY_ID; } }
        public string ReferencedTable { get { return this.FirstOrDefault().REFERENCED_TABLE; } }
        public long ReferencedID { get { return this.FirstOrDefault().REFERENCED_ID; } }
        public string UserLogin { get { return this.FirstOrDefault().USER_LOGIN; } }
        public char EntryType { get { return this.FirstOrDefault().ENTRY_TYPE[0]; } }
    }
}