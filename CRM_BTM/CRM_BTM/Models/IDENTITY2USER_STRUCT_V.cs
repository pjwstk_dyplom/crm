//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CRM_BTM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class IDENTITY2USER_STRUCT_V
    {
        public int IDENTITY_ID { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string USERNAME { get; set; }
        public long USER_ID { get; set; }
        public string HR_ID { get; set; }
        public Nullable<long> STRUCT_ID { get; set; }
        public Nullable<System.DateTime> USER_STRUCT_FROM { get; set; }
        public Nullable<System.DateTime> USER_STRUCT_TO { get; set; }
    }
}
