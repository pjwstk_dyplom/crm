﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Security.Principal;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Data.Entity.Infrastructure;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Mapping;
using CRM_BTM.Models.ActivityLogModels;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Text;
using CRM_BTM.Models.OfferModels;

namespace CRM_BTM.Models
{
    [MetadataType(typeof(ClientsMetadata))]
    public partial class CLIENT : IActivityLoggable
    {
        public long ActivityLogID { get => ID; }
        public Type ActivityLogType { get => typeof(CLIENT); }
        public string StorageTable { get => "CLIENT"; }
        
        [Display(Name = "Client")]
        public string fullName { get { return String.Concat(this.NAME, " ", LASTNAME); } }
    }

    [MetadataType(typeof(REPORTMetadata))]
    public partial class REPORT
    {
    }

    [MetadataType(typeof(AccountsMetadata))]
    public partial class ACCOUNT
    {
        public decimal RemainingPromoValue(CRMConnection db)
        {
            var CurrentOffersValueDB = db.ACCOUNT_PROMO_VALUE_V.Where(x => x.ACCOUNT_ID == this.ID).ToList();
            decimal CurrentOffersValue = CurrentOffersValueDB.Count()==0 ? 0 : (decimal)CurrentOffersValueDB.First().PROMO_PRICE;
            return CurrentOffersValue;
        }
    }

    public partial class ADDRESS_FULL_V
    {
        public string FullAddress
        {
            get
            {
                var sb = new StringBuilder();
                sb.Append(this.PROVINCE).Append("; ");
                sb.Append(this.COUNTY).Append("; ");
                sb.Append(this.CITY).Append("; ");
                sb.Append(this.POST_CODE).Append("; ");
                sb.Append(this.STREET).Append(" ");
                sb.Append(this.BUILDING_NUMBER);
                if (!String.IsNullOrEmpty(this.FLAT))
                    sb.Append("/").Append(this.FLAT);
                return sb.ToString();
            }
        }
    }

    [MetadataType(typeof(SegmentMetadata))]
    public partial class SEGMENT : IActivityLoggable
    {
        public long ActivityLogID { get => ID; }
        public Type ActivityLogType { get => typeof(CLIENT); }
        public string StorageTable { get => "SEGMENT"; }


    }

    [MetadataType(typeof(DivisionMetadata))]
    public partial class DIVISION : IActivityLoggable
    {
        public long ActivityLogID { get => ID; }
        public Type ActivityLogType { get => typeof(DIVISION); }
        public string StorageTable { get => "DIVISION"; }

    }

    [MetadataType(typeof(ContractMetadata))]
    public partial class CONTRACT
    {

    }

    [MetadataType(typeof(InteractMetadata))]
    public partial class INTERACT
    {

    }


    [MetadataType(typeof(Interact_typeMetadata))]
    public partial class INTERACT_TYPE : IActivityLoggable
    {
        public long ActivityLogID { get => ID; }
        public Type ActivityLogType { get => typeof(INTERACT_TYPE); }
        public string StorageTable { get => "INTERACT_TYPE"; }

    }

    [MetadataType(typeof(Interact_directionMetadata))]
    public partial class INTERACT_DIRECTION : IActivityLoggable
    {
        public long ActivityLogID { get => ID; }
        public Type ActivityLogType { get => typeof(INTERACT_DIRECTION); }
        public string StorageTable { get => "INTERACT_DIRECTION"; }

    }

    [MetadataType(typeof(Interact_mediumMetadata))]
    public partial class INTERACT_MEDIUM : IActivityLoggable
    {
        public long ActivityLogID { get => ID; }
        public Type ActivityLogType { get => typeof(INTERACT_MEDIUM); }
        public string StorageTable { get => "INTERACT_MEDIUM"; }


    }

    [MetadataType(typeof(InvoiceMetadata))]
    public partial class INVOICE
    {

    }

    [MetadataType(typeof(Invoice_other_itemsMetadata))]
    public partial class INVOICE_OTHER_ITEMS
    {

    }

    [MetadataType(typeof(Invoice_other_items_descMetadata))]
    public partial class INVOICE_OTHER_ITEMS_DESC
    {

    }

    [MetadataType(typeof(Invoice_typeMetadata))]
    public partial class INVOICE_TYPE : IActivityLoggable
    {
        public long ActivityLogID { get => ID; }
        public Type ActivityLogType { get => typeof(INVOICE_TYPE); }
        public string StorageTable { get => "INVOICE_TYPE"; }


    }

    [MetadataType(typeof(Invoice_statusMetadata))]
    public partial class INVOICE_STATUS : IActivityLoggable
    {
        public long ActivityLogID { get => ID; }
        public Type ActivityLogType { get => typeof(INVOICE_STATUS); }
        public string StorageTable { get => "INVOICE_STATUS"; }


    }

    [MetadataType(typeof(mtm_invoice_productMetadata))]
    public partial class MTM_INVOICE_PRODUCT
    {

    }


    [MetadataType(typeof(productMetadata))]
    public partial class PRODUCT
    {

    }

    [MetadataType(typeof(Product_statusMetadata))]
    public partial class PRODUCT_STATUS : IActivityLoggable
    {
        public long ActivityLogID { get => ID; }
        public Type ActivityLogType { get => typeof(PRODUCT_STATUS); }
        public string StorageTable { get => "PRODUCT_STATUS"; }


    }

    [MetadataType(typeof(product_attributeMetadata))]
    public partial class PRODUCT_ATTRIBUTE
    {

    }

    [MetadataType(typeof(product_attribute_definition))]
    public partial class PRODUCT_ATTRIBUTE_DEFINITION
    {
        public string ParamTypeFull { get {
                switch (this.PARAM_TYPE[0])
                {
                    case 'S': return "Strict";
                    case 'A': return "Any";
                    case 'R': return "Pattern";
                    default:
                        throw new InvalidCastException("PARAM_TYPE is not within allowed values.");
                }
            } }

        public bool ValueIsValid(CRMConnection db, string value)
        {
            switch (ParamTypeFull) {
                case "Any": return true;
                case "Strict":
                    var allowedValues = this.PRODUCT_ATTRIBUTE_VALUE_LIST.Select(avl => avl.VALUE);
                    return allowedValues.Contains(value);
                case "Pattern":
                    Regex rgx = new Regex(this.REGEXP);
                    return rgx.IsMatch(value);
                default:
                    throw new InvalidCastException("PARAM_TYPE is not within allowed values.");
            }
        }
    }

    [MetadataType(typeof(product_attribute_value_list))]
    public partial class PRODUCT_ATTRIBUTE_VALUE_LIST
    {

    }

    [MetadataType(typeof(mtm_attrdef_proddefMetadata))]
    public partial class MTM_ATTRDEF_PRODDEF
    {

    }

    [MetadataType(typeof(product_definitionMetadata))]
    public partial class PRODUCT_DEFINITION
    {
        public string CategoryPlusDescription { get { return this.PRODUCT_CATEGORY.DESCRIPTION + " - " + this.DESCRIPTION; } }
    }

    [MetadataType(typeof(product_categoryMetadata))]
    public partial class PRODUCT_CATEGORY : IActivityLoggable
    {
        public long ActivityLogID { get => ID; }
        public Type ActivityLogType { get => typeof(PRODUCT_CATEGORY); }
        public string StorageTable { get => "PRODUCT_CATEGORY"; }


    }

    [MetadataType(typeof(mtm_offer_productMetadata))]
    public partial class MTM_OFFER_PRODUCT
    {

    }

    [MetadataType(typeof(offerMetadata))]
    public partial class OFFER
    {
        public OfferOptionPricingDictionary OptionPricingPeriods { get; set; }

        private static int GetMonthsBetween(DateTime from, DateTime to)
        {
            if (from > to) return GetMonthsBetween(to, from);

            var monthDiff = Math.Abs((to.Year * 12 + (to.Month - 1)) - (from.Year * 12 + (from.Month - 1)));

            if (from.AddMonths(monthDiff) > to || to.Day < from.Day)
            {
                return monthDiff - 1;
            }
            else
            {
                return monthDiff;
            }
        }

        public int CurrentOfferMonth()
        {
            return GetMonthsBetween(this.SALE_DATE, DateTime.Today);
        }
    }

    [MetadataType(typeof(offer_optionMetada))]
    public partial class OFFER_OPTION
    {
        public OfferOptionPricingDictionary OptionPricingPeriods { get; set; }

        public bool canBeEdited()
        {
            return this.OFFER.Count == 0;
        }

        public void ClearProducts(CRMConnection conn)
        {
            foreach (var pr in this.OFFER_OPTION_PRODUCT)
            {
                conn.OFFER_OPT_PROD_FORCED_ATTR.RemoveRange(pr.OFFER_OPT_PROD_FORCED_ATTR);
                conn.OFFER_OPT_PROD_SCHEDULE.RemoveRange(pr.OFFER_OPT_PROD_SCHEDULE);
            }
            conn.OFFER_OPTION_PRODUCT.RemoveRange(this.OFFER_OPTION_PRODUCT);
        }
        
        public void SetSingleOptionPricingPeriods(CRMConnection db)
        {
            var OfferPricingPeriods = new OfferPricingPeriods(db, this.ID);
            this.OptionPricingPeriods = OfferPricingPeriods[this.ID];
        }
    }

    [MetadataType(typeof(offer_definitionMetada))]
    public partial class OFFER_DEFINITION
    {

    }

    [MetadataType(typeof(mtm_offer_opt2categoryMetadata))]
    public partial class mtm_offer_opt2category
    {

    }

    [MetadataType(typeof(offer_categoryMetadata))]
    public partial class OFFER_CATEGORY
    {

    }

    [MetadataType(typeof(mtm_offer_requirementMetadata))]
    public partial class MTM_OFFER_REQUIREMENT
    {

    }

    [MetadataType(typeof(offer_opt_prod_scheduleMetadata))]
    public partial class OFFER_OPT_PROD_SCHEDULE
    {

    }

    [MetadataType(typeof(offer_option_productMetadata))]
    public partial class OFFER_OPTION_PRODUCT
    {

    }

    [MetadataType(typeof(offer_requirement_definitionMetadata))]
    public partial class OFFER_REQUIREMENT_DEFINITION
    {

    }

    [MetadataType(typeof(offer_req_functionMetadata))]
    public partial class OFFER_REQ_FUNCTION
    {

    }

    [MetadataType(typeof(YourTasksListVMetadata))]
    public partial class YOUR_TASKS_LIST_V
    {

    }

    [MetadataType(typeof(crm_userMetadata))]
    public partial class CRM_USER
    {
        public static CRM_USER getFromIdentity(CRMConnection db, IIdentity i)
        {
            if (i == null)
                throw new ArgumentNullException("i", "Identity is null");
            if (!i.IsAuthenticated)
                throw new InvalidOperationException("User not authenticated");
            if (db == null)
                throw new ArgumentNullException("db", "Context is null");
            return db.CRM_USER.Where(u => u.IDENTITY_ID.Equals(i.Name)).SingleOrDefault();
        }
    }

    [MetadataType(typeof(addressMetadata))]
    public partial class ADDRESS
    {
        public CRMConnection DbConnection { get; set; }

        [Display(Name = "Address")]
        public string FullAddress
        {
            get
            {
                if (DbConnection == null)
                    return "";
                return DbConnection.ADDRESS_FULL_V.Where(x => x.ADDRESS_ID == this.ID).First().FullAddress;
            }
        }
    }

    [MetadataType(typeof(address_buildingMetadata))]
    public partial class ADDRESS_BUILDING
    {
    }
    [MetadataType(typeof(address_streetMetadata))]
    public partial class ADDRESS_STREET
    {
    }

    [MetadataType(typeof(address_cityMetadata))]
    public partial class ADDRESS_CITY
    {
    }

    [MetadataType(typeof(address_countyMetadata))]
    public partial class ADDRESS_COUNTY
    {
    }

    [MetadataType(typeof(address_provinceMetadata))]
    public partial class ADDRESS_PROVINCE
    {
    }

    [MetadataType(typeof(building_typeMetadata))]
    public partial class BUILDING_TYPE : IActivityLoggable
    {
        public long ActivityLogID { get => ID; }
        public Type ActivityLogType { get => typeof(BUILDING_TYPE); }
        public string StorageTable { get => "BUILDING_TYPE"; }

    }

    public partial class DEVICE_BRAND_MODEL : IActivityLoggable
    {
        public long ActivityLogID { get => ID; }
        public Type ActivityLogType { get => typeof(DEVICE_BRAND_MODEL); }
        public string StorageTable { get => "DEVICE_BRAND_MODEL"; }

    }

    [MetadataType(typeof(mtm_avalil_techMetadata))]
    public partial class MTM_AVAIL_TECH_ON_BUILDING
    {
    }



    [MetadataType(typeof(task_typeMetadata))]
    public partial class TASK_TYPE : IActivityLoggable
    {
        public long ActivityLogID { get => ID; }
        public Type ActivityLogType { get => typeof(TASK_TYPE); }
        public string StorageTable { get => "TASK_TYPE"; }

    }

    [MetadataType(typeof(task_subtypeMetadata))]
    public partial class TASK_SUBTYPE : IActivityLoggable
    {
        public long ActivityLogID { get => ID; }
        public Type ActivityLogType { get => typeof(TASK_SUBTYPE); }
        public string StorageTable { get => "TASK_SUBTYPE"; }

    }

    [MetadataType(typeof(task_statusMetadata))]
    public partial class TASK_STATUS : IActivityLoggable
    {
        public long ActivityLogID { get => ID; }
        public Type ActivityLogType { get => typeof(TASK_STATUS); }
        public string StorageTable { get => "TASK_STATUS"; }

    }

    [MetadataType(typeof(basketMetadata))]
    public partial class BASKET : IActivityLoggable
    {
        public long ActivityLogID { get => ID; }
        public Type ActivityLogType { get => typeof(BASKET); }
        public string StorageTable { get => "BASKET"; }
    }

    [MetadataType(typeof(queueMetadata))]
    public partial class QUEUE : IActivityLoggable
    {
        public long ActivityLogID { get => ID; }
        public Type ActivityLogType { get => typeof(QUEUE); }
        public string StorageTable { get => "QUEUE"; }
    }

    [MetadataType(typeof(tasksMetadata))]
    public partial class TASKS : IActivityLoggable
    {
        public long ActivityLogID { get => ID; }
        public Type ActivityLogType { get => typeof(TASKS); }
        public string StorageTable { get => "TASKS"; }
    }

    [MetadataType(typeof(task_paramMetadata))]
    public partial class TASK_PARAM
    {

    }

    [MetadataType(typeof(task_movement_historyMetadata))]
    public partial class TASK_MOVEMENT_HISTORY
    {

    }

    [MetadataType(typeof(task_param_definitionMetadata))]
    public partial class TASK_PARAM_DEFINITION : IActivityLoggable
    {
        public long ActivityLogID { get => ID; }
        public Type ActivityLogType { get => typeof(TASK_PARAM_DEFINITION); }
        public string StorageTable { get => "TASK_PARAM_DEFINITION"; }

    }

    public partial class TASK_PARAM_VALUE_LIST : IActivityLoggable
    {
        public long ActivityLogID { get => ID; }
        public Type ActivityLogType { get => typeof(TASK_PARAM_VALUE_LIST); }
        public string StorageTable { get => "TASK_PARAM_VALUE_LIST"; }

    }


    [MetadataType(typeof(technologyMetadata))]
    public partial class TECHNOLOGY : IActivityLoggable
    {
        public long ActivityLogID { get => ID; }
        public Type ActivityLogType { get => typeof(TECHNOLOGY); }
        public string StorageTable { get => "TECHNOLOGY"; }


    }

    [MetadataType(typeof(mtm_paramdef_tasktypeMetadata))]
    public partial class MTM_PARAMDEF_TASKTYPE : IActivityLoggable
    {
        public long ActivityLogID { get => ID; }
        public Type ActivityLogType { get => typeof(MTM_PARAMDEF_TASKTYPE); }
        public string StorageTable { get => "MTM_PARAMDEF_TASKTYPE"; }

    }

    [MetadataType(typeof(interactInfoMetadata))]
    public partial class INTERACT_INFO_V
    {

    }

    [MetadataType(typeof(device_typeMetadata))]
    public partial class DEVICE_TYPE : IActivityLoggable
    {
        public long ActivityLogID { get => ID; }
        public Type ActivityLogType { get => typeof(DEVICE_TYPE); }
        public string StorageTable { get => "DEVICE_TYPE"; }

    }

    [MetadataType(typeof(DEVICEMetadata))]
    public partial class DEVICE : IActivityLoggable
    {
        public long ActivityLogID { get => ID; }
        public Type ActivityLogType { get => typeof(DEVICE); }
        public string StorageTable { get => "DEVICE"; }
    }

    public partial class ACTIVITY_LOG
    {
        public static string EntryTypeFull(char entryType)
        {
            switch (entryType)
            {
                case 'A': return "Added";
                case 'M': return "Modified";
                case 'D': return "Deleted";
            }
            throw new ArgumentException("Invalid entry type");
        }

        public static void PrepareForDisplay(dynamic ViewBag, IActivityLoggable loggable)
        {
            ViewBag.ActivityLogTable = loggable.StorageTable;
            ViewBag.ActivityLogID = loggable.ActivityLogID;
        }
    }

    public partial class CRMAPP_ACCLIST_BASE
    {
        public void RefreshSQL(CRMConnection db)
        {
            db.CRMAPP_ACCLIST_P_SAVE_SQL_FOR_BASE(this.ID);
        }
    }

    public partial class CRMConnection : DbContext
    {
        public int SaveChangesLog(string userLogin, string param_1 = null, string param_2 = null)
        {
            int res;
            List<DbEntityEntry> addedEntries = new List<DbEntityEntry>();
            var entries = this.ChangeTracker.Entries();
            long entryID = GetEntryID();
            // Added are handled later because we want record ID to be already set
            foreach (var ent in entries)
            {
                if (ent.Entity is IActivityLoggable) {
                    if (ent.State == EntityState.Modified)
                        this.ACTIVITY_LOG.AddRange(GetAuditRecordsForChange(entryID, ent, userLogin, param_1, param_2));
                    else if (ent.State == EntityState.Added)
                        addedEntries.Add(ent);
                    else if (ent.State == EntityState.Deleted)
                        this.ACTIVITY_LOG.AddRange(GetAuditRecordsForChange(entryID, ent, userLogin, param_1, param_2));
                }
            }
                
            base.SaveChanges();

            foreach (var ent in addedEntries) {
                this.ACTIVITY_LOG.AddRange(GetAuditRecordsForInsert(entryID, ent, userLogin, param_1, param_2));
            }
            
            res = base.SaveChanges();

            return res;
        }

        private long GetEntryID()
        {
            string sql = "select activity_log_entry_id_seq.nextval from dual";
            var cmd = this.Database.Connection.CreateCommand() as OracleCommand;
            cmd.CommandText = sql;
            if (this.Database.Connection.State != ConnectionState.Open)
                this.Database.Connection.Open();
            long result = Convert.ToInt64(cmd.ExecuteScalar());
            Debug.WriteLine("Activity log entry id from sequence: " + result);
            return result;
        }

        private List<ACTIVITY_LOG> GetAuditRecordsForInsert(long entryID, DbEntityEntry dbEntry, string userLogin, string param_1 = null, string param_2 = null)
        {
            List<ACTIVITY_LOG> result = new List<ACTIVITY_LOG>();
            DateTime changeTime = DateTime.Now;
            string tableName = ((IActivityLoggable)dbEntry.Entity).StorageTable;
            long recordID = ((IActivityLoggable)dbEntry.Entity).ActivityLogID;

            foreach (string propertyName in dbEntry.CurrentValues.PropertyNames)
            {
                var currentValue = dbEntry.CurrentValues.GetValue<object>(propertyName);
                result.Add(new ACTIVITY_LOG()
                {
                    ENTRY_TYPE = "A",
                    ENTRY_ID = entryID,
                    REFERENCED_TABLE = tableName,
                    REFERENCED_ID = recordID,
                    REFERENCED_FIELD = propertyName,
                    VALUE_BEFORE = null,
                    VALUE_AFTER = currentValue == null ? null : currentValue.ToString(),
                    USER_LOGIN = userLogin,
                    ENTRY_TIME = changeTime,
                    PARAM_1 = param_1,
                    PARAM_2 = param_2
                });
            }
            
            return result;
        }

        private List<ACTIVITY_LOG> GetAuditRecordsForChange(long entryID, DbEntityEntry dbEntry, string userLogin, string param_1 = null, string param_2 = null)
        {
            List<ACTIVITY_LOG> result = new List<ACTIVITY_LOG>();
            DateTime changeTime = DateTime.Now;
            string tableName = ((IActivityLoggable)dbEntry.Entity).StorageTable;
            long recordID = ((IActivityLoggable)dbEntry.Entity).ActivityLogID;

            if (dbEntry.State == EntityState.Added)
            {
                // this should be handled in the other function
            }
            else if (dbEntry.State == EntityState.Deleted)
            {
                foreach (string propertyName in dbEntry.OriginalValues.PropertyNames)
                {
                    var originalValue = dbEntry.GetDatabaseValues().GetValue<object>(propertyName);
                    result.Add(new ACTIVITY_LOG()
                    {
                        ENTRY_TYPE = "D",
                        ENTRY_ID = entryID,
                        REFERENCED_TABLE = tableName,
                        REFERENCED_ID = recordID,
                        REFERENCED_FIELD = propertyName,
                        VALUE_BEFORE = originalValue == null ? null : originalValue.ToString(),
                        VALUE_AFTER = null,
                        USER_LOGIN = userLogin,
                        ENTRY_TIME = changeTime,
                        PARAM_1 = param_1,
                        PARAM_2 = param_2
                    });
                }
            }
            else if (dbEntry.State == EntityState.Modified)
            {
                foreach (string propertyName in dbEntry.OriginalValues.PropertyNames)
                {
                    // For updates, we only want to capture the columns that actually changed
                    var originalValue = dbEntry.GetDatabaseValues().GetValue<object>(propertyName);
                    var currentValue = dbEntry.CurrentValues.GetValue<object>(propertyName);
                    var isModified = !object.Equals(originalValue, currentValue);
                    if (isModified)
                    {
                        result.Add(new ACTIVITY_LOG()
                        {
                            ENTRY_TYPE = "M",
                            ENTRY_ID = entryID,
                            REFERENCED_TABLE = tableName,
                            REFERENCED_ID = recordID,
                            REFERENCED_FIELD = propertyName,
                            VALUE_BEFORE = originalValue == null ? null : originalValue.ToString(),
                            VALUE_AFTER = currentValue == null ? null : currentValue.ToString(),
                            USER_LOGIN = userLogin,
                            ENTRY_TIME = changeTime,
                            PARAM_1 = param_1,
                            PARAM_2 = param_2
                        });
                    }
                }
            }
            // Otherwise, don't do anything, we don't care about Unchanged or Detached entities

            return result;
        }
        
        [Obsolete]
        // This method is obsolete since we are using IActivityLog interface now, however this code was SO HARD to find that deleting it is unacceptable
        public string GetTableName(Type type)
        {
            var metadata = ((IObjectContextAdapter)this).ObjectContext.MetadataWorkspace;

            // Get the part of the model that contains info about the actual CLR types
            var objectItemCollection = ((ObjectItemCollection)metadata.GetItemCollection(DataSpace.OSpace));

            // Get the entity type from the model that maps to the CLR type
            var entityType = metadata
                    .GetItems<EntityType>(DataSpace.OSpace)
                          .Single(e => objectItemCollection.GetClrType(e) == type);

            // Get the entity set that uses this entity type
            var entitySet = metadata
                .GetItems<EntityContainer>(DataSpace.CSpace)
                      .Single()
                      .EntitySets
                      .Single(s => s.ElementType.Name == entityType.Name);

            // Find the mapping between conceptual and storage model for this entity set
            var mapping = metadata.GetItems<EntityContainerMapping>(DataSpace.CSSpace)
                          .Single()
                          .EntitySetMappings
                          .Single(s => s.EntitySet == entitySet);

            // Find the storage entity set (table) that the entity is mapped
            var tableEntitySet = mapping
                .EntityTypeMappings.Single()
                .Fragments.Single()
                .StoreEntitySet;

            // Return the table name from the storage entity set
            var tableName = tableEntitySet.MetadataProperties["Table"].Value ?? tableEntitySet.Name;
            
            return tableName.ToString();
        }
    }
    [MetadataType(typeof(CLIENT_SEARCH_VMetada))]
    public partial class CLIENT_SEARCH_V
    {

    }

    [MetadataType(typeof(ACCOUNT_INVOICES_VMetadata))]
    public partial class ACCOUNT_INVOICES_V
    {

    }

    [MetadataType(typeof(ACCOUNT_INVOICE_SUMMARY_VMetadata))]
    public partial class ACCOUNT_INVOICE_SUMMARY_V
    {
        public decimal Balance
        {
            get
            {
                return (decimal)(this.PAID_AMOUNT - this.TOTAL);
            }
        }

        public bool IsOverdue
        {
            get
            {
                if (Balance < -50 && MIN_DUE_UNPAID != null)
                {
                    DateTime firstDue = (DateTime)this.MIN_DUE_UNPAID;
                    return firstDue.AddMonths(3) < DateTime.Today;
                }
                return false;
            }
        }
    }

    [MetadataType(typeof(CRMAPP_JM_JOBS_INFOMetadata))]
    public partial class CRMAPP_JM_JOBS_INFO
    {

    }

    [MetadataType(typeof(CRMAPP_JM_TRG_INFOMetadata))]
    public partial class CRMAPP_JM_TRG_INFO
    {

    }

    [MetadataType(typeof(address_building_infoMetadata))]
    public partial class ADDRESS_BUILDING_INFO_V
    {

    }

    [MetadataType(typeof(DEVICE_BRAND_MODELMetadata))]
    public partial class DEVICE_BRAND_MODEL
    {

    }

    [MetadataType(typeof(CRMAPP_ACCLIST_BASEMetadata))]
    public partial class CRMAPP_ACCLIST_BASE
    {

    }

    [MetadataType(typeof(INSTALLER_DEVICES_STATE_VMetadata))]
    public partial class INSTALLER_DEVICES_STATE_V
    {

    }

    [MetadataType(typeof(INSTALLER_TASKS_OVERVIEW_VMetadata))]
    public partial class INSTALLER_TASKS_OVERVIEW_V
    {

    }

    [MetadataType(typeof(INSTALLER_TODAY_TASKSMetadata))]
    public partial class INSTALLER_TODAY_TASKS
    {

    }

    [MetadataType(typeof(INSTALLER_PRODUCT_OFFER_VMetadata))]
    public partial class INSTALLER_PRODUCT_OFFER_V
    {

    }

    [MetadataType(typeof(INSTALLER_AVAIL_DEVICES_VMetadata))]
    public partial class INSTALLER_AVAIL_DEVICES_V
    {

    }

    namespace CRM_BTM.Models.ViewModels
    {
        [MetadataType(typeof(InvoiceDetailsViewModelMetadata))]
        public partial class InvoiceDetailsViewModel
        {

        }
    }
}

