﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM_BTM.Models
{
    public class ViewMessage
    {
        public string Message { get; set; }
        public Types MessageType { get; set; }

        public enum Types
        {
            Success,
            Error,
            Info,
            Warning
        }

        public static string TypesToBootstrapClass(Types t)
        {
            switch (t)
            {
                case Types.Success: return "alert-success";
                case Types.Error: return "alert-danger";
                case Types.Info: return "alert-info";
                case Types.Warning: return "alert-warning";
                default:
                    return "alert-info";
            }
        }

        public ViewMessage(string msg, Types mtype = Types.Info)
        {
            Message = msg;
            MessageType = mtype;
        }

        public static List<ViewMessage> handleTempData(TempDataDictionary vd)
        {
            var result = new List<ViewMessage>();
            if (vd["messageType"] != null && vd["messageText"] != null)
            {
                result.Add(new ViewMessage(vd["messageText"].ToString(), (Types)vd["messageType"]));
            }
            int i = 0;
            while (vd[String.Concat("messageType", i)] != null)
            {
                result.Add(new ViewMessage(vd[String.Concat("messageText", i)].ToString(), (Types)vd[String.Concat("messageType", i)]));
                i++;
            }
            return result;
        }

        public static void addMessage(TempDataDictionary tempData, string messageText, Types messageType = Types.Info)
        {
            int i = 0;
            while (tempData[String.Concat("messageType", i)] != null)
            {
                i++;
            }
            tempData[String.Concat("messageType", i)] = messageType;
            tempData[String.Concat("messageText", i)] = messageText;
        }
    }
}