﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_BTM.Models
{
    public class InstallerModel
    {
        public IEnumerable<INSTALLER_TASKS_OVERVIEW_V> tskOver { get; set; }

        public IEnumerable<INSTALLER_DEVICES_STATE_V> devOver { get; set; }

        public IEnumerable<INSTALLER_TODAY_TASKS> tskToday { get; set; }

        public List<INSTALLER_PRODUCT_OFFER_V> prds { get; set; }

        public INSTALLER_TODAY_TASKS installTask { get; set; }

        public IEnumerable<INSTALLER_AVAIL_DEVICES_V> availDev { get; set; }

        public InstallerModel()
        {

            tskOver = new List<INSTALLER_TASKS_OVERVIEW_V>();
            devOver = new List<INSTALLER_DEVICES_STATE_V>();
            tskToday = new List<INSTALLER_TODAY_TASKS>();
            prds = new List<INSTALLER_PRODUCT_OFFER_V>();
            installTask = new INSTALLER_TODAY_TASKS();
            availDev = new List<INSTALLER_AVAIL_DEVICES_V>();
        }
    }
}