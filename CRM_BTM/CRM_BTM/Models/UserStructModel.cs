﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_BTM.Models
{
    public class UserStructModel
    {
       public IEnumerable<CRM_USER> crmUserModel { get; set; }

        public IEnumerable<USERS_STRUCT> usersStructModel { get; set; }

        public IEnumerable<TEAMS> teamsModel{ get; set; }

        public IEnumerable<TEAM_LEADER2IDENTITY_V> team2leader { get; set; }

        public IEnumerable<SALES_STRUCTURE_V> salesStructure { get; set; }

        public IEnumerable<IDENTITY2USER_STRUCT_V> allUsers { get; set; }
        public int crmUserId { get; set; }
        public Nullable<long> managerStructid { get; set; }
        public Nullable<int> teamId { get; set; }
        public Nullable<int> teamLeaderId { get; set; }
        public TEAMS teamEdit { get; set; }
        public USERS_STRUCT userStcEdt { get; set; }
        public IDENTITY2USER_STRUCT_V userIdent { get; set; }
        public UserStructModel()
        {
            crmUserModel = new List<CRM_USER>();
            usersStructModel = new List<USERS_STRUCT>();
            teamsModel = new List<TEAMS>();
            salesStructure = new List<SALES_STRUCTURE_V>();
            allUsers = new List<IDENTITY2USER_STRUCT_V>();
            team2leader = new List<TEAM_LEADER2IDENTITY_V>();
            teamEdit = new TEAMS();
            userStcEdt = new USERS_STRUCT();
            userIdent = new IDENTITY2USER_STRUCT_V();
        }

    }
}