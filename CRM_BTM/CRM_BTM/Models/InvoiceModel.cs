﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_BTM.Models
{

    public class CorrectInvProds
    {
        public MTM_INVOICE_PRODUCT InvProd { get; set; }
        public decimal newVal { get; set; }

        public CorrectInvProds()
        {
            InvProd = new MTM_INVOICE_PRODUCT();
        }
    }

    public class CorrectInvOths
    {
        public INVOICE_OTHER_ITEMS InvItm { get; set; }
        public decimal newVal { get; set; }

        public CorrectInvOths()
        {
            InvItm = new INVOICE_OTHER_ITEMS();
        }
    }

    public class InvoiceModel
    {
        public INVOICE CurrentInvoice { get; set; }

        public List<MTM_INVOICE_PRODUCT> CurrInvProds { get; set; }

        public List<INVOICE_OTHER_ITEMS> CurrInvOths { get; set; }

        public INVOICE CorrectInvoice { get; set; }

        public List<CorrectInvProds> newInvProds { get; set; }

        public List<CorrectInvOths> newInvOths { get; set; }

        public InvoiceModel()
        {
            CurrentInvoice = new INVOICE();
            CurrInvProds = new List<MTM_INVOICE_PRODUCT>();
            CurrInvOths = new List<INVOICE_OTHER_ITEMS>();
            newInvProds = new List<CorrectInvProds>();
            newInvOths = new List<CorrectInvOths>();
        }

    }


}