﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_BTM.Models
{
    public class AddressBaseModel
    {
        public IEnumerable<ADDRESS_BUILDING> building { get; set; }
        public IEnumerable<ADDRESS_STREET> street { get; set; }
        public IEnumerable<ADDRESS_CITY> city { get; set; }
        public IEnumerable<ADDRESS_COUNTY> county { get; set; }
        public IEnumerable<ADDRESS_PROVINCE> province { get; set; }
        public IEnumerable<ADDRESS_BUILDING_INFO_V> building_info { get; set; }
        public IEnumerable<ADDRESS> address { get; set; }
        public IEnumerable<MTM_AVAIL_TECH_ON_BUILDING> availTechnologies { get; set; }
        public IEnumerable<ACCOUNT> account { get; set; }
        public List<string> postCodeList { get; set; }

        public long provinceId { get; set; }
        public long countyId { get; set; }
        public long cityId { get; set; }
        public long streetID { get; set; }
        public long buildingId { get; set; }
        public string postCodeIn { get; set; }

        public ADDRESS_COUNTY AddCounty_inst { get; set; }

        public ADDRESS_BUILDING_INFO_V buildingInfoDetail { get; set; }

        public HttpPostedFileBase FileUpload { get; set; } //imageFile
        public string FilePath { get; set; }
        public AddressBaseModel()
        {
            building = new List<ADDRESS_BUILDING>();
            street = new List<ADDRESS_STREET>();
            city = new List<ADDRESS_CITY>();
            county = new List<ADDRESS_COUNTY>();
            province = new List<ADDRESS_PROVINCE>();
            postCodeList = new List<string>();
            building_info = new List<ADDRESS_BUILDING_INFO_V>();
            address = new List<ADDRESS>();
            availTechnologies = new List<MTM_AVAIL_TECH_ON_BUILDING>();
            account = new List<ACCOUNT>();
            AddCounty_inst = new ADDRESS_COUNTY();
            postCodeIn = null;
           /* provinceId =0;
            countyId = 0;
            cityId = 0;
            streetID = 0;*/
        }
    }
}