//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CRM_BTM.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class JM_PROCEDURE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public JM_PROCEDURE()
        {
            this.JM_ACTION_LIST = new HashSet<JM_ACTION_LIST>();
            this.JM_ACTIONS_HISTORY = new HashSet<JM_ACTIONS_HISTORY>();
            this.JM_ACTIONS_STACK = new HashSet<JM_ACTIONS_STACK>();
        }
    
        public long ID { get; set; }
        public string PROCEDURE_NAME { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JM_ACTION_LIST> JM_ACTION_LIST { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JM_ACTIONS_HISTORY> JM_ACTIONS_HISTORY { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JM_ACTIONS_STACK> JM_ACTIONS_STACK { get; set; }
    }
}
