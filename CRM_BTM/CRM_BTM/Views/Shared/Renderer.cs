﻿using CRM_BTM.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM_BTM.Views.Shared
{
    public class Renderer
    {
        public static string Messages(IEnumerable<ViewMessage> msgs)
        {
            if (msgs == null) return "";
            string res = "";
            foreach (ViewMessage msg in msgs)
            {
                string bootstrapClass = ViewMessage.TypesToBootstrapClass(msg.MessageType);                
                res = res + "<p class=\"p-3 m-3 " + bootstrapClass + " \">" + msg.Message + "</p>\n";
            } 
            return res;
        }
    }
}