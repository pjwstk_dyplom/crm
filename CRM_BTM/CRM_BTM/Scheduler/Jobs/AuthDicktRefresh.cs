﻿using CRM_BTM.Models;
using Quartz;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using CustomAuth.Authentication;

namespace CRM_BTM.Scheduler.Jobs
{
    public class AuthDicktRefresh : IJob
    {
        private static int TakeLimit = 20;

        public Task Execute(IJobExecutionContext context)
        {
               new Thread(() => {
                   Debug.WriteLine("AuthDicktRefresh job started"); 
                   
                       try {
                       AuthPrincipal.RefreshAuthDict();
                       } catch (Exception e) {
                       Debug.WriteLine("AuthDicktRefresh job faild");
                   }
                   
                   Debug.WriteLine("SendMail job ended");
               }).Start();
            return Task.CompletedTask;
        }
    }
}