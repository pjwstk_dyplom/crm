﻿using CRM_BTM.Email;
using CRM_BTM.Models;
using Quartz;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace CRM_BTM.Scheduler.Jobs
{
    public class SendMailJob : IJob
    {
        private static int TakeLimit = 20;

        public Task Execute(IJobExecutionContext context)
        {
            new Thread(() => {
                Debug.WriteLine("SendMail job started"); 
                CRMConnection db = new CRMConnection();
                db.Database.Connection.Open();
                var emails = db.CRMAPP_EMAILS.Where(x => x.STATUS.Equals("NEW")).Take(TakeLimit).ToList();
                foreach (var email in emails)
                {
                    try {
                        MailControl.SendMail(email.RECIPIENTS, email.SUBJECT, email.MAILBODY);
                        email.PROCESSED_DATE = DateTime.Now;
                        email.STATUS = "SENT";
                        db.SaveChanges();
                    } catch (Exception e) {
                        email.PROCESSED_DATE = DateTime.Now;
                        email.STATUS = "ERROR";
                        email.FAIL_COUNT++;
                        email.CRM_COMMENT = (e.InnerException == null ? e.Message : e.InnerException.Message);
                        db.SaveChanges();
                    }
                }
                Debug.WriteLine("SendMail job ended");
            }).Start();
            
            return Task.CompletedTask;
        }
    }
}