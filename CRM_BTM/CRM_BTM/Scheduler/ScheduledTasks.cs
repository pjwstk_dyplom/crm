﻿using CRM_BTM.Models;
using CRM_BTM.Scheduler.Jobs;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_BTM.Scheduler
{
    public class ScheduledTasks
    {
        public static async void Start()
        {
            StdSchedulerFactory factory = new StdSchedulerFactory();
            IScheduler scheduler = await factory.GetScheduler();
            await scheduler.Start();
            
            IJobDetail job = JobBuilder.Create<SendMailJob>().Build();

            ITrigger trigger = TriggerBuilder.Create()
                .WithSimpleSchedule
                  (s => s.WithIntervalInMinutes(5)
                         .RepeatForever()
                  )
                .Build();

            await scheduler.ScheduleJob(job, trigger);

            IJobDetail jobDict = JobBuilder.Create<AuthDicktRefresh>().Build();

            ITrigger triggerDict = TriggerBuilder.Create()
                .WithSimpleSchedule
                  (s => s.WithIntervalInMinutes(15)
                         .RepeatForever()
                  )
                .Build();

            await scheduler.ScheduleJob(jobDict, triggerDict);
        }
    }
}