﻿namespace CRM_BTM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class role_metadata : DbMigration
    {
        public override void Up()
        {
            AlterColumn("IDENTITY.ROLES", "RoleName", c => c.String(nullable: false, maxLength: 100, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("IDENTITY.ROLES", "RoleName", c => c.String(maxLength: 100, unicode: false));
        }
    }
}
