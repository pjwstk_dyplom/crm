﻿namespace CRM_BTM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "IDENTITY.CLAIMS",
                c => new
                    {
                        ClaimId = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Category = c.String(nullable: false, maxLength: 100, unicode: false),
                        Operation = c.String(nullable: false, maxLength: 100, unicode: false),
                        Description = c.String(nullable: false, maxLength: 4000, unicode: false),
                    })
                .PrimaryKey(t => t.ClaimId);
            
            CreateTable(
                "IDENTITY.ROLES",
                c => new
                    {
                        RoleId = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        RoleName = c.String(maxLength: 100, unicode: false),
                    })
                .PrimaryKey(t => t.RoleId);
            
            CreateTable(
                "IDENTITY.USERS",
                c => new
                    {
                        UserId = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Username = c.String(maxLength: 100, unicode: false),
                        FirstName = c.String(maxLength: 100, unicode: false),
                        LastName = c.String(maxLength: 100, unicode: false),
                        Email = c.String(maxLength: 100, unicode: false),
                        Password = c.String(maxLength: 4000, unicode: false),
                        IsActive = c.Decimal(nullable: false, precision: 1, scale: 0),
                        ActivationCode = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "IDENTITY.USERROLES",
                c => new
                    {
                        UserId = c.Decimal(nullable: false, precision: 10, scale: 0),
                        RoleId = c.Decimal(nullable: false, precision: 10, scale: 0),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("IDENTITY.USERS", t => t.UserId, cascadeDelete: true)
                .ForeignKey("IDENTITY.ROLES", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "IDENTITY.ROLECLAIMS",
                c => new
                    {
                        ClaimId = c.Decimal(nullable: false, precision: 10, scale: 0),
                        RoleId = c.Decimal(nullable: false, precision: 10, scale: 0),
                    })
                .PrimaryKey(t => new { t.ClaimId, t.RoleId })
                .ForeignKey("IDENTITY.CLAIMS", t => t.ClaimId, cascadeDelete: true)
                .ForeignKey("IDENTITY.ROLES", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.ClaimId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "IDENTITY.USERCLAIMS",
                c => new
                    {
                        ClaimId = c.Decimal(nullable: false, precision: 10, scale: 0),
                        UserId = c.Decimal(nullable: false, precision: 10, scale: 0),
                    })
                .PrimaryKey(t => new { t.ClaimId, t.UserId })
                .ForeignKey("IDENTITY.CLAIMS", t => t.ClaimId, cascadeDelete: true)
                .ForeignKey("IDENTITY.USERS", t => t.UserId, cascadeDelete: true)
                .Index(t => t.ClaimId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("IDENTITY.USERCLAIMS", "UserId", "IDENTITY.USERS");
            DropForeignKey("IDENTITY.USERCLAIMS", "ClaimId", "IDENTITY.CLAIMS");
            DropForeignKey("IDENTITY.ROLECLAIMS", "RoleId", "IDENTITY.ROLES");
            DropForeignKey("IDENTITY.ROLECLAIMS", "ClaimId", "IDENTITY.CLAIMS");
            DropForeignKey("IDENTITY.USERROLES", "RoleId", "IDENTITY.ROLES");
            DropForeignKey("IDENTITY.USERROLES", "UserId", "IDENTITY.USERS");
            DropIndex("IDENTITY.USERCLAIMS", new[] { "UserId" });
            DropIndex("IDENTITY.USERCLAIMS", new[] { "ClaimId" });
            DropIndex("IDENTITY.ROLECLAIMS", new[] { "RoleId" });
            DropIndex("IDENTITY.ROLECLAIMS", new[] { "ClaimId" });
            DropIndex("IDENTITY.USERROLES", new[] { "RoleId" });
            DropIndex("IDENTITY.USERROLES", new[] { "UserId" });
            DropTable("IDENTITY.USERCLAIMS");
            DropTable("IDENTITY.ROLECLAIMS");
            DropTable("IDENTITY.USERROLES");
            DropTable("IDENTITY.USERS");
            DropTable("IDENTITY.ROLES");
            DropTable("IDENTITY.CLAIMS");
        }
    }
}
