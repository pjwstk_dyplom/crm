﻿namespace CRM_BTM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Usrmetadata : DbMigration
    {
        public override void Up()
        {
            AlterColumn("IDENTITY.USERS", "Username", c => c.String(nullable: false, maxLength: 100, unicode: false));
            AlterColumn("IDENTITY.USERS", "FirstName", c => c.String(nullable: false, maxLength: 100, unicode: false));
            AlterColumn("IDENTITY.USERS", "LastName", c => c.String(nullable: false, maxLength: 100, unicode: false));
            AlterColumn("IDENTITY.USERS", "Email", c => c.String(nullable: false, maxLength: 100, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("IDENTITY.USERS", "Email", c => c.String(maxLength: 100, unicode: false));
            AlterColumn("IDENTITY.USERS", "LastName", c => c.String(maxLength: 100, unicode: false));
            AlterColumn("IDENTITY.USERS", "FirstName", c => c.String(maxLength: 100, unicode: false));
            AlterColumn("IDENTITY.USERS", "Username", c => c.String(maxLength: 100, unicode: false));
        }
    }
}
