﻿namespace CRM_BTM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class USERSALT : DbMigration
    {
        public override void Up()
        {
            AddColumn("IDENTITY.USERS", "Salt", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("IDENTITY.USERS", "Salt");
        }
    }
}
