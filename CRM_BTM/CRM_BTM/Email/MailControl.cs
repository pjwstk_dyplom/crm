﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace CRM_BTM.Email
{
    public class MailControl
    {
        public static string User = "pjatk.dyplom.crm@gmail.com"; 
        private static string Password = "2019_Amm";
        private static string SmtpServer = "smtp.gmail.com";
        private static int SmtpPort = 587;

        public static void SendMail(string Recipients, string Subject, string Body)
        {
            if (Recipients == null) return;
            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress(User);
                foreach (var rec in Recipients.Split(';'))
                    mail.To.Add(rec.Trim());
                mail.Subject = Subject;
                mail.Body = Body;
                mail.IsBodyHtml = true;

                using (SmtpClient smtp = new SmtpClient(SmtpServer, SmtpPort))
                {
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new NetworkCredential(User, Password);
                    smtp.EnableSsl = true;
                    try
                    {
                        smtp.Send(mail);
                        Debug.WriteLine("Email sent to " + Recipients + " (" + Subject + ")");
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine("Email error. To " + Recipients + " (" + Subject + ") => " + (e.InnerException == null ? e.Message : e.InnerException.Message));
                    }
                }
            }
        }
    }
}