CREATE OR REPLACE VIEW account_invoices_v AS
SELECT
  i.id,
  i.invoice_number,
  i.total,
  i.creation_date,
  i.send_date,
  i.due_date,
  i.payment_date,
  i.paid_amount,
  i.account_id,
  par.invoice_number AS parent_invoice_number,
  it.name AS invoice_type,
  iss.name AS invoice_status
FROM account a
INNER JOIN invoice i
  ON i.account_id = a.id
INNER JOIN invoice_status iss
  ON iss.id = i.invoice_status_id
INNER JOIN invoice_type it
  ON it.id = i.invoice_type_id
LEFT JOIN invoice par
  ON par.id = i.parent_invoice;
