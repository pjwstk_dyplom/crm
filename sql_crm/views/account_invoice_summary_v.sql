CREATE OR REPLACE VIEW account_invoice_summary_v AS
SELECT
  a.id AS account_id,
  a.client_id,
  NVL(COUNT(i.id), 0) AS invoice_count,
  NVL(SUM(i.total), 0) AS total,
  NVL(SUM(i.paid_amount), 0) AS paid_amount,
  MIN(CASE WHEN i.paid_amount < i.total THEN i.due_date END) AS min_due_unpaid
FROM account a
LEFT JOIN invoice i
  ON i.account_id = a.id
WHERE NVL(i.invoice_status_id, 0) <> 5 -- annulled
GROUP BY
  a.id,
  a.client_id;