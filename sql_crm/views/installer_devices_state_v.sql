CREATE OR REPLACE FORCE EDITIONABLE VIEW "CRM"."INSTALLER_DEVICES_STATE_V" (
    "USER_ID",
    "IDENTITY_ID",
    "DEVICE_TYPE",
    "DBM_ID",
    "BRAND",
    "MODEL",
    "REQUIRED_TODAY",
    "PICKED",
    "ASSIGNED"
) AS
    WITH picked AS (
        SELECT cu.id            user_id,
               cu.identity_id,
               dt.description   device_type,
               dbm.id           dev_id,
               dbm.brand,
               dbm.model,
               nvl(COUNT(DISTINCT d.id), 0) - nvl(SUM(
                   CASE
                       WHEN p.device_id IS NOT NULL THEN
                           1
                       ELSE
                           0
                   END
               ), 0) picked,
               SUM(
                   CASE
                       WHEN p.device_id IS NOT NULL THEN
                           1
                       ELSE
                           0
                   END
               ) assigned
        FROM device               d
        INNER JOIN crm_user             cu ON d.crm_user_id = cu.id
        INNER JOIN device_brand_model   dbm ON d.device_brand_model_id = dbm.id
        INNER JOIN device_type          dt ON dbm.device_type_id = dt.id
        LEFT JOIN product              p ON d.id = p.device_id
        GROUP BY cu.id,
                 cu.identity_id,
                 dt.description,
                 dbm.id,
                 dbm.brand,
                 dbm.model
    ), need AS (
        SELECT cu.id            user_id,
               cu.identity_id,
               dt.description   device_type,
               dbm.id           dev_id,
               dbm.brand,
               dbm.model,
               COUNT(t.id) today_ammount
        FROM tasks                  t
        INNER JOIN task_type              tskt ON tskt.id = t.task_type_id
                                     AND tskt.name = 'installation'
        INNER JOIN basket                 b ON t.basket_id = b.id
        INNER JOIN crm_user               cu ON b.user_id = cu.id
        INNER JOIN mtm_task_product       mtp ON t.id = mtp.task_id
        INNER JOIN mtm_offer_product      mop ON mtp.product_id = mop.product_id
                                            AND trunc(t.creation_date, 'dd') = trunc(mop.valid_from, 'dd')
        INNER JOIN product                p ON mtp.product_id = p.id
        INNER JOIN product_definition     pd ON p.product_definition_id = pd.id
        INNER JOIN offer                  o ON mop.offer_id = o.id
        INNER JOIN offer_option_product   oop ON o.offer_option_id = oop.offer_option_id
                                               AND oop.product_definition_id = pd.id
        INNER JOIN device_brand_model     dbm ON oop.device_brand_model_id = dbm.id
        INNER JOIN device_type            dt ON dbm.device_type_id = dt.id
        WHERE trunc(t.due_date, 'dd') = trunc(sysdate, 'dd')
        GROUP BY cu.id,
                 cu.identity_id,
                 dt.description,
                 dbm.id,
                 dbm.brand,
                 dbm.model
    ), matrix AS (
        SELECT user_id,
               identity_id,
               device_type,
               dev_id,
               brand,
               model
        FROM picked
        UNION
        SELECT user_id,
               identity_id,
               device_type,
               dev_id,
               brand,
               model
        FROM need
    )
    SELECT m.user_id,
           cu.identity_id,
           m.device_type,
           dbm.id dbm_id,
           m.brand,
           m.model,
           nvl(n.today_ammount, 0) - nvl(p.assigned, 0) required_today,
           nvl(p.picked, 0) picked,
           nvl(p.assigned, 0) assigned
    FROM matrix               m
    inner JOIN crm_user             cu ON m.user_id = cu.id
    inner JOIN device_brand_model   dbm ON dbm.id = m.dev_id
    left JOIN need                 n ON m.user_id = n.user_id
                         AND m.identity_id = n.identity_id
                         AND m.dev_id = n.dev_id
                         AND m.device_type = n.device_type
                         AND m.brand = n.brand
                         AND m.model = n.model
    LEFT JOIN picked               p ON m.user_id = p.user_id
                          AND m.identity_id = p.identity_id
                          AND m.dev_id = p.dev_id
                          AND m.device_type = p.device_type
                          AND m.brand = p.brand
                          AND m.model = p.model;