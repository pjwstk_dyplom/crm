CREATE OR REPLACE VIEW address_full_v AS
SELECT 
  a.id AS address_id,
  b.id AS building_id,
  s.id AS street_id,
  c.id AS city_id,
  co.id AS county_id,
  p.id AS province_id,
  a.flat,
  b.building_number,
  b.post_code,
  s.name AS street,
  c.name AS city,
  co.name AS county,
  p.name AS province
FROM address a
INNER JOIN address_building b
  ON a.address_building_id = b.id
INNER JOIN address_street s
  ON b.address_street_id = s.id
INNER JOIN address_city c
  ON s.address_city_id = c.id
INNER JOIN address_county co
  ON c.address_county_id = co.id
INNER JOIN address_province p
  ON co.address_province_id = p.id;