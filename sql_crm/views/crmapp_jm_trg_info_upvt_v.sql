CREATE OR REPLACE VIEW crmapp_jm_trg_info_upvt_v AS

SELECT 
  id || '-' || status AS pkey,
  id AS trigger_id,
  status,
  quantity
FROM crmapp_jm_trg_info
UNPIVOT(
  quantity
  FOR status IN (
    is_new AS 'Prepared',
    cancelled AS 'Cancelled',
    running AS 'Running',
    had_error AS 'Error',
    finished AS 'Fisnihed'
  ) 
);