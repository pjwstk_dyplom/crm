Create or Replace view client_search_v as
Select cl.id as ID,
       c.contract_number,
       cl.name,
       cl.lastname,
       cl.pesel,
       cl.nip,
       i.invoice_number,
       ac.name   as city,
       ass.name  as Street,
       ab.building_number,
       ad.flat,
       UPPER(ass.name || ' ' || ab.building_number || '/' || ad.flat || ', ' || ac.name)  as Adress,
       a.creation_date,
       ac.id    as cityId,
       ass.id   as streetId,
       ab.id    as buildingId,
       ad.id    as flatId      
 from account a
 left join contract c 
   on c.account_id = a.id
 left join client cl
   on cl.id = a.client_id
 left join address ad
   on ad.id = a.billing_address_id
 left join address_building ab
   on ab.id = ad.address_building_id
 left join address_street ass
   on ass.id = ab.address_street_id
 left join address_city ac
   on ac.id = ass.address_city_id
 left join address_county aco
   on aco.id = ac.address_county_id
 left join invoice i
   on i.account_id = a.id;