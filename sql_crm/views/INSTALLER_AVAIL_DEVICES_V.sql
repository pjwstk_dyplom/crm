create view INSTALLER_AVAIL_DEVICES_V as 
SELECT 
    cu.id user_id,
    cu.identity_id,
    dt.description device_type,
    dbm.id dev_mod_id,
    dbm.brand,
    dbm.model,
    d.id device_id,
    d.serial,
    d.mac
FROM device d
inner join crm_user            cu on d.crm_user_id= cu.id
inner join device_brand_model dbm on d.device_brand_model_id= dbm.id
inner join device_type         dt on dbm.device_type_id=dt.id 
left  join product              p on d.id = p.device_id
where 
    p.id is null