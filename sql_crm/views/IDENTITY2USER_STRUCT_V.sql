CREATE VIEW IDENTITY2USER_STRUCT_V AS 
SELECT
 u."UserId" IDENTITY_ID
,u."FirstName" FIRST_NAME
,u."LastName"   LAST_NAME
,u."Username" USERNAME
,cu.id USER_ID
,cu.hr_id HR_ID
,us.id STRUCT_ID
,us.valid_from USER_STRUCT_FROM
,us.valid_to   USER_STRUCT_TO
FROM IDENTITY.users u
inner join crm.crm_user cu on u."Username" = cu.identity_id
left join crm.users_struct  us on cu.id=us.user_id