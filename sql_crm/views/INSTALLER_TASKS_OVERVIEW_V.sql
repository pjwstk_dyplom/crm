create or replace view INSTALLER_TASKS_OVERVIEW_V as
with base as (
    SELECT
        b.user_id, 
        cu.identity_id,
        CASE WHEN trunc(t.DUE_DATE,'dd') = trunc(sysdate,'dd') THEN 'TODAY'
             WHEN trunc(t.DUE_DATE,'dd') = trunc(sysdate,'dd')+1 THEN 'TOMORROW' 
             ELSE 'NEXT 7 DAYS' end DUE_TIME,
        ts.name task_status,
        CASE WHEN trunc(t.DUE_DATE,'dd') = trunc(sysdate,'dd') THEN 1
             WHEN trunc(t.DUE_DATE,'dd') = trunc(sysdate,'dd')+1 THEN 2
             ELSE 3 end due_date_order,
        t.ID
    FROM tasks  t 
    inner join task_type            tskt on tskt.id = t.task_type_id
                                        and tskt.name= 'installation'
    inner join basket                  b on t.basket_id=b.id
    inner join crm_user               cu on cu.id = b.user_id
    inner join task_status            ts on t.task_status_id = ts.id
    where 
    t.due_date between trunc(sysdate,'dd') and trunc(sysdate,'dd')+7
),

piv as (
    select
        USER_ID,
        IDENTITY_ID,
        DUE_TIME,
        DUE_DATE_ORDER,	
        NEW,
        CONFIRMED,	
        DEFERRED,	
        COMPLETED,	
        CANCELLED
    from base 
    PIVOT 
    (count(id)
     FOR task_status
        IN ('new' "NEW",
            'confirmed' "CONFIRMED",
            'deferred' "DEFERRED" ,
            'completed' "COMPLETED",
            'cancelled' "CANCELLED"))
    order by due_date_order), 

 mvc_fix AS (
        SELECT user_id,
               identity_id,
               DUE_TIME,
               id max_task_id
        FROM (
            SELECT user_id,
                   identity_id,
                   DUE_TIME,
                   id,
                   ROW_NUMBER() OVER(
                       PARTITION BY user_id, identity_id, DUE_TIME
                       ORDER BY id DESC
                   ) rnum
            FROM base
        )
        WHERE rnum = 1
    )

Select
    piv.USER_ID,
    piv.IDENTITY_ID,
    piv.DUE_TIME,
    piv.DUE_DATE_ORDER,	
    piv.NEW,
    piv.CONFIRMED,	
    piv.DEFERRED,	
    piv.COMPLETED,	
    piv.CANCELLED,
    MVC_FIX.max_task_id
from piv
INNER JOIN MVC_FIX ON MVC_FIX.user_id = PIV.user_id
                  AND MVC_FIX.identity_id = PIV.identity_id
                  AND MVC_FIX.DUE_TIME = PIV.DUE_TIME

