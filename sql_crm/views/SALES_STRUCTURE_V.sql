create view crm.SALES_STRUCTURE_V as 
SELECT 
     cu.id crm_user_id
    ,cu.identity_id Username
    ,us.id usr_stc_id
    ,u."FirstName"  FirstName
    ,u."LastName"   LastName
    ,u."UserId"     IdentityId
    ,us.manager_id usr_stc_manager
    ,cu_mng.identity_id manager_username
    ,u_mng."FirstName"  FirstName_Manager
    ,u_mng."LastName"   LastName_Manager
    ,u_mng."UserId"     IdentityId_Manager
    ,sc.id sale_code_id
    ,sc.code
    ,tm.description Team
    ,tm.id team_id
    ,tm.team_leader_id usr_stc_team_leader_id
    ,cu_tm_ld.identity_id team_leader_identity_id
    ,u_tm_ld."FirstName"  FirstName_tm_leader
    ,u_tm_ld."LastName"   LastName_tm_leader
    ,u_tm_ld."UserId"     IdentityId_tm_leader
    
FROM crm.crm_user cu 
    inner join crm.users_struct us on cu.id = us.user_id 
                                    and  sysdate between us.valid_from and us.valid_to
    
    left join crm.users_struct us_mng on us.manager_id = us_mng.id
                                    and  sysdate between us_mng.valid_from and us_mng.valid_to    
    left join crm.crm_user     cu_mng on us_mng.user_id = cu_mng.id
                                            
    left join crm.sale_code    sc on us.id=sc.sales_struct_id 
                                and sysdate between sc.valid_from and sc.valid_to
                                
    Left join crm.teams        tm on sc.teams_id=tm.id
    left join crm.users_struct us_tm_ld on tm.team_leader_id = us_tm_ld.id
                                    and  sysdate between us_tm_ld.valid_from and us_tm_ld.valid_to    
    left join crm.crm_user     cu_tm_ld on us_tm_ld.user_id = cu_tm_ld.id
    
    left join IDENTITY.users u on cu.identity_id = u."Username"
    left join IDENTITY.users u_mng on cu_mng.identity_id = u_mng."Username"
    left join IDENTITY.users u_tm_ld on cu_tm_ld.identity_id = u_tm_ld."Username";
    
     
 
