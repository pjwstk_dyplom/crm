CREATE OR REPLACE VIEW account_promo_value_v AS
SELECT
  o.account_id,
  SUM(sch.price) AS promo_price
FROM offer o 
INNER JOIN mtm_offer_product ofpr
  ON ofpr.offer_id = o.id
INNER JOIN product pr
  ON pr.id = ofpr.product_id
INNER JOIN offer_option oo
  ON oo.id = o.offer_option_id
INNER JOIN offer_option_product oop
  ON oop.offer_option_id = oo.id
  AND oop.product_definition_id = pr.product_definition_id
INNER JOIN offer_opt_prod_schedule sch
  ON sch.offer_option_product_id = oop.id
WHERE 1=1
  AND ofpr.valid_to > SYSDATE
  AND sch.mth_num >= TRUNC(MONTHS_BETWEEN(SYSDATE, pr.activation_date))-1
GROUP BY
  o.account_id;