CREATE OR REPLACE VIEW your_tasks_movements_v AS
WITH 
  q_task_base AS (
    SELECT 
      tmh.task_id,
      b.user_id
    FROM task_movement_history tmh
    INNER JOIN basket b
      ON b.id = tmh.basket_id
    WHERE 1=1
      AND tmh.basket_id IS NOT NULL
      AND tmh.move_date >= TRUNC(ADD_MONTHS(SYSDATE, -1))
      
    UNION
    
    SELECT
      t.ID AS task_id,
      t.user_id_author AS user_id
    FROM tasks t
    WHERE 1=1
      AND t.creation_date >= TRUNC(ADD_MONTHS(SYSDATE, -1))
  )
  
  SELECT 
    t.id AS task_id,
    u.id AS user_id,
    taty.name AS task_type,
    tasu.name AS task_subtype,
    tast.name AS task_status,
    q.name AS queue_name
  FROM q_task_base tb
  INNER JOIN tasks t
    ON t.id = tb.task_id
  INNER JOIN crm_user u
    ON u.id = tb.user_id
  INNER JOIN task_type taty
    ON taty.id = t.task_type_id
  INNER JOIN task_subtype tasu
    ON tasu.id = t.task_subtype_id
  INNER JOIN task_status tast
    ON tast.id = t.task_status_id
  INNER JOIN queue q
    ON q.id = t.queue_id
  ;