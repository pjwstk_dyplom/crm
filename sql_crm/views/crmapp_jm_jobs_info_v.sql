CREATE OR REPLACE VIEW crmapp_jm_jobs_info_v AS

WITH
  q_last_error AS (
    SELECT
      rd.job_name,
      log_date,
      rd.error#,
      rd.errors
    FROM user_scheduler_job_run_details rd
    WHERE rd.job_name LIKE 'JOB_MACHINE%'
      AND rd.status = 'FAILED'
  ),
  
  q_recent AS (
    SELECT
      rd.job_name,
      COUNT(1) launch_count,
      SYSDATE + (NUMTODSINTERVAL(SUM(EXTRACT(DAY FROM rd.cpu_used*1000*60*60*24)) / 1000, 'SECOND') * 86400) - SYSDATE AS cpu_used_s,
      SYSDATE + (NUMTODSINTERVAL(SUM(EXTRACT(DAY FROM rd.run_duration*1000*60*60*24)) / 1000, 'SECOND') * 86400) - SYSDATE AS run_duration_s,
      SUM(CASE WHEN rd.status = 'FAILED' THEN 1 ELSE 0 END) failure_count
    FROM user_scheduler_job_run_details rd
    WHERE rd.job_name LIKE 'JOB_MACHINE%'
      AND log_date >= SYSDATE - INTERVAL '15' MINUTE
    GROUP BY
      rd.job_name
  )

SELECT 
  j.job_name,
  j.job_action,
  j.repeat_interval,
  j.enabled,
  j.run_count,
  j.failure_count,
  CAST(j.last_start_date AS DATE) AS last_start_date,
  CAST(j.next_run_date AS DATE) AS next_run_date,
  CAST(le.log_date AS DATE) AS last_error_date,
  le.error# AS last_error_code,
  le.errors AS last_error_message,
  NVL(re.launch_count, 0) AS recent_run_count,
  NVL(re.cpu_used_s, 0) AS recent_cpu_s,
  NVL(re.run_duration_s, 0) AS recent_duration_s,
  NVL(re.failure_count, 0) AS recent_failures,
  re.cpu_used_s / NULLIF(re.launch_count, 0) AS recent_avg_cpu_s,
  re.run_duration_s / NULLIF(re.launch_count, 0) AS recent_avg_duration_s
FROM user_scheduler_jobs j 
LEFT JOIN q_last_error le
  ON le.job_name = j.job_name
LEFT JOIN q_recent re
  ON re.job_name = j.job_name
WHERE j.job_name LIKE 'JOB_MACHINE%';