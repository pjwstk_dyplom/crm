create view TEAM_LEADER2IDENTITY_V as 
SELECT  
tm.description
,tm.team_code teamCode
,tm.id teamId
,u."FirstName" leaderFirstName
,u."LastName" leaderLastName
,us.id leaderStructId
,cu.id leaderCrmId
,u."UserId" leaderIdentityId
FROM crm.teams tm
inner join crm.users_struct us on tm.team_leader_id=us.id
inner join crm.crm_user cu on us.user_id=cu.id
inner join IDENTITY.users u on cu.identity_id=u."Username"
