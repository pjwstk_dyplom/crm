CREATE OR REPLACE VIEW YOUR_TASKS_LIST_V AS
SELECT 
  usr.id AS user_id,
  usr.identity_id AS identity_id,
  bas.name AS basket_name,
  tsk.id AS task_id,
  tsk.creation_date,
  tsk.due_date,
  tsk.close_date,
  taty.name AS task_type,
  tasu.name AS task_subtype,
  tast.name AS task_status,
  acco.id AS account_id,
  cl.id AS client_id,
  TRIM(cl.name || ' ' || cl.lastname) AS client_name
FROM crm_user usr
INNER JOIN basket bas
  ON bas.user_id = usr.id
INNER JOIN tasks tsk
  ON tsk.basket_id = bas.id
INNER JOIN task_type taty
  ON taty.id = tsk.task_type_id
INNER JOIN task_subtype tasu
  ON tasu.id = tsk.task_subtype_id
INNER JOIN task_status tast
  ON tast.id = tsk.task_status_id
INNER JOIN account acco
  ON acco.id = tsk.account_id
INNER JOIN client cl
  ON cl.id = acco.client_id