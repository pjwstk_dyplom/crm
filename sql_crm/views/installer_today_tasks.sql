CREATE OR REPLACE VIEW "CRM"."INSTALLER_TODAY_TASKS" AS
    SELECT cu.id,
           cu.identity_id,
           to_char(t.due_date, 'yyyy-mm-dd hh:mi') due_date,
           ts.name               status,
           c.name
           || ' '
           || c.lastname client_name,
           adc.name
           || ' ['
           || adp.name
           || ', '
           || aco.name
           || ']' city,
           ads.name              street,
           adb.building_number   building_num,
           ad.flat               flat_num,
           LISTAGG(pc.description, ' | ') WITHIN GROUP(
               ORDER BY pc.id
           ) products,
           LISTAGG(dbm.brand
                   || ' - '
                   || dbm.model, ' | ') WITHIN GROUP(
               ORDER BY pc.id
           ) devices,
           t.id                  task_id,
           c.id                  client_id,
           a.id                  account_id,
           o.id                  offer_id
    FROM tasks                  t
    INNER JOIN task_type              tskt ON t.task_type_id = tskt.id
                                 AND tskt.name = 'installation'
    INNER JOIN task_status            tsts ON t.task_status_id = tsts.id
                                   AND tsts.name IN (
        'awaiting',
        'new',
        'confirmed',
        'client refused installation',
        'client unavailable',
        'no technical possibilities',
        'completed'
    )
    INNER JOIN account                a ON t.account_id = a.id
    INNER JOIN client                 c ON a.client_id = c.id
    INNER JOIN address                ad ON a.install_address_id = ad.id
    INNER JOIN address_building       adb ON ad.address_building_id = adb.id
    INNER JOIN address_street         ads ON adb.address_street_id = ads.id
    INNER JOIN address_city           adc ON ads.address_city_id = adc.id
    INNER JOIN address_county         aco ON adc.address_county_id = aco.id
    INNER JOIN address_province       adp ON aco.address_province_id = adp.id
    INNER JOIN basket                 b ON t.basket_id = b.id
    INNER JOIN crm_user               cu ON b.user_id = cu.id
    INNER JOIN task_status            ts ON t.task_status_id = ts.id
    INNER JOIN mtm_task_product       mtp ON t.id = mtp.task_id
    INNER JOIN mtm_offer_product      mop ON mtp.product_id = mop.product_id
                                        AND trunc(t.creation_date, 'dd') = trunc(mop.valid_from, 'dd')
    INNER JOIN product                p ON mtp.product_id = p.id
    INNER JOIN product_definition     pd ON p.product_definition_id = pd.id
    INNER JOIN product_category       pc ON pd.product_category_id = pc.id
    INNER JOIN offer                  o ON mop.offer_id = o.id
    INNER JOIN offer_option_product   oop ON o.offer_option_id = oop.offer_option_id
                                           AND p.product_definition_id = oop.product_definition_id
    LEFT JOIN device_brand_model     dbm ON oop.device_brand_model_id = dbm.id
    WHERE trunc(t.due_date, 'dd') = trunc(sysdate, 'dd')
    GROUP BY cu.id,
             cu.identity_id,
             t.due_date,
             ts.name,
             c.name,
             c.lastname,
             adc.name,
             adp.name,
             aco.name,
             ads.name,
             adb.building_number,
             ad.flat,
             t.id,
             c.id,
             a.id,
             o.id
    ORDER BY due_date;