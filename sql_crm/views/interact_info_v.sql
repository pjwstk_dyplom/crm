create or replace view interact_info_v as
SELECT 
 it.name interact_type
, im.name interact_medium
, ind.name interact_direction
, t.creation_date Interact_date
, t.description
, cu.identity_id user_login
, cl.name || ' '  || cl.lastname customer_full_name
, cl.nip client_nip
, cl.pesel  client_pesel
, t.id interact_id
, t.interact_type_id
, t.interact_medium_id
, t.interact_direction_id 
, t.account_id
, cl.id client_id
, t.tasks_id
FROM crm.interact t 
inner join  interact_type it on t.interact_type_id=it.id
inner join interact_medium im on t.interact_medium_id=im.id
inner join interact_direction ind on t.interact_direction_id = ind.id
inner join account ac on t.account_id= ac.id
inner join client cl on ac.client_id= cl.id
left join crm_user cu on t.user_id=cu.id;




