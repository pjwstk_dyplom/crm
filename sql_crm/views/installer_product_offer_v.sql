create or replace view installer_product_offer_v as 
select 
    mop.offer_id,
    pd.description prod_def,
    pc.description prod_cat,
    p.id product_id,
    d.id mod_brand_id,
    dt.description dev_type,
    d.brand,
    d.model,
    dv.id device_id,
    dv.serial,
    dv.mac
from mtm_offer_product mop 
inner join product p on mop.product_id = p.id
inner join product_definition pd on  p.product_definition_id = pd.id
inner join product_category pc on pd.product_category_id = pc.id
inner join offer o on mop.offer_id = o.id
inner join offer_option_product oop on o.offer_option_id=oop.offer_option_id
                                   and pd.id = oop.product_definition_id
inner join device_brand_model d on oop.device_brand_model_id = d.id
inner join device_type dt on d.device_type_id = dt.id
left join device dv on p.device_id = dv.id