create view address_building_info_v as 
select 
building.building_number 
,building.post_code
,street.name  street
,city.name    city
,county.name county 
,province.name province 
,bt.description building_type

,building.id building_id
,street.id  street_id
,city.id    city_id
,county.id county_id 
,province.id province_id 
,bt.id building_type_id

,listagg(tech.description,' | ') within group (order by tech.description) technologies
,listagg(tech.id,' | ') within group (order by tech.description) technologie_ids
,listagg( tech_build.bandwidth_mb,' | ') within group (order by tech.description) bandwidths 


from  address_building building 
inner join address_street street  on building.address_street_id = street.id
inner join address_city city on street.address_city_id = city.id
inner join address_county county on city.address_county_id=county.id
inner join address_province province on county.address_province_id=province.id
inner join building_type bt on building.building_type_id = bt.id
left join mtm_avail_tech_on_building tech_build on building.id= tech_build.address_building_id
left join technology tech on tech_build.technology_id=tech.id
group by 
building.building_number
,building.post_code
,street.name  
,city.name    
,county.name 
,province.name 
,bt.description 

,building.id 
,street.id  
,city.id    
,county.id 
,province.id 
,bt.id 
