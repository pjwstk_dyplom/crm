CREATE OR REPLACE VIEW offers_active_v AS
SELECT
  DISTINCT
  oo.id AS offer_option_id,
  mor.value AS offer_req_value,
  mor.relation AS offer_req_relation,
  orf.function_name AS offer_req_function,
  mor2.value AS option_req_value,
  mor2.relation AS option_req_relation,
  orf2.function_name AS option_req_function
FROM offer_definition od

LEFT JOIN mtm_offer_requirement mor
  ON mor.offer_definition_id = od.id
LEFT JOIN offer_requirement_definition ord
  ON ord.id = mor.offer_requirement_def_id
LEFT JOIN offer_req_function orf
  ON orf.id = ord.offer_function_id
  
INNER JOIN offer_option oo
  ON oo.offer_definition_id = od.id
LEFT JOIN mtm_offer_requirement mor2
  ON mor2.offer_option_id = oo.id
LEFT JOIN offer_requirement_definition ord2
  ON ord2.id = mor2.offer_requirement_def_id
LEFT JOIN offer_req_function orf2
  ON orf2.id = ord2.offer_function_id
  
WHERE 1=1
  AND SYSDATE BETWEEN od.valid_from AND od.valid_to
  AND SYSDATE BETWEEN oo.valid_from AND oo.valid_to;