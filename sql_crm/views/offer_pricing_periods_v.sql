CREATE OR REPLACE VIEW OFFER_PRICING_PERIODS_V AS

WITH 

  q_base AS (

    SELECT 
      od.id offer_definition_id,
      oo.id offer_option_id,
      oop.id offer_option_product_id,
      oop.type,
      oop.length,
      oops.mth_num,
      oops.price,
      oops.id oops_id,
      LAG(oops.price) OVER(PARTITION BY oops.offer_option_product_id ORDER BY oops.mth_num) lag_price,
      LEAD(oops.price) OVER(PARTITION BY oops.offer_option_product_id ORDER BY oops.mth_num) lead_price
    FROM offer_definition od
    INNER JOIN offer_option oo
      ON oo.offer_definition_id = od.id
    INNER JOIN offer_option_product oop
      ON oop.offer_option_id = oo.id
    INNER JOIN offer_opt_prod_schedule oops
      ON oops.offer_option_product_id = oop.id
      
  ) 
  
  , q_step1 AS (
  
    SELECT 
      b.offer_definition_id,
      b.offer_option_id,
      b.offer_option_product_id,
      b.oops_id,
      b.type,
      b.length,
      --b.first_mth_mark,
      --MAX(first_mth_mark) OVER(PARTITION BY offer_option_product_id ORDER BY mth_num NULLS LAST ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) period_start,
      --b.last_mth_mark period_end,
      b.price,
      b.mth_num,
      b.lag_price,
      b.lead_price,
      CASE WHEN NVL(b.lag_price, -999) <> b.price THEN b.mth_num END mth_num_first,
      CASE WHEN NVL(b.lead_price, -999) <> b.price THEN b.mth_num END mth_num_last,
      CASE WHEN (b.type = 'L' AND b.mth_num IS NULL) OR b.type = 'U' THEN 'Y' ELSE 'N' END out_of_promo
    FROM q_base b
  
  ) 

  , q_step2 AS (
  
    SELECT 
      s1.offer_definition_id,
      s1.offer_option_id,
      s1.offer_option_product_id,
      s1.oops_id,
      s1.type,
      s1.length,
      s1.price,
      s1.mth_num_first,
      s1.mth_num_last,
      s1.out_of_promo,
      CASE WHEN s1.mth_num IS NOT NULL THEN
        MAX(mth_num_first) OVER(PARTITION BY s1.offer_option_product_id ORDER BY mth_num NULLS LAST ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
      END AS period_start,
      s1.mth_num_last period_end
    FROM q_step1 s1
  
  ) 
  
SELECT
  c.offer_definition_id,
  c.offer_option_id,
  c.offer_option_product_id,
  c.oops_id,
  c.type,
  c.length,
  NVL(MAX(c.length) OVER(PARTITION BY c.offer_option_id), 0) total_length,
  c.period_start,
  c.period_end,
  c.period_end - c.period_start + 1 period_length,
  c.price,
  c.out_of_promo
FROM q_step2 c
WHERE 1=1
  AND (c.type <> 'L' 
       OR (c.type = 'L' AND c.period_end IS NOT NULL)
       OR (c.type = 'L' AND C.out_of_promo = 'Y')
      );