CREATE OR REPLACE VIEW crmapp_jm_trg_info_v AS
WITH
  q_history AS (
    SELECT 
      ah.jm_action_trigger_id,
      ah.current_status,
      COUNT(1) AS cnt
    FROM jm_actions_history ah
    WHERE 1=1
      AND ah.start_time >= TRUNC(SYSDATE) - 7
    GROUP BY
      ah.jm_action_trigger_id,
      ah.current_status
      
    UNION ALL
    
    SELECT
      jm_action_trigger_id,
      'N' AS current_status,
      COUNT(1) AS cnt
    FROM jm_actions_stack
    GROUP BY
      jm_action_trigger_id
  ),
  
  q_history_pivot AS (
    SELECT
      qh.jm_action_trigger_id,
      NVL(qh.finished, 0) AS finished,
      NVL(qh.had_error, 0) AS had_error,
      NVL(qh.running, 0) AS running,
      NVL(qh.is_new, 0) AS is_new,
      NVL(qh.cancelled, 0) AS cancelled,
      NVL(qh.finished, 0) + NVL(qh.had_error, 0) + NVL(qh.running, 0) + NVL(qh.is_new, 0) + NVL(qh.cancelled, 0) AS generated
    FROM q_history 
    PIVOT (
      SUM(cnt)
      FOR current_status IN ('F' finished, 'E' had_error, 'R' running, 'N' is_new, 'C' cancelled)
    ) qh
  )
  
SELECT
  tr.id,
  tr.valid_from,
  tr.valid_to,
  tr.priority,
  tast.name AS task_status,
  taty.name AS task_type,
  tasu.name AS task_subtype,
  hp.generated,
  hp.is_new,
  hp.cancelled,
  hp.running,
  hp.had_error,
  hp.finished
FROM jm_action_trigger tr
INNER JOIN task_status tast
  ON tast.id = tr.task_status_id
INNER JOIN task_type taty
  ON taty.id = tr.task_type_id
INNER JOIN task_subtype tasu
  ON tasu.id = tr.task_subtype_id
LEFT JOIN q_history_pivot hp
  ON hp.jm_action_trigger_id = tr.id
WHERE SYSDATE BETWEEN tr.valid_from AND tr.valid_to;
