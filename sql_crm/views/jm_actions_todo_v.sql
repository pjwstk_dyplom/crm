CREATE OR REPLACE VIEW JM_ACTIONS_TODO_V AS
WITH
  q_triggers AS (
    SELECT /*+ MATERIALIZE */
      task.id AS task_id,
      tr.id AS jm_action_trigger_id,
      ROW_NUMBER() OVER(PARTITION BY task.id ORDER BY tr.priority DESC) priority
    FROM tasks task
    INNER JOIN jm_action_trigger tr
      ON tr.task_status_id = task.task_status_id
      AND tr.task_type_id = task.task_type_id
      AND tr.task_subtype_id = task.task_subtype_id
    WHERE 1=1
      AND SYSDATE BETWEEN tr.valid_from AND tr.valid_to
      AND NOT EXISTS(SELECT 1 FROM jm_actions_stack ac WHERE ac.tasks_id = task.id)
      AND NOT EXISTS(SELECT 1 FROM jm_actions_history ah WHERE ah.tasks_id = task.id AND ah.current_status = 'R')
  )
SELECT
  al.seq,
  qt.task_id,
  al.jm_procedure_id,
  qt.jm_action_trigger_id
FROM q_triggers qt
INNER JOIN jm_action_list al
  ON al.jm_action_trigger_id = qt.jm_action_trigger_id
WHERE 1=1
  AND qt.priority = 1
;

