CREATE OR REPLACE FUNCTION offer_req_00001(p_account_id IN account.id%TYPE) 
  RETURN NUMBER AS
  v_result NUMBER;
BEGIN
  SELECT
    COUNT(1)
  INTO
    v_result
  FROM account acco
  INNER JOIN product prod
    ON prod.account_id = acco.id
  INNER JOIN product_definition prde
    ON prod.product_definition_id = prde.id
  WHERE 1=1
    AND acco.id = p_account_id
    AND prod.product_status_id = 1 -- active
    AND prde.product_category_id = 1 -- broadband
  ;
  RETURN v_result;
END;
/