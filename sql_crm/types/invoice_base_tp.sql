create or replace type invoice_base_tp AS OBJECT 
(
INVOICE_NUMBER      VARCHAR2(50 BYTE),
TOTAL               NUMBER(10, 2),
CREATION_DATE       DATE,
SEND_DATE           DATE,
DUE_DATE            DATE,
PAYMENT_DATE        DATE,
PAID_AMOUNT         NUMBER(10, 2),
INVOICE_TYPE_ID     NUMBER(12, 0),
INVOICE_STATUS_ID   NUMBER(12, 0),
ACCOUNT_ID          NUMBER(12, 0),
PARENT_INVOICE      NUMBER(12, 0)
);

CREATE OR REPLACE TYPE invoice_base_nt AS TABLE OF invoice_base_tp;