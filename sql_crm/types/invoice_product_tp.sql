create or replace type invoice_product_tp AS OBJECT 
(
    INVOICE_ID   NUMBER(12, 0),
    PRODUCT_ID   NUMBER(12, 0),
    AMOUNT       NUMBER(10, 2)
);

CREATE OR REPLACE TYPE invoice_product_nt AS TABLE OF invoice_product_tp;

