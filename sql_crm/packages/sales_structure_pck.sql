create or replace PACKAGE SALES_STRUCTURE_PCK AS 

  /* TODO enter package declarations (types, exceptions, methods etc) here */ 
  
    PROCEDURE add_team (p_description    VARCHAR2
                      ,p_team_leader_id NUMBER
                      ,p_team_code      VARCHAR2);
            
    PROCEDURE add_user_to_struct (p_user_id     NUMBER
                                 ,p_manager_id  NUMBER := null
                                 ,p_team_id     number :=null);

    PROCEDURE add_sale_code ( p_sales_struct_id  NUMBER
                            ,p_teams_id         NUMBER
                            ,p_valid_from       DATE :=trunc(sysdate,'dd')
                            ,p_valid_to         DATE :='2200-01-01');
            

END SALES_STRUCTURE_PCK;
/

create or replace PACKAGE BODY SALES_STRUCTURE_PCK AS

  PROCEDURE add_team (p_description    VARCHAR2
                      ,p_team_leader_id NUMBER
                      ,p_team_code      VARCHAR2)
            
            
            AS
                out_id crm.teams.id%TYPE;
    BEGIN
            INSERT INTO crm.teams (description, team_leader_id,team_code) 
                        VALUES (p_description, p_team_leader_id,upper(p_team_code));
    COMMIT;
      EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      RAISE;
  END add_team;

  procedure add_user_to_struct (p_user_id     NUMBER
                                 ,p_manager_id  NUMBER := null
                                 ,p_team_id     number :=null)

            
            AS
                out_id crm.users_struct.id%type;
                last_id crm.users_struct.id%TYPE;
				chk_user_id crm.users_struct.id%TYPE;
                same_date_id crm.users_struct.id%TYPE;
BEGIN
    
    select count(*) into chk_user_id from crm.users_struct us 
    where us.user_id=p_user_id;
    
    select max(us.id) into same_date_id  from crm.users_struct us
    where 
    us.user_id = p_user_id
    and trunc(us.valid_from,'dd') = trunc(sysdate,'dd');
    
    IF (same_date_id is not null)
    THEN 
        UPDATE crm.users_struct 
        set manager_id = case when p_manager_id = 0 then null else p_manager_id end
        where 
        id = same_date_id;
        commit;
    ELSE     
    
        INSERT INTO crm.users_struct (user_id, valid_from, valid_to, manager_id) 
                    VALUES (p_user_id, trunc(sysdate,'dd'), '2200-01-01', case when p_manager_id = 0 then null else p_manager_id end)
                    RETURNING id into out_id;
                                                            
        commit;
        
        if(p_team_id != 0 and p_team_id is not null) 
        then
            begin
                add_sale_code(out_id, p_team_id);
                commit;
            end;
        end if;
        
        if chk_user_id!=0 
        then
            begin
                select distinct LAST_VALUE(us.id ignore nulls) over (partition by us.user_id order by us.valid_to rows between UNBOUNDED PRECEDING and unbounded following) into last_id
                from crm.users_struct us 
                where  us.user_id=p_user_id 
                    and us.id!=out_id;
                
                update crm.teams 
                set team_leader_id=out_id
                where
                team_leader_id=last_id;
                commit;
                
                update crm.sale_code sc
                set sc.sales_struct_id=out_id
                where 
                sc.sales_struct_id=last_id;
                commit;
            end ;
        end if;
        
    end if;
        
      EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      RAISE;
    
END add_user_to_struct;

  PROCEDURE add_sale_code ( p_sales_struct_id  NUMBER
                            ,p_teams_id         NUMBER
                            ,p_valid_from       DATE :=trunc(sysdate,'dd')
                            ,p_valid_to         DATE :='2200-01-01')
         
            
            AS
                out_id crm.sale_code.id%TYPE;
                p_team_code crm.teams.team_code%TYPE;
                p_code crm.sale_code.code%TYPE;
  BEGIN
        
        select distinct t.team_code into p_team_code from crm.teams t
        where 
        t.id=p_teams_id;
  
        select  LPAD(nvl(max(to_number(substr(s.code,1,4))),0)+1,4,0)||p_team_code into p_code from crm.sale_code s 
        where 
        s.teams_id=p_teams_id;
        
        insert into sale_code(sales_struct_id, teams_id, code, valid_from, valid_to)
                values (p_sales_struct_id, p_teams_id, p_code, p_valid_from, p_valid_to);
    commit;
      EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      RAISE;
    
  END add_sale_code;

END SALES_STRUCTURE_PCK;