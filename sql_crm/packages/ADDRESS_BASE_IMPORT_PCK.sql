CREATE OR REPLACE 
PACKAGE ADDRESS_BASE_IMPORT_PCK AS 

    PROCEDURE STANDARDIZE_INPUT;
    FUNCTION MERGE_INTO_AB RETURN VARCHAR2;
    PROCEDURE CLEAN_UP;
    PROCEDURE RUN_ALL (info OUT VARCHAR2);
    

END ADDRESS_BASE_IMPORT_PCK;
/
CREATE OR REPLACE
PACKAGE BODY ADDRESS_BASE_IMPORT_PCK AS

  PROCEDURE STANDARDIZE_INPUT AS
  BEGIN
  
    EXECUTE IMMEDIATE 'TRUNCATE TABLE ADDRESS_IMPORT_STANDARDIZED';

    INSERT INTO  address_import_standardized 
    SELECT 
    LOWER(t.region) province,
    LOWER(t.district) county,
    LOWER(t.city) city,
    CASE 
        WHEN dct.output_ IS NOT NULL THEN dct.output_||' '| |TRIM(BOTH ' ' FROM regexp_replace(LOWER(t.street),TRIM(BOTH ' ' FROM regexp_substr(LOWER(t.street),'(\S+[ .])(\S+)+',1,1,'i',1)),' ',1,1,'i')) 
        WHEN t.street IS NULL THEN LOWER(t.city)
        ELSE 'ul. ' ||Trim(BOTH ' ' FROM LOWER(t.street)) 
    END street_name,
    t.postcode,
    LOWER(t.number_) number_
    FROM address_import_source t
    LEFT JOIN address_import_street_type_dct dct ON dct.input_=TRIM(BOTH ' ' FROM regexp_substr(LOWER(t.street),'(\S+[ .])(\S+)+',1,1,'i',1))
    where 
        length(t.region) between 5 and 50
    and length(t.district) between 5 and 40
    and length(t.city)<=40
    and length(t.street)<=100
    and length(t.number_)<=20
    and length(t.postcode)<=10
    ;
    
  END STANDARDIZE_INPUT;

  FUNCTION MERGE_INTO_AB RETURN  VARCHAR2 IS
  
  p_building_type crm.building_type.id%TYPE;
  
  info VARCHAR2(1200);
  
  BEGIN
    SELECT max(id) INTO p_building_type
    FROM building_type 
    where description='postal-address';
        
       MERGE INTO address_province ap 
        USING (SELECT DISTINCT s.province FROM address_import_standardized s) s 
            ON (s.province   =   ap.name)
           WHEN NOT MATCHED THEN 
           INSERT (ap.name) VALUES (s.province);
      info := 'PROVINCES: '|| SQL%ROWCOUNT || ' | ';  
        
       MERGE INTO address_county ac 
        USING (SELECT DISTINCT s.county, ap.id province_id FROM address_import_standardized s
                INNER JOIN address_province  ap  ON s.province   =   ap.name)s
        ON (s.province_id = ac.address_province_id  
            and s.county = ac.name)        
        WHEN NOT MATCHED THEN 
           INSERT (ac.name, ac.address_province_id) VALUES (s.county, s.province_id);
        info :=info || 'COUNTIES: '|| SQL%ROWCOUNT || ' | ';
        
       MERGE INTO address_city act 
        USING (SELECT DISTINCT s.city, ap.id province_id, ac.id county_id FROM address_import_standardized s
                INNER JOIN address_province  ap  ON s.province   =   ap.name
                INNER JOIN address_county    ac  ON ap.id        =   ac.address_province_id  and s.county      = ac.name )s
        ON (s.county_id = act.address_county_id  
            and s.city = act.name)        
        WHEN NOT MATCHED THEN 
           INSERT (act.name, act.address_county_id) VALUES (s.city, s.county_id);
        info :=info || 'CITIES: '|| SQL%ROWCOUNT || ' | ';
        
       MERGE INTO address_street ast 
        USING (SELECT DISTINCT s.street_name, ap.id province_id, ac.id county_id, act.id city_id FROM address_import_standardized s
                INNER JOIN address_province  ap  ON s.province   =   ap.name
                INNER JOIN address_county    ac  ON ap.id        =   ac.address_province_id  and s.county      = ac.name
                INNER JOIN address_city      act ON ac.id        =   act.address_county_id   and s.city        = act.name)s
        ON (s.city_id = ast.address_city_id  
            and s.street_name = ast.name)        
        WHEN NOT MATCHED THEN 
           INSERT (ast.name, ast.address_city_id) VALUES (s.street_name, s.city_id);
        info :=info || 'STREETS: '|| SQL%ROWCOUNT || ' | ';
        
       MERGE INTO address_building  ab 
        USING (SELECT DISTINCT s.number_, s.postcode, ap.id province_id, ac.id county_id, act.id city_id, ast.id street_id  FROM address_import_standardized s
                INNER JOIN address_province  ap  ON s.province   =   ap.name
                INNER JOIN address_county    ac  ON ap.id        =   ac.address_province_id  and s.county      = ac.name
                INNER JOIN address_city      act ON ac.id        =   act.address_county_id   and s.city        = act.name
                INNER JOIN address_street    ast ON act.id       =   ast.address_city_id     and s.street_name = ast.name)s
        ON (s.street_id = ab.address_street_id  
            and s.number_ = ab.building_number)        
        WHEN NOT MATCHED THEN 
           INSERT (ab.building_number, ab.post_code, ab.address_street_id, ab.BUILDING_TYPE_ID ) VALUES (s.number_, s.postcode, s.street_id, p_building_type);
        
        info :=info || 'BUILDINGS: '|| SQL%ROWCOUNT || ' | ' ;
          
          RETURN info; 
  END MERGE_INTO_AB;

  PROCEDURE CLEAN_UP AS
  BEGIN
   EXECUTE IMMEDIATE 'TRUNCATE TABLE address_import_standardized';
   EXECUTE IMMEDIATE 'TRUNCATE TABLE address_import_source';
  END CLEAN_UP;

  PROCEDURE RUN_ALL (info OUT VARCHAR2) AS
  
  BEGIN
    
    standardize_input;
  info := merge_into_ab;
    clean_up;
    
    COMMIT;
    
  END RUN_ALL;

END ADDRESS_BASE_IMPORT_PCK;
/