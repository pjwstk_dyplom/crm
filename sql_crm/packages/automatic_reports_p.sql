CREATE OR REPLACE PACKAGE AUTOMATIC_REPORTS_P AS

  PROCEDURE ADD_REPORT(
    p_title VARCHAR2, 
    p_description VARCHAR2, 
    p_enabled NUMBER := 1, 
    p_interval VARCHAR2, 
    p_start_date DATE,
    p_type VARCHAR2 := 'PLSQL_BLOCK', 
    p_action VARCHAR2, 
    p_owner NUMBER, 
    p_export_path VARCHAR2 := NULL
  );
  
  PROCEDURE UPDATE_REPORT(
    p_report_id NUMBER,
    p_title VARCHAR2, 
    p_description VARCHAR2, 
    p_enabled NUMBER, 
    p_interval VARCHAR2, 
    p_start_date DATE,
    p_type VARCHAR2, 
    p_action VARCHAR2, 
    p_owner NUMBER, 
    p_export_path VARCHAR2
  );
  
  PROCEDURE ENABLE_REPORT(
    p_report_id IN NUMBER
  );
  
  PROCEDURE DISABLE_REPORT(
    p_report_id IN NUMBER
  );
  
  PROCEDURE REMOVE_REPORT(
    p_report_id IN NUMBER
  );
  
  PROCEDURE RUN_REPORT(
    p_report_id IN NUMBER
  );
  
  TYPE notifRowType IS RECORD (
    group_id NUMBER,
    groupcount NUMBER,
    emails VARCHAR2(4000)
  );
  TYPE notifTabType IS TABLE OF notifRowType;
  FUNCTION get_report_notifications(p_report_id IN NUMBER) RETURN notifTabType PIPELINED;
  
END AUTOMATIC_REPORTS_P;
/

CREATE OR REPLACE PACKAGE BODY AUTOMATIC_REPORTS_P AS
  
  -- Add report and return its ID
  PROCEDURE ADD_REPORT(
    p_title VARCHAR2, 
    p_description VARCHAR2, 
    p_enabled NUMBER := 1, 
    p_interval VARCHAR2, 
    p_start_date DATE,
    p_type VARCHAR2 := 'PLSQL_BLOCK', 
    p_action VARCHAR2, 
    p_owner NUMBER, 
    p_export_path VARCHAR2 := NULL
  )
  AS
    v_new_id NUMBER(12);
    v_sql VARCHAR2(4000);
  BEGIN
    -- Insert new value into REPORT table
    INSERT 
    INTO REPORT (
      title,
      description,
      create_date,
      enabled,
      interval,
      action,
      type,
      user_id,
      export_path
    )
    VALUES (
      p_title,
      p_description,
      sysdate,
      p_enabled,
      p_interval,
      p_action,
      p_type,
      p_owner,
      p_export_path
    ) RETURNING id INTO v_new_id;
    
    -- Add access for the report owner
    INSERT INTO REPORT_ACCESS (report_id, user_id) VALUES (v_new_id, p_owner);
    
    -- Create DBMS_SCHEDULE based on the values

      DBMS_SCHEDULER.create_job(
           job_name =>          'CRM_REPORT_' || v_new_id
         , job_type =>          p_type
         , job_action =>        p_action
         , start_date =>        p_start_date
         , repeat_interval =>   p_interval
         , enabled =>           CASE WHEN p_enabled = 1 THEN TRUE ELSE FALSE END
         , auto_drop =>         FALSE
         , comments =>          p_title || ': ' || p_description
        );
   
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      RAISE;
  END ADD_REPORT;

  PROCEDURE ADD_JOBATTR_TO_ARRAY_VCHAR(arr IN OUT sys.jobattr_array, j_name IN VARCHAR2, a_name IN VARCHAR2, a_value IN VARCHAR2) AS
    newattr sys.jobattr;
  BEGIN
    arr.extend(1);
    newattr := sys.jobattr(
      job_name => j_name,
      attr_name => a_name,
      attr_value => a_value
    );
    arr(arr.last) := newattr;
  END;
  
  PROCEDURE ADD_JOBATTR_TO_ARRAY_DATE(arr IN OUT sys.jobattr_array, j_name IN VARCHAR2, a_name IN VARCHAR2, a_value IN DATE) AS
    newattr sys.jobattr;
    a_timestamp TIMESTAMP WITH TIME ZONE;
  BEGIN
    arr.extend(1);
    a_timestamp := a_value;
    newattr := sys.jobattr(
      job_name => j_name,
      attr_name => a_name,
      attr_value => a_timestamp
    );
    arr(arr.last) := newattr;
  END;
  
  PROCEDURE ADD_JOBATTR_TO_ARRAY_BOOL(arr IN OUT sys.jobattr_array, j_name IN VARCHAR2, a_name IN VARCHAR2, a_value IN BOOLEAN) AS
    newattr sys.jobattr;
  BEGIN
    arr.extend(1);
    newattr := sys.jobattr(
      job_name => j_name,
      attr_name => a_name,
      attr_value => a_value
    );
    arr(arr.last) := newattr;
  END;

  -- Update job attributes transactionally
  PROCEDURE UPDATE_REPORT(
    p_report_id NUMBER,
    p_title VARCHAR2, 
    p_description VARCHAR2, 
    p_enabled NUMBER, 
    p_interval VARCHAR2, 
    p_start_date DATE,
    p_type VARCHAR2, 
    p_action VARCHAR2, 
    p_owner NUMBER, 
    p_export_path VARCHAR2
  )
  AS
    v_current       report%rowtype;
    v_start_date    DATE := SYSDATE;
    v_attr_array    sys.jobattr_array;
    v_job_name      VARCHAR2(50);
  BEGIN
    -- Get current state of the report
    SELECT * 
    INTO v_current 
    FROM report 
    WHERE report.id = p_report_id;
    
    -- Update database record
    UPDATE report
    SET 
      title = p_title,
      description = p_description,
      enabled = p_enabled,
      interval = p_interval,
      action = p_action,
      type = p_type,
      user_id = p_owner,
      export_path = p_export_path
    WHERE id = p_report_id;
    
    -- Update dbms schedule
    v_attr_array := sys.jobattr_array();
    v_job_name := 'CRM_REPORT_' || p_report_id;
    
    IF (NVL(v_current.title, 'xxx') <> NVL(p_title, 'xxx')) OR (NVL(v_current.description, 'xxx') <> NVL(p_description, 'xxx')) THEN
      ADD_JOBATTR_TO_ARRAY_VCHAR(
        arr => v_attr_array,
        j_name => v_job_name,
        a_name => 'COMMENTS',
        a_value => p_title || ': ' || p_description
      );
    END IF;
    
    IF (NVL(v_current.interval, 'xxx') <> NVL(p_interval, 'xxx')) THEN
      ADD_JOBATTR_TO_ARRAY_VCHAR(
        arr => v_attr_array,
        j_name => v_job_name,
        a_name => 'REPEAT_INTERVAL',
        a_value => p_interval
      );
    END IF;
    
    IF (NVL(v_current.action, 'xxx') <> NVL(p_action, 'xxx')) THEN
      ADD_JOBATTR_TO_ARRAY_VCHAR(
        arr => v_attr_array,
        j_name => v_job_name,
        a_name => 'JOB_ACTION',
        a_value => p_action
      );
    END IF;
    
    IF (NVL(v_current.type, 'xxx') <> NVL(p_type, 'xxx')) THEN
      ADD_JOBATTR_TO_ARRAY_VCHAR(
        arr => v_attr_array,
        j_name => v_job_name,
        a_name => 'JOB_TYPE',
        a_value => p_type
      );
    END IF;
    
    ADD_JOBATTR_TO_ARRAY_DATE(
      arr => v_attr_array,
      j_name => v_job_name,
      a_name => 'START_DATE',
      a_value => p_start_date
    );
    
    DBMS_SCHEDULER.SET_JOB_ATTRIBUTES(v_attr_array, 'TRANSACTIONAL');
    COMMIT;
    
  IF (NVL(v_current.enabled, 5) <> NVL(p_enabled, 5)) THEN
    IF p_enabled = 1 THEN 
      ENABLE_REPORT(p_report_id => p_report_id);
    ELSE
      DISABLE_REPORT(p_report_id => p_report_id);
    END IF;
  END IF;
    
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      RAISE;
  END UPDATE_REPORT;
  
  PROCEDURE ENABLE_REPORT(
    p_report_id IN NUMBER
  ) AS
    v_count NUMBER;
  BEGIN
    SELECT COUNT(1) INTO v_count FROM report r WHERE r.id = p_report_id;
    IF v_count = 0 THEN
      raise_application_error(-20001,'Report not found');
    ELSE 
      UPDATE report SET enabled = 1 WHERE id = p_report_id;
      dbms_scheduler.enable('CRM_REPORT_' || p_report_id);
      COMMIT;
    END IF;
  END ENABLE_REPORT;
  
  PROCEDURE RUN_REPORT(
    p_report_id IN NUMBER
  ) AS
    v_count NUMBER;
  BEGIN
    SELECT COUNT(1) INTO v_count FROM report r WHERE r.id = p_report_id;
    IF v_count = 0 THEN
      raise_application_error(-20001,'Report not found');
    ELSE 
      dbms_scheduler.run_job('CRM_REPORT_' || p_report_id);
      COMMIT;
    END IF;
  END RUN_REPORT;
  
  PROCEDURE DISABLE_REPORT(
    p_report_id IN NUMBER
  ) AS
    v_count NUMBER;
  BEGIN
    SELECT COUNT(1) INTO v_count FROM report r WHERE r.id = p_report_id;
    IF v_count = 0 THEN
      raise_application_error(-20001,'Report not found');
    ELSE 
      UPDATE report SET enabled = 0 WHERE id = p_report_id;
      dbms_scheduler.disable('CRM_REPORT_' || p_report_id);
      COMMIT;
    END IF;
  END DISABLE_REPORT;
  
  PROCEDURE REMOVE_REPORT(
    p_report_id IN NUMBER
  ) AS
    v_count NUMBER;
  BEGIN
    SELECT COUNT(1) INTO v_count FROM report r WHERE r.id = p_report_id;
    IF v_count = 0 THEN
      raise_application_error(-20001,'Report not found');
    ELSE 
      DELETE FROM report_access ra WHERE ra.report_id = p_report_id;
      DELETE FROM report r WHERE r.id = p_report_id;
      dbms_scheduler.drop_job('CRM_REPORT_' || p_report_id);
      COMMIT;
    END IF;
  END REMOVE_REPORT;
  
  FUNCTION get_report_notifications(p_report_id IN NUMBER) RETURN notifTabType PIPELINED AS
    rw notifRowType;
  BEGIN
    FOR rw IN (
      SELECT 
        TRUNC(ROWNUM/50)+1 AS group_id,
        MAX(TRUNC(ROWNUM/50)+1) OVER(PARTITION BY NULL) AS groupcount,
        LISTAGG(i."Email", ';') WITHIN GROUP(ORDER BY i."Email") AS emails
      FROM report_access ra
      INNER JOIN crm_user u
        ON u.id = ra.user_id
      INNER JOIN identity.users i
        ON i."Username" = u.identity_id
      WHERE 1=1
        AND ra.report_id = p_report_id
      GROUP BY
        TRUNC(ROWNUM/50)
    )
    LOOP
      PIPE ROW (rw);
    END LOOP;
    RETURN;
  END;
  

END AUTOMATIC_REPORTS_P;
/


