CREATE OR REPLACE PACKAGE file_utility_p
  AUTHID CURRENT_USER
AS
  PROCEDURE query2csv(
    p_source IN VARCHAR2, 
    p_columns IN VARCHAR2 DEFAULT '*', 
    p_where IN VARCHAR2 DEFAULT NULL,
    p_date_format IN VARCHAR2 DEFAULT 'yyyy-mm-dd', 
    p_directory IN VARCHAR2, 
    p_filename IN VARCHAR2,
    p_output_headers IN CHAR DEFAULT 'Y');
    
  PROCEDURE query2csv_full_sql(
    p_sql IN VARCHAR2, 
    p_date_format IN VARCHAR2 DEFAULT 'yyyy-mm-dd', 
    p_directory IN VARCHAR2, 
    p_filename IN VARCHAR2,
    p_output_headers IN CHAR DEFAULT 'Y');
END file_utility_p;
/

CREATE OR REPLACE PACKAGE BODY file_utility_p
AS

  PROCEDURE query2csv(
    p_source IN VARCHAR2, 
    p_columns IN VARCHAR2 DEFAULT '*', 
    p_where IN VARCHAR2 DEFAULT NULL,
    p_date_format IN VARCHAR2 DEFAULT 'yyyy-mm-dd', 
    p_directory IN VARCHAR2, 
    p_filename IN VARCHAR2,
    p_output_headers IN CHAR DEFAULT 'Y') 
  AS
    v_file                UTL_FILE.file_type;
    v_sql                 VARCHAR2(4000);
    v_file_line           VARCHAR2(4000);
    -- dynamic cursor handling variables
    TYPE CurTyp           IS REF CURSOR;
    v_cursor              CurTyp;
    v_cursor_id           NUMBER;
    v_cursor_col_count    NUMBER;
    v_desctab             DBMS_SQL.DESC_TAB;
    -- cursor values variables
    v_num                 NUMBER;
    v_date                DATE;
    v_string              VARCHAR2(500);
  BEGIN
    v_sql := 'SELECT ' || p_columns || ' FROM ' || p_source || CASE WHEN LENGTH(p_where) > 0 THEN 'WHERE ' || p_where ELSE '' END;
    query2csv_full_sql(p_sql => v_sql, p_date_format => p_date_format, p_directory => p_directory, p_filename => p_filename, p_output_headers => p_output_headers);
  END query2csv;
  
  PROCEDURE query2csv_full_sql(
    p_sql IN VARCHAR2, 
    p_date_format IN VARCHAR2 DEFAULT 'yyyy-mm-dd', 
    p_directory IN VARCHAR2, 
    p_filename IN VARCHAR2,
    p_output_headers IN CHAR DEFAULT 'Y') 
  AS
    v_file                UTL_FILE.file_type;
    v_file_line           VARCHAR2(4000);
    -- dynamic cursor handling variables
    TYPE CurTyp           IS REF CURSOR;
    v_cursor              CurTyp;
    v_cursor_id           NUMBER;
    v_cursor_col_count    NUMBER;
    v_desctab             DBMS_SQL.DESC_TAB;
    -- cursor values variables
    v_num                 NUMBER;
    v_date                DATE;
    v_string              VARCHAR2(500);
  BEGIN
    OPEN v_cursor FOR p_sql;
    
    -- Switch from native dynamic SQL to DBMS_SQL package.
    v_cursor_id := DBMS_SQL.TO_CURSOR_NUMBER(v_cursor);
    DBMS_SQL.DESCRIBE_COLUMNS(v_cursor_id, v_cursor_col_count, v_desctab);
    
    -- Define columns.
    FOR i IN 1 .. v_cursor_col_count LOOP
      CASE v_desctab(i).col_type
        WHEN 2 THEN DBMS_SQL.DEFINE_COLUMN(v_cursor_id, i, v_num);
        WHEN 12 THEN DBMS_SQL.DEFINE_COLUMN(v_cursor_id, i, v_date);
        ELSE DBMS_SQL.DEFINE_COLUMN(v_cursor_id, i, v_string, 500);
      END CASE;
    END LOOP;
    
    v_file := utl_file.fopen(location=>p_directory, filename=>p_filename, open_mode=>'w');
    
    IF p_output_headers = 'Y' THEN
      -- Read headers
      v_file_line := '';
      FOR i IN 1 .. v_cursor_col_count LOOP
        v_file_line := v_file_line || ',"' || v_desctab(i).col_name || '"';
      END LOOP;
      v_file_line := TRIM(',' FROM v_file_line);
      UTL_FILE.put_line(v_file, v_file_line);
    END IF;
    
    -- Fetch rows with DBMS_SQL package.
    WHILE DBMS_SQL.FETCH_ROWS(v_cursor_id) > 0 LOOP
      v_file_line := '';
      FOR i IN 1 .. v_cursor_col_count LOOP
        CASE v_desctab(i).col_type
          WHEN 2 THEN 
            DBMS_SQL.COLUMN_VALUE(v_cursor_id, i, v_num);
            v_file_line := v_file_line || ',' || v_num;
          WHEN 12 THEN 
            DBMS_SQL.COLUMN_VALUE(v_cursor_id, i, v_date);
            v_file_line := v_file_line || ',"' || TO_CHAR(v_date, p_date_format) || '"';
          ELSE 
            DBMS_SQL.COLUMN_VALUE(v_cursor_id, i, v_string);
            v_file_line := v_file_line || ',"' || v_string || '"';
        END CASE;
      END LOOP;
      v_file_line := TRIM(',' FROM v_file_line);
      UTL_FILE.put_line(v_file, v_file_line);
    END LOOP;
    
    -- close objects
    DBMS_SQL.CLOSE_CURSOR(v_cursor_id);
    UTL_FILE.fclose(v_file);
    
  EXCEPTION
    WHEN OTHERS THEN
      IF UTL_FILE.is_open(v_file) THEN
        UTL_FILE.fclose(v_file);
      END IF;
      RAISE;
  END query2csv_full_sql;
  
END file_utility_p;
/