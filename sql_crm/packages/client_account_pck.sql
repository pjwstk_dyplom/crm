CREATE OR REPLACE 
PACKAGE CLIENT_ACCOUNT_PCK AS 

  Function Add_Client(	P_Name Varchar2,
						P_Lastname Varchar2,
						P_Pesel Varchar2,
						P_Nip Varchar2,
						P_Segment_Id NUMBER,
						P_Email Varchar2,
						P_Phone_Primary Varchar2,
						P_Phone_Secondary Varchar2)
		Return Crm.Client.Id%Type;
		
	FUNCTION ADD_ACCOUNT (	P_CLIENT_ID NUMBER,
							P_BILLING_DAY NUMBER,
							P_DIVISION_ID NUMBER,
							P_INSTALL_ADDRESS_ID NUMBER,
							P_BILLING_ADRESS_ID NUMBER)
			RETURN CRM.account.id%TYPE;
			
	FUNCTION ADD_CONTRACT (	P_SIGN_DATE DATE,
							P_START_DATE DATE,
							P_END_DATE DATE,
							P_ACCOUNT_ID NUMBER,
							P_SALE_CODE_ID NUMBER)
			RETURN CRM.contract.id%TYPE;				

    Function EDIT_CLIENT ( P_id number,
                        P_Name Varchar2 default null,
						P_Lastname Varchar2 default null,
						P_Pesel Varchar2 default null,
						P_Nip Varchar2 default null,
						P_Segment_Id NUMBER default null,
						P_Email Varchar2 default null,
						P_Phone_Primary Varchar2 default null ,
						P_Phone_Secondary Varchar2 default null)
		Return Crm.Client.Id%Type;
END CLIENT_ACCOUNT_PCK;
/

create or replace PACKAGE BODY CLIENT_ACCOUNT_PCK AS

  Function Add_Client(	P_Name Varchar2,
						P_Lastname Varchar2,
						P_Pesel Varchar2,
						P_Nip Varchar2,
						P_Segment_Id NUMBER,
						P_Email Varchar2,
						P_Phone_Primary Varchar2,
						P_Phone_Secondary Varchar2)
		Return Crm.Client.Id%Type 
		
	AS
	OUT_ID CRM.client.id%TYPE;
	CHK_FLAG NUMBER;
  BEGIN
		 IF P_SEGMENT_ID IN (2,3) AND (P_NIP IS NULL OR LENGTH(P_NIP)!=10 )
				THEN RAISE_APPLICATION_ERROR(-20001,'IVALID NIP NUMBER FOR BUSINESS/SOHO CLIENT');
		END IF; 
		
		IF P_SEGMENT_ID = 1 AND (P_PESEL IS NULL OR LENGTH(P_PESEL)!=11)
				THEN RAISE_APPLICATION_ERROR(-20001,'IVALID PESEL NUMBER FOR HOME CLIENT');
		END IF; 		
		
											
  
		INSERT INTO CRM.CLIENT ( NAME 
								,LASTNAME 
								,PESEL 
								,NIP 
								,email 
								,PHONE_PRIMARY 
								,PHONE_SECONDARY 
								,SEGMENT_ID)
											VALUES ( P_NAME 
													,P_LASTNAME 
													,P_PESEL 
													,P_NIP 
													,P_email 
													,P_PHONE_PRIMARY 
													,P_PHONE_SECONDARY 
													,P_SEGMENT_ID)
					RETURNING ID INTO OUT_ID;
                    
                    DBMS_OUTPUT.PUT_LINE('NEW CLIENT OUT_ID: '|| OUT_ID);
		
		
    RETURN OUT_ID;
  END Add_Client;

  FUNCTION ADD_ACCOUNT (	P_CLIENT_ID NUMBER,
							P_BILLING_DAY NUMBER,
							P_DIVISION_ID NUMBER,
							P_INSTALL_ADDRESS_ID NUMBER,
							P_BILLING_ADRESS_ID NUMBER)
			RETURN CRM.account.id%TYPE 
			
			AS
			OUT_ID CRM.account.id%TYPE;
  BEGIN
    
	IF P_BILLING_DAY>=28  and P_BILLING_DAY<=0
				THEN RAISE_APPLICATION_ERROR(-20003,'INVALID BILLING DAY - value should be between 1 and 27');
		END IF; 
	
		INSERT INTO CRM.account (billing_address_id
								,BILLING_DAY
								,CLIENT_ID
								,CREATION_DATE
								,DIVISION_ID
								,INSTALL_ADDRESS_ID) 
													VALUES ( P_BILLING_ADRESS_ID
															,P_BILLING_DAY
															,P_CLIENT_ID
															,TRUNC(SYSDATE,'DD')
															,P_DIVISION_ID
															,P_INSTALL_ADDRESS_ID)
					RETURNING ID INTO OUT_ID;
					
	
    RETURN OUT_ID;
  END ADD_ACCOUNT;

  FUNCTION ADD_CONTRACT (	P_SIGN_DATE DATE,
							P_START_DATE DATE,
							P_END_DATE DATE,
							P_ACCOUNT_ID NUMBER,
							P_SALE_CODE_ID NUMBER)
			RETURN CRM.contract.id%TYPE 
			
			AS
			OUT_ID CRM.contract.id%TYPE;
			SALESMAN_CODE CRM.sale_code.code%TYPE;
			salesman_CONTRACT_cnt number;	
			P_CONTRACT_NUMBER CRM.contract.contract_number%TYPE;
  BEGIN
    
	SELECT s.code INTO salesman_code FROM CRM.sale_code S 
	WHERE  S.id=p_sale_code_id and p_sign_date between s.valid_from and s.valid_to;
	
	select SUBSTR(SALESMAN_CODE,1,4)||'/'||to_char(p_sign_date,'YY')||'/'||SUBSTR(SALESMAN_CODE,5,3)||'/'||LPAD(to_char(count(*)+1),4,0)  into P_CONTRACT_NUMBER from crm.contract c 
	where 
	SUBSTR(c.contract_number,1,4)||SUBSTR(c.contract_number,9,3) = SALESMAN_CODE
	and SUBSTR(c.contract_number,6,2)=to_char(p_sign_date,'YY');
	
	
	
	INSERT INTO CRM.contract(account_id
							,CONTRACT_NUMBER
							,END_DATE
							,SIGN_DATE
							,START_DATE )
								VALUES  (P_account_id
										,P_CONTRACT_NUMBER
										,P_END_DATE
										,P_SIGN_DATE
										,P_START_DATE )
			RETURNING ID INTO OUT_ID;										
	
	
	
    RETURN OUT_ID;
  END ADD_CONTRACT;
  
  
Function EDIT_CLIENT (  P_id number,
                        P_Name Varchar2 default null,
						P_Lastname Varchar2 default null,
						P_Pesel Varchar2 default null,
						P_Nip Varchar2 default null,
						P_Segment_Id NUMBER default null,
						P_Email Varchar2 default null,
						P_Phone_Primary Varchar2 default null ,
						P_Phone_Secondary Varchar2 default null)
		Return Crm.Client.Id%Type as 
        
        clnt_r crm.client%ROWTYPE;
        
        begin 
        
        select * into clnt_r from crm.client where id=p_id;

            if p_name != clnt_r.name then 
                begin
                dbms_output.put_line('name update '||clnt_r.name||' - >'|| p_name);
                    update client cl 
                    set cl.name=p_name
                    where  cl.id= p_id;
                    
                end;
            end if;        

            if P_Lastname != clnt_r.Lastname then 
                begin
                dbms_output.put_line('name update '||clnt_r.Lastname||' - >'|| p_Lastname);                
                    update client cl 
                    set cl.Lastname=P_Lastname
                    where  cl.id= p_id;
                    
                end;
            end if;        

            if P_Pesel != nvl(clnt_r.Pesel,0) then 
                begin
                dbms_output.put_line('name update '||clnt_r.Pesel||' - >'|| P_Pesel);                
                    update client cl 
                    set cl.Pesel=P_Pesel
                    where  cl.id= p_id;
                    
                end;
            end if;        

            if P_Nip != nvl(clnt_r.Nip,0) then 
                begin
                dbms_output.put_line('name update '||clnt_r.Nip||' - >'|| P_Nip);                
                    update client cl 
                    set cl.Nip=P_Nip
                    where  cl.id= p_id;
                    
                end;
                end if;        

            if P_Email != clnt_r.Email then 
                begin
                dbms_output.put_line('name update '||clnt_r.Email ||' - >'|| P_Email );                
                    update client cl 
                    set cl.Email=P_Email
                    where  cl.id= p_id;
                    
                end;
                end if;        

            if P_Phone_Primary != nvl(clnt_r.Phone_Primary,0) then 
                begin
                dbms_output.put_line('name update '||clnt_r.Phone_Primary||' - >'|| P_Phone_Primary);                
                    update client cl 
                    set cl.Phone_Primary=P_Phone_Primary
                    where  cl.id= p_id;
                    
                end;
            end if;        

            if P_Phone_Secondary != nvl(clnt_r.Phone_Secondary,0) then 
                begin
                dbms_output.put_line('name update '||clnt_r.Phone_Secondary||' - >'|| P_Phone_Secondary);                
                    update client cl 
                    set cl.Phone_Secondary=P_Phone_Secondary
                    where  cl.id= p_id;
                    
                end;
            end if;     


        return null;
        end EDIT_CLIENT;

END CLIENT_ACCOUNT_PCK;
/
