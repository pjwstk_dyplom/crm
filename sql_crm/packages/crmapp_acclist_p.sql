CREATE OR REPLACE PACKAGE crmapp_acclist_p AS
  PROCEDURE get_accounts(p_base_id IN NUMBER, p_request_id OUT NUMBER);
  FUNCTION generate_sql_for_base(p_base_id IN NUMBER) RETURN VARCHAR2;
  PROCEDURE save_sql_for_base(p_base_id IN NUMBER);
END crmapp_acclist_p;
/

CREATE OR REPLACE PACKAGE BODY crmapp_acclist_p AS
  PROCEDURE get_accounts(p_base_id IN NUMBER, p_request_id OUT NUMBER) AS
    v_sql VARCHAR2(4000);
    v_request_id NUMBER(12) := crmapp_acclist_request_seq.nextval;
    v_base_check NUMBER(1);
  BEGIN
    SELECT COUNT(1) INTO v_base_check FROM CRMAPP_ACCLIST_BASE b WHERE b.id = p_base_id;
    IF v_base_check > 0 THEN
      v_sql := 'INSERT INTO CRMAPP_ACCLIST_ACCOUNTS (request_id, base_id, account_id) SELECT DISTINCT ' || v_request_id || ', ' || p_base_id || ', a.id FROM account a WHERE 1=1';
      FOR rec IN (SELECT function_name, relation, value 
                  FROM CRMAPP_ACCLIST_CONDITIONS cond
                  INNER JOIN offer_requirement_definition rdef
                    ON rdef.id = cond.req_id
                  INNER JOIN offer_req_function func
                    ON func.id = rdef.offer_function_id
                  WHERE base_id = p_base_id)
      LOOP
        v_sql := v_sql || ' AND ' || rec.function_name || '(a.id) ' || CASE rec.relation WHEN '!' THEN '!=' ELSE rec.relation END || ' ' || TO_CHAR(rec.value); 
      END LOOP;
      --dbms_output.put_line(v_sql);
      EXECUTE IMMEDIATE v_sql;
      COMMIT;
      p_request_id := v_request_id;
    END IF;
  END get_accounts;
  
  FUNCTION generate_sql_for_base(p_base_id IN NUMBER) RETURN VARCHAR2 AS
    v_sql VARCHAR2(4000);
    v_from VARCHAR2(4000);
    v_columns VARCHAR2(4000);
  BEGIN
    v_from := 'CRMAPP_ACCLIST_ACCOUNTS ACCOUNTS INNER JOIN ACCOUNT CRM_ACCOUNT ON CRM_ACCOUNT.ID = ACCOUNTS.ACCOUNT_ID';
    FOR joins IN (SELECT 
                    DISTINCT join_clause
                  FROM CRMAPP_ACCLIST_COLUMNS c
                  INNER JOIN CRMAPP_ACCLIST_COLUMN_DEF cd
                    ON cd.ID = c.COLUMN_DEF_ID
                  INNER JOIN CRMAPP_ACCLIST_TABLE_JOIN tj
                    ON CD.TABLE_NAME = TJ.TABLE_NAME
                  WHERE c.base_id = p_base_id)
    LOOP
      v_from := v_from || joins.join_clause;
    END LOOP;
    
    v_columns := 'ACCOUNTS.ACCOUNT_ID';
    FOR cols IN (SELECT 
                    DISTINCT table_field, column_alias  
                  FROM CRMAPP_ACCLIST_COLUMNS c
                  INNER JOIN CRMAPP_ACCLIST_COLUMN_DEF cd
                    ON cd.ID = c.COLUMN_DEF_ID
                  WHERE c.base_id = p_base_id)
    LOOP
      v_columns := v_columns || ', ' || cols.table_field || ' AS ' || cols.column_alias;
    END LOOP;
    
    v_sql := 'SELECT ' || v_columns || ' FROM ' || v_from || ' WHERE ACCOUNTS.REQUEST_ID = :1';
    RETURN v_sql;
  END generate_sql_for_base;
  
  PROCEDURE save_sql_for_base(p_base_id IN NUMBER) AS
  BEGIN
    MERGE 
    INTO CRMAPP_ACCLIST_SQL a
    USING (
      SELECT
        p_base_id AS base_id,
        generate_sql_for_base(p_base_id) AS sql_code
      FROM DUAL
    ) b
    ON (a.base_id = b.base_id)
    WHEN MATCHED THEN
      UPDATE SET sql_code = b.sql_code, update_date = SYSDATE
    WHEN NOT MATCHED THEN
      INSERT (base_id, sql_code) VALUES(b.base_id, b.sql_code);
    COMMIT;
  END save_sql_for_base;
  
END crmapp_acclist_p;
/