CREATE OR REPLACE PACKAGE JOB_MACHINE_P AS
  PROCEDURE FEED_ACTIONS_STACK(p_trigger_id IN NUMBER := NULL);
  PROCEDURE EXECUTE_FIFO;
  PROCEDURE EXECUTE_FIFO_ALL;
END JOB_MACHINE_P;
/

CREATE OR REPLACE PACKAGE BODY JOB_MACHINE_P AS
  v_rows NUMBER := 0;

  FUNCTION MOVE_TO_HISTORY(p_action_id IN NUMBER, p_status IN VARCHAR2, p_comments VARCHAR2 := NULL) RETURN jm_actions_history.id%type AS
    v_action_row jm_actions_stack%rowtype;
    v_history_row_id jm_actions_history.id%type;
  BEGIN
    -- Get the action record
    SELECT jmas.* 
    INTO v_action_row 
    FROM jm_actions_stack jmas 
    WHERE jmas.id = p_action_id
    FOR UPDATE NOWAIT;
    -- Move the record to jm_actions_history
    DELETE FROM jm_actions_stack jmas WHERE jmas.id = v_action_row.id;
    INSERT INTO jm_actions_history(
      seq,
      tasks_id,
      jm_procedure_id,
      current_status,
      start_time,
      jm_action_trigger_id,
      comments
    )
    VALUES (
      v_action_row.seq,
      v_action_row.tasks_id,
      v_action_row.jm_procedure_id,
      p_status,
      sysdate,
      v_action_row.jm_action_trigger_id,
      p_comments
    ) RETURN id INTO v_history_row_id;
    COMMIT;
    RETURN v_history_row_id;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN -1;
    WHEN OTHERS THEN
      RAISE;
  END MOVE_TO_HISTORY;

  PROCEDURE FEED_ACTIONS_STACK(p_trigger_id IN NUMBER := NULL) AS
  BEGIN
    INSERT 
    INTO jm_actions_stack (
      seq,
      tasks_id,
      jm_procedure_id,
      jm_action_trigger_id
    )
    SELECT
      seq,
      task_id,
      jm_procedure_id,
      jm_action_trigger_id
    FROM jm_actions_todo_v
    WHERE jm_action_trigger_id = NVL(p_trigger_id, jm_action_trigger_id);
    v_rows := sql%rowcount;
    dbms_output.put_line('Inserted ' || v_rows || ' actions.');
    COMMIT;
  END FEED_ACTIONS_STACK;
  
  PROCEDURE EXECUTE_FIFO AS
    v_action_row jm_actions_stack%rowtype;
    v_procedure jm_procedure.procedure_name%type;
    v_history_row_id jm_actions_history.id%type;
  BEGIN
    -- Select the oldest action in stack
    SELECT jmas.*
    INTO v_action_row 
    FROM jm_actions_stack jmas 
    ORDER BY jmas.id 
    FETCH FIRST 1 ROW ONLY;
    -- Get the associated procedure name. If not found, record will be moved to history with status C.
    SELECT procedure_name INTO v_procedure FROM jm_procedure jmp WHERE jmp.id = v_action_row.jm_procedure_id;
    -- Move the record to jm_actions_history
    v_history_row_id := MOVE_TO_HISTORY(p_action_id => v_action_row.id, p_status => 'R');
    -- Execute
    BEGIN
      EXECUTE IMMEDIATE 'BEGIN ' || v_procedure || '(p_tasks_id => ' || v_action_row.tasks_id || '); END;';
      -- Update status in history: Finished
      UPDATE jm_actions_history jmah
      SET jmah.current_status = 'F',
          jmah.finish_time = sysdate,
          jmah.comments = 'Action ' || v_action_row.id || ' executed successfully.'
      WHERE jmah.id = v_history_row_id;
      COMMIT;
      dbms_output.put_line('Action ' || v_action_row.id || ' executed successfully. History ID: ' || v_history_row_id);
    EXCEPTION
      WHEN OTHERS THEN 
      BEGIN
        ROLLBACK;
        -- Update status in history: Error
        UPDATE jm_actions_history jmah
        SET jmah.current_status = 'E',
            jmah.finish_time = sysdate,
            jmah.comments = 'Action ' || v_action_row.id || ' caused an error.'
        WHERE jmah.id = v_history_row_id;
        COMMIT;
        dbms_output.put_line('Action ' || v_action_row.id || ' caused an error (' || SQLCODE || '). History ID: ' || v_history_row_id);
        dbms_output.put_line('Error ' || SQLCODE || ': ' || SQLERRM);
      END;
    END;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
    BEGIN
      ROLLBACK;
      dbms_output.put_line('Action ' || v_action_row.id || ' found no data. Cancelling.');
      v_history_row_id := MOVE_TO_HISTORY(p_action_id => v_action_row.id, p_status => 'C', p_comments => 'No data found.');
    END;
    WHEN OTHERS THEN
      RAISE;
  END EXECUTE_FIFO;
  
  PROCEDURE EXECUTE_FIFO_ALL AS
    v_count NUMBER;
  BEGIN
    SELECT COUNT(1) INTO v_count FROM jm_actions_stack;
    WHILE v_count > 0 LOOP
      EXECUTE_FIFO;
      SELECT COUNT(1) INTO v_count FROM jm_actions_stack;
    END LOOP;
  END;
  
END;
/