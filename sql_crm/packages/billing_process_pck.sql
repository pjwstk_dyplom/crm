create or replace PACKAGE BILLING_PROCESS_PCK AS 

  PROCEDURE invoice_base_f(bill_date DATE);
  PROCEDURE invoice_product_f(p_bill_date DATE, p_account_id crm.account.id%TYPE,p_invoice_id crm.invoice.id%TYPE DEFAULT null);

END BILLING_PROCESS_PCK;

/
create or replace PACKAGE BODY BILLING_PROCESS_PCK AS

PROCEDURE invoice_base_f(bill_date DATE) 
is 

R_INVOICE_NUMBER      VARCHAR2(50 BYTE);
R_TOTAL               NUMBER(10, 2);
R_CREATION_DATE       DATE;
R_SEND_DATE           DATE;
R_DUE_DATE            DATE;
R_PAYMENT_DATE        DATE;
R_PAID_AMOUNT         NUMBER(10, 2);
R_INVOICE_TYPE_ID     NUMBER(12, 0);
R_INVOICE_STATUS_ID   NUMBER(12, 0);
R_ACCOUNT_ID          NUMBER(12, 0);
R_PARENT_INVOICE      NUMBER(12, 0);
R_INVOICE_ID          crm.invoice.id%TYPE;

R_PROD_TOTAL               NUMBER(10, 2);
R_OTHER_TOTAL              NUMBER(10, 2);

CURSOR inv_c is
SELECT 
distinct
 a.id account_id
,LAST_VALUE(c.contract_number) over (partition by a.id 
                                    order by  c.end_date asc 
                                    rows between UNBOUNDED PRECEDING and UNBOUNDED FOLLOWING) contract_number
/*,o.offer_option_id
,mop.product_id*/
FROM crm.account a 
inner join crm.offer o on a.id=o.account_id  AND  ADD_MONTHS(TRUNC(o.sale_date,'dd'),1)<= trunc(bill_date,'dd')
inner join CRM.mtm_offer_product mop on o.id=mop.offer_id and trunc(bill_date,'dd') between mop.valid_from and mop.valid_to
inner join crm.contract c on a.id = c.account_id
where
a.billing_day= extract(day from bill_date);

begin 

for inv_i in inv_c
LOOP

R_INVOICE_NUMBER:=extract(year from bill_date)||'/'||
                  extract(month from bill_date)||'/'||
                  inv_i.contract_number;
R_TOTAL:=0;
R_CREATION_DATE:=trunc(bill_date,'dd');
R_SEND_DATE:=null;
R_DUE_DATE:=trunc(bill_date,'dd')+21;
R_PAYMENT_DATE:=null;
R_PAID_AMOUNT:=0;
select max(id) into R_INVOICE_TYPE_ID from crm.invoice_type where name='invoice';
select max(id) into R_INVOICE_STATUS_ID from crm.invoice_status where name='new';
R_ACCOUNT_ID:=inv_i.account_id;
R_PARENT_INVOICE:=null;

        DBMS_OUTPUT.PUT_LINE('BILL_DT: '||bill_date||'R_ACCOUNT_ID: '||R_ACCOUNT_ID);

                        
insert into crm.invoice(INVOICE_NUMBER, TOTAL, CREATION_DATE, SEND_DATE, DUE_DATE, PAYMENT_DATE, PAID_AMOUNT, INVOICE_TYPE_ID, INVOICE_STATUS_ID, ACCOUNT_ID, PARENT_INVOICE)                        
            values (R_INVOICE_NUMBER, R_TOTAL, R_CREATION_DATE, R_SEND_DATE, R_DUE_DATE, R_PAYMENT_DATE, R_PAID_AMOUNT,R_INVOICE_TYPE_ID, R_INVOICE_STATUS_ID, R_ACCOUNT_ID, R_PARENT_INVOICE)
            RETURNING ID INTO R_INVOICE_ID;
         

invoice_product_f(bill_date, R_ACCOUNT_ID, R_INVOICE_ID);            



select nvl(sum(amount),0) into R_PROD_TOTAL from mtm_invoice_product where invoice_id= R_INVOICE_ID;
select nvl(sum(amount),0) into R_OTHER_TOTAL from invoice_other_items where invoice_id= R_INVOICE_ID;



UPDATE crm.invoice 
set total = R_PROD_TOTAL+R_OTHER_TOTAL
where id=R_INVOICE_ID;

                        
END LOOP;

end;


procedure invoice_product_f(p_bill_date DATE, p_account_id crm.account.id%TYPE, p_invoice_id crm.invoice.id%TYPE )  is 

    CURSOR proc_v is 
    SELECT 
         a.id account_id
        ,mop.product_id
        ,oops.price
        ,months_between(trunc(p_bill_date,'mm') ,trunc(p.activation_date,'mm')) prod_age
        ,pc.description prod_category
    FROM crm.account a 
    INNER JOIN crm.offer o ON a.id=o.account_id
    INNER JOIN CRM.mtm_offer_product mop ON o.id=mop.offer_id and trunc(p_bill_date,'dd') between mop.valid_from and mop.valid_to
    INNER JOIN crm.product p ON p.id = mop.product_id 
    INNER JOIN crm.product_definition pd on p.product_definition_id=pd.id
    INNER JOIN crm.product_category pc on pd.product_category_id= pc.id
    INNER JOIN crm.offer_option_product oop ON p.product_definition_id = oop.product_definition_id and o.offer_option_id = oop.offer_option_id
    INNER JOIN CRM.offer_opt_prod_schedule oops ON oop.id = oops.offer_option_product_id 
                                                    AND nvl(CASE 
                                                        WHEN oop.type='U' THEN NULL
                                                        WHEN oop.type='L' AND months_between(trunc(p_bill_date,'mm') ,trunc(p.activation_date,'mm'))>oop.length THEN NULL
                                                        ELSE months_between(trunc(p_bill_date,'mm') ,trunc(p.activation_date,'mm')) END,0)  = nvl(oops.mth_num,0)
    where
        a.id = p_account_id;
        
     other_item_dsc crm.invoice_other_items_desc.id%type;   

begin 
FOR proc_i IN proc_v
LOOP


INSERT INTO crm.mtm_invoice_product(invoice_id,product_id, amount) values(p_invoice_id,proc_i.product_id, proc_i.price);


IF proc_i.prod_age=1 
THEN
BEGIN


    select id into other_item_dsc from invoice_other_items_desc d
    where 
        d.description=proc_i.prod_category||' installation fee';

        
     IF other_item_dsc is not null
     THEN
     BEGIN
     INSERT INTO invoice_other_items(invoice_id,amount, INVOICE_OTHER_ITEMS_DESC_ID)values (p_invoice_id, proc_i.price*1.5, other_item_dsc );    
     END;
     END IF;
EXCEPTION  
    WHEN NO_DATA_FOUND then null;
END;
END IF; 
END LOOP;
END;



END BILLING_PROCESS_PCK;
/