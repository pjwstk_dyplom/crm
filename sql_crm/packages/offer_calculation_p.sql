CREATE OR REPLACE PACKAGE offer_calculation_p AS
  
  TYPE funcResultRowType IS RECORD (
    account_id account.id%type,
    function_name offer_req_function.function_name%type,
    function_value mtm_offer_requirement.value%type
  );
  TYPE funcResultTab IS TABLE OF funcResultRowType;
  
  TYPE optionEvalRowType IS RECORD (
    offer_option_id NUMBER,
    option_evaluation NUMBER(1,0)
  );
  TYPE optionEvalTabType IS TABLE OF optionEvalRowType;
  
  FUNCTION functionResult(p_function_name IN offer_req_function.function_name%type, p_account_id IN account.id%type, p_verbose IN CHAR DEFAULT 'Y') RETURN NUMBER;
  FUNCTION currentFunctionsForAccount(p_account_id IN account.id%type) RETURN funcResultTab PIPELINED;
  FUNCTION requirementEvaluation(p_requirement_value IN mtm_offer_requirement.value%type, p_relation IN mtm_offer_requirement.relation%type, p_function_value IN mtm_offer_requirement.value%type) RETURN NUMBER;
  FUNCTION getOptionEvaluations(p_account_id IN account.id%type) RETURN optionEvalTabType PIPELINED;
END offer_calculation_p;
/

CREATE OR REPLACE PACKAGE BODY offer_calculation_p AS

  FUNCTION functionResult(p_function_name IN offer_req_function.function_name%type, p_account_id IN account.id%type, p_verbose IN CHAR DEFAULT 'Y') RETURN NUMBER AS
    v_result NUMBER;
    v_sql VARCHAR2(4000);
  BEGIN
    v_sql := 'SELECT ' || p_function_name || '(' || p_account_id || ') FROM dual';
    IF p_verbose = 'Y' THEN
      dbms_output.put_line('functionResult SQL: ' || v_sql);
    END IF;
    EXECUTE IMMEDIATE v_sql INTO v_result;
    RETURN v_result;
  END;
  
  FUNCTION currentFunctionsForAccount(p_account_id IN account.id%type) RETURN funcResultTab PIPELINED AS
    v_func_name VARCHAR2(100);
    v_res_row funcResultRowType;
    
    CURSOR cur_function_names IS 
      SELECT DISTINCT v.offer_req_function AS function_name FROM offers_active_v v WHERE v.offer_req_function IS NOT NULL
      UNION 
      SELECT DISTINCT v.option_req_function AS function_name FROM offers_active_v v WHERE v.option_req_function IS NOT NULL;

  BEGIN
    FOR func_rec IN cur_function_names LOOP
      v_func_name := func_rec.function_name;
      SELECT p_account_id, v_func_name, functionResult(p_function_name => v_func_name, p_account_id => p_account_id, p_verbose => 'N') INTO v_res_row FROM dual;
      PIPE ROW (v_res_row);
    END LOOP;
    RETURN;
  END;
  
  FUNCTION requirementEvaluation(p_requirement_value IN mtm_offer_requirement.value%type, p_relation IN mtm_offer_requirement.relation%type, p_function_value IN mtm_offer_requirement.value%type) RETURN NUMBER AS
  BEGIN
    CASE p_relation
      WHEN '>' THEN RETURN CASE WHEN p_function_value >  p_requirement_value THEN 1 ELSE 0 END;
      WHEN '<' THEN RETURN CASE WHEN p_function_value <  p_requirement_value THEN 1 ELSE 0 END;
      WHEN '=' THEN RETURN CASE WHEN p_function_value =  p_requirement_value THEN 1 ELSE 0 END;
      WHEN '!' THEN RETURN CASE WHEN p_function_value != p_requirement_value THEN 1 ELSE 0 END;
      ELSE RETURN NULL;
    END CASE;
  END;
  
  
  FUNCTION getOptionEvaluations(p_account_id IN account.id%type) RETURN optionEvalTabType PIPELINED AS
    rw optionEvalRowType;
  BEGIN
    FOR rw IN (
      WITH function_values AS (
        SELECT /*+ MATERIALIZE */
          t.function_name,
          t.function_value
        FROM TABLE(offer_calculation_p.currentFunctionsForAccount(p_account_id => p_account_id)) t
      )
      SELECT
        rv.offer_option_id,
        MIN(CASE 
          WHEN NVL(offer_calculation_p.requirementEvaluation(p_function_value => fval.function_value,  p_relation => rv.offer_req_relation, p_requirement_value => rv.offer_req_value), 1) = 1
           AND NVL(offer_calculation_p.requirementEvaluation(p_function_value => fval2.function_value, p_relation => rv.option_req_relation, p_requirement_value => rv.option_req_value), 1) = 1
          THEN 1 ELSE 0 
        END) AS option_evaluation
      FROM offers_active_v rv
      LEFT JOIN function_values fval
        ON fval.function_name = rv.offer_req_function
      LEFT JOIN function_values fval2
        ON fval2.function_name = rv.option_req_function
      GROUP BY 
        rv.offer_option_id
    ) 
    LOOP
      PIPE ROW (rw);
    END LOOP;
    RETURN;
  END;
  
END offer_calculation_p;
/

