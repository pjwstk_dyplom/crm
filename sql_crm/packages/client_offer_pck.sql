create or replace PACKAGE CLIENT_OFFER_PCK AS 

	FUNCTION ADD_OFFER(	O_ACCOUNT_ID 		NUMBER, 
						O_OFFER_OPTION_ID 	NUMBER, 
						O_SALE_CODE_ID 		NUMBER, 
						O_SALE_DATE 		DATE)
			RETURN CRM.OFFER.ID%TYPE;
        
    PROCEDURE ADD_PRODUCTS(IN_OFFER_ID      CRM.OFFER.ID%TYPE,
                           IN_ACCOUNT_ID    CRM.ACCOUNT.ID%TYPE); 
    
    PROCEDURE ADD_SALE_TASKS(IN_OFFER_ID     CRM.OFFER.ID%TYPE,
                            IN_ACCOUNT_ID    CRM.ACCOUNT.ID%TYPE, 
                            CN_SALE_CODE_ID  CRM.sale_code.id%TYPE,
                            IN_DEACT_TSK     CRM.tasks.id%TYPE DEFAULT null);

	PROCEDURE NEW_CLIENT_OFFER (C_Name 					IN	Varchar2,
								C_Lastname 				IN	Varcha,
								C_Pesel 				IN	Varchar2,
								C_Nip 					IN	Varchar2,
								C_Segment_Id 			IN	NUMBER,
								C_Email 				IN	Varchar2,
								C_Phone_Primary 		IN	Varchar2,
								C_Phone_Secondary 		IN	Varchar2,
								A_BILLING_DAY 			IN	NUMBER,
								A_DIVISION_ID 			IN	NUMBER,
								A_INSTALL_ADDRESS_ID	IN 	NUMBER,
								A_BILLING_ADRESS_ID 	IN	NUMBER,
								CN_SIGN_DATE 			IN	DATE,
								CN_START_DATE 			IN	DATE,
								CN_END_DATE 			IN	DATE,
								CN_SALE_CODE_ID 		IN	NUMBER,		
								O_OFFER_OPTION_ID 		IN	NUMBER, 
								O_SALE_DATE 			IN	DATE, 
								OUT_CLIENT_ID 			OUT CRM.client.id%TYPE
								);
                                
    	PROCEDURE NEW_ACCOUNT_OFFER(A_CLIENT_ID             IN  NUMBER,
                                    A_BILLING_DAY 			IN	NUMBER,
                                    A_DIVISION_ID 			IN	NUMBER,
                                    A_INSTALL_ADDRESS_ID	IN 	NUMBER,
                                    A_BILLING_ADRESS_ID 	IN	NUMBER,
                                    CN_SIGN_DATE 			IN	DATE,
                                    CN_START_DATE 			IN	DATE,
                                    CN_END_DATE 			IN	DATE,
                                    CN_SALE_CODE_ID 		IN	NUMBER,		
                                    O_OFFER_OPTION_ID 		IN	NUMBER, 
                                    O_SALE_DATE 			IN	DATE,
									OUT_ACCOUNT_ID		   OUT	NUMBER
                                    );
                                                                
                                    
                                
    PROCEDURE UPSELL_OFFER (IN_ACCOUNT_ID      IN CRM.ACCOUNT.ID%TYPE, 
                             IN_OFFER_OPTION_ID IN CRM.OFFER_OPTION.ID%TYPE, 
                             IN_SALE_CODE_ID    IN CRM.SALE_CODE.ID%TYPE, 
                             IN_SALE_DATE       IN CRM.OFFER.SALE_DATE%TYPE);
                             
    PROCEDURE ADD_UPGARDE_TASKS(IN_OFFER_ID     CRM.OFFER.ID%TYPE,
                                IN_ACCOUNT_ID   CRM.ACCOUNT.ID%TYPE, 
                                CN_SALE_CODE_ID CRM.sale_code.id%TYPE);
     
     PROCEDURE UPGRADE_OFFER (IN_ACCOUNT_ID      IN CRM.ACCOUNT.ID%TYPE, 
                              IN_OFFER_OPTION_ID IN CRM.OFFER_OPTION.ID%TYPE, 
                              IN_SALE_CODE_ID    IN CRM.SALE_CODE.ID%TYPE, 
                              IN_SALE_DATE       IN CRM.OFFER.SALE_DATE%TYPE);


END CLIENT_OFFER_PCK;
/

create or replace PACKAGE BODY CLIENT_OFFER_PCK AS

FUNCTION ADD_OFFER(	O_ACCOUNT_ID NUMBER, 
					O_OFFER_OPTION_ID NUMBER, 
					O_SALE_CODE_ID NUMBER, 
					O_SALE_DATE DATE)
		RETURN CRM.OFFER.ID%TYPE 
			
		AS
		OUT_ID CRM.offer.id%TYPE;	
  BEGIN

	INSERT INTO offer (	ACCOUNT_ID, 
						OFFER_OPTION_ID, 
						SALE_CODE_ID, 
						SALE_DATE ) 
										VALUES (O_ACCOUNT_ID, 
												O_OFFER_OPTION_ID, 
												O_SALE_CODE_ID, 
												O_SALE_DATE )
				RETURNING ID INTO OUT_ID;	
    RETURN OUT_ID;
  END ADD_OFFER;

    PROCEDURE ADD_PRODUCTS(IN_OFFER_ID CRM.OFFER.ID%TYPE, IN_ACCOUNT_ID CRM.ACCOUNT.ID%TYPE) AS

          OFFER_OPT_ID CRM.offer_option_product.id%TYPE;
          CUR_PRODUCT_DEFINITION NUMBER;
          CUR_LENGTH_OF NUMBER;
          CUR_TYPE_OF VARCHAR2(10);
          CUR_DEVICE_BRAND_MODEL_ID NUMBER;
          CUR_ID NUMBER; /* Id offer_option_product potrzebny do offer_opt_prod_forced_attr */

          CUR_2_attribute_def_id number;
          CUR_2_value varchar2(500);
          NEW_OFFER_ATR_ID NUMBER;

          NEW_PRODUCT_ID NUMBER;
          NEW_TASK_ID NUMBER;

          STATUS_ID NUMBER;
          TASK_TYPE_ID NUMBER;
          TASK_STATUS_ID NUMBER;
          TASK_SUBTYPE_ID NUMBER;
          TASK_QUEUE_ID NUMBER;
          TASK_BASKET_ID NUMBER;

          PROD_TECHNOLOGY_ID CRM.technology.ID%TYPE;

          INSTALL_TASK_ID NUMBER;
          ACTIVATION_TASK_ID NUMBER;

          CURSOR OfferProductCur IS 
                SELECT A.PRODUCT_DEFINITION_ID, A.LENGTH, A.TYPE, A.DEVICE_BRAND_MODEL_ID, A.ID 
                FROM CRM.offer_option_product A  
                INNER JOIN OFFER_OPTION OP ON OP.ID = A.OFFER_OPTION_ID
                INNER JOIN OFFER O ON O.OFFER_OPTION_ID = OP.ID 
                WHERE  
                    O.ID = IN_OFFER_ID;

          CURSOR OfferProductAttr IS 
                SELECT aat.product_attribute_def_id, aat.VALUE  
                FROM offer_opt_prod_forced_attr aat 
                WHERE aat.offer_option_product_id = CUR_ID; 

    BEGIN

      /* PRZYGOTOWANIE */
       STATUS_ID := CRM.CRM_UTILS.GET_PROD_STATUS_ID('AWAITING');

        SELECT OO.TECHNOLOGY_ID  INTO PROD_TECHNOLOGY_ID FROM OFFER_OPTION OO
        INNER JOIN OFFER O ON o.offer_option_id = OO.ID
        WHERE O.ID = IN_OFFER_ID;

      open OfferProductCur;
      loop
        fetch OfferProductCur into CUR_PRODUCT_DEFINITION,CUR_LENGTH_OF,CUR_TYPE_OF,CUR_DEVICE_BRAND_MODEL_ID,CUR_ID;
        exit when OfferProductCur%notfound;

             /* Sekcja tworzenia produków zwiazanych z dana oferta */       

              INSERT INTO CRM.PRODUCT (PRODUCT_DEFINITION_ID,PRODUCT_STATUS_ID,ACCOUNT_ID,TECHNOLOGY_ID)
                VALUES(CUR_PRODUCT_DEFINITION,STATUS_ID,IN_ACCOUNT_ID,PROD_TECHNOLOGY_ID)
                RETURNING ID INTO NEW_PRODUCT_ID;

              /* Sekcja tworzaca wpisy w tabeli MTM_OFFER_PRODUCT tworzac powiazania miedzy zakupiona oferta oraz produktami w niej zawartymi */    
               INSERT INTO CRM.MTM_OFFER_PRODUCT (OFFER_ID, PRODUCT_ID, VALID_FROM, VALID_TO)
                VALUES(IN_OFFER_ID,NEW_PRODUCT_ID, trunc(sysdate,'DD'),'2030-01-01');

              /* Sekcja dla atrybutów w offer_opt_prod_forced_attr */
              OPEN OfferProductAttr;

              LOOP
                FETCH OfferProductAttr into CUR_2_attribute_def_id,CUR_2_value;
                EXIT WHEN OfferProductAttr%notfound;
                INSERT INTO CRM.PRODUCT_ATTRIBUTE (PRODUCT_ID,PRODUCT_ATTR_DEF_ID, VALUE)
                VALUES(NEW_PRODUCT_ID,CUR_2_attribute_def_id,CUR_2_value)
                RETURNING  ID INTO NEW_OFFER_ATR_ID;

              END LOOP;
              CLOSE OfferProductAttr;

      END LOOP;
      CLOSE OfferProductCur;

    END ADD_PRODUCTS;

    PROCEDURE ADD_SALE_TASKS(IN_OFFER_ID CRM.OFFER.ID%TYPE, IN_ACCOUNT_ID CRM.ACCOUNT.ID%TYPE, CN_SALE_CODE_ID CRM.SALE_CODE.ID%TYPE, IN_DEACT_TSK CRM.TASKS.ID%TYPE DEFAULT NULL) IS 

    CRM_USER_ID CRM.users_struct.user_id%TYPE;
    INSTALL_TASK_ID CRM.tasks.id%TYPE;
    ACTIVATE_TASK_ID CRM.tasks.id%TYPE;
    INSTALL_PRODS_CNT NUMBER;


    CURSOR OFFER_PRODUCTS IS
        SELECT p.id, oop.device_brand_model_id 
        FROM product P 
        INNER JOIN product_definition pd on p.product_definition_id = pd.id
        INNER JOIN mtm_offer_product mop ON P.ID = mop.product_id
        INNER JOIN offer o ON o.id = mop.offer_id
        INNER JOIN offer_option_product oop ON oop.offer_option_id = o.offer_option_id
                                           AND oop.product_definition_id = pd.id
        WHERE 
        o.id = IN_OFFER_ID;

    BEGIN 

    SELECT USER_ID INTO CRM_USER_ID FROM USERS_STRUCT US 
    INNER JOIN sale_code SC ON us.id=sc.sales_struct_id
    WHERE 
    sc.id= CN_SALE_CODE_ID;

    /*CREATE ACTIVATION TASK TASK*/
        INSERT INTO TASKS (TASK_STATUS_ID,TASK_TYPE_ID,TASK_SUBTYPE_ID,CREATION_DATE,DUE_DATE,QUEUE_ID,BASKET_ID,USER_ID_AUTHOR, ACCOUNT_ID,PARENT_TASK_ID)
                 VALUES(CRM.CRM_UTILS.GET_TASK_STATUS_ID('NEW'),
                        CRM.CRM_UTILS.GET_TASK_TYPE_ID('WORK ORDER'),
                        CRM.CRM_UTILS.GET_TASK_SUBTYPE_ID('PRODUCT ACTIVATION'),
                        SYSDATE,
                        SYSDATE,
                        CRM.CRM_UTILS.GET_QUEUE_ID('DEVICE_INSTALLATION'),
                        NULL,
                        CRM_USER_ID,
                        IN_ACCOUNT_ID,
                        IN_DEACT_TSK)
        RETURNING ID INTO ACTIVATE_TASK_ID;
    COMMIT;

        /*CREATE INSTALLATION TASK TASK*/
        INSERT INTO TASKS (TASK_STATUS_ID,TASK_TYPE_ID,TASK_SUBTYPE_ID,CREATION_DATE,DUE_DATE,QUEUE_ID,BASKET_ID,USER_ID_AUTHOR, ACCOUNT_ID,PARENT_TASK_ID)
                 VALUES(CRM.CRM_UTILS.GET_TASK_STATUS_ID('NEW'),
                        CRM.CRM_UTILS.GET_TASK_TYPE_ID('INSTALLATION'),
                        CRM.CRM_UTILS.GET_TASK_SUBTYPE_ID('INSTALLATION'),
                        SYSDATE,
                        SYSDATE,
                        CRM.CRM_UTILS.GET_QUEUE_ID('DEVICE_INSTALLATION'),
                        NULL,
                        CRM_USER_ID,
                        IN_ACCOUNT_ID,
                        ACTIVATE_TASK_ID)
        RETURNING ID INTO INSTALL_TASK_ID;

        COMMIT;

        FOR r_prod in OFFER_PRODUCTS
        LOOP 

        insert into mtm_task_product (TASK_ID, PRODUCT_ID) values (ACTIVATE_TASK_ID, r_prod.id);

        if r_prod.device_brand_model_id is not null then 
        insert into mtm_task_product (TASK_ID, PRODUCT_ID) values (INSTALL_TASK_ID, r_prod.id);
        END IF;
                
        commit;

        END LOOP;
        
        SELECT COUNT(DISTINCT product_id) INTO INSTALL_PRODS_CNT 
        FROM mtm_task_product
        WHERE TASK_ID = INSTALL_TASK_ID;
        
        IF INSTALL_PRODS_CNT = 0  
            THEN   
            Begin
            
                UPDATE TASKS T 
                SET T.TASK_STATUS_ID = CRM.CRM_UTILS.GET_TASK_STATUS_ID('IN PROGRESS')
                WHERE  T.ID = INSTALL_TASK_ID;
                commit;
            END;
        END IF;
            
    END ADD_SALE_TASKS;

	procedure NEW_CLIENT_OFFER (C_Name 					IN	Varchar2,
								C_Lastname 				IN	Varchar2,
								C_Pesel 				IN	Varchar2,
								C_Nip 					IN	Varchar2,
								C_Segment_Id 			IN	NUMBER,
								C_Email 				IN	Varchar2,
								C_Phone_Primary 		IN	Varchar2,
								C_Phone_Secondary 		IN	Varchar2,
								A_BILLING_DAY 			IN	NUMBER,
								A_DIVISION_ID 			IN	NUMBER,
								A_INSTALL_ADDRESS_ID	IN 	NUMBER,
								A_BILLING_ADRESS_ID 	IN	NUMBER,
								CN_SIGN_DATE 			IN	DATE,
								CN_START_DATE 			IN	DATE,
								CN_END_DATE 			IN	DATE,
								CN_SALE_CODE_ID 		IN	NUMBER,		
								O_OFFER_OPTION_ID 		IN	NUMBER, 
								O_SALE_DATE 			IN	DATE, 
								OUT_CLIENT_ID 				OUT CRM.client.id%TYPE
								)



			AS 
				OUT_ACCOUNT_ID 	CRM.account.ID%TYPE;
				OUT_CONTRACT_ID CRM.contract.ID%TYPE;
				OUT_OFFER_ID 	CRM.offer.ID%TYPE;

			BEGIN  

			out_client_id:=CRM.client_account_pck.add_client( C_Name 				
															 ,C_Lastname 			
  															 ,C_Pesel 			
															 ,C_Nip 				
															 ,C_Segment_Id 		
															 ,C_Email 			
															 ,C_Phone_Primary 	
															 ,C_Phone_Secondary);

			out_account_id:=CRM.client_account_pck.add_account(  out_client_id 		
																,A_BILLING_DAY 		
																,A_DIVISION_ID 		
																,A_INSTALL_ADDRESS_ID 
																,A_BILLING_ADRESS_ID );                                                               

			out_contract_id:=CRM.client_account_pck.add_contract(CN_SIGN_DATE 		
																,CN_START_DATE 		
																,CN_END_DATE 		
																,out_account_id 		
																,CN_SALE_CODE_ID );           

			out_offer_id:=add_offer( out_account_id 		
									,O_OFFER_OPTION_ID 	
									,CN_SALE_CODE_ID 		
									,O_SALE_DATE );								


            ADD_PRODUCTS(out_offer_id, out_account_id);

            ADD_SALE_TASKS(out_offer_id, out_account_id, CN_SALE_CODE_ID);

			END NEW_CLIENT_OFFER;

    PROCEDURE NEW_ACCOUNT_OFFER(A_CLIENT_ID             IN  NUMBER,
                                A_BILLING_DAY 			IN	NUMBER,
                                A_DIVISION_ID 			IN	NUMBER,
                                A_INSTALL_ADDRESS_ID	IN 	NUMBER,
                                A_BILLING_ADRESS_ID 	IN	NUMBER,
                                CN_SIGN_DATE 			IN	DATE,
                                CN_START_DATE 			IN	DATE,
                                CN_END_DATE 			IN	DATE,
                                CN_SALE_CODE_ID 		IN	NUMBER,		
                                O_OFFER_OPTION_ID 		IN	NUMBER, 
                                O_SALE_DATE 			IN	DATE,
								OUT_ACCOUNT_ID		   OUT	NUMBER
                                ) 
            AS

                OUT_CONTRACT_ID CRM.contract.ID%TYPE;
                OUT_OFFER_ID 	CRM.offer.ID%TYPE;

			BEGIN  

			out_account_id:=CRM.client_account_pck.add_account(A_CLIENT_ID 		
                                                              ,A_BILLING_DAY 		
                                                              ,A_DIVISION_ID 		
                                                              ,A_INSTALL_ADDRESS_ID 
                                                              ,A_BILLING_ADRESS_ID );                                                               

			out_contract_id:=CRM.client_account_pck.add_contract(CN_SIGN_DATE 		
																,CN_START_DATE 		
																,CN_END_DATE 		
																,out_account_id 		
																,CN_SALE_CODE_ID );           

			out_offer_id:=add_offer( out_account_id 		
									,O_OFFER_OPTION_ID 	
									,CN_SALE_CODE_ID 		
									,O_SALE_DATE );								


            ADD_PRODUCTS(out_offer_id, out_account_id);

            ADD_SALE_TASKS(out_offer_id, out_account_id, CN_SALE_CODE_ID);

            END NEW_ACCOUNT_OFFER;

     PROCEDURE UPSELL_OFFER (IN_ACCOUNT_ID      IN CRM.ACCOUNT.ID%TYPE, 
                             IN_OFFER_OPTION_ID IN CRM.OFFER_OPTION.ID%TYPE, 
                             IN_SALE_CODE_ID    IN CRM.SALE_CODE.ID%TYPE, 
                             IN_SALE_DATE       IN CRM.OFFER.SALE_DATE%TYPE) 
            AS 
            OUT_OFFER_ID CRM.OFFER.ID%TYPE;

            BEGIN 

                OUT_OFFER_ID:=add_offer( IN_ACCOUNT_ID 		
                                        ,IN_OFFER_OPTION_ID 	
                                        ,IN_SALE_CODE_ID 		
                                        ,IN_SALE_DATE );								

                ADD_PRODUCTS(OUT_OFFER_ID, IN_ACCOUNT_ID);

                ADD_SALE_TASKS(out_offer_id, IN_ACCOUNT_ID, IN_SALE_CODE_ID);


            END UPSELL_OFFER;

    PROCEDURE ADD_UPGARDE_TASKS(IN_OFFER_ID     CRM.OFFER.ID%TYPE,
                                IN_ACCOUNT_ID   CRM.ACCOUNT.ID%TYPE, 
                                CN_SALE_CODE_ID CRM.sale_code.id%TYPE) 
            AS 

            DEACT_TSK_ID CRM.TASKS.ID%TYPE;
            CRM_USER_ID CRM.users_struct.user_id%TYPE;

            cursor DEACT_PRODS 
            IS
                Select  
                    p.id 
                from product p 
                inner join mtm_offer_product mop on p.id = mop.product_id
                where 
                    p.account_id = IN_ACCOUNT_ID
                    and mop.offer_id != IN_OFFER_ID
                    and (trunc(p.deactivation_date,'dd') >= trunc(sysdate,'dd') or p.deactivation_date is null ) ;

            BEGIN 

                SELECT USER_ID INTO CRM_USER_ID 
                FROM USERS_STRUCT US 
                INNER JOIN sale_code SC ON us.id=sc.sales_struct_id
                WHERE 
                    sc.id= CN_SALE_CODE_ID;            

                INSERT INTO TASKS (TASK_STATUS_ID,TASK_TYPE_ID,TASK_SUBTYPE_ID,CREATION_DATE,DUE_DATE,QUEUE_ID,BASKET_ID,USER_ID_AUTHOR, ACCOUNT_ID,PARENT_TASK_ID)
                         VALUES(CRM.CRM_UTILS.GET_TASK_STATUS_ID('NEW'),
                                CRM.CRM_UTILS.GET_TASK_TYPE_ID('deactivation'),
                                CRM.CRM_UTILS.GET_TASK_SUBTYPE_ID('upgrade'),
                                SYSDATE,
                                SYSDATE,
                                CRM.CRM_UTILS.GET_QUEUE_ID('DEVICE_INSTALLATION'),
                                NULL,
                                CRM_USER_ID,
                                IN_ACCOUNT_ID,
                                null)
                RETURNING ID INTO DEACT_TSK_ID;

                FOR TSK_PROD IN DEACT_PRODS
                LOOP

                 insert into mtm_task_product (TASK_ID, PRODUCT_ID) values (DEACT_TSK_ID, TSK_PROD.ID);

                END LOOP;

                COMMIT;                

              ADD_SALE_TASKS(IN_OFFER_ID, IN_ACCOUNT_ID, CN_SALE_CODE_ID, DEACT_TSK_ID);

            END ADD_UPGARDE_TASKS;


     PROCEDURE UPGRADE_OFFER (IN_ACCOUNT_ID      IN CRM.ACCOUNT.ID%TYPE, 
                              IN_OFFER_OPTION_ID IN CRM.OFFER_OPTION.ID%TYPE, 
                              IN_SALE_CODE_ID    IN CRM.SALE_CODE.ID%TYPE, 
                              IN_SALE_DATE       IN CRM.OFFER.SALE_DATE%TYPE)
            AS 

            OUT_OFFER_ID CRM.OFFER.ID%TYPE;

            BEGIN

                OUT_OFFER_ID:=add_offer( IN_ACCOUNT_ID 		
                                        ,IN_OFFER_OPTION_ID 	
                                        ,IN_SALE_CODE_ID 		
                                        ,IN_SALE_DATE );

                 ADD_PRODUCTS(OUT_OFFER_ID, IN_ACCOUNT_ID);                       

                 ADD_UPGARDE_TASKS(OUT_OFFER_ID,
                                   IN_ACCOUNT_ID,
                                   IN_SALE_CODE_ID);

            END UPGRADE_OFFER ;

END CLIENT_OFFER_PCK;
/
