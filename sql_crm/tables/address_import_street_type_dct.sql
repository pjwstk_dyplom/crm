create table crm.address_import_street_type_dct 
(
input_ varchar2(10),
output_ varchar2(10)
);
insert into address_import_street_type_dct (input_, output_) values ('al.','al.');  
insert into address_import_street_type_dct (input_, output_) values ('aleja','al.');  
insert into address_import_street_type_dct (input_, output_) values ('aleje','al.');  
insert into address_import_street_type_dct (input_, output_) values ('bulwar','bulwar');  
insert into address_import_street_type_dct (input_, output_) values ('osiedle','os.');  
insert into address_import_street_type_dct (input_, output_) values ('os.','os.');  
insert into address_import_street_type_dct (input_, output_) values ('o�.','os.');  
insert into address_import_street_type_dct (input_, output_) values ('pasa�','pasa�');  
insert into address_import_street_type_dct (input_, output_) values ('plac','pl.');  
insert into address_import_street_type_dct (input_, output_) values ('pl.','pl.');  
insert into address_import_street_type_dct (input_, output_) values ('rondo','rondo');  
insert into address_import_street_type_dct (input_, output_) values ('rynek','rynek');  
insert into address_import_street_type_dct (input_, output_) values ('skwer','skwer');  
insert into address_import_street_type_dct (input_, output_) values ('szosa','szosa');  
insert into address_import_street_type_dct (input_, output_) values ('trakt','trakt');  
insert into address_import_street_type_dct (input_, output_) values ('ulica','ul.');  
insert into address_import_street_type_dct (input_, output_) values ('ul.','ul.');  

