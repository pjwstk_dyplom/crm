CREATE TABLE crmapp_acclist_files (
  base_id NUMBER(12) NOT NULL,
  request_id NUMBER(12) NOT NULL,
  dirpath VARCHAR2(500) NOT NULL,
  filename VARCHAR2(200) NOT NULL,
  requestor VARCHAR2(50) NOT NULL,
  create_date DATE DEFAULT SYSDATE NOT NULL,
  CONSTRAINT crmapp_acclist_files_pk PRIMARY KEY (base_id, request_id),
  CONSTRAINT crmapp_acclist_files_fk1 FOREIGN KEY (base_id) REFERENCES crmapp_acclist_base (id) ON DELETE CASCADE
);
