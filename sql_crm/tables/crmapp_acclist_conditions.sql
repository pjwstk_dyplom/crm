CREATE TABLE CRMAPP_ACCLIST_CONDITIONS (
  base_id NUMBER(12) NOT NULL,
  req_id NUMBER(12) NOT NULL,
  relation CHAR(1)  DEFAULT '=' NOT NULL CHECK (relation in ('=', '<', '>', '!')),
  value NUMBER NOT NULL,
  CONSTRAINT crmapp_acclist_cond_pk PRIMARY KEY (base_id, req_id, relation),
  CONSTRAINT crmapp_acclist_cond_fk1 FOREIGN KEY (base_id) REFERENCES CRMAPP_ACCLIST_BASE (id) ON DELETE CASCADE,
  CONSTRAINT crmapp_acclist_cond_fk2 FOREIGN KEY (req_id) REFERENCES offer_requirement_definition (id)
);