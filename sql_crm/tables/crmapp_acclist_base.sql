CREATE TABLE crmapp_acclist_base (
  id NUMBER(12) GENERATED ALWAYS AS IDENTITY,
  title VARCHAR2(20) NOT NULL,
  description VARCHAR2(500) NOT NULL,
  author_id NUMBER(12) NOT NULL,
  CONSTRAINT crmapp_acclist_base_pk PRIMARY KEY (id)
);