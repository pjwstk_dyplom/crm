CREATE TABLE CRMAPP_JM_TRG_INFO_UPVT(	
  TRIGGER_ID NUMBER(12,0) NOT NULL, 
  STATUS VARCHAR2(9 BYTE) NOT NULL, 
  QUANTITY NUMBER,
  CONSTRAINT CRMAPP_JM_TRG_INFO_UPVT_pk PRIMARY KEY (TRIGGER_ID, STATUS)
 ) NOLOGGING;