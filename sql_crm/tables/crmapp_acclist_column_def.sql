CREATE TABLE CRMAPP_ACCLIST_COLUMN_DEF (
  id NUMBER(12) GENERATED ALWAYS AS IDENTITY,
  table_name VARCHAR2(50) NOT NULL,
  field_name VARCHAR2(50) NOT NULL,
  table_field AS (table_name || '.' || field_name),
  column_alias VARCHAR2(50) NOT NULL CHECK (REGEXP_LIKE(column_alias, '^[A-Z_]+$')),
  constraint CRMAPP_ACCLIST_COLUMN_DEF_pk PRIMARY KEY (id),
  constraint CRMAPP_ACCLIST_COLUMN_DEF_fk1 FOREIGN KEY (table_name) REFERENCES CRMAPP_ACCLIST_TABLE_JOIN(table_name)
);