CREATE OR REPLACE trigger sale_code_insert_trg 
before INSERT 
    ON CRM.SALE_CODE
    FOR EACH ROW 
DECLARE 
CNT NUMBER;
LAST_VALID_FROM DATE;
LAST_ID NUMBER;
BEGIN 
SELECT NVL(COUNT(*),0) INTO CNT FROM CRM.SALE_CODE SC
WHERE SC.SALES_STRUCT_ID=:NEW.SALES_STRUCT_ID;

 IF (:NEW.VALID_FROM >= :NEW.VALID_TO) 
    THEN 
        RAISE_APPLICATION_ERROR(-20001,' valid_to date should be later than valid_from date.');
    END IF;

IF CNT != 0 
THEN 
    BEGIN 

    SELECT MAX(SC.VALID_FROM) INTO LAST_VALID_FROM FROM CRM.SALE_CODE SC
    WHERE SC.SALES_STRUCT_ID=:NEW.SALES_STRUCT_ID;

        IF LAST_VALID_FROM>= :NEW.VALID_FROM 
            THEN
            RAISE_APPLICATION_ERROR(-20002,'valid from date should be later than latest valid from date ('||LAST_VALID_FROM||') of SALES_STRUCT_ID: '|| :NEW.SALES_STRUCT_ID);
            END IF;

          UPDATE SALE_CODE S 
          SET S.VALID_TO=:NEW.VALID_FROM-1
          WHERE 
          S.SALES_STRUCT_ID=:NEW.SALES_STRUCT_ID
          AND S.VALID_TO>=:NEW.VALID_FROM;


    END;
END IF;

END USERS_STRUCT_INSERT_TRG ;