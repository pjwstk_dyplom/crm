CREATE or replace TRIGGER USERS_STRUCT_INSERT_TRG 
BEFORE INSERT 
    ON CRM.users_struct 
    FOR EACH ROW 
DECLARE 
CNT NUMBER;
last_valid_from date;
last_id number;
BEGIN 
SELECT nvl(COUNT(*),0) INTO CNT FROM CRM.users_struct US
WHERE us.user_id= :new.user_id;

 if (:new.valid_from >= :new.valid_to) 
    then 
        RAISE_APPLICATION_ERROR(-20001,' valid_to date should be later than valid_from date.');
    end if;

if cnt != 0 
then 
    begin 
    
    select max(us.valid_from) into last_valid_from from crm.users_struct us 
    where us.user_id=:new.user_id;
        
        if last_valid_from>= :new.valid_from 
            then
            RAISE_APPLICATION_ERROR(-20002,'valid from date should be later than latest valid from date ('||last_valid_from||') of USER_ID: '|| :new.user_id);
            end if;
        
    
          update users_struct s 
          set s.valid_to=:new.valid_from-1
          where 
          s.user_id=:new.user_id
          and s.valid_to>=:new.valid_from;
          

          

          
          
            
    end;
end if;

END USERS_STRUCT_INSERT_TRG ;
