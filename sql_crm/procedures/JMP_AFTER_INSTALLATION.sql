create or replace procedure JMP_AFTER_INSTALLATION (p_tasks_id crm.tasks.id%type) as 

parent_task crm.tasks.parent_task_id%type;
deact_task crm.tasks.parent_task_id%type;


begin 

select MAX(t.parent_task_id) into parent_task from tasks t where  t.id=p_tasks_id;
select MAX(t.parent_task_id) into deact_task from tasks t where  t.id=parent_task;


update device d 
set d.crm_user_id = NULL
where 
d.id in (SELECT 
            p.device_id 
         FROM tasks t 
         inner join mtm_task_product mtp on t.id=mtp.task_id
         inner join product p on mtp.product_id = p.id
         where 
            t.id = p_tasks_id);

update product p 
set 
p.activation_date = sysdate ,
p.product_status_id = crm.crm_utils.get_prod_status_id('active')
where 
p.id in (SELECT 
            mtp.product_id 
         FROM tasks t 
         inner join mtm_task_product mtp on t.id=mtp.task_id
         where 
            t.id = parent_task );   


update tasks t 
set 
t.task_status_id = crm.crm_utils.get_task_status_id('completed'),
t.close_date = sysdate
where 
t.id = parent_task;

update tasks t 
set 
t.task_status_id = crm.crm_utils.get_task_status_id('completed')
where 
t.id = p_tasks_id;

if deact_task is not null 
    then 
        update tasks t 
        set 
        t.task_status_id = crm.crm_utils.get_task_status_id('in progress')
        where 
        t.id = deact_task;
    end if;

commit;
end;
/

declare 

ACTION_TRIGGER_ID crm.jm_action_trigger.id%type;
PROCEDURE_ID crm.jm_procedure.id%type;

begin 

insert into crm.jm_action_trigger( VALID_FROM, VALID_TO, PRIORITY, TASK_STATUS_ID, TASK_SUBTYPE_ID,TASK_TYPE_ID) values (to_date('2000-01-01','yyyy-mm-dd'),to_date('2200-01-01','yyyy-mm-dd'), 1, crm.crm_utils.GET_TASK_STATUS_ID('in progress'), crm.crm_utils.GET_TASK_SUBTYPE_ID('installation'),crm.crm_utils.GET_TASK_TYPE_ID('installation'))
RETURNING id into ACTION_TRIGGER_ID;
insert into crm.jm_procedure (procedure_name) values ('JMP_AFTER_INSTALLATION')
RETURNING id into PROCEDURE_ID;
insert into crm.jm_action_list (JM_ACTION_TRIGGER_ID, SEQ, JM_PROCEDURE_ID) values (ACTION_TRIGGER_ID, 1,PROCEDURE_ID) ;
commit;
end;

