CREATE OR REPLACE PROCEDURE crmapp_get_offer_options(
  p_account_id IN NUMBER, 
  o_refCursor OUT SYS_REFCURSOR
) AS
BEGIN
  OPEN o_refCursor FOR 
  SELECT offer_option_id, option_evaluation
    FROM TABLE(OFFER_CALCULATION_P.getOptionEvaluations(p_account_id => p_account_id));
END;
/