CREATE OR REPLACE PROCEDURE crmapp_jm_trg_info_refresh AS
BEGIN
  DELETE FROM crmapp_jm_trg_info;
  INSERT INTO crmapp_jm_trg_info
    SELECT * FROM crmapp_jm_trg_info_v;
    
  DELETE FROM crmapp_jm_trg_info_upvt;
  INSERT INTO crmapp_jm_trg_info_upvt
    SELECT * FROM crmapp_jm_trg_info_upvt_v;
    
  COMMIT;
END;
/