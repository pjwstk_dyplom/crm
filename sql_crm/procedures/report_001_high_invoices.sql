CREATE OR REPLACE PROCEDURE report_001_high_invoices (
  p_last_days IN NUMBER DEFAULT 30, 
  p_min_total IN NUMBER DEFAULT 100, 
  p_export_dir IN VARCHAR2 DEFAULT 'REPORT_001_HIGH_INVOICE_DIR',
  p_filename IN VARCHAR2 DEFAULT NULL,
  p_crm_report_id IN NUMBER DEFAULT NULL) 
AS
  c_report_name VARCHAR2(40) := 'High_invoices';
  v_directory_check NUMBER;
  v_dir_path VARCHAR2(4000);
  v_filename VARCHAr2(60);
  v_sql VARCHAR2(4000);
  
  v_group_id NUMBER;
  v_groupcount NUMBER;
  v_emails VARCHAR2(4000);
  CURSOR v_notification_cursor IS
    SELECT 
      group_id,
      groupcount,
      emails
    FROM TABLE(automatic_reports_p.get_report_notifications(p_report_id => p_crm_report_id));
BEGIN
  -- define the query
  v_sql := '
  SELECT
    i.invoice_number,
    i.account_id,
    i.total,
    i.creation_date,
    i.paid_amount,
    it.name invoice_type
  FROM invoice i
  INNER JOIN invoice_type it
    ON it.id = i.invoice_type_id
  WHERE 1=1
    AND i.creation_date >= TRUNC(sysdate - ' || p_last_days || ')
    AND i.total >= ' || p_min_total || '
  ';
  
  -- save to directory
  SELECT COUNT(1), MAX(directory_path) as dir_path INTO v_directory_check, v_dir_path FROM all_directories WHERE directory_name = p_export_dir;
  v_filename := NVL(p_filename, c_report_name || '_' || TO_CHAR(SYSDATE, 'yyyymmddhh24miss') || '.csv');
  IF v_directory_check = 1 THEN
    file_utility_p.query2csv_full_sql(
      p_sql => v_sql, 
      p_directory => p_export_dir, 
      p_filename => v_filename);
    -- send notifications
    OPEN v_notification_cursor; 
     LOOP 
     FETCH v_notification_cursor into v_group_id, v_groupcount, v_emails; 
        EXIT WHEN v_notification_cursor%notfound; 
        crmapp_send_mail(p_recipients => v_emails, p_subject => 'Report ' || c_report_name, p_body => 'File ' || v_filename || ' available at location: ' || v_dir_path);
     END LOOP; 
     CLOSE v_notification_cursor; 
  END IF;
  
END;
/