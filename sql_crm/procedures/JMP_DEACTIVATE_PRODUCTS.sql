CREATE OR REPLACE PROCEDURE CRM.JMP_DEACTIVATE_PRODUCTS (P_TASKS_ID CRM.TASKS.PARENT_TASK_ID%TYPE) 
AS
BEGIN 
    UPDATE PRODUCT P 
        SET
            P.DEACTIVATION_DATE = SYSDATE,
            P.PRODUCT_STATUS_ID = CRM.CRM_UTILS.GET_PROD_STATUS_ID('deactivated'),
            P.DEVICE_ID = NULL
        WHERE 
        P.ID IN (Select MTP.PRODUCT_ID FROM mtm_task_product MTP WHERE TASK_ID = P_TASKS_ID);
        
    UPDATE MTM_OFFER_PRODUCT MOP 
        set 
            MOP.VALID_TO = SYSDATE
        WHERE
            mop.product_id IN (Select MTP.PRODUCT_ID FROM mtm_task_product MTP WHERE TASK_ID = P_TASKS_ID)
        AND (MOP.VALID_TO >= SYSDATE OR MOP.VALID_TO IS NULL) ;
        
    update tasks t 
    set 
        t.task_status_id = crm.crm_utils.get_task_status_id('completed'),
        t.close_date = sysdate
    where 
        t.id = P_TASKS_ID;    

commit;
END ;
/

begin 

insert into crm.jm_action_trigger( VALID_FROM, VALID_TO, PRIORITY, TASK_STATUS_ID, TASK_SUBTYPE_ID,TASK_TYPE_ID)
                          values (to_date('2000-01-01','yyyy-mm-dd'),
                                  to_date('2200-01-01','yyyy-mm-dd'), 
                                  1, 
                                  crm.crm_utils.GET_TASK_STATUS_ID('in progress'), 
                                  CRM.CRM_UTILS.GET_TASK_SUBTYPE_ID('upgrade'),
                                  CRM.CRM_UTILS.GET_TASK_TYPE_ID('deactivation'))
RETURNING id into ACTION_TRIGGER_UPDT_ID;

insert into crm.jm_action_trigger( VALID_FROM, VALID_TO, PRIORITY, TASK_STATUS_ID, TASK_SUBTYPE_ID,TASK_TYPE_ID)
                          values (to_date('2000-01-01','yyyy-mm-dd'),
                                  to_date('2200-01-01','yyyy-mm-dd'), 
                                  1, 
                                  crm.crm_utils.GET_TASK_STATUS_ID('in progress'), 
                                  CRM.CRM_UTILS.GET_TASK_SUBTYPE_ID('product deactivation'),
                                  CRM.CRM_UTILS.GET_TASK_TYPE_ID('deactivation'))
RETURNING id into ACTION_TRIGGER_DEACT_ID;

insert into crm.jm_procedure (procedure_name) 
                      values ('CRM.JMP_DEACTIVATE_PRODUCTS')
RETURNING id into PROCEDURE_ID;

insert into crm.jm_action_list (JM_ACTION_TRIGGER_ID, SEQ, JM_PROCEDURE_ID) 
                        values (ACTION_TRIGGER_UPDT_ID, 1,PROCEDURE_ID) ;
                        
insert into crm.jm_action_list (JM_ACTION_TRIGGER_ID, SEQ, JM_PROCEDURE_ID) 
                        values (ACTION_TRIGGER_DEACT_ID, 1,PROCEDURE_ID) ;                        
commit;
end;