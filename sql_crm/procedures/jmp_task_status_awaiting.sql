create or replace PROCEDURE jmp_task_status_awaiting(p_tasks_id tasks.id%type) AS
BEGIN
  UPDATE tasks
  SET task_status_id = 3
  WHERE id = p_tasks_id;
END;