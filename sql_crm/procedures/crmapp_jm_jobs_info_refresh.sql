CREATE OR REPLACE PROCEDURE crmapp_jm_jobs_info_refresh AS
BEGIN
  DELETE FROM crmapp_jm_jobs_info;
  INSERT 
  INTO crmapp_jm_jobs_info (
    JOB_NAME, 
    JOB_ACTION, 
    REPEAT_INTERVAL, 
    ENABLED, 
    RUN_COUNT, 
    FAILURE_COUNT, 
    LAST_START_DATE, 
    NEXT_RUN_DATE, 
    LAST_ERROR_DATE, 
    LAST_ERROR_CODE, 
    LAST_ERROR_MESSAGE, 
    RECENT_RUN_COUNT, 
    RECENT_CPU_S, 
    RECENT_DURATION_S, 
    RECENT_FAILURES, 
    RECENT_AVG_CPU_S, 
    RECENT_AVG_DURATION_S
  )
  SELECT
    JOB_NAME, 
    JOB_ACTION, 
    REPEAT_INTERVAL, 
    ENABLED, 
    RUN_COUNT, 
    FAILURE_COUNT, 
    LAST_START_DATE, 
    NEXT_RUN_DATE, 
    LAST_ERROR_DATE, 
    LAST_ERROR_CODE, 
    LAST_ERROR_MESSAGE, 
    RECENT_RUN_COUNT, 
    RECENT_CPU_S, 
    RECENT_DURATION_S, 
    RECENT_FAILURES, 
    ROUND(RECENT_AVG_CPU_S, 3), 
    ROUND(RECENT_AVG_DURATION_S, 3)
  FROM crmapp_jm_jobs_info_v;
  COMMIT;

  DELETE FROM crmapp_jm_jobs_info_snap WHERE snapshot_datetime < ADD_MONTHS(TRUNC(SYSDATE, 'year'), -12);
  INSERT 
  INTO crmapp_jm_jobs_info_snap (
    JOB_NAME, 
    SNAPSHOT_DATETIME, 
    RECENT_RUN_COUNT, 
    RECENT_CPU_S, 
    RECENT_DURATION_S, 
    RECENT_FAILURES, 
    RECENT_AVG_CPU_S, 
    RECENT_AVG_DURATION_S
  )
  SELECT  
    v.job_name,
    SYSDATE AS snapshot_datetime,
    v.recent_run_count,
    v.recent_cpu_s,
    v.recent_duration_s,
    v.recent_failures,
    v.recent_avg_cpu_s,
    v.recent_avg_duration_s
  FROM crmapp_jm_jobs_info v;
  COMMIT;
END;
/