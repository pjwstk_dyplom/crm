CREATE OR REPLACE PROCEDURE crmapp_send_mail(p_recipients IN VARCHAR2, p_subject IN VARCHAR2, p_body IN VARCHAR2) AS
BEGIN
  INSERT INTO CRMAPP_EMAILS (recipients, subject, mailbody)
  VALUES (p_recipients, p_subject, p_body);
  COMMIT;
END;
/