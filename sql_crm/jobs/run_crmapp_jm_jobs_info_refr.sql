BEGIN
    DBMS_SCHEDULER.CREATE_JOB (
            job_name => 'RUN_CRMAPP_JM_JOBS_INFO_REFR',
            job_type => 'STORED_PROCEDURE',
            job_action => 'CRMAPP_JM_JOBS_INFO_REFRESH',
            number_of_arguments => 0,
            start_date => NULL,
            repeat_interval => 'FREQ=MINUTELY;INTERVAL=15',
            end_date => NULL,
            enabled => TRUE,
            auto_drop => FALSE,
            comments => '');
END;
