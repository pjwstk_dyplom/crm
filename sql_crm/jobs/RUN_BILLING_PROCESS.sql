BEGIN 
dbms_scheduler.create_job('"RUN_BILLING_PROCESS"',
job_type=>'PLSQL_BLOCK', job_action=>
'BEGIN
 billing_process_pck.invoice_base_f(sysdate);
 end;'
, number_of_arguments=>0,
start_date=>NULL, repeat_interval=> 
'FREQ=DAILY;BYTIME=000100'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
'Running daily billing process'
);
COMMIT; 
END; 
