BEGIN
    DBMS_SCHEDULER.CREATE_JOB (
            job_name => '"JOB_MACHINE_CONSUMER_1"',
            job_type => 'STORED_PROCEDURE',
            job_action => 'CRM.JOB_MACHINE_P.EXECUTE_FIFO_ALL',
            number_of_arguments => 0,
            start_date => NULL,
            repeat_interval => 'FREQ=MINUTELY;INTERVAL=5',
            end_date => NULL,
            enabled => FALSE,
            auto_drop => FALSE,
            comments => '');

         
     
 
    DBMS_SCHEDULER.SET_ATTRIBUTE( 
             name => '"JOB_MACHINE_CONSUMER_1"', 
             attribute => 'store_output', value => TRUE);
    DBMS_SCHEDULER.SET_ATTRIBUTE( 
             name => '"JOB_MACHINE_CONSUMER_1"', 
             attribute => 'logging_level', value => DBMS_SCHEDULER.LOGGING_FAILED_RUNS);
      
   
  
    
    DBMS_SCHEDULER.enable(
             name => '"JOB_MACHINE_CONSUMER_1"');
END;
