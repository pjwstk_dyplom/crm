-- create temporary table of products boundle with time periods. 
create table tmp_offer_build as 
with cal as 
(
select a.min_dt+rownum dt from  (select min(trunc(p.activation_date,'dd')) min_dt from crm.product p) a
connect by rownum <(SELECT trunc(sysdate,'dd')- min(trunc(p.activation_date,'dd'))
    FROM crm.product p)   
),
 ops as 
 (
 SELECT op.id op_id, op.valid_from, op.valid_to , listagg(pd.id ,' | ' ) within group (order by pd.id ) pds FROM offer_option op
inner join offer_option_product oop on op.id=oop.offer_option_id
inner join product_definition pd on oop.product_definition_id= pd.id
group by op.id, op.valid_from, op.valid_to
),

 prod_boundle as 
 (
SELECT 
p.account_id,
p.id prod_id,
p.activation_date,
p.deactivation_date,
c.dt, 
p.product_definition_id,
listagg(pd.id ,' | ' ) within group (order by pd.id ) over  (partition by p.account_id, c.dt) pds  FROM crm.product p 
inner join cal c on c.dt between p.activation_date and nvl(p.deactivation_date,c.dt)
inner join crm.product_definition pd on p.product_definition_id=pd.id
inner join crm.product_category pc on pd.product_category_id= pc.id
), 
 of_prod as (
SELECT  op.op_id, pb.account_id, pb.prod_id, pb.product_definition_id ,min(pb.dt) valid_from , 
case when max(pb.dt) = trunc(sysdate-1,'dd') 
    then to_date('2222-02-02','yyyy-mm-dd') 
 else max(pb.dt) end valid_to
FROM prod_boundle pb
inner join ops  op on op.pds=pb.pds 
group by op.op_id, pb.account_id, pb.prod_id, pb.product_definition_id
)

SELECT * FROM of_prod;

-- temporary change of mtm_offer_product offer_id constraint
    ALTER TABLE MTM_OFFER_PRODUCT
DROP CONSTRAINT MTM_OFFER_PRODUCT_OFFER;

      ALTER TABLE "CRM"."MTM_OFFER_PRODUCT" ADD CONSTRAINT "MTM_OFFER_PRODUCT_OFFER" FOREIGN KEY ("OFFER_ID")
	  REFERENCES "CRM"."OFFER" ("ID") DEFERRABLE ENABLE;
      
      

-- create offer and mtm_offer_product_product data
declare 

offer_id  offer.id%TYPE;

CURSOR c_of_base  is 
SELECT distinct b.op_id, b.account_id, b.valid_from, nvl(b.valid_to,to_date('2222-02-02','yyyy-mm-dd')) valid_to  FROM tmp_offer_build b;

begin 

for base in c_of_base
loop

insert into crm.offer(account_id, sale_code_id, sale_date, offer_option_id) 
        values  (base.account_id, 2, base.valid_from, base.op_id ) 
        RETURNING id into offer_id;
       --commit; 

execute IMMEDIATE (
' insert into CRM.mtm_offer_product (product_id, offer_id, valid_from, valid_to)
select 
prod_id,
'||offer_id||' as offer_id,
valid_from,
valid_to
from tmp_offer_build b
where 
b.op_id ='||base.op_id||'
and b.account_id = '||base.account_id||'
and b.valid_from =to_date('''||to_char(base.valid_from,'yyyy-mm-dd')||''',''yyyy-mm-dd'')
and b.valid_to =to_date('''||to_char(base.valid_to,'yyyy-mm-dd')||''',''yyyy-mm-dd'')
'
);
--commit;
end loop;


end;

-- reverse constraint change
      ALTER TABLE MTM_OFFER_PRODUCT
DROP CONSTRAINT MTM_OFFER_PRODUCT_OFFER;

      ALTER TABLE "CRM"."MTM_OFFER_PRODUCT" ADD CONSTRAINT "MTM_OFFER_PRODUCT_OFFER" FOREIGN KEY ("OFFER_ID")
	  REFERENCES "CRM"."OFFER" ("ID") ENABLE;


drop table tmp_offer_build;




