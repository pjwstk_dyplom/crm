create table tmp_tsk_off as 
SELECT distinct t.id, mop.offer_id FROM tasks t
inner join mtm_task_product mtp on mtp.task_id= t.id
inner join mtm_offer_product mop on mop.product_id= mtp.product_id and mop.valid_from = t.CREATION_DATE;
commit;


declare 
p_tsk_id crm.tasks.id%type;

cursor tsk is 
select ID, t.CREATION_DATE, t.DUE_DATE, t.CLOSER_ID, t.USER_ID_AUTHOR, t.ACCOUNT_ID from tasks t 
where 
t.task_type_id=61 and
t.parent_task_id is  null
for update of t.parent_task_id;

begin 

for ctsk in tsk
loop 

insert into tasks ( TASK_STATUS_ID,	TASK_TYPE_ID,	TASK_SUBTYPE_ID, queue_id,	CREATION_DATE,	DUE_DATE, USER_ID_AUTHOR, ACCOUNT_ID) 
values (1, 1, 1,1,  ctsk.CREATION_DATE, ctsk.DUE_DATE, ctsk.USER_ID_AUTHOR , ctsk.ACCOUNT_ID )
RETURNING id into p_tsk_id;




insert into mtm_task_product mtp (task_id, product_id)
select p_tsk_id, mop.product_id from mtm_offer_product mop 
where mop.offer_id = (select tmp.offer_id from tmp_tsk_off  tmp where  tmp.id= ctsk.id);


update tasks t 
set t.parent_task_id = p_tsk_id
where 
t.id = ctsk.id;


end loop;

end;

drop table tmp_tsk_off; commit;
