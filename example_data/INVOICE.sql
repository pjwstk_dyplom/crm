set SERVEROUTPUT on;
declare 

CURSOR cur_dt is
select a.min_dt+rownum dt from  (select min(trunc(p.valid_from,'dd')) min_dt from crm.mtm_offer_product p) a
connect by rownum <(SELECT trunc(sysdate,'dd')- min(trunc(p.valid_from,'dd'))
    FROM crm.mtm_offer_product p);   
    
begin 

for i_dt in cur_dt 
loop
 dbms_output.put_line(i_dt.dt);
 billing_process_pck.invoice_base_f(i_dt.dt);

end loop;

end ;
