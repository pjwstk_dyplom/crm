OPTIONS (SKIP = 1, ERRORS = 3, ROWS = '1000', DIRECT = TRUE)
LOAD DATA
INFILE 'interact.csv'
BADFILE 'bad.txt'
APPEND
INTO TABLE interact
FIELDS TERMINATED BY ";" optionally enclosed by '"'
TRAILING NULLCOLS
(
  creation_date DATE "yyyy-mm-dd hh24:mi:ss",
  account_id CHAR,
  interact_type_id CHAR,
  interact_direction_id CHAR,
  interact_medium_id CHAR,
  user_id CHAR,
  description CHAR,
  tasks_id CHAR
)
