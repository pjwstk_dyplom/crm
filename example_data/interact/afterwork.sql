select * from dual;
ALTER TABLE interact enable constraint INTERACT_USER;
ALTER TABLE interact enable constraint INTERACT_TASKS;
ALTER TABLE interact enable constraint INTERACT_INTERACT_TYPE;
ALTER TABLE interact enable constraint INTERACT_INTERACT_MEDIUM;
ALTER TABLE interact enable constraint INTERACT_INTERACT_DIRECTION;
ALTER TABLE interact enable constraint INTERACT_ACCOUNT;
exit