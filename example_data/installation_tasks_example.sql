DECLARE 
taskType    task_type.id%type;
taskSubtype task_subtype.id%type;
paramDef    task_param_definition.id%type;
queueId     queue.id%type;
offerId     offer.id%type;
taskStatus  task_status.id%TYPE;
nTaskId     tasks.id%type;
cursor taskBase 
is 
        SELECT distinct
            --p.id prod_id, 
            o.id offer_id,
            o.account_id
        FROM product p 
        left join  device                 d    on p.device_id = d.id
        inner join product_definition    pd    on p.product_definition_id = pd.id
        inner join mtm_offer_product    mop    on p.id = mop.product_id
        inner join offer                  o    on mop.offer_id = o.id
        inner join offer_option_product oop    on o.OFFER_OPTION_ID = oop.OFFER_OPTION_ID
                                              AND pd.id = oop.PRODUCT_DEFINITION_ID
        inner join device_brand_model   dbm    on oop.DEVICE_BRAND_MODEL_ID = dbm.id
        where 
        d.id is null;
        
cursor products 
is 
SELECT mop.product_id FROM offer o 
inner join mtm_offer_product mop on o.id=mop.offer_id
where 
o.id =offerId;
        
BEGIN 

select id into taskType from task_type where  name ='installation';
select id into taskSubtype from task_subtype where  name = 'installation';
Select id into taskStatus from task_status  where name = 'new';
select id into queueId  from queue where name='device_installation';
select tp.id into paramDef from task_param_definition tp
                        inner join mtm_paramdef_tasktype mt ON tp.id = mt.task_param_definition_id
                                                           AND mt.task_type_id = taskType
                                                           and mt.task_subtype_id = taskSubtype;
                                                           
FOR nTask in taskBase
LOOP
 offerId := nTask.offer_id;
 
 insert into tasks (TASK_STATUS_ID, TASK_TYPE_ID, TASK_SUBTYPE_ID, CREATION_DATE, DUE_DATE, CLOSE_DATE, CLOSER_ID, QUEUE_ID, BASKET_ID, USER_ID_AUTHOR, PARENT_TASK_ID, ACCOUNT_ID)
            values (taskStatus, taskType, taskSubtype, sysdate, sysdate+7, null, null, queueId, null, 3, null, nTask.account_id)
returning id into nTaskId;
commit;

insert into task_param (TASK_PARAM_DEFINITION_ID, TASK_ID, VALUE)
                VALUES (paramDef, nTaskId, nTask.offer_id);
commit;                

    FOR nProd in products
        LOOP
        insert into mtm_task_product (TASK_ID, PRODUCT_ID) 
                              values (nTaskId, nProd.product_id);
                              commit;
        
        
        END LOOP;


END LOOP;

END;


