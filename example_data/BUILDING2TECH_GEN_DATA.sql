--set serveroutput on;
declare 
 b_id crm.address_building.id%type;
 b_type crm.building_type.description%type; 
 b_fl number;
 
 cursor buildings is 
 select  ab.id, bt.description, count(distinct ad.flat) fl from  address_building  ab
inner join building_type bt on ab.building_type_id = bt.id
left join address ad on ab.id=ad.address_bbuilding_id
/*left join mtm_avail_tech_on_building  btch on ab.id= btch.address_building_id 
left join technology tch on btch.technology_id=tch.id*/
group by ab.id, bt.description;

begin 

open buildings;
loop 
fetch buildings into b_id, b_type, b_fl;
exit when buildings%notfound;

if b_type='detached' and mod(b_id, 2)=0 then 

insert into mtm_avail_tech_on_building (hp, address_building_id, technology_id, bandwidth_mb) values 
                                        (decode(b_fl,0,1,b_fl), b_id,3,1024);

ELSIF b_type='detached' and mod(b_id, 2) !=0 then

insert into mtm_avail_tech_on_building (hp, address_building_id, technology_id, bandwidth_mb) values 
                                        (decode(b_fl,0,1,b_fl), b_id,4,10);


ELSIF b_type='housing' and mod(b_id, 2)=0 then 

insert into mtm_avail_tech_on_building (hp, address_building_id, technology_id, bandwidth_mb) values 
                                        (decode(b_fl,0,1,b_fl), b_id,3,1024);

ELSIF b_type='housing' and mod(b_id, 2) !=0 then

insert into mtm_avail_tech_on_building (hp, address_building_id, technology_id, bandwidth_mb) values 
                                        (decode(b_fl,0,1,b_fl), b_id,2,128);


ELSIF b_type='block' and mod(b_id, 2)=0 then 

insert into mtm_avail_tech_on_building (hp, address_building_id, technology_id, bandwidth_mb) values 
                                        (decode(b_fl,0,1,b_fl), b_id,3,1024);

insert into mtm_avail_tech_on_building (hp, address_building_id, technology_id, bandwidth_mb) values 
                                        (decode(b_fl,0,1,b_fl), b_id,2,128);
                                        

ELSIF b_type='block' and mod(b_id, 2) !=0 then

insert into mtm_avail_tech_on_building (hp, address_building_id, technology_id, bandwidth_mb) values 
                                        (decode(b_fl,0,1,b_fl), b_id,1,512);    

insert into mtm_avail_tech_on_building (hp, address_building_id, technology_id, bandwidth_mb) values 
                                        (decode(b_fl,0,1,b_fl), b_id,2,128);
                                        

ELSIF b_type='multi-family' and mod(b_id, 2)=0 then 

insert into mtm_avail_tech_on_building (hp, address_building_id, technology_id, bandwidth_mb) values 
                                        (decode(b_fl,0,1,b_fl), b_id,2,128);

ELSIF b_type='multi-family' and mod(b_id, 2) !=0 then

insert into mtm_avail_tech_on_building (hp, address_building_id, technology_id, bandwidth_mb) values 
                                        (decode(b_fl,0,1,b_fl), b_id,1,512);                                           
end if;


end loop ;
close buildings;
end ;

 