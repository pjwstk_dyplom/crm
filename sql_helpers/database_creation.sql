-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2020-04-08 13:34:34.908

-- tables
-- Table: account
CREATE TABLE account (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    client_id number(12)  NOT NULL,
    creation_date date  NOT NULL,
    billing_day number(2)  DEFAULT 1 NOT NULL CHECK (billing_day < 28),
    division_id number(12)  NOT NULL,
    install_address_id number(20)  NOT NULL,
    billing_address_id number(20)  NOT NULL,
    CONSTRAINT account_pk PRIMARY KEY (id)
) ;

-- Table: activity_log
CREATE TABLE activity_log (
    id number(16) GENERATED ALWAYS AS IDENTITY,
    entry_id number NOT NULL,
    entry_time date  NOT NULL,
    referenced_table varchar2(40)  NOT NULL CHECK (referenced_table=upper(referenced_table)),
    referenced_id number(16)  NOT NULL,
    referenced_field varchar2(40)  NOT NULL CHECK (referenced_field=upper(referenced_field)),
    value_before varchar2(4000)  DEFAULT null NULL,
    value_after varchar2(4000)  DEFAULT null NULL,
    param_1 varchar2(255)  DEFAULT null NULL,
    param_2 varchar2(255)  DEFAULT null NULL,
    user_login varchar2(255)  NOT NULL,
    entry_type char  NOT NULL CHECK (entry_type IN ('A', 'M', 'D')),
    CONSTRAINT activity_log_pk PRIMARY KEY (id)
) ;

CREATE INDEX activity_log_idx_1 
on activity_log 
(entry_time ASC)
 SORT
;

CREATE INDEX activity_log_idx_2 
on activity_log 
(referenced_table ASC)
;

CREATE INDEX activity_log_idx_3 
on activity_log 
(referenced_id ASC)
;

CREATE INDEX activity_log_idx_4 
on activity_log 
(referenced_field ASC)
;

CREATE /* BITMAP */ INDEX activity_log_idx_5 
on activity_log 
(entry_type ASC)
;

CREATE INDEX activity_log_idx_6 
on activity_log 
(entry_id ASC)
;

-- Table: address
CREATE TABLE address (
    id number(20) GENERATED ALWAYS AS IDENTITY,
    flat varchar2(20)  NULL,
    address_building_id number(20)  NOT NULL,
    insert_date date  DEFAULT sysdate NOT NULL,
    CONSTRAINT address_pk PRIMARY KEY (id)
) ;

CREATE INDEX address_idx_1 
on address 
(address_building_id ASC)
;

-- Table: address_building
CREATE TABLE address_building (
    id number(20) GENERATED ALWAYS AS IDENTITY,
    building_number varchar2(20)  NOT NULL,
    address_street_id number(12)  NOT NULL,
    building_type_id number(12)  NOT NULL,
    post_code varchar2(10)  NOT NULL,
    CONSTRAINT address_building_pk PRIMARY KEY (id)
) ;

CREATE INDEX address_building_idx_1 
on address_building 
(address_street_id ASC)
;

CREATE INDEX address_building_idx_2 
on address_building 
(building_type_id ASC)
;

CREATE /* BITMAP */ INDEX address_building_idx_3 
on address_building 
(post_code ASC)
;

-- Table: address_city
CREATE TABLE address_city (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    name varchar2(40)  NOT NULL CHECK (name = lower(name)),
    address_county_id number(12)  NOT NULL,
    CONSTRAINT address_city_pk PRIMARY KEY (id)
) ;

CREATE /* BITMAP */ INDEX address_city_idx_1 
on address_city 
(address_county_id ASC)
;

-- Table: address_county
CREATE TABLE address_county (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    name varchar2(40)  NOT NULL CHECK (name = lower(name)),
    address_province_id number(12)  NOT NULL,
    CONSTRAINT address_county_pk PRIMARY KEY (id)
) ;

CREATE /* BITMAP */ INDEX address_county_idx_1 
on address_county 
(address_province_id ASC)
;

-- Table: address_province
CREATE TABLE address_province (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    name varchar2(50)  NOT NULL CHECK (name=lower(name)),
    CONSTRAINT address_province_pk PRIMARY KEY (id)
) ;

CREATE /* BITMAP */ INDEX address_province_idx_1 
on address_province 
(name ASC)
;

-- Table: address_street
CREATE TABLE address_street (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    name varchar2(100)  NOT NULL CHECK (name = lower(name)),
    address_city_id number(12)  NOT NULL,
    CONSTRAINT address_street_pk PRIMARY KEY (id)
) ;

CREATE INDEX address_street_idx_1 
on address_street 
(name ASC)
;

CREATE INDEX address_street_idx_2 
on address_street 
(address_city_id ASC)
;

-- Table: basket
CREATE TABLE basket (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    user_id number(12)  NOT NULL,
    name varchar2(20)  NOT NULL CHECK (name = lower(name)),
    is_default number(1)  DEFAULT 0 NOT NULL CHECK (is_default in (0, 1)),
    CONSTRAINT user_basket_name_unique UNIQUE (user_id, name),
    CONSTRAINT basket_pk PRIMARY KEY (id)
) ;

CREATE INDEX basket_idx_1 
on basket 
(user_id ASC)
;

-- Table: building_type
CREATE TABLE building_type (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    description varchar2(50)  NOT NULL CHECK (description = lower(description)),
    CONSTRAINT building_type_unique_desc UNIQUE (description),
    CONSTRAINT building_type_pk PRIMARY KEY (id)
) ;

-- Table: client
CREATE TABLE client (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    name varchar2(50)  NOT NULL CHECK (regexp_like(name,'[A-Z][a-z]*')),
    lastname varchar2(50)  NOT NULL CHECK (regexp_like(lastname,'[A-Z][a-z]*(-[A-Z][a-z]*)?')),
    pesel varchar2(11)  NULL,
    nip varchar2(50)  NULL,
    segment_id number(12)  NOT NULL,
    email varchar2(50)  NOT NULL CHECK (email=lower(email)),
    phone_primary varchar2(20)  NOT NULL,
    phone_secondary varchar2(20)  NULL,
    CONSTRAINT client_pesel_unique UNIQUE (pesel),
    CONSTRAINT client_nip_unique UNIQUE (nip),
    CONSTRAINT check_nip_or_pesel CHECK (coalesce(nip, pesel) is not null),
    CONSTRAINT client_pk PRIMARY KEY (id)
) ;

-- Table: contract
CREATE TABLE contract (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    sign_date date  NOT NULL,
    start_date date  NOT NULL,
    end_date date  NOT NULL,
    account_id number(12)  NOT NULL,
	contract_number varchar2(16) NOT NULL,
    CONSTRAINT dates_check CHECK (start_date <= end_date),
    CONSTRAINT contract_pk PRIMARY KEY (id),
	CONSTRAINT contract_number_unique UNIQUE (contract_number)
) ;

-- Table: crm_user
CREATE TABLE crm_user (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    identity_id varchar2(128 CHAR)  NOT NULL,
    hr_id varchar2(50)  NULL,
    CONSTRAINT crm_user_pk PRIMARY KEY (id)
) ;

CREATE INDEX user_idx_3 
on crm_user 
(hr_id ASC)
;

-- Table: device
CREATE TABLE device (
    id number(12)  GENERATED ALWAYS AS IDENTITY,
    serial varchar2(100)  NOT NULL,
    mac varchar2(50)  NULL,
    device_brand_model_id number(12)  NOT NULL,
    crm_user_id number(12)  NULL,
    CONSTRAINT device_unique_serial UNIQUE (serial),
    CONSTRAINT device_unique_mac UNIQUE (mac),
    CONSTRAINT device_pk PRIMARY KEY (id)
) ;


-- Table: device_type
CREATE TABLE device_type (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    description varchar2(150)  NOT NULL CHECK (description = lower(description)),
    CONSTRAINT device_type_unique_desc UNIQUE (description),
    CONSTRAINT device_type_pk PRIMARY KEY (id)
) ;

-- Table: device_brand_model
CREATE TABLE device_brand_model (
    id number(12)   GENERATED ALWAYS AS IDENTITY,
    brand varchar2(255)  NOT NULL CHECK (brand = lower(brand)),
    model varchar2(255)  NOT NULL CHECK (model = lower(model)),
	device_type_id number(12)  NOT NULL,
    CONSTRAINT device_model_brand_unique UNIQUE (brand, model),
    CONSTRAINT device_brand_model_pk PRIMARY KEY (id)
) ;

CREATE INDEX device_brand_model_idx_1 
on device_brand_model 
(device_type_id ASC)
;

-- Table: division
CREATE TABLE division (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    name varchar2(40)  NOT NULL CHECK (name = lower(name)),
    CONSTRAINT division_unique_name UNIQUE (name),
    CONSTRAINT division_pk PRIMARY KEY (id)
) ;

-- Table: interact
CREATE TABLE interact (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    creation_date date  NOT NULL,
    account_id number(12)  NOT NULL,
    interact_type_id number(12)  NOT NULL,
    interact_direction_id number(12)  NOT NULL,
    interact_medium_id number(12)  NOT NULL,
    user_id number(12)  NOT NULL,
    description varchar2(4000)  NULL,
    tasks_id number(12)  NULL,
    CONSTRAINT interact_pk PRIMARY KEY (id)
) ;

CREATE INDEX interact_idx_1 
on interact 
(account_id ASC)
;

CREATE INDEX interact_idx_2 
on interact 
(interact_type_id ASC)
;

CREATE INDEX interact_idx_3 
on interact 
(interact_direction_id ASC)
;

CREATE INDEX interact_idx_4 
on interact 
(interact_medium_id ASC)
;

CREATE INDEX interact_idx_5 
on interact 
(interact_type_id ASC,interact_direction_id ASC,interact_medium_id ASC)
;

-- Table: interact_direction
CREATE TABLE interact_direction (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    name varchar2(50)  NOT NULL CHECK (name = lower(name)),
    CONSTRAINT interact_direction_unique_name UNIQUE (name),
    CONSTRAINT interact_direction_pk PRIMARY KEY (id)
) ;

-- Table: interact_medium
CREATE TABLE interact_medium (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    name varchar2(50)  NOT NULL CHECK (name = lower(name)),
    CONSTRAINT interact_medium_unique_name UNIQUE (name),
    CONSTRAINT interact_medium_pk PRIMARY KEY (id)
) ;

-- Table: interact_type
CREATE TABLE interact_type (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    name varchar2(50)  NOT NULL CHECK (name = lower(name)),
    CONSTRAINT interact_type_unique_name UNIQUE (name),
    CONSTRAINT interact_type_pk PRIMARY KEY (id)
) ;

-- Table: invoice
CREATE TABLE invoice (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    invoice_number varchar2(50)  NOT NULL CHECK (invoice_number = upper(invoice_number)),
    total number(10,2)  NOT NULL,
    creation_date date  DEFAULT sysdate NULL,
    send_date date  NULL,
    due_date date  NULL,
    payment_date date  NULL,
    paid_amount number(10,2)  DEFAULT 0 NOT NULL,
    invoice_type_id number(12)  NOT NULL,
    invoice_status_id number(12)  NOT NULL,
    account_id number(12)  NOT NULL,
    parent_invoice number(12)  NULL,
    CONSTRAINT invoice_number_unique_ak UNIQUE (invoice_number),
    CONSTRAINT send_date_check CHECK (send_date <= trunc(due_date - 8)),
    CONSTRAINT invoice_pk PRIMARY KEY (id)
) ;

CREATE INDEX idx_invoice_creation_date 
on invoice 
(creation_date ASC)
;

CREATE INDEX idx_invoice_due_date 
on invoice 
(due_date ASC)
;

CREATE INDEX idx_invoice_payment_date 
on invoice 
(payment_date ASC)
;

CREATE /* BITMAP */ INDEX invoice_idx_4 
on invoice 
(invoice_status_id ASC)
;

CREATE INDEX invoice_idx_5 
on invoice 
(account_id ASC)
;

-- Table: invoice_other_items
CREATE TABLE invoice_other_items (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    invoice_id number(12)  NOT NULL,
    amount number(10,2)  NOT NULL,
    invoice_other_items_desc_id number(12)  NOT NULL,
    CONSTRAINT invoice_other_items_pk PRIMARY KEY (id)
) ;

CREATE INDEX invoice_other_items_idx_1 
on invoice_other_items 
(invoice_id ASC)
;

CREATE INDEX invoice_other_items_idx_2 
on invoice_other_items 
(invoice_other_items_desc_id ASC)
;

-- Table: invoice_other_items_desc
CREATE TABLE invoice_other_items_desc (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    description varchar2(150)  NOT NULL,
    CONSTRAINT invoice_other_items_desc_pk PRIMARY KEY (id)
) ;

-- Table: invoice_status
CREATE TABLE invoice_status (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    name varchar2(20)  NOT NULL CHECK (name=lower(name)),
    CONSTRAINT invoice_status_unique_name UNIQUE (name),
    CONSTRAINT invoice_status_pk PRIMARY KEY (id)
) ;

-- Table: invoice_type
CREATE TABLE invoice_type (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    name varchar2(20)  NOT NULL CHECK (name=lower(name)),
    CONSTRAINT invoice_type_unique_name UNIQUE (name),
    CONSTRAINT invoice_type_pk PRIMARY KEY (id)
) ;

-- Table: jm_action_list
CREATE TABLE jm_action_list (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    jm_action_trigger_id number(12)  NOT NULL,
    seq number(3)  DEFAULT 1 NOT NULL,
    jm_procedure_id number(12)  NOT NULL,
    CONSTRAINT jm_action_seq_unique UNIQUE (jm_action_trigger_id, seq),
    CONSTRAINT jm_action_list_pk PRIMARY KEY (id)
) ;

CREATE INDEX jm_action_list_idx_1 
on jm_action_list 
(jm_action_trigger_id ASC)
;

-- Table: jm_action_trigger
CREATE TABLE jm_action_trigger (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    valid_from date  DEFAULT trunc(sysdate) NOT NULL,
    valid_to date  DEFAULT to_date('2199-01-01', 'yyyy-mm-dd') NOT NULL,
    priority number  DEFAULT 1 NOT NULL,
    task_status_id number(12)  NULL,
    task_subtype_id number(12)  NULL,
    task_type_id number(12)  NULL,
    CONSTRAINT validity_dates_jmat CHECK (valid_to >= valid_from),
    CONSTRAINT jm_action_trigger_pk PRIMARY KEY (id)
) ;

CREATE INDEX jm_action_trigger_idx_1 
on jm_action_trigger 
(task_type_id ASC,task_subtype_id ASC,task_status_id ASC)
;

-- Table: jm_actions_history
CREATE TABLE jm_actions_history (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    seq number(3)  NOT NULL,
    tasks_id number(12)  NOT NULL,
    jm_procedure_id number(12)  NOT NULL,
    current_status char(1)  DEFAULT 'R' NOT NULL CHECK (current_status in ('R', 'F', 'C', 'E')),
    start_time date  NULL,
    finish_time date  NULL,
    jm_action_trigger_id number(12)  NOT NULL,
    comment varchar2(255) NULL,
    CONSTRAINT jm_actions_history_pk PRIMARY KEY (id)
) ;

CREATE INDEX jm_actions_history_idx_1 
on jm_actions_history 
(tasks_id ASC)
;

CREATE INDEX jm_actions_history_idx_2 
on jm_actions_history 
(current_status ASC)
;

-- Table: jm_actions_stack
CREATE TABLE jm_actions_stack (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    seq number(3)  NOT NULL,
    tasks_id number(12)  NOT NULL,
    jm_procedure_id number(12)  NOT NULL,
    jm_action_trigger_id number(12)  NOT NULL,
    CONSTRAINT jm_actions_stack_pk PRIMARY KEY (id)
) ;

-- Table: jm_procedure
CREATE TABLE jm_procedure (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    procedure_name varchar2(100)  NOT NULL CHECK (procedure_name=upper(procedure_name)),
    CONSTRAINT jm_procedure_unique_name UNIQUE (procedure_name),
    CONSTRAINT jm_procedure_pk PRIMARY KEY (id)
) ;

-- Table: mtm_attrdef_proddef
CREATE TABLE mtm_attrdef_proddef (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    product_definition_id number(12)  NOT NULL,
    prod_attr_def_id number(12)  NOT NULL,
    valid_from date  NOT NULL,
    valid_to date  NOT NULL,
    is_required char(1)  NOT NULL CHECK (is_required in ('N', 'Y')),
    CONSTRAINT validity_dates_mtmatpr CHECK (valid_from <= valid_to),
    CONSTRAINT mtm_attrdef_proddef_pk PRIMARY KEY (id)
) ;

CREATE INDEX mtm_attrdef_proddef_idx_1 
on mtm_attrdef_proddef 
(product_definition_id ASC)
;

CREATE INDEX mtm_attrdef_proddef_idx_2 
on mtm_attrdef_proddef 
(prod_attr_def_id ASC)
;

-- Table: mtm_avail_tech_on_building
CREATE TABLE mtm_avail_tech_on_building (
    hp number(7)  NOT NULL,
    address_building_id number(20)  NOT NULL,
    technology_id number(12)  NOT NULL,
    bandwidth_mb number(12)  NOT NULL,
    CONSTRAINT mtm_avail_tech_on_building_pk PRIMARY KEY (address_building_id,technology_id)
) ;

CREATE INDEX mtm_avail_tech_on_build_idx_1 
on mtm_avail_tech_on_building 
(address_building_id ASC)
;

CREATE INDEX mtm_avail_tech_on_build_idx_2 
on mtm_avail_tech_on_building 
(technology_id ASC)
;

-- Table: mtm_invoice_product
CREATE TABLE mtm_invoice_product (
    invoice_id number(12)  NOT NULL,
    product_id number(12)  NOT NULL,
    amount number(10,2)  NOT NULL,
    CONSTRAINT mtm_invoice_product_pk PRIMARY KEY (invoice_id,product_id)
) ;

-- Table: mtm_offer_opt2category
CREATE TABLE mtm_offer_opt2category (
    offer_category_id number(12)  NOT NULL,
    offer_option_id number(12)  NOT NULL,
    CONSTRAINT mtm_offer_opt2category_pk PRIMARY KEY (offer_category_id, offer_option_id)
) ;

-- Table: mtm_offer_product
CREATE TABLE mtm_offer_product (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    product_id number(12)  NOT NULL,
    offer_id number(12)  NOT NULL,
    valid_from date  NOT NULL,
    valid_to date  NOT NULL,
    CONSTRAINT validity_dates_mtmofpr CHECK (valid_from <= valid_to),
    CONSTRAINT mtm_offer_product_pk PRIMARY KEY (id)
) ;

CREATE INDEX mtm_offer_product_idx_1 
on mtm_offer_product 
(product_id ASC)
;

CREATE INDEX mtm_offer_product_idx_2 
on mtm_offer_product 
(offer_id ASC)
;

-- Table: mtm_offer_requirement
CREATE TABLE mtm_offer_requirement (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    offer_definition_id number(12)  NULL,
    offer_option_id number(12)  NULL,
    offer_requirement_def_id number(12)  NOT NULL,
    value number  NOT NULL,
    relation char(1)  DEFAULT '=' NOT NULL CHECK (relation in ('=', '<', '>', '!')),
    CONSTRAINT mtm_offer_requirement_pk PRIMARY KEY (id)
) ;

-- Table: mtm_paramdef_tasktype
CREATE TABLE mtm_paramdef_tasktype (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    task_param_definition_id number(12)  NOT NULL,
    task_type_id number(12)  NOT NULL,
    task_subtype_id number(12)  NOT NULL,
    valid_from date  NOT NULL,
    valid_to date  NOT NULL,
    CONSTRAINT validity_dates_mtmpata CHECK (valid_to >= valid_from),
    CONSTRAINT mtm_paramdef_tasktype_pk PRIMARY KEY (id)
) ;

CREATE INDEX mtm_paramdef_tasktype_idx_1 
on mtm_paramdef_tasktype 
(task_type_id ASC,task_subtype_id ASC)
;

-- Table: mtm_task_product
CREATE TABLE mtm_task_product (
    task_id number(12)  NOT NULL,
    product_id number(12)  NOT NULL,
    CONSTRAINT mtm_task_product_pk PRIMARY KEY (task_id,product_id)
) ;

-- Table: offer
CREATE TABLE offer (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    account_id number(12)  NOT NULL,
    sale_code_id number(12)  NOT NULL,
    sale_date date  NOT NULL,
    offer_option_id number(12)  NOT NULL,
    CONSTRAINT offer_pk PRIMARY KEY (id)
) ;

CREATE INDEX offer_idx_1 
on offer 
(account_id ASC)
;

CREATE INDEX offer_idx_2 
on offer 
(offer_option_id ASC)
;

CREATE INDEX offer_idx_3 
on offer 
(sale_code_id ASC)
;

CREATE INDEX offer_idx_4 
on offer 
(sale_date ASC)
;

-- Table: offer_category
CREATE TABLE offer_category (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    description varchar2(150)  NOT NULL,
    full_description varchar2(4000)  NOT NULL,
    CONSTRAINT offer_category_pk PRIMARY KEY (id)
) ;

-- Table: offer_definition
CREATE TABLE offer_definition (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    invoice_name varchar2(50)  NOT NULL,
    internal_name varchar2(100)  NOT NULL,
    valid_from date  NOT NULL,
    valid_to date  NOT NULL,
    CONSTRAINT validity_dates_check_offdef CHECK (valid_to >= valid_from),
    CONSTRAINT offer_definition_pk PRIMARY KEY (id)
) ;

-- Table: offer_opt_prod_forced_attr
CREATE TABLE offer_opt_prod_forced_attr (
    offer_option_product_id number(12)  NOT NULL,
    product_attribute_def_id number(12)  NOT NULL,
    value varchar2(255)  NOT NULL,
    CONSTRAINT offer_opt_prod_forced_attr_pk PRIMARY KEY (offer_option_product_id,product_attribute_def_id)
) ;

-- Table: offer_opt_prod_schedule
CREATE TABLE offer_opt_prod_schedule (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    offer_option_product_id number(12)  NOT NULL,
    mth_num number(2)  NULL,
    price number(10,2)  NOT NULL,
    CONSTRAINT offer_opt_prod_schedule_pk PRIMARY KEY (id)
) ;

-- Table: offer_option
CREATE TABLE offer_option (
    id number(12)  NOT NULL,
    offer_definition_id number(12)  NOT NULL,
    invoice_name varchar2(50)  NOT NULL,
    internal_name varchar2(150)  NOT NULL,
    description varchar2(4000)  NOT NULL,
    valid_from date  NOT NULL,
    valid_to date  NOT NULL,
    technology_id number(12)  NOT NULL,
    CONSTRAINT validity_dates_check_offopt CHECK (valid_to >= valid_from),
    CONSTRAINT offer_option_pk PRIMARY KEY (id)
) ;

-- Table: offer_option_product
CREATE TABLE offer_option_product (
    id number(12)  GENERATED ALWAYS AS IDENTITY,
    offer_option_id number(12)  NOT NULL,
    product_definition_id number(12)  NOT NULL,
    type char(1)  NOT NULL CHECK (type in ('U', 'L')),
    length number(2)  NULL CHECK (length is null or length <= 36),
    device_brand_model_id number(12)  NULL,
    CONSTRAINT offer_option_product_pk PRIMARY KEY (id)
) ;

-- Table: offer_req_function
CREATE TABLE offer_req_function (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    function_name varchar2(50)  NOT NULL,
    CONSTRAINT offer_req_procedure_fun_unique UNIQUE (function_name),
    CONSTRAINT offer_req_function_pk PRIMARY KEY (id)
) ;

-- Table: offer_requirement_definition
CREATE TABLE offer_requirement_definition (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    description varchar2(150)  NOT NULL,
    full_description varchar2(4000)  NOT NULL,
    offer_function_id number(12)  NOT NULL,
    CONSTRAINT offer_requirement_definitio_pk PRIMARY KEY (id)
) ;

CREATE INDEX offer_requirement_def_idx_1 
on offer_requirement_definition 
(offer_function_id ASC)
;

-- Table: product
CREATE TABLE product (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    product_definition_id number(12)  NOT NULL,
    activation_date date  NULL,
    deactivation_date date  NULL,
    product_status_id number(12)  NOT NULL,
    parent_product_id number(12)  NULL,
    account_id number(12)  NOT NULL,
    device_id number(12)  NULL,
    technology_id number(12)  NULL,
    CONSTRAINT product_pk PRIMARY KEY (id)
) ;

CREATE INDEX product_idx_1 
on product 
(product_definition_id ASC)
;

CREATE /* BITMAP */ INDEX product_idx_2 
on product 
(product_status_id ASC)
;

CREATE INDEX product_idx_3 
on product 
(parent_product_id ASC)
;

CREATE INDEX product_idx_4 
on product 
(account_id ASC)
;

CREATE INDEX product_idx_5 
on product 
(device_id ASC)
;

CREATE INDEX product_idx_6 
on product 
(technology_id ASC)
;

-- Table: product_attribute_value_list
CREATE TABLE product_attribute_value_list (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    product_attr_def_id number(12)  NOT NULL,
    value varchar2(50)  NOT NULL,
    CONSTRAINT product_attribute_value_lis_pk PRIMARY KEY (id)
) ;

CREATE INDEX product_attr_value_list_idx_1 
on product_attribute_value_list 
(product_attr_def_id ASC)
;

-- Table: product_attribute
CREATE TABLE product_attribute (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    product_id number(12)  NOT NULL,
    product_attr_def_id number(12)  NOT NULL,
    value varchar2(255)  NULL,
    CONSTRAINT product_attribute_ak_1 UNIQUE (product_id, product_attr_def_id),
    CONSTRAINT product_attribute_pk PRIMARY KEY (id)
) ;

CREATE INDEX product_attribute_idx_1 
on product_attribute 
(product_id ASC)
;

CREATE INDEX product_attribute_idx_2 
on product_attribute 
(product_attr_def_id ASC)
;

CREATE INDEX product_attribute_idx_3 
on product_attribute 
(product_attr_def_id ASC,value ASC)
;

-- Table: product_attribute_definition
CREATE TABLE product_attribute_definition (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    param_type char(1)  NOT NULL CHECK (param_type in ('R', 'A', 'S')),
    description varchar2(150)  NOT NULL,
    full_description varchar2(255)  NOT NULL,
    regexp varchar2(255)  NULL,
    CONSTRAINT product_attribute_definitio_pk PRIMARY KEY (id)
) ;

-- Table: product_category
CREATE TABLE product_category (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    description varchar2(50)  NOT NULL CHECK (description = lower(description)),
    CONSTRAINT product_category_unique_desc UNIQUE (description),
    CONSTRAINT product_category_pk PRIMARY KEY (id)
) ;

-- Table: product_definition
CREATE TABLE product_definition (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    description varchar2(150)  NOT NULL CHECK (description = lower(description)),
    product_category_id number(12)  NOT NULL,
    CONSTRAINT product_def_unique_desc UNIQUE (description),
    CONSTRAINT product_definition_pk PRIMARY KEY (id)
) ;

-- Table: product_status
CREATE TABLE product_status (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    name varchar2(20)  NOT NULL CHECK (name = lower(name)),
    CONSTRAINT product_status_unique_name UNIQUE (name),
    CONSTRAINT product_status_pk PRIMARY KEY (id)
) ;

-- Table: queue
CREATE TABLE queue (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    name varchar2(30)  NOT NULL CHECK (name = lower(name)),
    CONSTRAINT queue_unique_name UNIQUE (name),
    CONSTRAINT queue_pk PRIMARY KEY (id)
) ;

-- Table: report
CREATE TABLE report (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    title varchar2(100)  NOT NULL CHECK (title = lower(title)),
    description varchar2(4000)  NOT NULL,
    create_date date  DEFAULT sysdate NOT NULL,
    enabled number(1)  DEFAULT 1 NOT NULL CHECK (enabled in (0, 1)),
    interval varchar2(20)  NOT NULL,
    action varchar2(4000)  NOT NULL CHECK (length(action) < 32000),
    type varchar2(20)  NOT NULL,
    user_id number(12)  NOT NULL,
    export_path varchar2(255)  NULL,
    CONSTRAINT report_unique_pathtitle UNIQUE (title, export_path),
    CONSTRAINT report_pk PRIMARY KEY (id)
) ;

-- Table: report_access
CREATE TABLE report_access (
    report_id number(12)  NOT NULL,
    user_id number(12)  NOT NULL,
    CONSTRAINT report_access_pk PRIMARY KEY (report_id,user_id)
) ;

-- Table: sale_code
CREATE TABLE sale_code (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    code varchar2(20)  NOT NULL CHECK (code=upper(code)),
    teams_id number(12)  NOT NULL,
    valid_from date  NOT NULL,
    valid_to date  NOT NULL,
    sales_struct_id number(12)  NOT NULL,
    CONSTRAINT validity_dates_mtmsaco CHECK (valid_from <= valid_to),
    CONSTRAINT sale_code_pk PRIMARY KEY (id)
) ;

CREATE INDEX sale_code_idx_1 
on sale_code 
(teams_id ASC)
;

CREATE INDEX sale_code_idx_2 
on sale_code 
(sales_struct_id ASC)
;

-- Table: segment
CREATE TABLE segment (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    name varchar2(40)  NOT NULL CHECK (name = lower(name)),
    CONSTRAINT segment_unique_name UNIQUE (name),
    CONSTRAINT segment_pk PRIMARY KEY (id)
) ;

-- Table: task_movement_history
CREATE TABLE task_movement_history (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    task_id number(12)  NOT NULL,
    queue_id number(12)  NOT NULL,
    basket_id number(12),
    move_date date  DEFAULT sysdate NOT NULL,
    CONSTRAINT task_movement_history_pk PRIMARY KEY (id)
) ;

CREATE INDEX idx_task_movement_history_dt 
on task_movement_history 
(move_date ASC)
;

CREATE INDEX task_movement_history_idx_2 
on task_movement_history 
(task_id ASC)
;

CREATE INDEX task_movement_history_idx_3 
on task_movement_history 
(queue_id ASC)
;

CREATE INDEX task_movement_history_idx_4 
on task_movement_history 
(basket_id ASC)
;

-- Table: task_param
CREATE TABLE task_param (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    task_param_definition_id number(12)  NOT NULL,
    task_id number(12)  NOT NULL,
    value varchar2(50)  NOT NULL,
    CONSTRAINT task_param_pk PRIMARY KEY (id)
) ;

CREATE INDEX task_param_idx_1 
on task_param 
(task_id ASC)
;

-- Table: task_param_definition
CREATE TABLE task_param_definition (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    param_type char(1)  NOT NULL CHECK (param_type in ('R', 'A', 'S')),
    description varchar2(150)  NOT NULL,
    full_description varchar2(255)  NOT NULL,
    regexp varchar2(255)  NULL,
    CONSTRAINT task_param_definition_pk PRIMARY KEY (id)
) ;

-- Table: task_param_value_list
CREATE TABLE task_param_value_list (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    task_param_definition_id number(12)  NOT NULL,
    value varchar2(50)  NOT NULL,
    CONSTRAINT task_param_def_val_unique UNIQUE (task_param_definition_id, value),
    CONSTRAINT task_param_value_list_pk PRIMARY KEY (id)
) ;

CREATE INDEX task_param_value_list_idx_1 
on task_param_value_list 
(task_param_definition_id ASC)
;

-- Table: task_status
CREATE TABLE task_status (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    name varchar2(30)  NOT NULL CHECK (name = lower(name)),
    CONSTRAINT task_status_unique_name UNIQUE (name),
    CONSTRAINT task_status_pk PRIMARY KEY (id)
) ;

-- Table: task_subtype
CREATE TABLE task_subtype (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    name varchar2(50)  NOT NULL CHECK (name = lower(name)),
    CONSTRAINT task_subtype_unique_name UNIQUE (name),
    CONSTRAINT task_subtype_pk PRIMARY KEY (id)
) ;

-- Table: task_type
CREATE TABLE task_type (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    name varchar2(30)  NOT NULL CHECK (name = lower(name)),
    CONSTRAINT task_type_unique_name UNIQUE (name),
    CONSTRAINT task_type_pk PRIMARY KEY (id)
) ;

-- Table: tasks
CREATE TABLE tasks (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    task_status_id number(12)  NOT NULL,
    task_type_id number(12)  NOT NULL,
    task_subtype_id number(12)  NOT NULL,
    creation_date date  NOT NULL,
    due_date date  NULL,
    close_date date  NULL,
    closer_id number(12)  NULL,
    queue_id number(12)  NOT NULL,
    basket_id number(12)  NULL,
    user_id_author number(12)  NOT NULL,
    parent_task_id number(12)  NULL,
    account_id number(12)  NOT NULL,
    CONSTRAINT tasks_pk PRIMARY KEY (id)
) ;

CREATE /* BITMAP */ INDEX tasks_idx_1 
on tasks 
(task_status_id ASC)
;

CREATE /* BITMAP */ INDEX tasks_idx_2 
on tasks 
(task_type_id ASC)
;

CREATE INDEX tasks_idx_3 
on tasks 
(task_subtype_id ASC)
;

CREATE INDEX tasks_idx_4 
on tasks 
(creation_date ASC)
;

-- Table: teams
CREATE TABLE teams (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    description varchar2(150)  NOT NULL,
    team_leader_id number(12)  NOT NULL,
	TEAM_CODE VARCHAR2(3 BYTE) NOT NULL, 
    CONSTRAINT teams_pk PRIMARY KEY (id),
	CONSTRAINT TEAM_CODE_UNIQUE UNIQUE (TEAM_CODE)
) ;

CREATE INDEX teams_idx_1 
on teams 
(team_leader_id ASC)
;

-- Table: technology
CREATE TABLE technology (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    description varchar2(150)  NOT NULL CHECK (description = lower(description)),
    CONSTRAINT technology_pk PRIMARY KEY (id)
) ;

-- Table: users_struct
CREATE TABLE users_struct (
    id number(12) GENERATED ALWAYS AS IDENTITY,
    user_id number(12)  NOT NULL,
    valid_from date  NOT NULL,
    valid_to date  NOT NULL,
    manager_id number(12)  NULL,
    CONSTRAINT validity_dates_usst CHECK (valid_from <= valid_to),
    CONSTRAINT users_struct_pk PRIMARY KEY (id)
) ;

CREATE INDEX idx_sales_struct_validity 
on users_struct 
(valid_from ASC,valid_to ASC)
;

CREATE INDEX users_struct_idx_2 
on users_struct 
(user_id ASC)
;

CREATE INDEX users_struct_idx_3 
on users_struct 
(manager_id ASC)
;

-- foreign keys
-- Reference: device_crm_user (table: device)
ALTER TABLE device ADD CONSTRAINT device_crm_user
    FOREIGN KEY (crm_user_id)
    REFERENCES crm_user (id);
    
-- Reference: device_device_brand_model (table: device)
ALTER TABLE device ADD CONSTRAINT device_device_brand_model
    FOREIGN KEY (device_brand_model_id)
    REFERENCES device_brand_model (id);
    
-- Reference: offer_option_product_device_brand_model (table: offer_option_product)
ALTER TABLE offer_option_product ADD CONSTRAINT offer_opt_prod_dev_brnd_model
    FOREIGN KEY (device_brand_model_id)
    REFERENCES device_brand_model (id);    
            
            
-- Reference: Account_Client (table: account)
ALTER TABLE account ADD CONSTRAINT Account_Client
    FOREIGN KEY (client_id)
    REFERENCES client (id);

-- Reference: account_adress_billing (table: account)
ALTER TABLE account ADD CONSTRAINT account_adress_billing
    FOREIGN KEY (billing_address_id)
    REFERENCES address (id);

-- Reference: account_adress_install (table: account)
ALTER TABLE account ADD CONSTRAINT account_adress_install
    FOREIGN KEY (install_address_id)
    REFERENCES address (id);

-- Reference: account_division (table: account)
ALTER TABLE account ADD CONSTRAINT account_division
    FOREIGN KEY (division_id)
    REFERENCES division (id);

-- Reference: account_product (table: product)
ALTER TABLE product ADD CONSTRAINT account_product
    FOREIGN KEY (account_id)
    REFERENCES account (id);

-- Reference: address_building_adress_street (table: address_building)
ALTER TABLE address_building ADD CONSTRAINT address_building_adress_street
    FOREIGN KEY (address_street_id)
    REFERENCES address_street (id);

-- Reference: address_building_building_type (table: address_building)
ALTER TABLE address_building ADD CONSTRAINT address_building_building_type
    FOREIGN KEY (building_type_id)
    REFERENCES building_type (id);

-- Reference: address_city_address_province (table: address_city)
ALTER TABLE address_city ADD CONSTRAINT address_city_address_province
    FOREIGN KEY (address_county_id)
    REFERENCES address_county (id);

-- Reference: adress_address_building (table: address)
ALTER TABLE address ADD CONSTRAINT adress_address_building
    FOREIGN KEY (address_building_id)
    REFERENCES address_building (id);

-- Reference: adress_street_adress_city (table: address_street)
ALTER TABLE address_street ADD CONSTRAINT adress_street_adress_city
    FOREIGN KEY (address_city_id)
    REFERENCES address_city (id);

-- Reference: attr_val_list_prod_attr_def (table: product_attribute_value_list)
ALTER TABLE product_attribute_value_list ADD CONSTRAINT attr_val_list_prod_attr_def
    FOREIGN KEY (product_attr_def_id)
    REFERENCES product_attribute_definition (id);

-- Reference: basket_user (table: basket)
ALTER TABLE basket ADD CONSTRAINT basket_user
    FOREIGN KEY (user_id)
    REFERENCES crm_user (id);

-- Reference: client_segment (table: client)
ALTER TABLE client ADD CONSTRAINT client_segment
    FOREIGN KEY (segment_id)
    REFERENCES segment (id);

-- Reference: contract_account (table: contract)
ALTER TABLE contract ADD CONSTRAINT contract_account
    FOREIGN KEY (account_id)
    REFERENCES account (id);

-- Reference: device_brand_model_device_type (table: device_brand_model)
ALTER TABLE device_brand_model ADD CONSTRAINT device_brand_model_device_type
    FOREIGN KEY (device_type_id)
    REFERENCES device_type (id);

-- Reference: interact_account (table: interact)
ALTER TABLE interact ADD CONSTRAINT interact_account
    FOREIGN KEY (account_id)
    REFERENCES account (id);

-- Reference: interact_interact_direction (table: interact)
ALTER TABLE interact ADD CONSTRAINT interact_interact_direction
    FOREIGN KEY (interact_direction_id)
    REFERENCES interact_direction (id);

-- Reference: interact_interact_medium (table: interact)
ALTER TABLE interact ADD CONSTRAINT interact_interact_medium
    FOREIGN KEY (interact_medium_id)
    REFERENCES interact_medium (id);

-- Reference: interact_interact_type (table: interact)
ALTER TABLE interact ADD CONSTRAINT interact_interact_type
    FOREIGN KEY (interact_type_id)
    REFERENCES interact_type (id);

-- Reference: interact_tasks (table: interact)
ALTER TABLE interact ADD CONSTRAINT interact_tasks
    FOREIGN KEY (tasks_id)
    REFERENCES tasks (id);

-- Reference: interact_user (table: interact)
ALTER TABLE interact ADD CONSTRAINT interact_user
    FOREIGN KEY (user_id)
    REFERENCES crm_user (id);

-- Reference: invoice_account (table: invoice)
ALTER TABLE invoice ADD CONSTRAINT invoice_account
    FOREIGN KEY (account_id)
    REFERENCES account (id);

-- Reference: invoice_invoice (table: invoice)
ALTER TABLE invoice ADD CONSTRAINT invoice_invoice
    FOREIGN KEY (parent_invoice)
    REFERENCES invoice (id);

-- Reference: invoice_invoice_status (table: invoice)
ALTER TABLE invoice ADD CONSTRAINT invoice_invoice_status
    FOREIGN KEY (invoice_status_id)
    REFERENCES invoice_status (id);

-- Reference: invoice_invoice_type (table: invoice)
ALTER TABLE invoice ADD CONSTRAINT invoice_invoice_type
    FOREIGN KEY (invoice_type_id)
    REFERENCES invoice_type (id);

-- Reference: invoice_other_items_desc (table: invoice_other_items)
ALTER TABLE invoice_other_items ADD CONSTRAINT invoice_other_items_desc
    FOREIGN KEY (invoice_other_items_desc_id)
    REFERENCES invoice_other_items_desc (id);

-- Reference: invoice_other_items_invoice (table: invoice_other_items)
ALTER TABLE invoice_other_items ADD CONSTRAINT invoice_other_items_invoice
    FOREIGN KEY (invoice_id)
    REFERENCES invoice (id) DEFERRABLE ENABLE;

-- Reference: jm_action_list_jm_procedure (table: jm_action_list)
ALTER TABLE jm_action_list ADD CONSTRAINT jm_action_list_jm_procedure
    FOREIGN KEY (jm_procedure_id)
    REFERENCES jm_procedure (id);

-- Reference: jm_action_list_trigger (table: jm_action_list)
ALTER TABLE jm_action_list ADD CONSTRAINT jm_action_list_trigger
    FOREIGN KEY (jm_action_trigger_id)
    REFERENCES jm_action_trigger (id);

-- Reference: jm_action_trigger_task_status (table: jm_action_trigger)
ALTER TABLE jm_action_trigger ADD CONSTRAINT jm_action_trigger_task_status
    FOREIGN KEY (task_status_id)
    REFERENCES task_status (id);

-- Reference: jm_action_trigger_task_subtype (table: jm_action_trigger)
ALTER TABLE jm_action_trigger ADD CONSTRAINT jm_action_trigger_task_subtype
    FOREIGN KEY (task_subtype_id)
    REFERENCES task_subtype (id);

-- Reference: jm_action_trigger_task_type (table: jm_action_trigger)
ALTER TABLE jm_action_trigger ADD CONSTRAINT jm_action_trigger_task_type
    FOREIGN KEY (task_type_id)
    REFERENCES task_type (id);

-- Reference: jm_actions_hist_action_trigger (table: jm_actions_history)
ALTER TABLE jm_actions_history ADD CONSTRAINT jm_actions_hist_action_trigger
    FOREIGN KEY (jm_action_trigger_id)
    REFERENCES jm_action_trigger (id);

-- Reference: jm_actions_history_procedure (table: jm_actions_history)
ALTER TABLE jm_actions_history ADD CONSTRAINT jm_actions_history_procedure
    FOREIGN KEY (jm_procedure_id)
    REFERENCES jm_procedure (id);

-- Reference: jm_actions_history_tasks (table: jm_actions_history)
ALTER TABLE jm_actions_history ADD CONSTRAINT jm_actions_history_tasks
    FOREIGN KEY (tasks_id)
    REFERENCES tasks (id);

-- Reference: jm_actions_stack_jm_procedure (table: jm_actions_stack)
ALTER TABLE jm_actions_stack ADD CONSTRAINT jm_actions_stack_jm_procedure
    FOREIGN KEY (jm_procedure_id)
    REFERENCES jm_procedure (id);

-- Reference: jm_actions_stack_tasks (table: jm_actions_stack)
ALTER TABLE jm_actions_stack ADD CONSTRAINT jm_actions_stack_tasks
    FOREIGN KEY (tasks_id)
    REFERENCES tasks (id);

-- Reference: jm_actions_stack_trigger (table: jm_actions_stack)
ALTER TABLE jm_actions_stack ADD CONSTRAINT jm_actions_stack_trigger
    FOREIGN KEY (jm_action_trigger_id)
    REFERENCES jm_action_trigger (id);

-- Reference: mtm_avail_tech_building (table: mtm_avail_tech_on_building)
ALTER TABLE mtm_avail_tech_on_building ADD CONSTRAINT mtm_avail_tech_building
    FOREIGN KEY (address_building_id)
    REFERENCES address_building (id);

-- Reference: mtm_avail_tech_on_address_tech (table: mtm_avail_tech_on_building)
ALTER TABLE mtm_avail_tech_on_building ADD CONSTRAINT mtm_avail_tech_on_address_tech
    FOREIGN KEY (technology_id)
    REFERENCES technology (id);

-- Reference: mtm_invoice_product_invoice (table: mtm_invoice_product)
ALTER TABLE mtm_invoice_product ADD CONSTRAINT mtm_invoice_product_invoice
    FOREIGN KEY (invoice_id)
    REFERENCES invoice (id) DEFERRABLE ENABLE;

-- Reference: mtm_invoice_product_product (table: mtm_invoice_product)
ALTER TABLE mtm_invoice_product ADD CONSTRAINT mtm_invoice_product_product
    FOREIGN KEY (product_id)
    REFERENCES product (id);

-- Reference: mtm_offer_product_offer (table: mtm_offer_product)
ALTER TABLE mtm_offer_product ADD CONSTRAINT mtm_offer_product_offer
    FOREIGN KEY (offer_id)
    REFERENCES offer (id);

-- Reference: mtm_offer_product_product (table: mtm_offer_product)
ALTER TABLE mtm_offer_product ADD CONSTRAINT mtm_offer_product_product
    FOREIGN KEY (product_id)
    REFERENCES product (id);

-- Reference: mtm_offer_req2definition (table: mtm_offer_requirement)
ALTER TABLE mtm_offer_requirement ADD CONSTRAINT mtm_offer_req2definition
    FOREIGN KEY (offer_requirement_def_id)
    REFERENCES offer_requirement_definition (id);

-- Reference: mtm_offer_requirement2def (table: mtm_offer_requirement)
ALTER TABLE mtm_offer_requirement ADD CONSTRAINT mtm_offer_requirement2def
    FOREIGN KEY (offer_definition_id)
    REFERENCES offer_definition (id);

-- Reference: mtm_offer_requirement2opt (table: mtm_offer_requirement)
ALTER TABLE mtm_offer_requirement ADD CONSTRAINT mtm_offer_requirement2opt
    FOREIGN KEY (offer_option_id)
    REFERENCES offer_option (id);

-- Reference: mtm_pd_tt_task_param_def (table: mtm_paramdef_tasktype)
ALTER TABLE mtm_paramdef_tasktype ADD CONSTRAINT mtm_pd_tt_task_param_def
    FOREIGN KEY (task_param_definition_id)
    REFERENCES task_param_definition (id);

-- Reference: mtm_pd_tt_task_subtype (table: mtm_paramdef_tasktype)
ALTER TABLE mtm_paramdef_tasktype ADD CONSTRAINT mtm_pd_tt_task_subtype
    FOREIGN KEY (task_subtype_id)
    REFERENCES task_subtype (id);

-- Reference: mtm_pd_tt_task_type (table: mtm_paramdef_tasktype)
ALTER TABLE mtm_paramdef_tasktype ADD CONSTRAINT mtm_pd_tt_task_type
    FOREIGN KEY (task_type_id)
    REFERENCES task_type (id);

-- Reference: mtm_prodattrdef_attrdef (table: mtm_attrdef_proddef)
ALTER TABLE mtm_attrdef_proddef ADD CONSTRAINT mtm_prodattrdef_attrdef
    FOREIGN KEY (prod_attr_def_id)
    REFERENCES product_attribute_definition (id);

-- Reference: mtm_proddef_attrdef (table: mtm_attrdef_proddef)
ALTER TABLE mtm_attrdef_proddef ADD CONSTRAINT mtm_proddef_attrdef
    FOREIGN KEY (product_definition_id)
    REFERENCES product_definition (id);

-- Reference: mtm_task_product_product (table: mtm_task_product)
ALTER TABLE mtm_task_product ADD CONSTRAINT mtm_task_product_product
    FOREIGN KEY (product_id)
    REFERENCES product (id);

-- Reference: mtm_task_product_tasks (table: mtm_task_product)
ALTER TABLE mtm_task_product ADD CONSTRAINT mtm_task_product_tasks
    FOREIGN KEY (task_id)
    REFERENCES tasks (id);

-- Reference: offer_account (table: offer)
ALTER TABLE offer ADD CONSTRAINT offer_account
    FOREIGN KEY (account_id)
    REFERENCES account (id);

-- Reference: offer_category2mtm (table: mtm_offer_opt2category)
ALTER TABLE mtm_offer_opt2category ADD CONSTRAINT offer_category2mtm
    FOREIGN KEY (offer_category_id)
    REFERENCES offer_category (id);

-- Reference: offer_offer_option (table: offer)
ALTER TABLE offer ADD CONSTRAINT offer_offer_option
    FOREIGN KEY (offer_option_id)
    REFERENCES offer_option (id);

-- Reference: offer_opr2mtm (table: mtm_offer_opt2category)
ALTER TABLE mtm_offer_opt2category ADD CONSTRAINT offer_opr2mtm
    FOREIGN KEY (offer_option_id)
    REFERENCES offer_option (id);

-- Reference: offer_opt_prod_forced_attr_def (table: offer_opt_prod_forced_attr)
ALTER TABLE offer_opt_prod_forced_attr ADD CONSTRAINT offer_opt_prod_forced_attr_def
    FOREIGN KEY (product_attribute_def_id)
    REFERENCES product_attribute_definition (id);

-- Reference: offer_option2product (table: offer_option_product)
ALTER TABLE offer_option_product ADD CONSTRAINT offer_option2product
    FOREIGN KEY (offer_option_id)
    REFERENCES offer_option (id);

-- Reference: offer_option_offer_definition (table: offer_option)
ALTER TABLE offer_option ADD CONSTRAINT offer_option_offer_definition
    FOREIGN KEY (offer_definition_id)
    REFERENCES offer_definition (id);
	
-- Reference: offer_option_technology (table: offer_option)
ALTER TABLE offer_option ADD CONSTRAINT offer_option_technology
    FOREIGN KEY (technology_id)
    REFERENCES technology (id);	

-- Reference: offer_option_product2forced (table: offer_opt_prod_forced_attr)
ALTER TABLE offer_opt_prod_forced_attr ADD CONSTRAINT offer_option_product2forced
    FOREIGN KEY (offer_option_product_id)
    REFERENCES offer_option_product (id);

-- Reference: offer_option_product2schedule (table: offer_opt_prod_schedule)
ALTER TABLE offer_opt_prod_schedule ADD CONSTRAINT offer_option_product2schedule
    FOREIGN KEY (offer_option_product_id)
    REFERENCES offer_option_product (id);

-- Reference: offer_option_product_def (table: offer_option_product)
ALTER TABLE offer_option_product ADD CONSTRAINT offer_option_product_def
    FOREIGN KEY (product_definition_id)
    REFERENCES product_definition (id);

-- Reference: offer_req_def2function (table: offer_requirement_definition)
ALTER TABLE offer_requirement_definition ADD CONSTRAINT offer_req_def2function
    FOREIGN KEY (offer_function_id)
    REFERENCES offer_req_function (id);

-- Reference: offer_sale_code (table: offer)
ALTER TABLE offer ADD CONSTRAINT offer_sale_code
    FOREIGN KEY (sale_code_id)
    REFERENCES sale_code (id);

-- Reference: product_attr_product_attr_def (table: product_attribute)
ALTER TABLE product_attribute ADD CONSTRAINT product_attr_product_attr_def
    FOREIGN KEY (product_attr_def_id)
    REFERENCES product_attribute_definition (id);

-- Reference: product_attribute_product (table: product_attribute)
ALTER TABLE product_attribute ADD CONSTRAINT product_attribute_product
    FOREIGN KEY (product_id)
    REFERENCES product (id);

-- Reference: product_definition2category (table: product_definition)
ALTER TABLE product_definition ADD CONSTRAINT product_definition2category
    FOREIGN KEY (product_category_id)
    REFERENCES product_category (id);

-- Reference: product_device (table: product)
ALTER TABLE product ADD CONSTRAINT product_device
    FOREIGN KEY (device_id)
    REFERENCES device (id);

-- Reference: product_parent_product (table: product)
ALTER TABLE product ADD CONSTRAINT product_parent_product
    FOREIGN KEY (parent_product_id)
    REFERENCES product (id);

-- Reference: product_product_category (table: product)
ALTER TABLE product ADD CONSTRAINT product_product_category
    FOREIGN KEY (product_definition_id)
    REFERENCES product_definition (id);

-- Reference: product_product_status (table: product)
ALTER TABLE product ADD CONSTRAINT product_product_status
    FOREIGN KEY (product_status_id)
    REFERENCES product_status (id);

-- Reference: product_technology (table: product)
ALTER TABLE product ADD CONSTRAINT product_technology
    FOREIGN KEY (technology_id)
    REFERENCES technology (id);

-- Reference: province_district (table: address_county)
ALTER TABLE address_county ADD CONSTRAINT province_district
    FOREIGN KEY (address_province_id)
    REFERENCES address_province (id);

-- Reference: report_access_report (table: report_access)
ALTER TABLE report_access ADD CONSTRAINT report_access_report
    FOREIGN KEY (report_id)
    REFERENCES report (id);

-- Reference: report_access_user (table: report_access)
ALTER TABLE report_access ADD CONSTRAINT report_access_user
    FOREIGN KEY (user_id)
    REFERENCES crm_user (id);

-- Reference: report_user (table: report)
ALTER TABLE report ADD CONSTRAINT report_user
    FOREIGN KEY (user_id)
    REFERENCES crm_user (id);

-- Reference: sale_code_teams (table: sale_code)
ALTER TABLE sale_code ADD CONSTRAINT sale_code_teams
    FOREIGN KEY (teams_id)
    REFERENCES teams (id);

-- Reference: sales_struct_sale_code (table: sale_code)
ALTER TABLE sale_code ADD CONSTRAINT sales_struct_sale_code
    FOREIGN KEY (sales_struct_id)
    REFERENCES users_struct (id);

-- Reference: sales_struct_user_user (table: users_struct)
ALTER TABLE users_struct ADD CONSTRAINT sales_struct_user_user
    FOREIGN KEY (user_id)
    REFERENCES crm_user (id);

-- Reference: task_movement_history_basket (table: task_movement_history)
ALTER TABLE task_movement_history ADD CONSTRAINT task_movement_history_basket
    FOREIGN KEY (basket_id)
    REFERENCES basket (id);

-- Reference: task_movement_history_queue (table: task_movement_history)
ALTER TABLE task_movement_history ADD CONSTRAINT task_movement_history_queue
    FOREIGN KEY (queue_id)
    REFERENCES queue (id);

-- Reference: task_movement_history_tasks (table: task_movement_history)
ALTER TABLE task_movement_history ADD CONSTRAINT task_movement_history_tasks
    FOREIGN KEY (task_id)
    REFERENCES tasks (id);

-- Reference: task_param_task_param_def (table: task_param)
ALTER TABLE task_param ADD CONSTRAINT task_param_task_param_def
    FOREIGN KEY (task_param_definition_id)
    REFERENCES task_param_definition (id);

-- Reference: task_param_tasks (table: task_param)
ALTER TABLE task_param ADD CONSTRAINT task_param_tasks
    FOREIGN KEY (task_id)
    REFERENCES tasks (id);

-- Reference: task_param_val_task_param_def (table: task_param_value_list)
ALTER TABLE task_param_value_list ADD CONSTRAINT task_param_val_task_param_def
    FOREIGN KEY (task_param_definition_id)
    REFERENCES task_param_definition (id);

-- Reference: tasks_account (table: tasks)
ALTER TABLE tasks ADD CONSTRAINT tasks_account
    FOREIGN KEY (account_id)
    REFERENCES account (id);

-- Reference: tasks_basket (table: tasks)
ALTER TABLE tasks ADD CONSTRAINT tasks_basket
    FOREIGN KEY (basket_id)
    REFERENCES basket (id);

-- Reference: tasks_queue (table: tasks)
ALTER TABLE tasks ADD CONSTRAINT tasks_queue
    FOREIGN KEY (queue_id)
    REFERENCES queue (id);

-- Reference: tasks_task_status (table: tasks)
ALTER TABLE tasks ADD CONSTRAINT tasks_task_status
    FOREIGN KEY (task_status_id)
    REFERENCES task_status (id);

-- Reference: tasks_task_subtype (table: tasks)
ALTER TABLE tasks ADD CONSTRAINT tasks_task_subtype
    FOREIGN KEY (task_subtype_id)
    REFERENCES task_subtype (id);

-- Reference: tasks_task_type (table: tasks)
ALTER TABLE tasks ADD CONSTRAINT tasks_task_type
    FOREIGN KEY (task_type_id)
    REFERENCES task_type (id);

-- Reference: tasks_tasks (table: tasks)
ALTER TABLE tasks ADD CONSTRAINT tasks_tasks
    FOREIGN KEY (parent_task_id)
    REFERENCES tasks (id);

-- Reference: tasks_user (table: tasks)
ALTER TABLE tasks ADD CONSTRAINT tasks_user
    FOREIGN KEY (user_id_author)
    REFERENCES crm_user (id);

-- Reference: teams_sales_struct (table: teams)
ALTER TABLE teams ADD CONSTRAINT teams_sales_struct
    FOREIGN KEY (team_leader_id)
    REFERENCES users_struct (id);

-- Reference: users_struct_users_struct (table: users_struct)
ALTER TABLE users_struct ADD CONSTRAINT users_struct_users_struct
    FOREIGN KEY (manager_id)
    REFERENCES users_struct (id);

-- End of file.

